const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/assets/js/apps/index.js', 'public/vue/js').vue({ version: 2 });
   
    mix.styles([

       'public/template/adminlte/css/bootstrap.min.css',
       'public/template/adminlte/css/font-awesome.min.css',
       'public/template/adminlte/css/ionicons.min.css',
       'public/template/adminlte/css/AdminLTE.min.css',
       'public/template/adminlte/css/_all-skins.min.css', 
       'public/template/adminlte/css/_all.css', 
       'public/template/adminlte/css/style.css',
       'public/template/adminlte/css/style-header-new.css',
      

    ], 'public/vue/css/bundle.css').vue({ version: 2 });
    mix.scripts([
        'public/template/adminlte/js/jquery.min.js',
        'public/template/adminlte/js/adminlte.min.js',
        'public/template/adminlte/js/bootstrap.min.js',
        'public/template/adminlte/js/icheck.min.js',
        'public/template/adminlte/js/icheck.js',
        
       
    ], 'public/vue/js/bundle.js').vue({ version: 2 });

//mix.js('resources/js/app.js', 'public/js').sourceMaps();
