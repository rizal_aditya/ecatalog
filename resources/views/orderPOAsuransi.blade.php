<html>

<head>
    <title>PO Asuransi</title>
   
</head>
<style>
body {
        font-size: 12px;
    }

    .body-surat {
        margin-top: 20px;
        font-size: 11px;
    }

    .body-surat p {
        margin-top: 2px;
        margin-bottom: 2px;
    }

    p.header-surat {
        margin-top: 2px;
        margin-bottom: 2px;
    }

    .box-header-surat {
        width: 30%;
    }

    #table-pertanggunan {
        margin-top: 20px;
        margin-bottom: 20px;
        font-size: 12px;
    }

    #table-pertanggunan td {
        padding: 3px;
    }

    #table-barang {
        width: 100%;
    }

    #table-barang tbody tr.pertama td {
        padding-top: 40px;
    }

    #table-barang tbody tr.terakhir td {
        padding-bottom: 40px;
    }

    #table-barang td {
        font-weight: bold;
        font-size: 10px;
        padding: 5px;
    }

    #table-barang th {
        font-weight: bold;
        font-size: 10px;
       
        padding: 5px;
    }
.mbot-0 {
    margin-bottom: 0px;
}

.mbot-10 {
    margin-bottom: 10px;
}

.text-center {
    text-align: center;
}

.text-right {
    text-align: right;
}

.text-justify {
    text-align: justify;
    margin: auto;
}

.font-biru {
    color: blue;
}

table {
    border-collapse: collapse;
}

.full-width {
    width: 100%;
}

.table-transport thead tr td {
    background-color: grey;
    padding: 5px;
    text-align: center;
    font-weight: bold;
}

.table-transport tbody tr td {
    border-top: 0;
    border-bottom: 0;
    padding: 5px 10px;
}

.table-transport tbody tr.pertama td {
    padding-top : 40px;
}

.table-transport tbody tr.terakhir td {
    padding-bottom : 40px;
}

.table-transport tfoot tr td {
    padding: 5px 10px;
}

.bolder {
    font-weight: bold;
}

.no-border-left {
    border-left: 0;
}

.no-border-right {
    border-right: 0;
}

.no-border-bottom {
    border-bottom: 0;
}

.no-border-top {
    border-top: 0;
}

.border-bottom-only {
    border-top : 0;
    border-left : 0;
    border-right: 0;
}

.border-top-only {
    border-bottom : 0;
    border-left : 0;
    border-right: 0;
}

.border-right-only {
    border-bottom : 0;
    border-left : 0;
    border-top: 0;
}

.border-left-only {
    border-bottom : 0;
    border-top : 0;
    border-right: 0;
}

.no-border {
    border: 0;
}

.table-ttd {
    padding-left: 120px;
    padding-right: 120px;
}

.padl-50 {
    padding-left : 50px;
}

.font-9 {
    font-size: 9px;
}

.font-10 {
    font-size: 10px;
}

.font-11 {
    font-size: 11px;
}

</style>

<body>
    <table width="100%" border="0">
        <tr>
            <td class="text-right" width="85%">
                PT WIJAYA KARYA (Persero) Tbk.
            </td>
            <td rowspan="2" class="text-center" style="padding-bottom:10px;padding-top:10px">
                <img src="{{ url('img/logo-baru.png') }}" width="100" height="60">
            </td>
        </tr>
        <tr>
            <td class="text-right font-9">
                - INDUSTRY - INFRASTRUCTURE & BUILDING - ENERGY & INDUSTRIAL PLANT - REALTY & PROPERTY - INVESTMENT
            </td>
        </tr>
        <tr>
            <td class="font-11 text-center" colspan="2" style="padding-top:10px; border-top:1px solid blue">
                Jl. D.I. Panjaitan Kav. 9-10. Jakarta 13340, PO Box 4174/JKTJ, Phone: +62-21 8192808, 8508640, 8508650, Fax: +62-21 8590 4147
            </td>
        </tr>
    </table>
    <div class="body-surat">
        <div class="box-header-surat">
            <p class="header-surat">Kepada,</p>
            <p class="header-surat bolder">{{ $order->vendor_asuransi->name }}</p>
            <p class="header-surat">{{ $order->vendor_asuransi->address }}</p>
            <p class="header-surat">Telp. {{ $order->vendor_asuransi->phone }}</p>
            <p class="header-surat bolder">Up. {{ $order->up }}</p>
            <p class="header-surat bolder">Perihal. {{ $order->perihal }}</p>
        </div>
        <p>Dengan Hormat, </p>
        <p>Berdasarkan perjanjian open cover marine cargo insurance tahun {{ $order->vendor_asuransi->tahun }} No. {{ $order->vendor_asuransi->cargo_number }}, maka bersama ini kami sampaikan data-data barang kiriman yang akan diasuransikan sebagai berikut : </p>
        <table id="table-barang" border="0">
            <tr>
                <th>Nama Barang dan Jenis Barang</th>
                <th width="8%" align="center">Jml Btg</th>
                <th width="12%" align="center">Tonase Kg</th>
                <th width="15%" align="center">Harga</th>
            </tr>
            <tbody>
              @foreach ($order->product as $value)  
                    <tr class="">
                        <td>{{ $value->product_name }}</td>
                        <td align="center">{{ $value->qty }}</td>
                        <td align="center">{{ $value->weight }}</td>
                        <td align="center">{{ $value->sub_total->convert }}</td>
                    </tr>
                @endforeach 
            </tbody>
            <tfoot>
                <tr>
                    <td align="center" colspan="2">Total</td>
                    <td align="center">{{ $order->total_weight }}</td>
                    <td align="center">{{ $order->sub_total->convert }}</td>
                </tr>

                @if($order->sub_total->original < $order->vendor_asuransi->nilai_harga_minimum )
             
                    <tr>
                        <td align="" colspan="2" style="font-weight: thin;"><i>Harga minimum asuransi = {{ $order->vendor_asuransi->nilai_harga_minimum }}}</i></td>
                        <td align="center"></td>
                        <td align="center">{{ $order->vendor_asuransi->nilai_harga_minimum }}}</td>
                    </tr>


                @endif
               
            </tfoot>
        </table>
        <table width="100%" border="0" id="table-pertanggunan">
            <tr>
                <td width="25%">Nilai Pertanggungan</td>
                <td width="2%">:</td>
                <td>{{ $order->sub_total->convert }}</td>
            </tr>
            <tr>
                <td>Lokasi Muat</td>
                <td>:</td>
                <td>
                    {{ $order->vendor_product->text }} {{ $order->vendor_product->address }}
                </td>
            </tr>
            <tr>
                <td>Tujuan</td>
                <td>:</td>
                <td>
                    {{ $order->project->name }} | {{ $order->project->address }}
                </td>
            </tr>
            <tr>
                <td>Jenis Pengiriman</td>
                <td>:</td>
                <td>
                    Darat dan Laut
                </td>
            </tr>
            <tr>
                <td>Tanggal Pengiriman</td>
                <td>:</td>
                <td>
                     {{ $order->created_at }} s.d  {{ $order->tgl_ambil }}

                </td>
            </tr>
            <tr>
                <td valign="top">Nama Forwarding</td>
                <td valign="top">:</td>
                <td>
                     
                    @if ($order->transport == 'true') 
             
                        <span class="bolder">
                            {{ $order->vendor_transport->name  }}
                        </span>
                        <br>
                          {{ $order->vendor_transport->address  }}
                        <br>
                         {{ $order->vendor_transport->pengiriman  }}
                    @endif
                </td>
            </tr>
        </table>
        <p>Demikian kami sampaikan, atas perhatiannya kami ucapkan terima kasih.</p>
        <p>Deklarasi ini dibuat dan disampaikan kepada ASURANSI JASA TANIA</p>
        <p>Tanggal, {{ $order->created_at }}</p>

        <table border="0" width="100%">
            <tr>
                <td width="60%">
                    Hormat Kami,
                </td>
                <td>
                    Telah diterima dan disetujui untuk dicover
                </td>
            </tr>
            <tr>
                <td>
                    <span class="bolder">PT.Wijaya Karya (persero) Tbk.</span><br>
                    DIVISI {{ $order->devision }}
                </td>
                <td>
                    <span class="bolder">{{ $order->vendor_asuransi->directur_name }}</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php echo str_repeat('<br>', 10) ?>
                </td>
            </tr>
            <tr>
                <td class="bolder">
                    {{ $order->gm_devision }}
                </td>
                <td class="bolder">
                    {{ $order->vendor_asuransi->directur_name }}
                </td>
            </tr>
        </table>
        <?php echo str_repeat('<br>', 3) ?>
        <span class="bolder">Note</span> : {{ $order->catatan }}
    </div>
</body>

</html>