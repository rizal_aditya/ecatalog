<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<?php $font = env('FONT_PDF');?>
<style type="text/css">
	
      @font-face {
        font-family: 'Arial';
        font-weight: normal;
        font-style: normal;
        font-variant: normal;
        src: url("http://e-catalog.loc/file/font_pdf/arial.ttf");
      }
      body {
        font-family: Arial;
        font-size: 12px;
      }
	p{
	    margin: 0px 0px!important;
	}

	.bold-i{
		font-weight: 800;

	}

	.paper {
		width: 18cm;
		display: table;
		margin: auto;
		padding: 10px 10px;
	}

	table {
		width: 100%;
		border-collapse: collapse;
	}

	.bordered th,
	.bordered td {
		border: 1px solid #000;
		padding: 5px;
		font-size: 11px;
		padding: 2px 5px;
	}

	.bordered th {
		padding: 10px 5px;
	}

	.header td {
		vertical-align: top;
	}

	.font-14 {
		font-size: 14px;
	}

	.font-16 {
		font-size: 16px;
	}

	.font-18 {
		font-size: 18px;
	}

	.font-20 {
		font-size: 20px;
	}

	.m-0 {
		margin: 0;
	}

	.mtop-0 {
		margin-top: 0;
	}
    .mtop-10 {
		margin-top: 10!important;
	}
	.mbot-0 {
		margin-bottom: 0px;
	}

	.mbot-10 {
		margin-bottom: 10px!important;
	}

	.mbot-20 {
		margin-bottom: 20px;
	}

	.mbot-30 {
		margin-bottom: 30px;
	}

	h5 {
		font-size: 22px;
	}

	.bg-grey {
		background: #898989;
	}

	.bg-white {
		background: #FFFFFF;
	}

	ol {
		list-style-type: decimal;
	}

	.bolder {
		font-weight: bold;
	}

	.sign {
		height: 150px;
		width: 200px;
	}

	.sign-1 {
		height: 150px;
		width: 200px;
	}

	ul {
		list-style-type: none;
	}

	ul li:before {
		content: '-';
		position: absolute;
		margin-left: -20px;
	}

	.text-right {
		text-align: right;
	}

	ol.a {
		list-style-position: outside;
	}
	
    .separator {
		display: block;
		width: 100%;
		height: 2px;
		background: #000;
		border-bottom: 2px solid #000;
		border-top: 2px solid #000;
		margin-top: 15px;
	}

	.font-dev{
	  font-size: 12px;
    font-weight: 600;
	}
</style>

<div class="paper">
	
	<table class="header">
		<tr>
			<td><img src="{{ url('img/logo-baru.png') }}" width="100" height="60"></td>
			<td>
				<h5 class="mtop-0 m-0 font-16 bold-i">PT. WIJAYA KARYA (Persero) Tbk</h5>
				<p class="font-dev">{{ $order->devision }}</p>
				<p class="m-0 font-14">Proyek : {{ $order->project_name }}</p>
			</td>
			<td>
				<h5 class="mtop-0 m-0 font-14 bold-i">PEMESANAN BARANG</h5>
				<p class="m-0 font-14">No.Pemesanan Barang : {{ $order->order_code }}</p>
				<p class="m-0 font-14">No.Surat : {{ $order->no_surat }}</p>
				<!-- <p class="m-0 font-14">No.DPPM :</p> -->
			</td>
		</tr>
	</table>
	<div class="separator"></div>
	<table class="mbot-20" border="0">
		<tr>
			<td width="50%">
				<p>Kepada:</p>
				<p class="bolder">{{ $order->vendor_name }}</p>
				<p>{{ $order->vendor_address }}</p>
				<p><span class="bolder">Fax:</span> {{ $order->vendor_no_fax }}</p>
				<p class="bolder">Up: {{ $order->up_product }}</p>
			</td>
			<td valign="top" align="center">
				<p>Jakarta, {{ $order->created_at }}</p>

			</td>
		</tr>
	</table>
	<table class="mbot-20" border="0">
		<tr>
			<td>
				<p class="mbot-10">Perihal : {{ $order->perihal_product }}</p>
				<p>Dengan Hormat, </p>
				<p class="m-0">Berdasarkan Perjanjian Jual Beli</p>
				<p class="m-0">Nomor Kontrak : {{ $order->contract }}</p>
				<p class="m-0">Tanggal Kontrak:  {{ $order->contract_update }}</p>
				<p class="m-0">Maka dengan ini kami memesan material seperti yang tercantum dibawah ini :</p>
			</td>
		</tr>
	</table>
	<table class="bordered">
		<thead>
			<tr>
				<th style="width: 30px;">No</th>
				<th>Nama Barang dan Spesifikasi</th>
				<th>Vol. <br>(Batang)</th>
				<th>Volume (Kg)</th>
				<th>Harga Satuan <br>(Rp / Kg)</th>
				<th width="20%">Total Harga</th>
			</tr>
		</thead>
		<tbody>
		
			 @foreach ($order->product as $value)
				
				
				
				
		
				<tr>
					<td>1</td>
					<td>{{ $value->product_name }}</td>
					<td class="text-right">{{ $value->qty }}</td>
					<td class="text-right">{{ $value->weight }}</td>
					<td class="text-right">{{ $value->price }}</td>
					<td class="text-right">{{ $value->sub_total }}</td>
				</tr>
			
			<tr>
				<td></td>
				<td></td>
				<td class="text-right bolder">{{ $value->jml_qty }}</td>
				<td class="text-right bolder">{{ $value->jml_weight  }}</td>
				<td></td>
				<td></td>
			</tr>
			  @endforeach 
			<tr>
				<td rowspan="8" colspan="2" align="center" style="padding-top:5px"><span class="bolder"></span></td>
				<td colspan="3">&nbsp;</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3" class="text-right bolder">Sub Total</td>
				<td class="text-right">{{ $order->sub_total }}</td>
			</tr>
			<tr>
				<td colspan="3" class="text-right">Potongan UM 10%</td>
				<td class="text-right">-</td>
			</tr>
			<tr>
				<td colspan="3" class="text-right">Dasar Pengenaan Pajak</td>
				<td class="text-right">{{ $order->sub_total }}</td>
			</tr>
			<tr>
				<td colspan="3" class="text-right">PPN 11%</td>
				<td class="text-right">{{ $order->ppn }}</td>
			</tr>
			<tr>
				<td colspan="3" class="text-right">Pph 1,5%</td>
				<td class="text-right">{{ $order->pph }}</td>
			</tr>
			
			<tr>
				<td colspan="3" class="text-right bolder">Total = Subtotal + PPN</td>
				<td class="text-right">{{ $order->total }}</td>
			</tr>
			<tr>
				<td colspan="3" class="text-right bolder">Tagihan Yang Harus Dibayarkan</td>
				<td class="text-right">{{ $order->bill }}</td>
			</tr>
		</tbody>
	</table>
	<p style="page-break-after: always;	"></p>
	<p class="mtop-10">Kondisi :</p>
	<ol class="a">
		<li class="mbot-10">
			Barang yang terkirim harus memenuhi persyaratan/spesifikasi sesuai dengan pesanan kami, dalam keadaan baik dan baru
		</li>

		<li class="mbot-10">
			Barang yang tidak memenuhi persyaratan harus diganti barang baru tanpa tambahan biaya
		</li>

		<li class="mbot-10">Material {{ $order->shipping_name }} {{ $order->vendor_name }} dikirim ke
			<span class="bolder">{{ $order->project_name }}</span> dengan angkutan yang ditunjuk oleh PT. Wijaya Karya ( Persero) Tbk.
		</li>

		<li class="mbot-10">
			Pembayaran dilakukan dengan pola {{ $order->payment_name }} setelah akseptasi Bank,
			beban bunga menjadi tanggunan pihak supplier
		</li>

		<li class="mbot-10">
			Pengiriman material ke proyek yang bersangkutan harus disertai Mill Certificate dan surat jalan dari Pabrik.
		</li>

		<li class="mbot-10">
			Penyimpangan terhadap hal-hal diatas dapat menyebabkan pembatalan pemesanan.
		</li>

		<li class="mbot-10">
			Melaksanakan SMK3L (Sistem Manajemen Kesehatan, Keselamatan Kerja, dan Lingkungan) dan SMP (Sistem Manajemen Pengamanan).
		</li>
	</ol>
	<br>
	<table border="0">
		<tr>
			<td style="width: 60%;" valign="top">
				<p class="m-0">Menyetujui,</p>
				<p class="m-0 bolder">{{ $order->vendor_name }}</p>
				<br>
				<br>
				<br>

			</td>
			<td valign="top" align="center">
				<p class="m-0">Pemesan,</p>
				<p class="m-0 bolder">PT Wijaya Karya (Persero)Tbk.
					
			</td>
		</tr>
		<tr>
			<td class="sign">
				<u></u>

				<p class="m-0"></p>
			</td>
			<td align="center" class="sign">
				<u>{{ $order->branch_manager }}</u>

				<p class="m-0">GM {{ $order->gm_devision }}</p>
			</td>
		</tr>
	</table>
	<table width="100%">
		<tr>
			<td colspan="2" style="padding-top:10px">
				<p class="mbot-0">Tembusan</p>
				<ul>
					<li><i>Proyek {{ $order->project_name }}</i></li>
					<li><i>{{ $order->devision }}</i></li>
				</ul>
			</td>
		</tr>
	</table>
</div>
