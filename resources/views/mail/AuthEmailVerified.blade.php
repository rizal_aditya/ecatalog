<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Template Email</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet" type="text/css">

    <style type="text/css">
        *,
        ::before,
        ::after {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
    </style>
</head>

<body style="background: #f5f5f5;font-family: Poppins, sans-serif;color: #707070;padding-top: 30px; padding-bottom: 30px;">
    <table style="padding: 30px 0px;width:500px;margin: 0px auto 0px auto;">
        <tr>
            <td>
                <div class="wrap">
                    <div class="wrap-inner" style="background:#fff;border-radius:5px;min-height:300px;padding:20px;border: 1px solid #d3d3d3;">
                        <h2 class="center" style="text-align:center !important;margin: 10px 0px;">
                            <a href="{{ $base_url.'/img/logo.png' }}"><img src="{{ $base_url.'/img/logo.png' }}" style="margin-bottom:0px;" height="80" alt="logo"></a>
                        </h2>
                        <h3 style="text-align:center;">Reset Your Password</h3>
                    
                        
                        <h3 style="text-align:left;margin: 25px 0px 0px;">Hai, {{ $username }}</h3>

                        <h4>Tap the button below to reset your customer account password. If you didn't request a new password, you can safely delete this email.</h4>

                        <table style="margin: 15px 0px; width: 100%;">
                            <tr>
                                <td>
                                  
                                       <a style="border-radius:5px;padding:15px;color: #ffffff;
    background: #2196f3;text-decoration: none;" href="{{ $base_url.'/new-password?ref='.$encript }}">Reset Password</a>
                                  

                                </td>
                            </tr>
                        </table>

                       

                        

                        <table style="margin: 15px 0px; width: 100%;">
                             <tr>
                                <td>Atas perhatiannya, kami ucapkan terima kasih.</td>
                             </tr>
                        </table>

                        <table  style="margin: 0px 0px 15px; width: 100%;">
                             <tr>
                                <td>Salam hangat,</td>
                             </tr>
                             <tr>
                                <td>Admin Wijaya Karya</td>
                             </tr>
                        </table>
                            
                       
                    </div>
                    <table style="padding-top: 20px; width: 100%;">
                        <tr>
                            <td>
                                <p class="center footer" style="text-align:center !important;font-size:14px;">© <?php echo date("Y"); ?> Escm Wijaya Karya.</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</body>

</html>