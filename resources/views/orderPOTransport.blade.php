<html>

<head>
    <title>PO Transportasi</title>
   
</head>
<style>
    body {
        font-size: 11px;
    }

    .box-body-surat {
        margin-left: 10%;
        margin-right: 10%;
    }

    .box-body-surat p {
        margin-top: 5px;
        text-align: justify;
        margin-bottom: 5px;
    }

    #table-header {
        font-size: 11px;
    }

    #table-header td {
        padding: 3px;
    }

    #table-header td.border {
        border-bottom: 2px solid black
    }

    td ol.alphabetic-list {
        list-style-type: lower-alpha;
    }

    .mbot-0 {
    margin-bottom: 0px;
}

.mbot-10 {
    margin-bottom: 10px;
}

.text-center {
    text-align: center;
}

.text-right {
    text-align: right;
}

.text-justify {
    text-align: justify;
    margin: auto;
}

.font-biru {
    color: blue;
}

table {
    border-collapse: collapse;
}

.full-width {
    width: 100%;
}

.table-transport thead tr td {
    background-color: grey;
    padding: 5px;
    text-align: center;
    font-weight: bold;
}

.table-transport tbody tr td {
    border-top: 0;
    border-bottom: 0;
    padding: 5px 10px;
}

.table-transport tbody tr.pertama td {
    padding-top : 40px;
}

.table-transport tbody tr.terakhir td {
    padding-bottom : 40px;
}

.table-transport tfoot tr td {
    padding: 5px 10px;
}

.bolder {
    font-weight: bold;
}

.no-border-left {
    border-left: 0;
}

.no-border-right {
    border-right: 0;
}

.no-border-bottom {
    border-bottom: 0;
}

.no-border-top {
    border-top: 0;
}

.border-bottom-only {
    border-top : 0;
    border-left : 0;
    border-right: 0;
}

.border-top-only {
    border-bottom : 0;
    border-left : 0;
    border-right: 0;
}

.border-right-only {
    border-bottom : 0;
    border-left : 0;
    border-top: 0;
}

.border-left-only {
    border-bottom : 0;
    border-top : 0;
    border-right: 0;
}

.no-border {
    border: 0;
}

.table-ttd {
    padding-left: 120px;
    padding-right: 120px;
}

.padl-50 {
    padding-left : 50px;
}

.font-9 {
    font-size: 9px;
}

.font-10 {
    font-size: 10px;
}

.font-11 {
    font-size: 11px;
}

</style>

<body>
    <table width="100%" border="0">
        <tr>
            <td class="text-right" width="85%">
                PT WIJAYA KARYA (Persero) Tbk.
            </td>
            <td rowspan="2" class="text-center" style="padding-bottom:10px;padding-top:10px">
                <img src="{{ url('img/logo-baru.png') }}" width="100" height="60">
            </td>
        </tr>
        <tr>
            <td class="text-right font-11">
                {{ $order->devision }}
            </td>
        </tr>
        <tr>
            <td class="font-11 text-center" colspan="2" style="padding-top:10px; border-top:1px solid blue">
                Jl. D.I. Panjaitan Kav. 9-10. Jakarta 13340, PO Box 4174/JKTJ, Phone: +62-21 8192808, 8508640, 8508650, Fax: +62-21 8590 4147
            </td>
        </tr>
    </table>
    <div class="box-body-surat">
        <h3 class="text-center"><u>PERJANJIAN PENGANGKUTAN</u></h3>
        <table border="0" width="100%" id="table-header" border="0">
            <tr>
                <td class="" width="30%">Nomor</td>
                <td width="2%">:</td>
                <td>{{ $order->order_no }}</td>
            </tr>
            <tr>
                <td class="">Tanggal</td>
                <td>:</td>
                <td>{{ $order->created_at }}</td>
            </tr>
            <tr>
                <td class=" ">Proyek</td>
                <td class="">:</td>
                <td class="">{{ $order->project->name }}</td>
            </tr>
            <tr>
                <td class=" ">UP</td>
                <td class="">:</td>
                <td class="">{{ $order->up }}</td>
            </tr>
            <tr>
                <td class="bolder border">Perihal</td>
                <td class="border">:</td>
                <td class="border">{{ $order->perihal }}</td>
            </tr>
        </table>
        
        <p>
           SURAT PERJANJIAN ini termasuk semua lampirannya merupakan bagian yang tidak terpisahkan dari surat
           perjanjian, selanjutnya disebut Perjanjian dibuat pada {{ $order->vendor_transport->contract->startdate->terbilang }} ({{ $order->vendor_transport->contract->startdate->original }})
        </p>
        <table border="0" width="100%">
            <tr>
                <td class="bolder" width="58%">Mengacu pada Perjanjian Pengadaan Jasa Transportasi</td>
                <td class="bolder" width="10%">No.</td>
                <td class="bolder" width="2%">:</td>
                <td class="bolder">{{ $order->vendor_transport->contract->number }}</td>
            </tr>
            <tr>
                <td></td>
                <td class="bolder">Tanggal</td>
                <td class="bolder">:</td>
                <td class="bolder">{{ $order->vendor_transport->contract->startdate->convert }}</td>
            </tr>
        </table>
        <table border="0" width="100%">
            <tr>
                <td width="7%" valign="top">I.</td>
                <td width="40%" valign="top" class="bolder">PT. WIJAYA KARYA ( Persero) Tbk</td>
                <td width="2%" valign="top">:</td>
                <td valign="top" style="text-align:justify">
                    Berkedudukan di Jl. D.I. Panjaitan Kav.9 Jakarta 13340, yang dalam hal ini diwakili oleh {{ $order->gm_devision }}
                    selaku General Manager Divisi {{ $order->devision }}, oleh karena itu sah untuk mewakili perusahaan
                    yang selanjutnya disebut PIHAK PERTAMA
                </td>
            </tr>
            <tr>
                <td valign="top">II.</td>
                <td valign="top" class="bolder">{{ $order->vendor_transport->name }}</td>
                <td valign="top">:</td>
                <td valign="top" style="text-align:justify">
                    Berkedudukan di
                     {{ $order->vendor_transport->address }} yang dalam hal ini diwakili oleh
                     {{ $order->vendor_transport->directur_name }} selaku Direktur Utama
                    oleh karena itu sah untuk mewakili Perusahaan {{ $order->vendor_transport->name }}:
                    yang selanjutnya disebut PIHAK KEDUA
                </td>
            </tr>
        </table>
        <p>
            Menyatakan bahwa PIHAK KEDUA telah sepakat untuk mengikat dan berjanji akan melaksanakan ketentuan - ketentuan
            dan syarat-syarat sebagai berikut :
        </p>
        <table width="100%" border="0">
            <tr>
                <td valign="top" width="6%" style="padding-left:20px" rowspan="4">1.</td>
                <td valign="top" class="bolder" width="30%" rowspan="4">
                    Untuk Melaksanakan
                </td>
                <td width="2%" valign="top" rowspan="4">:</td>
                <td valign="top" colspan="2">
                   Pekerjaan pengangkutan besi beton dari pabrik {{ $order->vendor_product->text }} atas PO No : {{ $order->order_no }} ke {{ $order->project->name }}, {{ $order->project->address }}, dan melaksanakan / mengikuti semua prosedur dan persyaratan seperti ISO 9001, 2000, SMP, K3/OHSAS 18001 : 1999,
                </td>
            </tr>
            <tr>
                <td valign="top" width="5%">a.</td>
                <td style="padding-left:10px">
                    Dalam melaksanakan pekerjaan dilapangan harus memperhatikan perlengkapan K3 (Seperti : Helm, sabuk pengaman,
                    sepatu safety, dll).
                </td>
            </tr>
            <tr>
                <td valign="top">b.</td>
                <td style="padding-left:10px">
                    Untuk pengangkutan material yang menggunakan jalan umum harus mengikuti semua peraturan lalu lintas dan angkutan
                    jalan No. 14/1992, dan menjaga keselamatan pekerja dan barang.
                </td>
            </tr>
            <tr>
                <td valign="top">c.</td>
                <td style="padding-left:10px">
                    Dalam pelaksanaan pekerjaan baik itu dilokasi pabrikasi pekerjaan atau di lokasi pabrikasi (workshop) harus mematuhi
                    ketentuan peraturan SMK3L/OHSAS (System Manajement Keselamatan dan Kesehatan Kerja Lingkungan) yang diterapkan oleh
                    PT. Wijaya Karya (Persero) Tbk, dan tidak diperkenankan memperkerjakan Tenaga Kerja dengan usia dibawah umur yang
                    ditentukan oleh DEPNAKER, serta mengasuransikan Tenaga Kerja yang diperkerjakan di lokasi Proyek (Asuransi JAMSOSTEK)
                </td>
            </tr>
            <tr>
                <td style="padding-left:20px" valign="top">2.</td>
                <td class="bolder" valign="top">Sifat Kontrak</td>
                <td valign="top">:</td>
                <td colspan="2">
                    <span class="bolder">FOT (Freight on truck)</span><br>
                    Lokasi Ambil : {{ $order->vendor_product->text }}<br>
                    Wilayah : {{ $order->vendor_product_location }}<br>
                    Alamat : {{ $order->vendor_product->address }}<br>
                    Lokasi Kirim :  {{ $order->project->name }}<br>
                    Alamat Proyek : {{ $order->project->address }}<br>
                    <span class="bolder">Contact Person : {{ $order->vendor_product->phone }}</span><br>
                    <span class="bolder">HP : {{ $order->vendor_product->phone }}</span>
                </td>
            </tr>
            <tr>
                <td style="padding-left:20px" valign="top">3.</td>
                <td class="bolder" valign="top">Volume Pekerjaan</td>
                <td valign="top">:</td>
                <td colspan="2" class="bolder">
                    {{ $order->total_weight  }} Kg atau  {{ $order->total_qty }} Batang
                </td>
            </tr>
            <tr>
                <td style="padding-left:20px" valign="top">4.</td>
                <td class="bolder" valign="top">Biaya Pekerjaan</td>
                <td valign="top">:</td>
                <td colspan="2" class="">
                    <span class="bolder">
                        {{ $order->transport_price }}
                    </span><br>
                   
                </td>
            </tr>
            <!-- <tr>
                    <td style="padding-left:20px" valign="top" rowspan="2">5.</td>
                    <td class="bolder" valign="top" rowspan="2">Cara Pembayaran</td>
                    <td valign="top" rowspan="2">:</td>
                    <td colspan="" class="" valign="top">
                        5.1,
                    </td>
                    <td>
                        Konfensional 30 (tiga puluh) hari jika nilai dibawah 100 juta dengan
                        kondisi setelah barang terkirim & diterima proyek, dan invoice tagihan diterima dengan lengkap
                        & benar oleh PT. WIKA.
                    </td>
                </tr> -->
            <tr>
                <td style="padding-left:20px" valign="top" rowspan="">5.</td>
                <td class="bolder" valign="top" rowspan="">Cara Pembayaran</td>
                <td valign="top" rowspan="">:</td>
                <td colspan="" class="" valign="top">
                    5.1,
                </td>
                <td>
                    {{ $order->transport_name }} jika nilai diatas 100 juta dengan
                    kondisi setelah barang terkirim & diterima proyek, dan invoice tagihan diterima dengan lengkap
                    & benar oleh PT. WIKA.
                </td>
            </tr>
            <tr>
                <td style="padding-left:20px" valign="top" rowspan="3">6.</td>
                <td class="bolder" valign="top" rowspan="3">Ketentuan & Syarat-Syarat Umum</td>
                <td valign="top" rowspan="3">:</td>
                <td colspan="" class="" valign="top">
                    6.1,
                </td>
                <td>
                    PT. Wijaya Karya berhak memutuskan/membatalkan SPK ini secara sepihak
                    tanpa tuntutan apapun dari {{ $order->vendor_transport->name }}, apabila pengiriman
                    terlambat 2 (dua) hari dari waktu pelaksanaan yang ditetapkan oleh PT. Wijaya Karya (Persero).
                </td>
            </tr>
            <tr>
                <td colspan="" class="" valign="top">
                    6.2,
                </td>
                <td>
                    Kerusakan/ kehilangan barang diluar cakupan asuransi merupakan tanggung jawab {{ $order->vendor_transport->name }}.
                </td>
            </tr>
            <tr>
                <td colspan="" class="" valign="top">
                    6.3,
                </td>
                <td>
                    Waktu pelaksanaan sejak diterbitkan perjanjian ini.
                </td>
            </tr>
            <tr>
                <td style="padding-left:20px" valign="top" rowspan="2">7.</td>
                <td class="bolder" valign="top" rowspan="2">Force Majeure</td>
                <td valign="top" rowspan="2">:</td>
                <td colspan="" class="" valign="top">
                    7.1,
                </td>
                <td style="text-align:justify">
                    Dalam SPK ini Force Majeure berarti suatu peristiwa yang berada diluar kemampuan
                    PT. Wijaya Karya (Persero) Tbk dan {{ $order->vendor_transport->name }} yang menyebabkan pelaksanaan
                    pekerjaan ini mengalami keterlambatan disebabkan karena : (Kerusuhan, Gempa Bumi, Tsunami,
                    Ombak besar, Kegiatan Teroris, Sabotase, Pemberontakan).
                </td>
            </tr>
            <tr>
                <td colspan="" class="" valign="top">
                    7.2,
                </td>
                <td style="text-align:justify">
                    Apabila terjadi peristiwa Force Majeure, yang menyebabkan terjadinya gangguan pengiriman material
                    ke lokasi proyek, maka {{ $order->vendor_transport->name }} harus mengirimkan pemberitahuan tertulis kepada
                    PT. Wijaya Karya (Persero) Tbk. selambat-lambatnya dalam waktu 3 (tiga) hari setelah perjalanan
                    pengiriman material terhambat karena peristiwa Force Majeure tersebut.
                </td>
            </tr>
            <tr>
                <td style="padding-left:20px" valign="top" rowspan="2">8.</td>
                <td class="bolder" valign="top" rowspan="2">Penyelesaian Perselisihan</td>
                <td valign="top" rowspan="2">:</td>
                <td colspan="" class="" valign="top">
                    8.1,
                </td>
                <td style="text-align:justify">
                    Pada dasarnya setiap perselisihan atau perbedaan pendapat dalam menjalankan
                    pekerjaan ini akan diselesaikan dengan cara musyawarah untuk mufakat
                </td>
            </tr>
            <tr>
                <td colspan="" class="" valign="top">
                    8.2,
                </td>
                <td style="text-align:justify">
                    Bila musyawarah tidak berhasil mencapai kesepakatan, semua perbedaan pendapat atau
                    sengketa atau perselisihan yang timbul dalam Perjanjian ini akan diputus dan diselesaikan
                    melalui Pengadilan Negeri yang berwenang sesuai dengan ketentuan perundang udangan yang berlaku
                </td>
            </tr>
        </table>
        <p>
            Demikian surat ini dibuat dalam rangkap 2 (dua) bermaterai cukup, setelah disetujui dan ditandatangani
            oleh kedua belah pihak dinyatakan sah dan mempunyai kekuatan hukum yang sama.
        </p>
       
        <table class="full-width" border="0">
            <tr>
                <td width="60%"></td>
                <td width="40%">
                    Dikeluarkan di : Jakarta<br>
                    Pada Tanggal : {{ $order->updated_at }}
                </td>
            </tr>
            <tr>
                <td class="">
                    Menyetujui/menyanggupi<br>
                    Untuk dan atas nama angkutan
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="">
                    <span class="bolder">
                        {{ $order->vendor_transport->name }}
                    </span>
                   

                </td>
                <td>
                    <span class="bolder">
                        PT Wijaya Karya (Persero) Tbk.
                    </span><br>
                    DIVISI {{ $order->devision }}
                    
                </td>
            </tr>
            <tr>
                <td class="">
                    <span class="bolder">
                        <u>
                            {{ $order->vendor_transport->directur_name }}
                        </u>
                    </span><br>
                    <i>
                        DIREKTUR UTAMA
                    </i>
                </td>
                <td>
                    <span class="bolder">
                        <u>
                            {{ $order->gm_devision }}
                        </u>
                    </span><br>
                    <i>
                        General Manager
                    </i>
                </td>
            </tr>
        </table>
    </div>
    <p style="page-break-after: always;	"></p>



    <h3 class="mbot-0">LAMPIRAN PERJANJIAN PENGANGKUTAN</h3>
    No : T2102220004<br>
    Tgl : {{ $order->updated_at }}
    <h3 class="text-center">
        Rincian Volume dan Harga Satuan<br>
        <!-- Pekerjaan : <span class="font-biru">Angkutan Besi Beton???</span><br> -->
       {{ $order->project->name }}<br>
    </h3>
    <table class="full-width table-transport" border="1">
        <thead>
            <tr>
                <td width="5%">No</td>
                <td width="25%">Uraian Pekerjaan</td>
                <td width="10%">
                    Jumlah<br>
                    (Btg)
                </td>
                <td width="15%">
                    Volume (Kg)
                </td>
                <td width="15%">
                    Harga<br>
                    Satuan
                </td>
                <td width="20%">
                    Jumlah
                </td>
                <td width="10%">
                    Keterangan
                </td>
            </tr>
        </thead>

        <tbody>
                @foreach ($order->product as $value)
                
                <tr>
                    <td class="text-center">{{ $value->no }}</td>
                    <td>
                        {{ $value->product_name }}
                    </td>

                    <td class="text-right">
                       {{ $value->qty }}
                    </td>
                    <td class="text-right">
                        {{ $value->weight }}
                    </td>
                    <td class="text-center">
                      {{ $value->price }}
                    </td>
                    <td class="text-right">
                        {{ $value->sub_total }}
                    </td>
                    <td></td>
                </tr>
               @endforeach   
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="text-right bolder">
                   {{ $order->total_qty }}</td>
                <td class="text-right bolder">
                    {{ $order->total_weight  }}
                </td>
                <td class="text-right no-border-right bolder">
                    Rp.
                </td>
                <td class="text-right no-border-left bolder">
                    {{ $order->sub_total }}
                </td>
                <td></td>
            </tr>
           
               
            
            <tr>
                <td colspan="5" class="no-border-bottom no-border-right"></td>
                <td>&nbsp;</td>
                <td rowspan="3"></td>
            </tr>
            <tr>
                <td colspan="3" class="border-left-only"></td>
                <td class="text-right bolder no-border">Jumlah</td>
                <td class="text-right border-right-only bolder">
                    Rp.
                </td>
                <td class="bolder text-right no-border-left">
                    {{ $order->sub_total }}
                </td>
            </tr>
            <tr>
                <td colspan="3" class="no-border-top no-border-right"></td>
                <td class="text-right bolder border-bottom-only">Dibulatkan</td>
                <td class="text-right no-border-top no-border-left bolder">
                    Rp.
                </td>
                <td class="bolder text-right no-border-left">
                    {{ $order->sub_total }}
                </td>
            </tr>
        </tfoot>

         
    </table>
    <br>
    <table class="full-width" border="0">
        <tr>
            <td class="padl-50" colspan="2">
                Terbilang : <br>
                <i>{{ $order->terbilang }}</i>
            </td>
        </tr>
        <tr>
            <td class="padl-50" colspan="2">
                Catatan : <br>
                <i>{{ $order->catatan }}</i>
            </td>
        </tr>
        <tr>
            <td width="60%"></td>
            <td width="40%">
                Dikeluarkan di : Jakarta<br>
                Pada Tanggal : {{ $order->updated_at }}
            </td>
        </tr>
        <tr>
            <td class="padl-50">
                Menyetujui/menyanggupi<br>
                Untuk dan atas nama angkutan
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="padl-50">
                <span class="bolder">
                    {{ $order->vendor_transport->name }}
                </span>
                

            </td>
            <td>
                <span class="bolder">
                    PT Wijaya Karya (Persero) Tbk.
                </span><br>
                    DIVISI {{ $order->devision }}

            </td>
        </tr>
        <tr>
            <td class="padl-50">
                <span class="bolder">
                    {{ $order->vendor_transport->directur_name }}
                </span><br>
                   DIREKTUR UTAMA
            </td>
            <td>
                <span class="bolder">
                    {{ $order->gm_devision }}
                </span><br>
                GM DIVISI {{ $order->devision }}
            </td>
        </tr>
    </table>
</body>

</html>