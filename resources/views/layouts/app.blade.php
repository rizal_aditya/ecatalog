<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="theme-color" content="#ff4500">
    <meta name="description" content="WIKA Wijaya Karya">
    
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ Session::token() }}"> 
    <!-- <meta name="csrf-token" content="{{ csrf_token() }}" /> -->
    <title>{{ config('app.name') }} | {{ $title  }} </title>
     <script type="text/javascript">
        const BASE_URL = window.location.origin;

    </script>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">  
    <link rel="stylesheet" href="{{ mix('/vue/css/bundle.css') }}">
   
    </head>

    <body> 

    <div class="preloader"><span></span></div><!-- /.preloader -->
    <div class="page-wrapper">      
        @yield('content')
    </div>
     
     

     <script type="text/javascript" src="{{ mix('/vue/js/index.js') }}"></script>
     <script type="text/javascript" src="{{ mix('/vue/js/bundle.js') }}"></script> 

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/3.1.0/socket.io.js" integrity="sha512-+l9L4lMTFNy3dEglQpprf7jQBhQsQ3/WvOnjaN/+/L4i0jOstgScV0q2TjfvRF4V+ZePMDuZYIQtg5T4MKr+MQ==" crossorigin="anonymous"></script> -->

 

   <!--  <script id="script">
    var s = document.createElement("script")
    s.src = "https://notix.io/ent/current/enot.min.js"
    s.onload = function (sdk) {
        sdk.startInstall({
            appId: "1004f86b362c0431dd8d1209de0b48b",
            loadSettings: true
        })
    }
    document.head.append(s)
</script> -->

<script>
    $(function () {
     // $('.modal').modal('toggle');
   });
</script>

      </body>      
</html>
