const { Client } = require('whatsapp-web.js');
const client = new Client();
const qrcode = require('qrcode')
const http = require("http");

const fs = require("fs");
const { body, validationResult } = require('express-validator');
const express = require('express');
const cors = require('cors');
const axios = require("axios");
const app = express();
app.use(cors());
const server = http.createServer(app);
const socketIO = require('socket.io');



const { phoneNumberFormatter } = require('./formatter');
const io = socketIO(server,{
    cors:{
        origin:"*",
        methods:"*",
        allowedHeaders:"*",
    },
    allowEIO3: true // false by default
});
const request = require("request");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
var execPHP = require('./execphp.js')();

const configs = {
    port: 3000, // custom port to access server
  
};


// app.get('/chat', (req, res) => {
//   res.sendFile('./chat.html', {
//     root: __dirname
//   });
// });


app.use('/', function (request, response, next) {
    execPHP.parseFile(request.originalUrl, function (phpResult) {
        response.write(phpResult);
        response.end();
    });
});



io.on("connection", function (socket) {
   
     socket.on('chat_count', msg => {
        io.emit('chat_count', msg);
    });
     
    socket.on('chat_message', msg => {
        io.emit('chat_message', msg);
    });

   

    socket.on('user_chat', msg => {
        io.emit('user_chat', msg);

    });

    socket.on('cart_count', msg => {
        io.emit('cart_count', msg);
    });

    socket.on('order_count', msg => {
        io.emit('order_count', msg);
    });

    client.on('qr', (qr) => {
          qrcode.toDataURL(qr, (err, url) => {
          io.emit('qr', url);
         });
    });

    client.on('ready', () => {
        io.emit('ready',client.info);
      
    });




    // client.on('message', async msg => {
    // console.log('MESSAGE RECEIVED', msg);

    // });

    client.on('disconnected', (reason) => {
         io.emit('logout',reason);           
    });

    


     
});    


    
client.initialize();



server.listen(configs.port, () => {
    console.log(`Server listening on 3000`);
});
