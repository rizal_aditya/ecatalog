require('./../bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueSwal from 'vue-swal';
import Vuex from 'vuex';
import VueCookies from 'vue-cookies';
import ToggleButton from 'vue-js-toggle-button';
import excel from 'vue-excel-export'; 
import PerfectScrollbar from 'vue2-perfect-scrollbar';
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css';
import VueToastify from "vue-toastify";
import store from './store'; // Import store
import VMdDateRangePicker from "v-md-date-range-picker";



Vue.use(require('vue-cookies'))


Vue.use(PerfectScrollbar);
Vue.use(VueSwal);
Vue.use(VueCookies);
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(ToggleButton);
Vue.use(excel);
Vue.use(VMdDateRangePicker);
Vue.use(VueToastify,{
    position:"center-right",
    canTimeout:true,
    errorDuration:2000,
    successDuration:2000,
    alertInfoDuration:2000,
    duration:2000,
});



import VueSlickCarousel from 'vue-slick-carousel'
import 'vue-slick-carousel/dist/vue-slick-carousel.css'
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css'
Vue.component('VueSlickCarousel',VueSlickCarousel);





//import chat from './../components/frontend/index/chat.vue';
//import VueSocketIO from 'vue-socket.io'
// import SocketIO from 'socket.io-client'

// /* Establish Connection */
// const socketConnection = SocketIO('http://localhost:3000');

// Vue.use(new VueSocketIO({
//     debug: true,
//     connection:'http://e-catalog.loc/',
//     options: { path: "/chat" },

//   })
// );






Vue.component('loading-block', require('./../components/layout/LoadingBlock.vue').default);



const AppsName = "E-Catalog";
const Login = AppsName+' | Login';
const BodyLogin = "login-page";
const BodyDashboard1 = "";
const BodyDashboard2 = "sidebar-mini";
const SocketIo =  "ws://localhost:3000/";
 
 
 
const router = new VueRouter({
    mode:'history',
    routes: [
        {
            path: '/',
            props: { SocketIo:SocketIo},
            component: require('./../components/frontend/layout/index.vue').default,
            children: [
                {
                    
                    path: "/",
                    props: { Apps:AppsName +' | PT Wijaya Karya',ShowPR:true,ShowHistory:false,Page:'index'},
                    component: require('./../components/frontend/index/index.vue').default,

                },
                { 
                   
                    path: "/category/all",
                    props: { Apps:AppsName +' | Semua Kategori', Title:'Semua Kategori',Param:'all',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },
                { 
                   
                    path: "/category/material/",
                    props: { Apps:AppsName +' | Kategori Material',Title:'Material',Param:'material',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },
                { 
                   
                    path: "/category/upah/",
                    props: { Apps:AppsName +' | Kategori Upah',Title:'Upah',Param:'upah',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },
                { 
                   
                    path: "/category/alat/",
                    props: { Apps:AppsName +' | Kategori Alat',Title:'Alat',Param:'alat',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },
                 { 
                   
                    path: "/category/subkon/",
                    props: { Apps:AppsName +' | Kategori Subkon',Title:'Subkon',Param:'subkon',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },
                { 
                   
                    path: "/category/tertinggi/",
                    props: { Apps:AppsName +' | Kategori Tertinggi',Title:'Produk Dengan Harga Tertinggi',Param:'tertinggi',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },
                { 
                   
                    path: "/category/terendah/",
                    props: { Apps:AppsName +' | Kategori Terendah',Title:'Produk Dengan Harga Terendah',Param:'terendah',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },
                { 
                   
                    path: "/category/terkontrak/",
                    props: { Apps:AppsName +' | Kategori Terkontrak',Title:'Produk Terkontrak',Param:'terkontrak',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },
                 { 
                   
                    path: "/category/non-kontrak/",
                    props: { Apps:AppsName +' | Kategori Non Kontrak',Title:'Produk Non Kontrak',Param:'non-kontrak',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },

                { 
                   
                    path: "/category/start-0/",
                    props: { Apps:AppsName +' | Non Rating',Title:'Produk Non Rating',Param:'start-0',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },

                { 
                   
                    path: "/category/start-1/",
                    props: { Apps:AppsName +' | Rating 1',Title:'Produk Rating Bintang 1',Param:'start-1',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },

                { 
                   
                    path: "/category/start-2/",
                    props: { Apps:AppsName +' | Rating 2',Title:'Produk Rating Bintang 2',Param:'start-2',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },

                { 
                   
                    path: "/category/start-3/",
                    props: { Apps:AppsName +' | Rating 3',Title:'Produk Rating Bintang 3',Param:'start-3',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },

                { 
                   
                    path: "/category/start-4/",
                    props: { Apps:AppsName +' | Rating 4',Title:'Produk Rating Bintang 4',Param:'start-4',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },

                { 
                   
                    path: "/category/start-5/",
                    props: { Apps:AppsName +' | Rating 5',Title:'Produk Rating Bintang 5',Param:'start-5',ShowPR:true,ShowHistory:false,Page:'category'},
                    component: require('./../components/frontend/index/all.vue').default,
                },

                { 
                   
                    path: "/location/:id",
                    props: { Apps:AppsName +' | Lokasi Produk',Title:'Lokasi',ShowPR:true,ShowHistory:false,Page:'location'},
                    component: require('./../components/frontend/index/location.vue').default,
                },

               
               
                { 
                   
                    path: "/product/preview/:id",
                    props: { Apps:AppsName +' | Preview Produk',SocketIo:SocketIo,ShowPR:true,ShowHistory:false,Page:'detail'},
                    component: require('./../components/frontend/index/preview.vue').default,
                },
                {
                   
                    path: '/cart',
                    props: { Apps:AppsName +' | Keranjang',SocketIo:SocketIo,ShowPR:false,ShowHistory:false,Page:'cart'},
                    component: require('./../components/frontend/cart/index.vue').default
                },
                {
                  
                    path: '/wishlist',
                    props: { Apps:AppsName +' | Wishlist',ShowPR:false,ShowHistory:false,},
                    component: require('./../components/frontend/wishlist/index.vue').default
                },
                
                {
                 
                    path: "/notification",
                    props: { Apps:AppsName +' | Notifikasi',ShowPR:false,ShowHistory:false,},
                    component: require('./../components/frontend/cart/notification.vue').default,
                }, 

                {
                  
                    path: "/history",
                    props: { Apps:AppsName +' | Riwayat',ShowPR:false,ShowHistory:true},
                    component: require('./../components/frontend/cart/history.vue').default,
                },
                {
                  
                    path: "/history/detail/:id",
                    props: { Apps:AppsName +' | Detail Riwayat',ShowPR:false,ShowHistory:false},
                    component: require('./../components/frontend/cart/detail.vue').default,
                },
                {
                   
                    path: "/order/transaction",
                    props: { Apps:AppsName +' |  Transaksi',ShowPR:false,ShowHistory:false},
                    component: require('./../components/frontend/cart/order.vue').default,
                },
                {
                  
                    path: "/chat",
                    props: { Apps:AppsName +' | Chat',SocketIo:SocketIo},
                    component: require('./../components/frontend/chat/chat.vue').default,
                },
                {
                  
                    path: "/chat/:id",
                    props: { Apps:AppsName +' | Chat',SocketIo:SocketIo},
                    component: require('./../components/frontend/chat/chat.vue').default,
                },
             
            ],    
        },
        {
           
            path: '/login',
            props: { Apps:Login,Body:BodyLogin,SocketIo:SocketIo },
            component: require('./../components/auth/login.vue').default
        },
        {
           
            path: '/forgot-password',
            props: { Apps: AppsName +' | Forgot Password',Body:BodyLogin,SocketIo:SocketIo },
            component: require('./../components/auth/forgotPassword.vue').default
        },
        {
           
            path: '/forgot-success',
            props: { Apps: AppsName +' | Confirmation Success',Body:BodyLogin,SocketIo:SocketIo },
            component: require('./../components/auth/confirmationEmail.vue').default
        },
        {
           
            path: '/new-password',
            props: { Apps: AppsName +' | New Password',Body:BodyLogin,SocketIo:SocketIo },
            component: require('./../components/auth/newPassword.vue').default
        },

        {
           
            path: '/confirmation-success',
            props: { Apps: AppsName +' | Confirmation Success',Body:BodyLogin,SocketIo:SocketIo },
            component: require('./../components/auth/confirmationSuccess.vue').default
        },
       
        {
           
            path: '/register',
            props: { Apps:Login,Body:BodyLogin },
            component: require('./../components/auth/register.vue').default
        },
        
        {
            path: "/",
            redirect: "/vendor",
            props: { BodyLogin:BodyLogin,BodyDashboard1:BodyDashboard1,BodyDashboard2:BodyDashboard2 },
            component: require('./../components/layout/LayoutPrivate.vue').default,
            children: [
                {
                 
                    path: "/vendor",
                    props: { Apps:AppsName +' | Dashboard'},
                    component: require('./../components/dashboard/DashboardVendor.vue').default,
                },
                {
                    
                    path: "/profile",
                    props: { Apps:AppsName +' | Profile'},
                    component: require('./../components/vendor/account/profile.vue').default,
                },
                {
                  
                    path: "/vendor/chat",
                    props: { Apps:AppsName +' | Chat',SocketIo:SocketIo},
                    component: require('./../components/vendor/chat/chat.vue').default,
                },
                 {
                  
                    path: "/vendor/chat/:id",
                    props: { Apps:AppsName +' | Chat',SocketIo:SocketIo},
                    component: require('./../components/vendor/chat/chat.vue').default,
                },
                {
                    
                   
                    path: "/product",
                    props: { Apps:AppsName +' | Produk'},
                    component: require('./../components/product/index.vue').default,
                },
                {
                    
                   
                    path: "/product-report",
                    props: { Apps:AppsName +' | Produk Report'},
                    component: require('./../components/product/report.vue').default,
                },
                  {
                    
                   
                    path: "/product-archive",
                    props: { Apps:AppsName +' | Produk Arsip'},
                    component: require('./../components/product/archive.vue').default,
                },
              
                {
                    
                  
                    path: "/product/detail/:id",
                    props: { Apps:AppsName +' | Detail Produk'},
                    component: require('./../components/product/preview.vue').default,
                },
                {
                    
                    path: "/product/add",
                    props: { Apps:AppsName +' | Tambah Produk'},
                    component: require('./../components/product/add.vue').default,
                },
                {
                    
                    path: "/product/edit/:id",
                    props: { Apps:AppsName +' | Edit Produk'},
                    component: require('./../components/product/edit.vue').default,
                },
                {
                   
                    path: "/vendor/lelang",
                    props: { Apps:AppsName +' | Lelang'},
                    component: require('./../components/lelang/index.vue').default,
                }, 

                {
                 
                    path: "/vendor/lelang/riwayat",
                    props: { Apps:AppsName +' | Riwayat Data Lelang'},
                    component: require('./../components/lelang/history.vue').default,
                },
                {
                  
                    path: "/vendor/rfq",
                    props: { Apps:AppsName +' | RFQ'},
                    component: require('./../components/rfq/index.vue').default,
                },
                {
                    path: "/order-product-all",
                    props: { Apps:AppsName +' | Order All Produk'},
                    component: require('./../components/order/all.vue').default,
                },

                {
                    path: "/order-product-new",
                    props: { Apps:AppsName +' | Order New Produk '},
                    component: require('./../components/order/new.vue').default,
                },  

                {
                    path: "/order-product-rev",
                    props: { Apps:AppsName +' | Order Revisi Produk'},
                    component: require('./../components/order/revision.vue').default,
                }, 

                {
                  
                    path: "/order/detail/:id",
                    props: { Apps:AppsName +' | Order Detail'},
                    component: require('./../components/order/detail.vue').default,
                },





            ],
            
        },

       
       
    ],
});



new Vue({
    router,
    store, // Tambahkan store di sini
    components : {
        // VueSlickCarousel,
        //chat
    },
    mounted() {
    },
    methods: {
    }
}).$mount('#app');
