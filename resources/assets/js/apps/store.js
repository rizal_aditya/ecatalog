import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    filterBack: null, // Tambahkan state untuk menyimpan data
  },
  mutations: {
    setFilterBack(state, data) {
      state.filterBack = data;
    },
  },
  actions: {},
  getters: {
    getFilterBack: (state) => state.filterBack,
  },
});
