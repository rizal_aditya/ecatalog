<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;




Route::get('/', function () {
    return redirect('login');

});

    
    Route::get('login', 'App\Http\Controllers\GlobalController@Login')->name('login');
    Route::post('login', 'App\Http\Controllers\GlobalController@Access');
    Route::get('sso/user', 'App\Http\Controllers\GlobalController@SsoPublic');
    Route::get('forgot-password', 'App\Http\Controllers\GlobalController@ForgotPassword');
    Route::get('forgot-success', 'App\Http\Controllers\GlobalController@ForgotConfirmation');
    Route::get('confirmation-success', 'App\Http\Controllers\GlobalController@ConfirmationSuccess');
    Route::get('register', 'App\Http\Controllers\GlobalController@Register');

    Route::get('/new-password/{ref?}','App\Http\Controllers\GlobalController@NewPassword', function ($ref = null) {
        return $ref;
    });


  

    Route::middleware(['vendor'])->group(function () {
 
       Route::get('vendor', 'App\Http\Controllers\VendorController@Dashboard');
       Route::get('profile', 'App\Http\Controllers\VendorController@Profile');
       Route::get('vendor/chat', 'App\Http\Controllers\VendorController@Chat');
       Route::get('vendor/chat/{id}', 'App\Http\Controllers\VendorController@ChatRoom');
       Route::get('product', 'App\Http\Controllers\VendorController@Product');
       Route::get('product-archive', 'App\Http\Controllers\VendorController@ProductArchive');
       Route::get('product/add', 'App\Http\Controllers\VendorController@AddProduct');
       Route::get('product/edit/{id}', 'App\Http\Controllers\VendorController@EditProduct');
       Route::get('product/detail/{id}', 'App\Http\Controllers\VendorController@ProductPreview');
       Route::get('product-report/', 'App\Http\Controllers\VendorController@ProductReport');
       
       Route::get('vendor/lelang', 'App\Http\Controllers\VendorController@Lelang');
       Route::get('vendor/lelang/riwayat', 'App\Http\Controllers\VendorController@RiwayatLelang');

       Route::get('vendor/rfq', 'App\Http\Controllers\VendorController@Rfq');
       // Route::get('order-transport/', 'App\Http\Controllers\VendorController@OrderTransportasi');
       // Route::get('order-transport/{type}', 'App\Http\Controllers\VendorController@OrderTransportasi');
       // Route::get('order-insurance', 'App\Http\Controllers\VendorController@OrderAsuransi');
       // Route::get('order-insurance/{type}', 'App\Http\Controllers\VendorController@OrderAsuransi');
       // Route::get('order-product', 'App\Http\Controllers\VendorController@OrderProduct');
       // Route::get('order-product/{type}', 'App\Http\Controllers\VendorController@OrderProduct');

         Route::get('order-product-all', 'App\Http\Controllers\VendorController@OrderProductAll');
         Route::get('order-product-new', 'App\Http\Controllers\VendorController@OrderProductNew');
         Route::get('order-product-rev', 'App\Http\Controllers\VendorController@OrderProductRev');
         Route::get('order/detail/{id}', 'App\Http\Controllers\VendorController@Detail');
       
    });


    Route::middleware(['user'])->group(function () {
       Route::get('pdftranport', 'App\Http\Controllers\GlobalController@transport');
       Route::get('pdfasuransi', 'App\Http\Controllers\GlobalController@asuransi');
       Route::get('chat', 'App\Http\Controllers\GlobalController@Chat');
       Route::get('chat/{id}', 'App\Http\Controllers\GlobalController@ChatRoom');    
       Route::get('/', 'App\Http\Controllers\GlobalController@Index'); 
       Route::get('product/preview/{id}', 'App\Http\Controllers\GlobalController@ProductPreview');
       Route::get('cart', 'App\Http\Controllers\GlobalController@Cart');
       
       Route::get('wishlist', 'App\Http\Controllers\GlobalController@Wishlist');
       Route::get('history', 'App\Http\Controllers\GlobalController@History');
       Route::get('history/detail/{id}', 'App\Http\Controllers\GlobalController@Detail');
       Route::get('notification', 'App\Http\Controllers\GlobalController@Notification');
       Route::get('order/transaction', 'App\Http\Controllers\GlobalController@OrderProses');
       Route::get('location/{id}', 'App\Http\Controllers\GlobalController@location');

       Route::get('category/{id}', 'App\Http\Controllers\GlobalController@category');
       Route::get('filter', 'App\Http\Controllers\GlobalController@filter'); 
       Route::get('search/{id}', 'App\Http\Controllers\GlobalController@search');
     //  Route::get('token_otp', 'App\Http\Controllers\GlobalController@formOtp');
       // Route::get('createpdf', 'App\Http\Controllers\GlobalController@create'); 
    });

   


    
Route::get('/logout', function () {

    Auth::logout();
    setcookie('token', '', -1, '/');
    setcookie('access', '', -1, '/');
    Cache::flush();
    return redirect('login');
});
 
   
   