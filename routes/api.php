<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



use App\Http\Controllers\API\AuthApiController;
use App\Http\Controllers\API\FrontendApiController;
use App\Http\Controllers\API\ProductApiController;
use App\Http\Controllers\API\CartApiController;
use App\Http\Controllers\API\OrderApiController;
use App\Http\Controllers\API\HistoryApiController;
use App\Http\Controllers\API\DashboardApiController;
use App\Http\Controllers\API\NotificationApiController;
use App\Http\Controllers\API\PrivyApiController;
use App\Http\Controllers\API\ChatApiController;
use App\Http\Controllers\API\SsoApiController;






Route::post('auth/login',[AuthApiController::class, 'Login']);
Route::post('auth/login-wa',[AuthApiController::class, 'LoginWa']);

Route::group(['middleware' => 'jwt.auth'], function () {

     Route::get('dashboard', [DashboardApiController::class, 'index']);

     Route::get('user', [AuthApiController::class, 'getAuthUser']);
     Route::get('user/menu', [AuthApiController::class, 'sidebar']);
     Route::get('user/top', [AuthApiController::class, 'top']);
     Route::get('location', [AuthApiController::class, 'location']);
     Route::get('update/count/view', [AuthApiController::class, 'countView']);

     Route::get('user/chat', [ChatApiController::class, 'chat']);
     Route::post('user/chatroom', [ChatApiController::class, 'RoomList']);
     Route::get('update/count/chat', [ChatApiController::class, 'countChat']);
     Route::get('user/check-notif', [ChatApiController::class, 'checkNotif']);
     Route::get('user/vendor', [AuthApiController::class, 'getvendor']);
     Route::post('vendor/update', [AuthApiController::class, 'UpdateVendor']);
     Route::get('vendor/delphoto', [AuthApiController::class, 'DeletePhoto']);
     

     Route::get('frontend', [FrontendApiController::class, 'index']); 
     Route::get('listrequestpr', [FrontendApiController::class, 'listrequestpr']); 
     Route::get('rate/{id}', [FrontendApiController::class, 'rate']);
  
    
    
     Route::get('search/{id}', [FrontendApiController::class, 'search']);
     Route::get('product/preview/{id}', [FrontendApiController::class, 'preview']);
     Route::post('payment/preview', [FrontendApiController::class, 'previewPrice']);
     Route::post('vendor-transport', [CartApiController::class, 'vendorTransport']);
     Route::post('transport-price', [CartApiController::class, 'vendorPrice']);


     Route::get('wishlist', [FrontendApiController::class, 'wishlist']);
     Route::post('search-wishlist', [FrontendApiController::class, 'SearchWishlist']);
     
     Route::post('wishlist/save', [FrontendApiController::class, 'saveWishlist']);

     
     
     Route::get('product/vendor', [ProductApiController::class, 'index']);
      Route::get('product/vendor-report', [ProductApiController::class, 'report']);
     Route::get('product/vendor-archive', [ProductApiController::class, 'archive']);
     Route::get('product/create', [ProductApiController::class, 'create']);
     Route::get('product/edit/{id}', [ProductApiController::class, 'edit']);
   
     Route::post('product/store', [ProductApiController::class, 'store']);
     Route::put('product/update/{id}', [ProductApiController::class, 'update']);
     Route::delete('product/destroy/{id}', [ProductApiController::class, 'destroy']);
     Route::delete('product/revert/{id}', [ProductApiController::class, 'revert']);
     Route::post('product/level', [ProductApiController::class, 'GetLevel']);
     Route::post('product/getweight', [ProductApiController::class, 'GetWeight']);
     Route::post('product/payment-delete', [ProductApiController::class, 'deletePayment']);
     Route::post('product/payment-add', [ProductApiController::class, 'addPayment']);
     Route::post('product/photo-delete', [ProductApiController::class, 'deletePhoto']);
     Route::post('product/photo-add', [ProductApiController::class, 'addPhoto']);
     Route::post('product/filter', [ProductApiController::class, 'search']);
     
     // Route::get('order-transport/{id}', [OrderApiController::class, 'transport']);
     // Route::get('product-transport/{id}', [OrderApiController::class, 'OrderTransport']);
     // Route::post('filter-transport', [OrderApiController::class, 'FilterTransport']);

     // Route::get('order-insurance/{id}', [OrderApiController::class, 'insurance']);
     // Route::get('product-insurance/{id}', [OrderApiController::class, 'OrderInsurance']);
     // Route::post('filter-insurance', [OrderApiController::class, 'FilterInsurance']);
     
     // Route::get('order-product/{id}', [OrderApiController::class, 'product']);
     // Route::get('product-order/{id}', [OrderApiController::class, 'OrderProduct']);
     // Route::post('filter-product', [OrderApiController::class, 'FilterProduct']);

     // Route::get('order-detail/{id}', [OrderApiController::class, 'detail']);
     // Route::post('order/rev-request', [OrderApiController::class, 'RevRequest']);
 
     Route::post('sendcart', [CartApiController::class, 'store']);
     Route::post('update-qty', [CartApiController::class, 'UpdateQuantity']);
     Route::post('delete-product', [CartApiController::class, 'DeleteProduct']);

     Route::post('delete-vendor', [CartApiController::class, 'DeleteVendor']);

     Route::post('update-data', [CartApiController::class, 'UpdateData']);
    
    Route::get('cart', [CartApiController::class, 'index']);
    Route::get('update/count/cart', [CartApiController::class, 'countChat']);
    Route::get('summary', [CartApiController::class, 'summary']);
    Route::post('create-order', [CartApiController::class, 'insertOrder']);
    Route::get('notification', [NotificationApiController::class, 'index']);
    

    
    Route::get('history/check-document', [HistoryApiController::class, 'checkDocument']);
    Route::get('history', [HistoryApiController::class, 'history']);
    Route::get('history/detail/{id}', [HistoryApiController::class, 'detail']);
    Route::get('forword/cart/{order_code}', [CartApiController::class, 'ForwordToCart']);
    Route::post('filter-history', [HistoryApiController::class, 'FilterHistory']);

    Route::post('po-approve/request', [HistoryApiController::class, 'ApprovalManager']);

    

    Route::get('auth/logout',[AuthApiController::class, 'Logout']);
    Route::get('verified/{id}', [HistoryApiController::class, 'verified']);

    Route::post('privy/backend/order',[PrivyApiController::class, 'GetOrderBackend']);
    Route::get('lcts/{id}', [PrivyApiController::class, 'GetLcts']);
    Route::post('privy/order',[PrivyApiController::class, 'GetOrder']);
    Route::post('privy/sign/refresh', [PrivyApiController::class, 'RequestSign']);
    Route::post('privy/sign/request', [PrivyApiController::class, 'RequestOtp']);
    Route::post('privy/sign/process', [PrivyApiController::class, 'ProsesOtp']); 

    Route::get('send-wa', [PrivyApiController::class, 'sendWa']);
    Route::get('send-notix', [PrivyApiController::class, 'sendNotix']);
    
    
    Route::post('open_chat', [FrontendApiController::class, 'openChat']);      
    Route::post('chat/send', [ChatApiController::class, 'send']);
    Route::post('chat/search_vendor', [ChatApiController::class, 'searchVendor']);  

    Route::get('ecatalog-ads', [FrontendApiController::class, 'ads']);

     Route::get('/access',[AuthApiController::class, 'index']);
 Route::post('/access',[AuthApiController::class, 'index']);

});


require 'mobile.php';