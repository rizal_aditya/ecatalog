<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


use App\Http\Controllers\Mobile\AuthMobileApiController;
use App\Http\Controllers\Mobile\ActivityApiController;
use App\Http\Controllers\Mobile\DashboardMobileApiController;
use App\Http\Controllers\API\PrivyApiController;
//mobile apps
Route::post('mobile/auth/login',[AuthMobileApiController::class, 'Login']);
Route::post('mobile/forgot-password',[AuthMobileApiController::class, 'ForgotPassword']);

Route::post('mobile/new-password',[AuthMobileApiController::class, 'NewPassword']);


Route::group(['middleware' => 'jwt.auth'], function () {

    //mobile apps
    Route::get('mobile/activity',[ActivityApiController::class, 'activity']);
    Route::get('mobile/privy/form',[PrivyApiController::class, 'GetFormAssign']);
    Route::post('mobile/privy/form/{status_id}',[PrivyApiController::class, 'GetFormAssign']);
    
    Route::post('mobile/privy/sign/request', [PrivyApiController::class, 'RequestOtp']);
    Route::post('mobile/privy/sign/refresh', [PrivyApiController::class, 'RequestSign']);
    Route::post('mobile/privy/sign/process', [PrivyApiController::class, 'ProsesOtp']); 
    // Route::post('mobile/verfikasi',[PrivyApiController::class, 'Ecatalog']);

    Route::get('mobile/dashboard-ecatalog',[DashboardMobileApiController::class, 'Ecatalog']);
    Route::post('mobile/dashboard-ecatalog',[DashboardMobileApiController::class, 'Ecatalog']);

    Route::get('mobile/dashboard-emarket',[DashboardMobileApiController::class, 'Emarket']);
    Route::post('mobile/dashboard-emarket',[DashboardMobileApiController::class, 'Emarket']);

    Route::get('mobile/dashboard-boq',[DashboardMobileApiController::class, 'Boq']);
    Route::post('mobile/dashboard-boq',[DashboardMobileApiController::class, 'Boq']);

    Route::get('mobile/dashboard-rfq',[DashboardMobileApiController::class, 'Rfq']);
    Route::post('mobile/dashboard-rfq',[DashboardMobileApiController::class, 'Rfq']);
   
    Route::post('mobile/auth/logout',[AuthMobileApiController::class, 'Logout']);

});
