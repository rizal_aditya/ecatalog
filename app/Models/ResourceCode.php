<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ResourceCode extends Model
{
     // use SoftDeletes;

    public $table = 'resources_code';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'resources_code_id',
        'code',
        'parent_code',
        'name',
        'unspsc',
        'unspsc_name',
        'description',
        'status',
        'sts_matgis',
        'level',
        'approve_date',
        'approve_by',
        'input_date',  
    ];


    
    
}
