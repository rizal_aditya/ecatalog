<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectNew extends Model
{
     // use SoftDeletes;

    public $table = 'project_new';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'departemen_id',
        'kategori_id',
        'jenis_id',
        'created_at',
        'is_deleted',
        'location_id',
        'no_spk',
        'deleted_by',
        'deleted_time',
        'alamat',
        'lat',
        'long',
        'contact_person',
        'no_hp',
        'kode_spk_sap',
       
    ];

   


    public function location()
    {
        return $this->belongsTo('App\Models\Location','location_id');
    }


    
    
}
