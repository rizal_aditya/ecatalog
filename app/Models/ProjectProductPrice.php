<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectProductPrice extends Model
{
     // use SoftDeletes;

    public $table = 'project_product_price';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'project_id',
        'amandemen_id',
        'product_id',
        'payment_id',
        'price',
        'is_deleted',
        'location_id',
       
    ];


    
    
}
