<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Viewer extends Model
{
     // use SoftDeletes;

    public $table = 'viewer';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
      
        'product_id',
        'user_id',
        'vendor_id'   
    ];


    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }


    
    
}
