<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProduct extends Model
{
     // use SoftDeletes;

    public $table = 'order_product';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'order_no',
        'product_id',
        'qty',
        'product_uom_id',
        'order_product_status',
        'created_by',
        'created_at',
        'updated_by',
        'update_at',
        'is_deleted',
        'payment_method_id',
        'price',
        'include_price',
        'weight',
        'full_name_product',
        'payment_method_name',
        'uom_name',
        'vendor_id',
        'vendor_name',
        'json_include_price',
        'nilai_asuransi',
        'asuransi_id',
        'biaya_transport',
        'transportasi_id',
        'id_smcb',
        'periode_smcb',
        'price_smcb'
            
    ];


    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }


    
    
}
