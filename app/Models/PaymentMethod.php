<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
     // use SoftDeletes;

    public $table = 'payment_method';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'enum_payment_method_id',
        'day',
        'description',
        'is_deleted',
        'created_by',
        'updated_by',
        'deleted_by',
        'deleted_time',
       
    ];

    //  public function location()
    // {
    //     return $this->belongsTo('App\Models\Location','location_id');
    // }
    
    
}
