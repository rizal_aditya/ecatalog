<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class RoleUser extends Model
{
     // use SoftDeletes;

    public $table = 'users_roles';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'role_id'
       
    ];


    public function role()
    {
        return $this->belongsTo('App\Models\Roles','role_id');
    }
    
}
