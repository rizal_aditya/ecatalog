<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
     // use SoftDeletes;

    public $table = 'order';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'order_no',
        'total_price',
        'order_status',
        'payment_method_id',
        'shipping_id',
        'perihal',
        'created_at',
        'created_by',
        'updated_by',
        'update_at',
        'is_deleted',
        'project_id',
        'catatan',
        'tgl_diambil',
        'no_surat',
        'dp',
        'location_id',
        'location_name',
        'approve_sequence',
        'is_approve_complete',
        'vendor_id',
        'vendor_name',
        'pdf_name',
        'is_matgis',
        'kontrak_id',
        'biaya_transport',
        'transportasi_id',
        'nilai_asuransi',
        'asuransi_id',
        'jenis_asuransi',
        'order_gabungan_id',
        'generatepdf_time',
        'token',    
    ];


    public function user()
    {
        return $this->belongsTo('App\Models\User','created_by');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\ProjectNew','project_id');
    }
    
    
}
