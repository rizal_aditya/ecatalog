<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentProduct extends Model
{
     // use SoftDeletes;

    public $table = 'payment_product';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'payment_id',
        'price',
        'json_location',
        'location_id',
        'notes',
       
    ];

    //  public function location()
    // {
    //     return $this->belongsTo('App\Models\Location','location_id');
    // }
    
    
}
