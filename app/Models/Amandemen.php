<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Amandemen extends Model
{
     // use SoftDeletes;

    public $table = 'amandemen';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'id_project',
        'no_amandemen',
        'start_contract',
        'end_contract',
        'volume',
        'harga',
        'created_by',
        'scm_id',
    ];


    
    
}
