<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ProductGallery extends Model
{
     // use SoftDeletes;

    public $table = 'product_gallery';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'filename',
       
    ];


    
    
}
