<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectProduct extends Model
{
     // use SoftDeletes;

    public $table = 'project_products';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'project_id',
        'product_id',
        'is_kontrak',
        'is_deleted',
        'harga',
       
    ];


    
    
}
