<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
     // use SoftDeletes;

    public $table = 'cart';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'checklist',
        'order_no',
        'vendor_id',
        'vendor_name',
        'location_id',
        'location_name',
        'project_id',
        'pr_request_id',
        'destination_id',
        'project_address',
        'shipping_id',
        'transport',
        'vendor_transport_id',
        'transport_id',
        'transport_fee',
        'transport_name',
        'total_transport',
        'min_weight_transport',
        'insurance',
        'insurance_id',
        'insurance_vendor_id',
        'insurance_name',
        'insurance_min_price',
        'premi',
        'total_insurance',
        'catatan',
        'perihal',
        'tgl_ambil',
        'created_by',
        'type',
        'action',
        'reading',
        'status',

    ];


    //  public function vendor()
    // {
    //     return $this->belongsTo('App\Models\Vendor','vendor_id');
    // }
    
    
}
