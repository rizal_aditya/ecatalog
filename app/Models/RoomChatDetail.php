<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class RoomChatDetail extends Model
{
     // use SoftDeletes;

    public $table = 'room_chat_detail';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'room_id',
        'from',
        'to',
        'textmessages',
        'reading',
        'type'     
    ];


    
    
}
