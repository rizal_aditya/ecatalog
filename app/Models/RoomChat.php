<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class RoomChat extends Model
{
     // use SoftDeletes;

    public $table = 'room_chat';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'from',
        'to',
        'active'     
    ];

     
    
    
}
