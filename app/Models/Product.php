<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
     // use SoftDeletes;

    public $table = 'product';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'code_lama',
        'code_1_lama',
        'code',
        'code_1',
        'cek',
        'no_contract',
        'specification_id',
        'size_id',
        'price',
        'berat_unit',
        'reference',
        'vendor_id',
        'location_id',
        'term_of_delivery_id',
        'volume',
        'uom_id',
        'note',
        'attachment',
        'is_deleted',
        'category_id',
        'created_by',
        'is_include',
        'include_price',
        'deleted_by', 
        'width',
        'height',
        'verified_status',
    ];


      public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor','vendor_id');
    }
    

      public function ResourcesCode()
    {
        return $this->belongsTo('App\Models\ResourcesCode','code_1');
    }
    
    
}
