<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class LogPassword extends Model
{
     // use SoftDeletes;

    public $table = 'users_log';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'password_last',
        'status'
       
    ];


    
}
