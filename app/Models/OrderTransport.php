<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class OrderTransport extends Model
{
     // use SoftDeletes;

    public $table = 'order_transportasi';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'order_no',
        'perihal',
        'catatan',
        'biaya_transport',
        'keterangan_transport',
        'transportasi_id',
        'vendor_id',
        'location_origin_id',
        'location_destination_id',
        'project_id',
        'category_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'order_gabungan_id',
        'data_traller',
        'pdf_name',
        'generatepdf_time',
        'weight_minimum',
           
    ];


    public function kontrak()
    {
        return $this->belongsTo('App\Models\ContractTransport','vendor_id');
    }

    // public function project()
    // {
    //     return $this->belongsTo('App\Models\ProjectNew','project_id');
    // }
    
    
}
