<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
     // use SoftDeletes;

    public $table = 'project';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'no_surat',
        'tanggal',
        'is_deleted',
        'group_id',
        'user_ids',
        'vendor_id',
        'no_contract',
        'file_contract',
        'start_contract',
        'end_contract',
        'departemen_pemantau_id',
        'user_pemantau_id',
        'volume',
        'harga',
        'payment_method_id',
        'status',
        'last_amandemen_id',
        'scm_id',
        'volume_sisa',
        'volume_terpakai',
        'category_id',
        'created_by',
        'updated_by',
        'deleted_by',
       
    ];


    
    
}
