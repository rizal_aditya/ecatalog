<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
     // use SoftDeletes;

    public $table = 'vendor';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'address',
        'email',
        'description',
        'start_contract',
        'end_contract',
        'file_contract',
        'is_delete',
        'no_contract',
        'is_margis',
        'departement',
        'no_fax',
        'no_telp',
        'nama_direktur',
        'wise_id',
        'code_bp',
        // 'created_at',
        // 'updated_at',
        'registration_number',
        'dir_pos',
        'ttd_name',
        'ttd_pos',   
    ];


    
    
}
