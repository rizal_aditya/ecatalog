<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
     // use SoftDeletes;

    public $table = 'ref_locations';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'location_id',
        'parent_id',
        'level',
        'country_id',
        'province_id',
        'regency_id',
        'district_id',
        'village_id',
        'country_name',
        'province_name',
        'regency_name',
        'district_name',
        'village_name',
        'name_prefix',
        'name',
        'full_name',
        'stereotype',
        'row_status',
    ];


    
}
