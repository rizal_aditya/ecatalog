<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{
     // use SoftDeletes;

    public $table = 'roles';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'scm_id'     
    ];


    
    
}
