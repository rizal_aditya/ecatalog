<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
     // use SoftDeletes;

    public $table = 'groups';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'area_id',
        'general_manager',
        'scm_id',
        'role_id_general_manager',
        'departemen_code2',
        'departemen_code',
        'ttd',
        'akses_proyek'
    ];


   
    
}
