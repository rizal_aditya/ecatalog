<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ContractTransport extends Model
{
     // use SoftDeletes;

    public $table = 'kontrak_transportasi';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'no_contract',
        'vendor_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'is_deleted',
        'start_date',
        'end_date',
        'tgl_kontrak',
        'status',
        'last_amandemen_id',
        'deleted_by',
        'deleted_time',
        'data_trailer',
        'weight_minimum'
         
    ];


   
    
}
