<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;



class AuthEmailVerified extends Mailable
{
    use Queueable, SerializesModels;
    public $username;
    public $encript;
    public $base_url;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username,$encript,$base_url)
    {
        $this->username = $username;
        $this->encript = $encript;
        $this->base_url = $base_url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME', 'Wika Ecatalog'))->subject('Forgot Password')->view('mail.AuthEmailVerified');

    }
}
