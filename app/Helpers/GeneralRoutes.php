<?php

namespace App\Helpers;

class GeneralRoutes
{

     public static function AuthRouter()
     {
         	
         	if(isset($_COOKIE['token'])) 
         	{
         
         		$token = $_COOKIE['token'];
			$url = env('APP_URL').'api/user';
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			   'Content-Type: application/json',
			   'Authorization: Bearer ' . $token
			   ));
			$data = curl_exec($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);
			return $data;
	     }else{
	     	return redirect()->to('login');
	     }	

     }	


}	