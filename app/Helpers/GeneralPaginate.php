<?php

namespace App\Helpers;

class GeneralPaginate
{

    /**
     * build child menu
     * @param $child
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function limit() 
    {   
        $paginate = env('PAGINATE', '30');
        return $paginate;
    }

    public static function uploadPhotoFolder() 
    {   
        $img = env('URL_FILE', 'images/profile/');
        return $img;
    }

    public static function FileProduct() 
    {   
        $img = env('URL_PRODUCT', 'images/product/');
        return $img;
    }

    public static function FileAkun() 
    {   
        $img = env('URL_FILE', 'images/profile/');
        return $img;
    }

    public static function FilePublic() 
    {   
        $img = env('URL_FILE', 'img/');
        return $img;
    }


    public static function uploadFileFolder() 
    {   
        $img = env('URL_FILE', 'file/');
        return $img;
    }

    public static function uploadInvoiceFolder(){

        $img = env('URL_INVOICE', 'images/invoice/');
        return $img;  
    }

   

}