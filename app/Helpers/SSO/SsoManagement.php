<?php
namespace App\Helpers\SSO;
use App\Helpers\SSO\GeneralSso;
use JWTAuth;
class SsoManagement
{

    public static function Connection(){

        $myBroker = new GeneralSso();
        $myBroker->attach(true); 
      
    }

  public static function GetUser()
  {
        $myBroker = new GeneralSso();
        return $myBroker->getUserInfo();
  }

  public static  function LoginSso($username,$password,$access,$token){
      $myBroker = new GeneralSso();
      //return array($username,$password,$access,$token);
      return $myBroker->login($username,$password,$access,$token);
  }

  public static  function LoginSsoWa($username,$access,$token){
      $myBroker = new GeneralSso();
      //return array($username,$password,$access,$token);
      return $myBroker->loginWa($username,$access,$token);
  }

  public static function LogoutSso()
  {
        $myBroker = new GeneralSso();
        $myBroker->logout();
        if (isset($_COOKIE['token'])) {
            unset($_COOKIE['token']); 
            setcookie('token', null, -1, '/'); 
            unset($_COOKIE['access']); 
            setcookie('access', null, -1, '/');
        } 
       
      
       
  }


  public static function ConvertCode($action, $string)
   {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'loginsso';
        $secret_iv = 'wika';

        $expSeconds = 500;

        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt' ) {
            $time = pack('N', time());
            $output = openssl_encrypt($time . $string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if($action == 'decrypt' ) {
            $output = false;
            $data = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
            if ($data) {
                //$time = unpack('N', substr($data, 0, 4));
                //if (time() - $time[1] <= $expSeconds) {
                    $strDecoded = substr($data, 4);
                    $output = $strDecoded;
                //}
            }

        }

        return $output;
    }

 


   

}
