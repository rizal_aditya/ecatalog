<?php

namespace App\Http\Controllers;
use Request;
use Illuminate\Support\Facades\Config;
use App\Helpers\SSO\SsoManagement;
use App\Helpers\SSO\GeneralSso;
use Auth;
use DB;
use Illuminate\Support\Facades\Redirect;
class VendorController extends Controller
{
   
    public function __construct()
    {
        SsoManagement::Connection();
        SsoManagement::GetUser();
    }


    public function Dashboard(Request $request)
    {
        $title = 'Dashboard';
        return view('landingpage.index')->with(['title'=>$title]);
    }

     public function Profile(Request $request)
    {
        $title = 'Profile';
        return view('landingpage.index')->with(['title'=>$title]); 

    }

     public function Chat(Request $request)
    {
        $title = 'Chat';
        return view('landingpage.index')->with(['title'=>$title]); 

    }
 
    public function ChatRoom(Request $request,$id)
    {

       
        
        $user = DB::table('room_chat as a')
        ->select('b.first_name','b.last_name')
        ->join('users as b','a.to','=','b.id')
        ->where('a.id',$id)->first();
        if($user !=null)
        {
            $vendor = $user->first_name.' '.$user->last_name;
        }else{
            $vendor = "";
        }    
        $title = 'Chat '.$vendor;
        return view('landingpage.index')->with(['title'=>$title]); 

    } 


   

   

     

      public function Detail(Request $request)
    {
        $title = 'Order Detail';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

     public function Product(Request $request)
    {
        $title = 'Produk';
        return view('landingpage.index')->with(['title'=>$title]); 

    }

     public function ProductArchive(Request $request)
    {
        $title = 'Produk Arsip';
        return view('landingpage.index')->with(['title'=>$title]); 

    }

     public function ProductPreview(Request $request)
    {
        $title = 'Preview Produk';
        return view('landingpage.index')->with(['title'=>$title]); 

    }


    public function ProductReport(){

        $title = 'Produk Report';
        return view('landingpage.index')->with(['title'=>$title]);
    }


      public function AddProduct(Request $request)
    {
        $title = 'Tambah Produk';
        return view('landingpage.index')->with(['title'=>$title]); 

    }

      public function EditProduct(Request $request)
    {
        $title = 'Edit Produk';
        return view('landingpage.index')->with(['title'=>$title]); 

    }
    

     public function Lelang(Request $request)
    {
        $title = 'Lelang';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

     public function RiwayatLelang(Request $request)
    {
        $title = 'Riwayat Lelang';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

      public function Rfq(Request $request)
    {
        $title = 'RFQ';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

      public function OrderProductAll(Request $request)
    {
        $title = 'Order All Produk ';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

      public function OrderProductNew(Request $request)
    {
        $title = 'Order New Produk';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

      public function OrderProductRev(Request $request)
    {
        $title = 'Order Revisi Produk';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

    

   

    public function Logout(Request $request)
    {
 
            $user = SsoManagement::GetUser();
            // $update = DB::table('users')->where('id',$user['id'])->update(['active_chat'=>'false']);
            // if($update > 0)
            // {
                  SsoManagement::LogoutSso();
                 
            // }    
         
            return redirect()->to('login');
       
    } 

   
    
}
