<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Config;
use PDF;
use DB;
use App\Helpers\GeneralEncrypt;
use App\Http\Request\RequestCart;
use App\Http\Request\RequestOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Cache;
use App\Http\Request\Validation\ValidationAuth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Request\RequestAuth;
use App\Models\User;
use App\Models\RoleUser;
use Auth;
use App\Helpers\GeneralHelpers;
class GlobalController extends Controller
{
   
    public function __construct(){
       

       
    }


    public function SsoPublic(Request $request)
    {
      $key = 'routes_web';
      $cachedData = Cache::get($key);
      if ($cachedData) {
            $response = $cachedData;
      }else{
           $response = array('status'=>false,'data'=>[],'messages'=>'User Belum Login');
      }
       return $response;
     
    }

    public function Login(Request $request)
    {

        if (Auth::check()) 
        {
           return redirect('/');    
        }

             
        $title = 'Login';
        return view('landingpage.index')->with(['title'=>$title]); 
       
    }


    public function Access(Request $request)
    {
        $credentials = $request->only('username', 'password');
        RequestAuth::requestHash($request->username,$request->password); 
        $validation = ValidationAuth::validation($request);

        $key = 'routes_web';
        $cachedData = Cache::get($key);
       
        if($validation)
        {

            return response()->json($validation,400); 

        }else{
           
           $user = User::where('username',$credentials['username'])->first();
           if($user)
           {

                if ($user->active == 2)
                {  
                    $messages3 = array('status'=>'Akun anda ditangguhkan admin');
                    return response()->json(['status'=>false,'messages' => $messages3],400);
                }else{
            
                    // Authentication successful
                      
                        try
                        {
                            //access login api
                            if (! $token = JWTAuth::attempt($credentials)){
                                $messages1 = array('username'=>'Nama Pengguna tidak valid');
                                $messages2 = array('password'=>'Kata Sandi tidak valid');
                               
                               
                                if ($user ==null)
                                {
                                    return response()->json(['status'=>false,'messages' => $messages1],400);
                                }

                               
                                 
                                if (!Hash::check($credentials['password'], $user->password))
                                {
                                    return response()->json(['status'=>false, 'messages' => $messages2],400); 
                                }
         
                            }
                            //access login web
                            if (Auth::attempt($credentials)) 
                            {
                                
                                  
                               $auth = Auth::User();
                               $RoleUser = RoleUser::where('user_id',$auth->id)->first();
                               $access =  $RoleUser->role_id;
                              
                               $userHeader = array('id'=>$auth->id,'username'=>$auth->id);
                               $token = compact('token');
                               
                               $user = array('id'=>$auth->id,'username'=>$auth->username,'email'=>$auth->email,'phone'=>$auth->phone,'first_name'=>$auth->first_name,'last_name'=>$auth->last_name,'address'=>$auth->address,'vendor_id'=>$auth->vendor_id);

                               $data = array('status'=>true,'data'=>$user,'message'=>'Login User Success');
                               Cache::put($key, $data, 86400); // 86400 detik =  (24 jam)
                                    
                               // setcookie('access', $access, time() + (86400 * 30), "/"); // 86400 = 1 day
                               // setcookie('token', $token['token'], time() + (86400 * 30), "/"); // 86400 = 1 day

                               return response()->json(['status'=>true,'user_header'=>$userHeader,'access'=>$access,'token'=>$token['token']],200);


                               
                               
                            
                            }

                            

                        } catch (JWTException $e) {
                           return response()->json(['error' => 'validasi tidak valid'], 500);
                        } 

                       
                }
               
            }
        
        
        }    
    }

    public function ForgotPassword(Request $request){

        $title = 'Forgot Password';
        return view('landingpage.index')->with(['title'=>$title]); 
    }
   
    public function ForgotConfirmation(Request $request){
       
       $title = 'Confirmation Success';
       return view('landingpage.index')->with(['title'=>$title]); 

    }

    public function ConfirmationSuccess(Request $request){
       
       $title = 'Confirmation Success';
       return view('landingpage.index')->with(['title'=>$title]); 

    }


    public function NewPassword(Request $request){
       
       $title = 'New Password';
       return view('landingpage.index')->with(['title'=>$title]); 

    }

    

    public function Index(Request $request)
    {
        
       if (Auth::check()) {

             return view('landingpage.index')->with(['title'=>'PT Wijaya Karya']); 
          
        }else{
          return $this->ClearCache();
        } 
      
    }



    public function category(Request $request)
    {

        if (Auth::check()) 
        {

            $urisegment = Request::segment(2); 
            if($urisegment =='all')
            {
              $title = 'Semua Kategori';
            }else{
              $title = 'Kategori '.ucwords(strtolower($urisegment));   
            }    
            
            return view('landingpage.index')->with(['title'=>$title]);
          
        }else{
           return $this->ClearCache();  
        } 

         

    } 


    public function location(Request $request)
    {
        if (Auth::check()) 
        {

            $title = 'Lokasi Produk';   
            return view('landingpage.index')->with(['title'=>$title]); 

        }else{

           return $this->ClearCache();  
        }

    } 

    public function search(Request $request)
    {
        $urisegment = Request::segment(2); 
        if($urisegment !='')
        {
           $title = 'Pencarian '.ucwords(strtolower($urisegment));   
        }   
        
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

    public function rate(Request $request)
    {
        $urisegment = Request::segment(2); 
        if($urisegment =='start-0')
        {
          $title = 'Non Rating';
        }else{
          $title = 'Rating '.ucwords(strtolower($urisegment));   
        }    
        
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

    public function filter(Request $request)
    {
        
        $title = 'Filter';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

 
    
    public function Register(Request $request)
    {
        $title = 'Register';
        return view('landingpage.index')->with(['title'=>$title]); 

    }
   
     public function ProductPreview(Request $request,$id)
    {

      if (Auth::check()) 
      {
            $product = DB::table('product')->select('name')->where('id',$id)->first();
            if($product !=NULL)
            {
              $product_name = $product->name;
            }else{
              $product_name = '';
            }    
            $title = 'Preview Produk '.$product_name;
            return view('landingpage.index')->with(['title'=>$title]); 

       }else{

         return  $this->ClearCache(); 
       } 

    }

     public function Cart(Request $request)
    {
      if (Auth::check()) 
      {

        $title = 'Keranjang';
        return view('landingpage.index')->with(['title'=>$title]); 

      }else{
        
        return $this->ClearCache(); 
      }   

    }

     public function Chat(Request $request)
    {
        $title = 'Chat';
        return view('landingpage.index')->with(['title'=>$title]); 

    }
    
    public function ChatRoom(Request $request,$id)
    {

       
        
        $user = DB::table('room_chat as a')
        ->select('b.first_name','b.last_name')
        ->join('users as b','a.to','=','b.id')
        ->where('a.id',$id)->first();
        if($user !=null)
        {
            $vendor = $user->first_name.' '.$user->last_name;
        }else{
            $vendor = "";
        }    
        $title = 'Chat '.$vendor;
        return view('landingpage.index')->with(['title'=>$title]); 

    }
    
     public function History(Request $request)
    {
       if (Auth::check()) 
       { 
       
        $title = 'Riwayat';
        return view('landingpage.index')->with(['title'=>$title]); 

        }else{
         return $this->ClearCache(); 
        }  

    } 

      public function Detail(Request $request)
    {
        $title = 'Detail Riwayat';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

    public function Wishlist(Request $request)
    {

       if (Auth::check()) 
       { 
         
        $title = 'Wishlist';
        return view('landingpage.index')->with(['title'=>$title]); 

       }else{
        return $this->ClearCache(); 
       }   

    } 

       public function Notification(Request $request)
    {
        $title = 'Notifikasi';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

     public function OrderProses(Request $request)
    {
        $title = 'Order Transaksi';
        return view('landingpage.index')->with(['title'=>$title]); 

    } 

    public function formOtp(Request $request)
    {
        $title = 'Form OTP';
        return view('landingpage.index')->with(['title'=>$title]); 

    }

    public function ClearCache()
    {
          Auth::logout();
          setcookie('token', '', -1, '/');
          setcookie('access', '', -1, '/');
          return redirect('/login');  
          

    }


    public function transport(){
         
        $cart = RequestCart::QueryGetCart();
        $dataID = RequestCart::getCart($cart);
        $request = $dataID[0];
         
         $product = $request->product; 

         $shipping = collect($request->shipping)
                ->first(fn($shipping) => $shipping->value === $request->shipping_id)
                ->text ?? null; // mencari shiping name

         $project_name = collect($request->project)
                ->first(fn($project) => $project->value === $request->project_id)
                ->text ?? null; // mencari shiping name  

         $project_address = collect($request->project)
                ->first(fn($project) => $project->value === $request->project_id)
                ->address ?? null; // mencari shiping name   

         $transport_name = collect($request->transport_logistic)
                ->first(fn($transport) => $transport->value === $request->transport_id)
                ->text ?? null; // mencari shiping name                      

        $payment =  'Transfer';
        // $project_name = $project_name;
        $devision =  strtoupper($request->division->branch_manager);
        $vendor_name = $request->vendor->text;
        $directur_name = $request->vendor->directur_name;
        $address = $request->vendor->address;
        $no_fax = $request->vendor->no_fax;
     
        $order_code = $request->order_no;
   
        $gm_devision = $request->division->general_manager;
        $branch_manager = $request->division->branch_manager;
        $res = [];
        foreach($product as $key =>$val)
        {
               $res[$key]['no'] = $key + 1;
               $res[$key]['product_name'] = $val->product_name;
               $res[$key]['volume'] = $val->volume;
               $res[$key]['qty'] = $val->qty;
               $res[$key]['weight'] = $val->weight; 
               $res[$key]['price'] = GeneralHelpers::Rupiah($val->price);
               $res[$key]['sub_total'] = GeneralHelpers::Rupiah($request->total_price); 
               // $res[$key]['jml_weight'] = $val->weight * $val->qty;
               // $res[$key]['jml_qty'] =  $val->qty;
        }          

        $data = array(
                    'project'=>['id'=>$request->project_id,'name'=>$project_name,'address'=>$project_address],
                    'product' => $res,
                    'total_qty' =>  $request->total_qty,
                    'total_weight' => $request->total_weight,
                    'sub_total'=> number_format($request->total_price,0,',','.') ,
                    'terbilang'=> RequestOrder::TerbilangNumber($request->total_price),
                    'order_no'=>$order_code,
                    'devision'=>$devision,
                    'up'=>$request->up_transport,
                    'perihal'=>$request->perihal_transport,
                    'catatan'=>$request->catatan_transport,
                    'vendor_product'=> $request->vendor,
                    'vendor_product_location'=> $request->location_name,
                    'vendor_transport'=> RequestOrder::VendorTransportAmandemenID($request->transport_id,$transport_name),
                    'transport_name' => $request->transport_name,
                    'transport_price'=> GeneralHelpers::Rupiah($request->total_transport),
                    'transport_fee'=> $request->transport_fee,
                    'created_by'=>$request->created_by,
                    'weight_minimum'=>$request->min_weight_transport,
                    'created_at'=> $request->created_at,
                    'updated_at'=> $request->updated_at,
                    'gm_devision'=>$gm_devision,
                    'branch_manager'=>$branch_manager
            );


           $result = json_decode(json_encode($data), FALSE);
           // return $result;
           // exit();
 
        $title = 'transport'; 
        return view('orderPOTransport')->with(['title'=>$title,'order'=> $result]); 
    }


    public function asuransi(){
         
        $cart = RequestCart::QueryGetCart();
        $dataID = RequestCart::getCart($cart);
        $request = $dataID[0];
         
        $product = $request->product; 
        
        $project_name = collect($request->project)
                ->first(fn($project) => $project->value === $request->project_id)
                ->text ?? null; // mencari shiping name  

         $project_address = collect($request->project)
                ->first(fn($project) => $project->value === $request->project_id)
                ->address ?? null; // mencari shiping name   

         $transport_name = collect($request->transport_logistic)
                ->first(fn($transport) => $transport->value === $request->transport_id)
                ->text ?? null; // mencari shiping name  
        
        

        $payment =  'Transfer';
        // $project_name = $project_name;
        $devision =  strtoupper($request->division->branch_manager);
        $vendor_name = $request->vendor->text;
        $directur_name = $request->vendor->directur_name;
        $address = $request->vendor->address;
        $no_fax = $request->vendor->no_fax;
       
     
        $order_code = $request->order_no;
      
        $gm_devision = $request->division->general_manager;
        $branch_manager = $request->division->branch_manager;
        $res = [];
        foreach($product as $key =>$val)
        {
               $res[$key]['no'] = $key + 1;
               $res[$key]['product_name'] = $val->product_name;
               $res[$key]['volume'] = $val->volume;
               $res[$key]['qty'] = $val->qty;
               $res[$key]['weight'] = $val->weight; 
               $res[$key]['price'] = GeneralHelpers::Rupiah($val->price);
               $res[$key]['sub_total'] = ['original'=>$request->total_price,'convert'=>GeneralHelpers::Rupiah($request->total_price)]; 
        }          

        $data = array(
                    'project'=>['id'=>$request->project_id,'name'=>$project_name,'address'=>$project_address],
                    'product' => $res,
                    'total_qty' =>  $request->total_qty,
                    'total_weight' => $request->total_weight,
                    'sub_total'=> ['original'=>$request->total_price,'convert'=>GeneralHelpers::Rupiah($request->total_price)] ,
                    'terbilang'=> RequestOrder::TerbilangNumber($request->total_price),
                    'order_no'=>$order_code,
                    'devision'=>$devision,
                    'up'=>$request->up_insurance,
                    'perihal'=>$request->perihal_insurance,
                    'catatan'=>$request->catatan_insurance,
                    'vendor_product'=> $request->vendor,
                    'vendor_transport'=> RequestOrder::VendorTransportAmandemenID($request->transport_id,$transport_name),
                    'vendor_asuransi'=> RequestOrder::VendorAsuransiAmandemenID($request->insurance_id),
                   
                    'created_by'=>$request->created_by,
                    'transport'=>$request->transport,
                    'created_at'=>  $request->created_at,
                    'tgl_ambil'=>  GeneralHelpers::tanggal($request->tgl_ambil),
                    'updated_at'=> $request->updated_at,
                    'gm_devision'=>$gm_devision,
                    'branch_manager'=>$branch_manager
            );


           $result = json_decode(json_encode($data), FALSE);
           // return $result;
           // exit();
 
        $title = 'transport'; 
        return view('orderPOAsuransi')->with(['title'=>$title,'order'=> $result]); 
    }
    

    
    
    
}
