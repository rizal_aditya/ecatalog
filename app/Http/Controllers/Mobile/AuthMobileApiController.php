<?php

namespace App\Http\Controllers\Mobile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\GeneralPaginate;
use App\Helpers\GeneralEncrypt;
use App\Helpers\SSO\SsoManagement;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\User;
use App\Models\RoleUser;
use App\Models\Vendor;
use App\Models\LogPassword;
use App\Http\Request\RequestAuth;
use Auth;
use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\AuthEmailVerified;
use DB;


class AuthMobileApiController extends Controller
{

   
    public function __construct()
    {  
      
    }

    public function Login(Request $request){
        
        $credentials = $request->only('username', 'password');
        $user = User::where('username',$credentials['username'])->first();
        
        // DB::table('cart')->where('created_by',$user->id)->delete();
        if($user)
        {
            $log = LogPassword::where('user_id',$user->id)->count();
            if($log ==null)
            {
                if(sha1($credentials['password']) == strtolower($user->password))
                {
                   LogPassword::create(['user_id'=>$user->id,'status'=>'true','password_last'=>$user->password]); 
                   User::where('username',$credentials['username'])->update(['password'=>bcrypt($credentials['password'])]);
                }
            }     
        }    
        
       // $Auth = array('username'=>$credentials['username'],'password'=>$credentials['password']); 
        try {
            if (! $token = JWTAuth::attempt($credentials))
            {   
                $validation = RequestAuth::LoginAuth($credentials['username'],$credentials['password']);
                if($validation !=null || $validation !="")
                {
                   return response()->json($validation,400);  
                }
              
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
       
        $token = compact('token');
        $auth = Auth::User();
        $RoleUser = RoleUser::where('user_id',$auth->id)->first();
      
        $access =  $access =  DB::table('privilleges as a')->join('menu as b','a.menu_id','=','b.id')->select('b.id','b.name','b.url as slug')->where(['a.role_id'=>$RoleUser->role_id,'a.function_id'=>2])->get();
        $data = array('status'=>true,'token'=>$token['token'],'access'=>$access);
        return response()->json($data,200);   

      
     }


     


      public function Logout(Request $request)
      {
        
           //$user = SsoManagement::GetUser();
           if($request->token !="")
           {
               try {
                JWTAuth::invalidate($request->token);
            
                    return response()->json(['status' => true, 'message'=> "You have successfully logged out."],200);

                } catch (JWTException $e) {
                    // something went wrong whilst attempting to encode the token
                    return response()->json(['status' => false, 'error' => 'Failed to logout, please try again.'], 500);
                }


           }else{
                   return response()->json(['status' => false, 'message'=> "Token required"],500);
           } 
       
           

       }


    public function ForgotPassword(Request $request){
  
      $user =  User::where('email',$request->email)->first();
      if($user !=null)
      {
              $base_url = url('');
              $encript = GeneralEncrypt::encryptCode('encrypt',$user->email); 
              Mail::to($user->email)->send(new AuthEmailVerified($user->username,$encript,$base_url));
           return response()->json(['status' => true, 'message'=> "Forgot email successfully"],200);
      }else{
           return response()->json(['status' => false, 'message'=> "Email tidak valid"],500);
      } 
      
       
    }  

    public function NewPassword(Request $request)
    {
        $email = GeneralEncrypt::encryptCode('decrypt', $request->ref);
        $password = $request->password;
        if($password =="" && $request->confirm_password ==""){
            $data = array('password'=>'Password Kosong','confirm_password'=>'Konfirmasi password masih kosong');
            return response()->json(['status' => false, 'result'=>$data, 'message'=> 'Password & konfirmasi password masih kosong'],500); 
        }else{

            if($password == $request->confirm_password)
            {  
                if($email == false)
                {
                   $data = array('messages'=>'Token email sudah exprired, coba ulangi lagi');  
                   return response()->json(['status' => false, 'result'=>$data, 'message'=>'Token email sudah exprired,coba ulangi lagi'],500);   
                }else{
                   $user =  User::where('email',$email)->update(['password'=> bcrypt($password)]);
                   return response()->json(['status' => true, 'message'=> 'Berhasil password sudah diupdate'],200); 
                }    
                
                
            }else{
                return response()->json(['status' => false, 'message'=> 'Konfirmasi password tidak valid'],500);   
            }
        }    
      
    } 

    public function getAuthUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        
        $data = compact('user');

        return $data['user']->id;
    }

    


   


    

    
   
    


}    