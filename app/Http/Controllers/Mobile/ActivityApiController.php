<?php

namespace App\Http\Controllers\Mobile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Wishlist;
use App\Helpers\GeneralPaginate;
use App\Http\Request\RequestHistory;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestPrivy;




use Auth;
use DB;

class ActivityApiController extends Controller
{

   
    public function __construct()
    {   
         $this->perPage = GeneralPaginate::limit();
       // $this->perPage = 2;
        $this->UploadFolder = GeneralPaginate::uploadPhotoFolder();
        $this->UploadFileFolder = GeneralPaginate::uploadFileFolder();
        
    }


    public function checkDocument()
    {
        $results = RequestHistory::TotalApproval();
        return response()->json($results);
    }

    

    public function activity(Request $request)
    {
        $_res = array();
        $auth = Auth::User();
        $id = $auth->id;

        $check = RequestHistory::CheckApproval($id);

        $order = DB::table('order as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.location_name','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.pdf_name','a.is_approve_complete');
        $order->join('users as b','a.created_by','=','b.id');
        //$order->join('order as c','a.order_no','=','c.order_no');
        $order->join('groups as e','b.group_id','=','e.id');
        $order->whereNotNull('a.pdf_name');
        $order->whereNotNull('a.token');
        if($check  !=null)
        {

            $order->leftJoin('approve_po_list as d', function($join) {
                $join->on('a.order_no', '=', 'd.order_no'); 
                $join->whereRaw('a.approve_sequence + 1 = d.sequence');
                $join->where('a.order_status', '=', 1);
                $join->where('a.is_approve_complete', '=', 0);
            });

            $order->where('e.id',$check->departemen_id);
            //$order->GroupBy('a.order_no'); 
             $order->orderBy('a.id','DESC');  
        }else{

            $order->where('a.created_by',$id);
            $order->orderBy('a.id','DESC');  
        }    

        $results = $order->paginate(5);
      
        $description = '';
        $_res = RequestHistory::GetDataOrder($results,$this->perPage,$request,$description);
        return response()->json($_res);

    }

    public function detail($id,Request $request)
    {
        $_res = array();
        $auth = Auth::User();
        $vendor_id = $auth->vendor_id;
        $order = DB::table('order as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.location_name','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.pdf_name','a.is_approve_complete');
        $order->join('users as b','a.created_by','=','b.id');
        //$order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.order_no'=>$id]);
        $order->whereNotNull('a.pdf_name');
        $order->whereNotNull('a.token');
        $order->orderBy('a.id','DESC');
        $results = $order->first();
      
       
        $_res = RequestHistory::GetDataOrderDetail($results);
        return response()->json($_res);

    }




    public function verified($id,Request $request)
    {
       
       $arr = array();
       $apprv = DB::table('alasan_cancel')->where('jenis',$id)->get();
       
        foreach ($apprv as $key => $val)
        {
           $arr[$key]['value'] = $val->id;
           $arr[$key]['text'] = $val->alasan;
        }      
      

        return $arr;

    }


    public function FilterHistory(Request $request){

        $_res = array();
        $auth = Auth::User();
        $id = $auth->id;
        $order = DB::table('order as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.location_name','a.perihal','a.catatan','a.created_at','a.vendor_id','a.project_id','b.first_name','b.last_name','b.group_id','a.pdf_name','a.is_approve_complete','a.no_surat','a.vendor_name','a.location_name');
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->join('project_new as d','a.project_id','=','d.id');

        if($request->no_order !="")
        {
          
            $order->where('a.order_no','LIKE','%'.$request->no_order.'%'); 
        } 

        if($request->no_surat !="")
        {
           
            $order->where('a.no_surat','LIKE','%'.$request->no_surat.'%'); 
        } 

        if($request->project_id !="")
        {
            $order->where(['a.project_id'=>$request->project_id]); 
        }  

         if($request->vendor_name !="")
        {
            $order->where('a.vendor_name','LIKE','%'.$request->vendor_name.'%'); 
        }  

        if($request->perihal !="")
        {
            $order->where('a.perihal','LIKE','%'.$request->perihal.'%'); 
        }  

         if($request->department_id !="")
        {
            $order->where(['d.departemen_id'=>$request->department_id]); 
        }  

        if($request->startdate !="" && $request->enddate !="")
        {
             $order->whereBetween('a.created_at', [$request->startdate, $request->enddate]);
        }    

        if($request->status_id !="")
        {
            $order->where(['a.order_status'=>$request->status_id]); 
        }  

        //$order->whereNotNull('a.pdf_name');
        //$order->whereNotNull('a.token');
       // $order->where('a.created_by',$id);
        $order->orderBy('a.id','DESC');
        $results = $order->paginate($this->perPage);

        $description = RequestHistory::descriptionSearch($request);
        $_res = RequestHistory::GetDataOrder($results,$this->perPage,$request,$description);
        return response()->json($_res);

    }





   //  public function productRevision(Request $request){


   //      $_res = array();
   //      $auth = Auth::User();
   //      $id = $auth->vendor_id; 
   //      $order = DB::table('order as a');
   //      $order->select('a.id','a.order_no as order_code','a.project_id','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id');
   //      $order->join('users as b','a.created_by','=','b.id');
   //      $order->join('order as c','a.order_no','=','c.order_no');
   //      $order->where(['a.vendor_id'=>110,'c.is_approve_complete'=>1]);
   //      $order->where('c.order_status',4);
   //      $results = $order->paginate($this->perPage);

   //      $type ="product";
   //      $description = '';
   //      $_res = RequestHistory::GetDataOrder($results,$this->perPage,$request,$description,$type);
   //      return response()->json($_res);

   //  }
    

   //   public function FilterProduct(Request $request)
   //   {

   //      $_res = array();
   //      $auth = Auth::User();
   //      $id = $auth->vendor_id; 
   //      $search = $request->search;


   //      $order = DB::table('order as a');
   //      $order->select('a.id','a.order_no as order_code','a.project_id','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id');
   //      $order->join('users as b','a.created_by','=','b.id');
   //      $order->join('order as c','a.order_no','=','c.order_no');    
   //      $order->join('project_new as d','a.project_id','=','d.id');
   //      $order->where(['a.vendor_id'=>110,'c.is_approve_complete'=>1]);
   //      $order->where('d.name','LIKE',''.$search.'%');
   //      $order->orWhere('a.order_no','LIKE',''.$search.'%');
   //      $order->orWhere('d.alamat','LIKE',''.$search.'%');
   //      $results = $order->paginate($this->perPage);


     
   //      $type ="product";
   //      $description = $search;
   //      $_res = RequestHistory::GetDataOrder($results,$this->perPage,$request,$description,$type);
   //      return response()->json($_res);
   //  }
    
   //  public static function OrderProduct($id,Request $request){
         
   //   $__temp_ = array();
   //   $product = DB::table('order_product as a')
   //               ->select('b.id','b.name as product_name','a.qty','a.price','a.weight','b.uom_id')
   //               ->join('product as b','a.product_id','=','b.id')
   //               ->where(['a.order_no'=>$id])
   //               ->paginate(2);

     
   //    foreach ($product as $key => $val)
   //    {
   //         $__temp_[$key]['id'] = $val->id;
   //         $__temp_[$key]['photo'] = RequestFrontend::PhotoProduct($val->id);
   //         $__temp_[$key]['product_name'] = RequestFrontend::limitText($val->product_name,'50');
   //         $__temp_[$key]['qty'] = $val->qty;
   //         $__temp_[$key]['weight'] = $val->weight.' Kg';
   //         $__temp_[$key]['payment'] = RequestHistory::OrderPayment($id);
   //         $__temp_[$key]['price'] = RequestHistory::Rupiah($val->price);
   //         $__temp_[$key]['total_price'] = RequestHistory::Rupiah($val->qty * $val->weight * $val->price);
   //    }       
     
   //   if($product->nextPageUrl() !=null)
   //   {
   //       $param1 = explode('=',$product->nextPageUrl());
   //       $next = (int)$param1[1];
        
   //   }else{
   //       $next = null;
         
   //   } 


   //   if($product->previousPageUrl() !=null) {

   //       $param2 = explode('=',$product->previousPageUrl());
   //       $prev = (int)$param2[1];
   //   }else{
   //        $prev = null;
         
   //    } 
    

   //   $results['result'] = $__temp_;
   //   $results['total'] = $product->total();
   //   $results['lastPage'] = $product->lastPage();
   //   $results['currentPage'] = $prev;
   //   $results['nextPageUrl'] = $next;
     
   //   return $results;

  
   // }
    


    


}    