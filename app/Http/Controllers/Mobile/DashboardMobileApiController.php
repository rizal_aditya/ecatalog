<?php

namespace App\Http\Controllers\Mobile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Wishlist;
use App\Helpers\GeneralPaginate;
use App\Http\Request\RequestHistory;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestPrivy;
use App\Http\Request\Mobile\RequestDashboardEcatalog;
use App\Http\Request\Mobile\RequestDashboardEmarket;
use App\Http\Request\Mobile\RequestDashboardBoq;
use App\Http\Request\Mobile\RequestDashboardRfq;
use Auth;
use DB;

class DashboardMobileApiController extends Controller
{

   
    public function __construct()
    {   
        
        
    }


    public function Ecatalog(Request $request)
    {
       $data['panel'] = RequestDashboardEcatalog::TopPanelEcatalog();
       $data['category'] = RequestDashboardEcatalog::Category();
       $data['devision'] = RequestDashboardEcatalog::Devision();
       $data['mounth'] = RequestDashboardEcatalog::Mounth();

        if($request->category_id !="")
        {
           $category =  $request->category_id;
        }else{
           $category = 'AF0000';//default category
        } 
    

        if($request->departemen_id !="")
        {
           $groupDept =  $request->departemen_id;
        }else{
           $groupDept = '';
        } 

        if($request->tahun)
        {
           $tahun = $request->tahun;
        }else{
           $tahun = date('Y');
        } 

        if($request->bulan)
        {
           $bulan = $request->bulan;
        }else{
          //$bulan = date('Y-m-d');
          $bulan = '';
        } 
        

        $data['evaluasi_monitoring'] = RequestDashboardEcatalog::GetEvalMon($groupDept,$tahun,$bulan,$category);
        // $data['penyerapan_dept'] = RequestDashboardEcatalog::PenyerapanDept($groupDept,$tahun,$bulan,$category); 
        // $data['pembelian_vendor'] = RequestDashboardEcatalog::GetPembelianVendor($groupDept, $tahun,$bulan,$category);
        // $data['top_product'] = RequestDashboardEcatalog::GetTop10Product($tahun,$bulan,$category);
        // $data['forecast'] = RequestDashboardEcatalog::GetForecast($bulan);
       return response()->json($data,200);
    }


    public function Emarket(Request $request){

       if($request->category_id !="")
        {
           $category =  $request->category_id;
        }else{
           $category = '';//default category
        } 

       $data['panel'] = RequestDashboardEmarket::TopPanelEmarket($category);
       $data['sumber_daya'] = RequestDashboardEcatalog::Category();
       $data['devision'] = RequestDashboardEcatalog::Devision();
       $data['project'] = RequestDashboardEcatalog::Project();
       $data['vendor'] = RequestDashboardEcatalog::Vendor();
       $data['product'] = RequestDashboardEcatalog::Products();
       $data['top_vendor'] = RequestDashboardEcatalog::TopVendor();
       return $data;

    }



    public function Boq(Request $request){

       if($request->category_id !="")
        {
           $category =  $request->category_id;
        }else{
           $category = '';//default category
        } 

        $data['panel'] = RequestDashboardBoq::GetBoqPanel($category);
        $data['devision'] = RequestDashboardEcatalog::Devision();
        $data['sumber_daya'] = RequestDashboardEcatalog::Category();
        $data['project'] = RequestDashboardEcatalog::Project();
        return $data;

    }


     public function Rfq(Request $request){

       if($request->category_id !="")
        {
           $category =  $request->category_id;
        }else{
           $category = '';//default category
        } 

        $data['panel'] = RequestDashboardRfq::GetRfqPanel($category);
        $data['devision'] = RequestDashboardEcatalog::Devision();
        $data['sumber_daya'] = RequestDashboardEcatalog::Category();
        $data['project'] = RequestDashboardEcatalog::Project();
        return $data;

    }
    


    


}    