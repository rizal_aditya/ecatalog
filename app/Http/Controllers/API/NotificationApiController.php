<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\GeneralPaginate;

use Illuminate\Support\Facades\Validator;

use App\Models\User;

use App\Http\Request\RequestNotif;
use Auth;
use DB;

class NotificationApiController extends Controller
{

   
    public function __construct()
    {  
       $this->perPage = GeneralPaginate::limit();
    }

    public function index(Request $request)
    {

        $notif = DB::table('notification as a')
                ->select('a.id','a.created_at','a.deskripsi','a.id_pengirim','a.id_penerima','b.photo')
                ->join('users as b','a.id_pengirim','=','b.id')  
                ->where(['a.id_penerima'=>Auth::User()->id])->OrderBy('a.id','DESC')->paginate($this->perPage);
        $description = "";        
        $_res = RequestNotif::GetDataPage($notif,$this->perPage,$request,$description);
        return response()->json($_res);
    }




    
   
    


}    