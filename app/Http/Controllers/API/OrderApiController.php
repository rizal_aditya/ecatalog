<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Wishlist;
use App\Helpers\GeneralPaginate;
use App\Http\Request\RequestOrder;
use App\Http\Request\RequestFrontend;
use Auth;
use DB;

class OrderApiController extends Controller
{

   
    public function __construct()
    {   
        $this->perPage = GeneralPaginate::limit();
       
        $this->UploadFolder = GeneralPaginate::uploadPhotoFolder();
        $this->UploadFileFolder = GeneralPaginate::uploadFileFolder();
    }

    
    
    
    public function transport($type,Request $request)
    {
       
       if($type =="revision")
       {
           $result = $this->transportRevision($request);
       }else if($type =="new"){
           $result = $this->transportNew($request);
       }else{
           $result = $this->transportAll($request);
           
       }
     return $result; 
    }


    public function transportAll(Request $request)
    {
        
        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_transportasi as a');
        $order->select('a.id','a.order_no as order_code','a.vendor_id','a.location_origin_id as origin_id','a.location_destination_id as destination_id','a.project_id','a.perihal','a.catatan','a.created_at','b.first_name','b.last_name','b.group_id','a.weight_minimum as weight_min','a.biaya_transport as fee_transport','c.location_name');
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>$id,'c.is_approve_complete'=>1]);
        $results = $order->paginate($this->perPage);
     
        $type ="transport";
        $description = '';
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);

    }

     public function transportNew(Request $request)
    {

        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_transportasi as a');
        $order->select('a.id','a.order_no as order_code','a.vendor_id','a.location_origin_id as origin_id','a.location_destination_id as destination_id','a.project_id','a.perihal','a.catatan','a.created_at','b.first_name','b.last_name','b.group_id','a.weight_minimum as weight_min','a.biaya_transport as fee_transport');
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where('c.order_status',2);  
        $order->where(['a.vendor_id'=>$id,'c.is_approve_complete'=>1]);
        $results = $order->paginate($this->perPage);
     
        $type ="transport";
        $description = '';
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);
    }

    public function transportRevision(Request $request)
    {
        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_transportasi as a');
        $order->select('a.id','a.order_no as order_code','a.vendor_id','a.location_origin_id as origin_id','a.location_destination_id as destination_id','a.project_id','a.perihal','a.catatan','a.created_at','b.first_name','b.last_name','b.group_id','a.weight_minimum as weight_min','a.biaya_transport as fee_transport');
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where('c.order_status',4);  
        $order->where(['a.vendor_id'=>$id,'c.is_approve_complete'=>1]);
        $results = $order->paginate($this->perPage);
     
        $type ="transport";
        $description = '';
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);

    }

    public function FilterTransport(Request $request){

        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $search = $request->search;
        $order = DB::table('order_transportasi as a');
        $order->select('a.id','a.order_no as order_code','a.vendor_id','a.location_origin_id as origin_id','a.location_destination_id as destination_id','a.project_id','a.perihal','a.catatan','a.created_at','b.first_name','b.last_name','b.group_id','a.weight_minimum as weight_min','a.biaya_transport as fee_transport');
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->join('project_new as d','a.project_id','=','d.id');
        $order->where(['a.vendor_id'=>$id,'c.is_approve_complete'=>1]);
        $order->where('d.name','LIKE',''.$search.'%');
        $order->orWhere('a.order_no','LIKE',''.$search.'%');
        $order->orWhere('d.alamat','LIKE',''.$search.'%');
        $results = $order->paginate($this->perPage);
     
        $type ="transport";
        $description = $search;
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);
    }

    public function OrderTransport($id,Request $request){
     $__temp_ = array();    
     $product = DB::table('order_transportasi_d as a')
                 ->select('c.biaya_transport','b.id','b.name as product_name','a.qty','a.price','a.weight','b.uom_id')
                 ->join('product as b','a.product_id','=','b.id')
                 ->join('order_transportasi as c','a.order_no','=','c.order_no')
                 ->where(['a.order_no'=>$id])
                 ->paginate(2);
     
      foreach ($product as $key => $val)
      {

           $__temp_[$key]['id'] = $val->id;
           $__temp_[$key]['photo'] = RequestFrontend::PhotoProduct($val->id);
           $__temp_[$key]['product_name'] = RequestFrontend::limitText($val->product_name,'50');
           $__temp_[$key]['qty'] = $val->qty;
           $__temp_[$key]['weight'] = $val->weight.' Kg';
           $__temp_[$key]['payment'] = RequestOrder::OrderPayment($id);
           $__temp_[$key]['price'] = RequestOrder::Rupiah($val->price);
           $__temp_[$key]['total_price'] = RequestOrder::Rupiah($val->qty * $val->weight * $val->price);
      }       
     
     if($product->nextPageUrl() !=null)
     {
         $param1 = explode('=',$product->nextPageUrl());
         $next = (int)$param1[1];
        
     }else{
         $next = null;
         
     } 


     if($product->previousPageUrl() !=null) {

         $param2 = explode('=',$product->previousPageUrl());
         $prev = (int)$param2[1];
     }else{
          $prev = null;
         
      } 
    

     $results['result'] = $__temp_;
     $results['total'] = $product->total();
     $results['lastPage'] = $product->lastPage();
     $results['currentPage'] = $prev;
     $results['nextPageUrl'] = $next;
     
     return $results;


   }

    public function insurance($type,Request $request)
    {
       if($type =="revision")
       {
           $result = $this->insuranceRevision($request);
       }else if($type =="new"){
           $result = $this->insuranceNew($request);
       }else{
           $result = $this->insuranceAll($request);
           
       }
     return $result; 
       

    }


    public function insuranceAll($request)
    {
       
        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_asuransi as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.nilai_harga_minimum as min_price');
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        
        $order->where(['a.vendor_id'=>442,'c.is_approve_complete'=>1]);
        $results = $order->paginate($this->perPage);

        $type ="insurance";
        $description = '';
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);

    }

    public function insuranceNew($request)
    {
       
        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_asuransi as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.nilai_harga_minimum as min_price');
    
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>442,'c.is_approve_complete'=>1]);
        $order->where('c.order_status',2);
        $results = $order->paginate($this->perPage);

        $type ="insurance";
        $description = '';
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);

    }

    public function insuranceRevision(Request $request)
    {
       
        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_asuransi as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.nilai_harga_minimum as min_price');
    
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>442,'c.is_approve_complete'=>1]);
        $order->where('c.order_status',4);
        $results = $order->paginate($this->perPage);

        $type ="insurance";
        $description = '';
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);

    }

    public function FilterInsurance(Request $request){

        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $search = $request->search;
        $order = DB::table('order_asuransi as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.nilai_harga_minimum as min_price');

        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->join('project_new as d','a.project_id','=','d.id');
        $order->where(['a.vendor_id'=>442,'c.is_approve_complete'=>1]);
        $order->where('d.name','LIKE',''.$search.'%');
        $order->orWhere('a.order_no','LIKE',''.$search.'%');
        $order->orWhere('d.alamat','LIKE',''.$search.'%');
        $results = $order->paginate($this->perPage);
     
        $type ="insurance";
        $description = $search;
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);
    }


    public function OrderInsurance($id,Request $request){
     $__temp_ = array();
     $product = DB::table('order_asuransi_d as a')
                 ->select('b.id','b.name as product_name','a.qty','a.price','a.weight','b.uom_id','c.nilai_asuransi as insurance_val')
                 ->join('product as b','a.product_id','=','b.id')
                 ->join('order_asuransi as c','a.order_no','=','c.order_no')
                 ->where(['a.order_no'=>$id])
                 ->paginate(2);
             

     
      foreach ($product as $key => $val)
      {

           $__temp_[$key]['id'] = $val->id;
           $__temp_[$key]['photo'] = RequestFrontend::PhotoProduct($val->id);
           $__temp_[$key]['product_name'] = RequestFrontend::limitText($val->product_name,'50');
           $__temp_[$key]['qty'] = $val->qty;
           $__temp_[$key]['weight'] = $val->weight.' Kg';
           $__temp_[$key]['payment'] = RequestOrder::OrderPayment($id);
           $__temp_[$key]['price'] = RequestOrder::Rupiah($val->price);
           $__temp_[$key]['total_price'] = RequestOrder::Rupiah($val->qty * $val->weight * $val->price);
      }       
     
     if($product->nextPageUrl() !=null)
     {
         $param1 = explode('=',$product->nextPageUrl());
         $next = (int)$param1[1];
        
     }else{
         $next = null;
         
     } 


     if($product->previousPageUrl() !=null) {

         $param2 = explode('=',$product->previousPageUrl());
         $prev = (int)$param2[1];
     }else{
          $prev = null;
         
      } 
    

     $results['result'] = $__temp_;
     $results['total'] = $product->total();
     $results['lastPage'] = $product->lastPage();
     $results['currentPage'] = $prev;
     $results['nextPageUrl'] = $next;
     
     return $results;


    }


    public function RevRequest(Request $request){

        $data = DB::table('log_cancel_po')->where('no_order',$request->order_no)->first();
         if($data ==null)
         {
             DB::table('log_cancel_po')->insert(['id_alasan'=>$request->level_id,'status_cancel'=>$request->status_id,'keterangan'=>$request->noted,'user_id'=>Auth::User()->id,'no_order'=>$request->order_no]);
              
              return response()->json(['status'=>true,'message'=>'Revisi berhasil dikirim ...'],200);
             
         }else{
              return response()->json(['status'=>false,'message'=>'Revisi sudah dalam proses preview ...'],400);   
         } 
    }


    public function product($type,Request $request)
    {
       

        if($type =="revision")
       {
           $result = $this->productRevision($request);
       }else if($type =="new"){
           $result = $this->productNew($request);
       }else{
           $result = $this->productAll($request);
           
       }
       return $result; 


    }


    public  function productAll(Request $request){


        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id;
        $order = DB::table('order as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.location_name');
        $order->join('users as b','a.created_by','=','b.id');
       // $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>$id,'a.is_approve_complete'=>1]);
        
        $results = $order->paginate($this->perPage);
        $type ="product";
        $description = '';
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);

    }


    public function productNew(Request $request){


        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.location_name');
        $order->join('users as b','a.created_by','=','b.id');
       // $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>$id,'a.is_approve_complete'=>1]);
        $order->where('a.order_status',2);
        $results = $order->paginate($this->perPage);

        $type ="product";
        $description = '';
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);

    }


    public function productRevision(Request $request){


        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.location_name');
        $order->join('users as b','a.created_by','=','b.id');
        //$order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>$id,'a.is_approve_complete'=>1]);
        $order->where('a.order_status',4);
        $results = $order->paginate($this->perPage);

        $type ="product";
        $description = '';
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);

    }

     public function detail($id,Request $request)
    {
        $_res = array();
        $auth = Auth::User();
        $vendor_id = $auth->vendor_id;
        $order = DB::table('order as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.location_name','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.pdf_name','a.is_approve_complete');
        $order->join('users as b','a.created_by','=','b.id');
        //$order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.order_no'=>$id]);
        $order->orderBy('a.id','DESC');
        $results = $order->first();
      
       
        $_res = RequestOrder::GetDataOrderDetail($results);
        return response()->json($_res);

    }
    

     public function FilterProduct(Request $request)
     {

        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $search = $request->search;


        $order = DB::table('order as a');
        $order->select('a.id','a.order_no as order_code','a.project_id','a.perihal','a.catatan','a.created_at','a.vendor_id','b.first_name','b.last_name','b.group_id','a.location_name');
        $order->join('users as b','a.created_by','=','b.id');
        //$order->join('order as c','a.order_no','=','c.order_no');    
        $order->join('project_new as d','a.project_id','=','d.id');
        $order->where(['a.vendor_id'=>$id]);
        $order->where('d.name','LIKE',''.$search.'%');
        $order->orWhere('a.order_no','LIKE',''.$search.'%');
        $order->orWhere('d.alamat','LIKE',''.$search.'%');
        $results = $order->paginate($this->perPage);


     
        $type ="product";
        $description = $search;
        $_res = RequestOrder::GetDataOrder($results,$this->perPage,$request,$description,$type);
        return response()->json($_res);
    }
    
    public static function OrderProduct($id,Request $request){
         
     $__temp_ = array();
     $product = DB::table('order_product as a')
                 ->select('b.id','b.name as product_name','a.qty','a.price','a.weight','b.uom_id')
                 ->join('product as b','a.product_id','=','b.id')
                 ->where(['a.order_no'=>$id])
                 ->paginate(2);

     
      foreach ($product as $key => $val)
      {
           $__temp_[$key]['id'] = $val->id;
           $__temp_[$key]['photo'] = RequestFrontend::PhotoProduct($val->id);
           $__temp_[$key]['product_name'] = RequestFrontend::limitText($val->product_name,'50');
           $__temp_[$key]['qty'] = $val->qty;
           $__temp_[$key]['weight'] = $val->weight.' Kg';
           $__temp_[$key]['payment'] = RequestOrder::OrderPayment($id);
           $__temp_[$key]['price'] = RequestOrder::Rupiah($val->price);
           $__temp_[$key]['total_price'] = RequestOrder::Rupiah($val->qty * $val->weight * $val->price);
      }       
     
     if($product->nextPageUrl() !=null)
     {
         $param1 = explode('=',$product->nextPageUrl());
         $next = (int)$param1[1];
        
     }else{
         $next = null;
         
     } 


     if($product->previousPageUrl() !=null) {

         $param2 = explode('=',$product->previousPageUrl());
         $prev = (int)$param2[1];
     }else{
          $prev = null;
         
      } 
    

     $results['result'] = $__temp_;
     $results['total'] = $product->total();
     $results['lastPage'] = $product->lastPage();
     $results['currentPage'] = $prev;
     $results['nextPageUrl'] = $next;
     
     return $results;

  
   }
    


    


}    