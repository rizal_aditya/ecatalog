<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Wishlist;
use App\Helpers\GeneralPaginate;
use App\Http\Request\RequestCart;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestAuth;
use App\Http\Request\RequestServer;


use App\Http\Request\Validation\ValidationOrder;

use Auth;
use DB;


class CartApiController extends Controller
{

   
    public function __construct()
    {   
        $this->perPage = GeneralPaginate::limit();
        
        $this->UploadFolder = GeneralPaginate::uploadPhotoFolder();
        $this->UploadFileFolder = GeneralPaginate::uploadFileFolder();
    }

    public function index(Request $request)
    {
         $result = array();
         $cart = RequestCart::QueryGetCart();

         $result['cart']  =  RequestCart::getCart($cart);
         return response()->json($result);

    }
  
    public function summary(Request $request)
    {
         $result = array();
         
         $result['product']  = RequestCart::GetProductSummary();
         $result['total_product']  = (int)count($result['product']);
         $result['total_price'] =  (int)RequestCart::GettotalPriceProduct();
         $result['total_volume']  =  RequestCart::GetTotalVolumeSummary();
         $result['total_qty']  =  (int)RequestCart::GetTotalQuantitySummary();
         $result['total_transport']  =  (int)RequestCart::GetTotalTransportSummary();
         $result['total_insurance']  =  (int)RequestCart::GetTotalInsuranceSummary();
         $result['total_premi']  =  RequestCart::GetTotalPremiSummary();
         $result['total_order']  =  $result['total_price'] +  $result['total_transport'] + $result['total_insurance'];
         return response()->json($result);

    } 
    


    public function store(Request $request)
    {
        
        

               //check vendor  jika beda 
               $CheckVendor = RequestCart::CheckVendor($request->vendor_id);
               if($CheckVendor ==null)
               {
              
                 $cart =  RequestCart::InsertCart($request->project_id,$request->pr_request_id,$request->vendor_id,$request->vendor_name,$request->location_id,$request->location_name,$request->type);
                  
                  $cart_id = $cart->id;
                  //check lokasi  jika beda 
                  
                  

               }else{  
                  $cart_id = $CheckVendor->id;

                  $CheckLocation = RequestCart::CheckLocation($request->location_id);
                  if($CheckLocation ==null)
                  {
                        $cart =  RequestCart::InsertCart($request->project_id,$request->pr_request_id,$request->vendor_id,$request->vendor_name,$request->location_id,$request->location_name,$request->type);


                       RequestCart::InsertProductCart($cart->id,$request->product_id,$request->photo,$request->product_name,$request->qty,$request->price,$request->weight,$request->payment_id,$request->payment_name,$request->product_uom_id,$request->product_uom_name);
                  }  
               } 



               $CheckProduct = RequestCart::CheckProduct($request->product_id,$request->payment_id);
               if($CheckProduct ==null)
               {

                $qty = $request->qty;
                RequestCart::InsertProductCart($cart_id,$request->product_id,$request->photo,$request->product_name,$qty,$request->price,$request->weight,$request->payment_id,$request->payment_name,$request->product_uom_id,$request->product_uom_name);
               }else{
                 
                 if($CheckProduct->payment_id == $request->payment_id)
                 {
                    $qty = $CheckProduct->qty + 1;
                    RequestCart::UpdateProductCart($cart_id,$request->product_id,$qty,$request->payment_id);
                 }else{
                    $qty = $request->qty;
                    RequestCart::InsertProductCart($cart_id,$request->product_id,$request->photo,$request->product_name,$qty,$request->price,$request->weight,$request->payment_id,$request->payment_name,$request->product_uom_id,$request->product_uom_name);  
                 }   
             

               } 
                 
                $countCart = Cart::where(['created_by'=>Auth::User()->id,'reading'=>'false'])->count();
                $notif = array('to'=>Auth::User()->id,'count'=>$countCart);
                return response()->json(['status'=>true,'count'=>$notif,'message'=>'Sukses produk dipindahkan ke keranjang'],200); 
                          
       
    }

    
    public function ForwordToCart($order_code)
    {

        //check Approv
        $order = RequestCart::GetOrderID($order_code);
        $orderProduct = RequestCart::GetOrderProduct($order_code);

     
        $orderTransport = RequestCart::GetOrderTransport($order_code);
        $orderInsurance = RequestCart::GetOrderInsurance($order_code);
        $photoProduct = RequestFrontend::PhotoProduct($orderProduct->product_id);
        $type_product =  RequestFrontend::TypeProduct($orderProduct->product_id,'slug');
        $product_uom_name =RequestFrontend::SatuanProduct($orderProduct->product_uom_id);



        $insertCart = array('checklist'=>true,'order_no'=>$order_code,'vendor_id'=>$order->vendor_id,'vendor_name'=>$order->vendor_name,'pr_request_id'=>$order->pr_request_id,'location_id'=>$order->location_id,'location_name'=>$order->location_name,'type'=>$type_product,'created_by'=>$order->created_by,'project_id'=>$order->project_id,'destination_id'=>$order->destination_id,'project_address'=>$order->project_address,'shipping_id'=>$order->shipping_id,'catatan_produk'=>$order->catatan_produk,'perihal_product'=>$order->perihal_product,'tgl_ambil'=>$order->tgl_diambil,'action'=>'rev','reading'=>'false');
        $merge = array_merge($insertCart,$orderTransport,$orderInsurance);
        $cart = Cart::Create($merge);

        $insertProduct = array('cart_id'=>$cart->id,'product_id'=>$orderProduct->product_id,'product_name'=>$orderProduct->product_name,'price'=>$orderProduct->price,'volume'=>$orderProduct->weight * $orderProduct->qty,'weight'=>$orderProduct->weight,'qty'=>$orderProduct->qty,'photo'=>$photoProduct,'product_uom_id'=>$orderProduct->product_uom_id,'payment_id'=>$orderProduct->payment_id,'payment_name'=>$orderProduct->payment_name,'created_by'=>$order->created_by,'product_uom_name'=>$product_uom_name);

        DB::table('cart_product')->insert($insertProduct);
         
        //deleted
        if($order->pdf_name)
        {
          unlink('file/order/'.$order->pdf_name);  
        }    
        
         
        DB::table('order')->where('order_no',$order_code)->delete();
        DB::table('order_product')->where('order_no',$order_code)->delete();
        
        if($orderTransport['transport'] == 'true')
        {
           DB::table('order_transportasi')->where('order_no',$order_code)->delete(); 
           DB::table('order_transportasi_d')->where('order_no',$order_code)->delete(); 
        } 

        if($orderInsurance['insurance'] == 'true')
        {
           DB::table('order_asuransi')->where('order_no',$order_code)->delete(); 
           DB::table('order_asuransi_d')->where('order_no',$order_code)->delete();
        }    
        

       $countCart = Cart::where(['created_by'=>$order->created_by,'reading'=>'false'])->count();
       $notif = array('to'=>Auth::User()->id,'count'=>$countCart);
       return response()->json(['status'=>true,'count'=>$notif,'message'=>'Order Berhasil Dipindahkan'],200); 

         
 
    }
    
    public function countChat(){

      $recipient = DB::table('cart')->where(['created_by'=>Auth::User()->id,'reading'=>'false'])->first();
      DB::table('cart')->where(['created_by'=>Auth::User()->id,'reading'=>'false'])->update(['reading'=>'true']);
      $reading = DB::table('cart')->where(['created_by'=>Auth::User()->id,'reading'=>'false'])->count();
     return array('status'=>true,'count'=>$reading,'messages'=>'berhasil update notif chat');

    }

    public function UpdateQuantity(Request $request)
    {
         $data = DB::table('cart_product')->where(['id'=>$request->id])->first();
         // if($request->qty > 100000)
         // {
         //     return response()->json(['status'=>false,'active'=>true,'message'=>'Jumlah quantity maksimal 100000'],400); 
         // }else{
            $volume = (float)$data->weight * (int)$request->qty;
            $subtotal = (int)$data->price  * (int)$request->qty  *  (float)$data->weight ;
            DB::table('cart_product')->where(['id'=>$request->id])
            ->update(['qty'=>$request->qty,'volume'=>$volume]);
            
            $total_volume = RequestCart::TotalVolume($data->cart_id); 
            $total_price = RequestCart::TotalPrice($data->cart_id); 
            return response()->json(['status'=>true,'active'=>false,'total_volume'=>$total_volume,'subtotal'=>$subtotal,'total_price'=>$total_price,'message'=>''],200);  
         //}
        
        
        
    }

    public function DeleteProduct(Request $request){

        $jml_product = DB::table('cart_product')->where(['cart_id'=>$request->cart_id])->count();
        if($jml_product <2)
        {
           //destroy vendor
           DB::table('cart')->where('id',$request->cart_id)->delete();    
        }else{
           DB::table('cart_product')->where(['id'=>$request->id])->delete();  
        }
     
    }

    public function DeleteVendor(Request $request){
        
          
        $integerIDs = array_map('intval', explode(',', $request->data)); 
        $data = DB::table('cart')->whereIn('id',$integerIDs)->get();
        foreach($data as $key =>$val)
        {
           $del = DB::table('cart')->where('checklist',true)->delete();
        }    
       
    } 
    

   

    public function UpdateData(Request $request){


         if($request->type =="checklist"){

             DB::table('cart')->where('id',$request->id)->update(['checklist'=>$request->checklist]); 


         }else if($request->type =="project_id"){
            $data = json_decode($request->data);
            $req1 = json_encode($data);
            $req2 = json_decode($req1);
            DB::table('cart')->where('id',$request->id)->update(['project_id'=>$req2->project_id,'project_address'=>$req2->address,'destination_id'=>$req2->destination_id,'vendor_transport_id'=>0,'transport_id'=>0,'transport_fee'=>'']); 

            return response()->json(['status'=>true,'vendor_transport_id'=>0,'transport_id'=>0,'transport_fee'=>0],200);

         }else if($request->type =="up_transport"){
            // if($request->data ==""){$status ='false';}else{ $status = 'true'; } 
            $status ='false';
            DB::table('cart')->where('id',$request->id)->update(['up_transport'=>$request->data,'status'=>$status]); 
         
         }else if($request->type =="perihal_transport"){
            // if($request->data ==""){$status ='false';}else{ $status = 'true'; } 
            $status ='false';
            DB::table('cart')->where('id',$request->id)->update(['perihal_transport'=>$request->data,'status'=>$status]); 
        }else if($request->type =="catatan_transport") {
            // if($request->data ==""){$status = 'false';}else{ $status = 'true'; }   
            $status ='false'; 
            DB::table('cart')->where('id',$request->id)->update(['catatan_transport'=>$request->data,'status'=>$status]);                   

         }else if($request->type =="up_product"){
            // if($request->data ==""){$status ='false';}else{ $status = 'true'; } 
            $status ='false';
            DB::table('cart')->where('id',$request->id)->update(['up_product'=>$request->data,'status'=>$status]); 
                  

         }else if($request->type =="catatan_product") {
            // if($request->data ==""){$status = 'false';}else{ $status = 'true'; }   
            $status ='false'; 
            DB::table('cart')->where('id',$request->id)->update(['catatan_product'=>$request->data,'status'=>$status]); 
          
         }else if($request->type =="perihal_product"){
            // if($request->data ==""){$status ='false';}else{ $status = 'true'; } 
            $status ='false';
            DB::table('cart')->where('id',$request->id)->update(['perihal_product'=>$request->data,'status'=>$status]); 
         }else if($request->type =="tgl_ambil"){
           // if($request->data ==""){$status ='false';}else{ $status = 'true'; } 
            $status ='false';
            DB::table('cart')->where('id',$request->id)->update(['tgl_ambil'=>$request->data,'status'=>$status]);
         }else if($request->type =="shipping"){
            if($request->data ==2)
            {
                DB::table('cart')->where('id',$request->id)->update(['shipping_id'=>$request->data,'transport'=>'true','insurance'=>'true']);
            }else{
               DB::table('cart')->where('id',$request->id)->update(['shipping_id'=>$request->data,'transport'=>'false','insurance'=>'false']);
               
                 RequestCart::TransportFalse($request->id,'false');
                 RequestCart::InsuranceFalse($request->id,'false');
            } 

             $result = DB::table('cart')->where('id',$request->id)->first();
             return response()->json(['status'=>true,'result'=>$result,'message'=>'Berhasil update'],200);
         

         }else if($request->type =="transport"){ 
            if($request->data == 'true')
            {
                $status = 'true';
                 DB::table('cart')->where('id',$request->id)->update(['transport'=>$status]);
            }else{
                $status = 'false';
                RequestCart::TransportFalse($request->id,$status);
                 
            }     

             return response()->json(['status'=>true,'transport'=>$status],200);

        }else if($request->type =="up_insurance"){
            // if($request->data ==""){$status ='false';}else{ $status = 'true'; } 
            $status ='false';
            DB::table('cart')->where('id',$request->id)->update(['up_insurance'=>$request->data,'status'=>$status]); 
        
        }else if($request->type =="perihal_insurance"){
            // if($request->data ==""){$status ='false';}else{ $status = 'true'; } 
            $status ='false';
            DB::table('cart')->where('id',$request->id)->update(['perihal_insurance'=>$request->data,'status'=>$status]); 
        
        }else if($request->type =="catatan_insurance") {
            // if($request->data ==""){$status = 'false';}else{ $status = 'true'; }   
            $status ='false'; 
            DB::table('cart')->where('id',$request->id)->update(['catatan_insurance'=>$request->data,'status'=>$status]);         
            
         }else if($request->type =="vendor_transport"){ 
            DB::table('cart')->where('id',$request->id)->update(['vendor_transport_id'=>$request->data]); 
         }else if($request->type =="transport_id"){ 
            $data = json_decode($request->data);
            $req1 = json_encode($data);
            $req2 = json_decode($req1);
           
            DB::table('cart')->where('id',$request->id)->update(['transport_id'=>$req2->transport_id,'min_weight_transport'=>$req2->min_weight_transport]); 
            //return response()->json(['status'=>true,'min_weight_transport'=>$req2->min_weight_transport],200);
            
         }else if($request->type =="transport_fee"){
            $data = json_decode($request->data);
            $req1 = json_encode($data);
            $req2 = json_decode($req1); 

          

            if($req2->total_volume < $req2->min_weight_transport)
            {
                $total_transport = $req2->min_weight_transport * $req2->transport_fee;
            }else{
                $total_transport = $req2->total_volume * $req2->transport_fee;
            }


            DB::table('cart')->where('id',$request->id)->update(['transport_fee'=>$req2->transport_fee,'transport_name'=>$req2->transport_name,'total_transport'=>$total_transport]);

            return response()->json(['status'=>true,'transport_fee'=>$req2->transport_fee,'total_transport'=>$total_transport],200);


         }else if($request->type =="insurance"){
            if($request->data == 'true')
            {
                $status = 'true';
                $list = RequestCart::getInsurance();
                DB::table('cart')->where('id',$request->id)->update(['insurance'=>$status]); 
                

            }else{
                $list = [];
                $status = 'false';
                RequestCart::InsuranceFalse($request->id,$status);
            } 
             
            
             return response()->json(['status'=>true,'insurance'=>$status,'result'=>$list],200);
             

          }else if($request->type =="insurance_id"){
            $data = json_decode($request->data);
            $req1 = json_encode($data);
            $req2 = json_decode($req1);

            $total = RequestCart::getTotalInsurance($request->id,$req2->insurance_min_price,$req2->premi);

            DB::table('cart')->where('id',$request->id)->update(['insurance_id'=>$req2->insurance_id,'insurance_vendor_id'=>$req2->insurance_vendor_id,'insurance_name'=>$req2->insurance_name,'insurance_min_price'=>$req2->insurance_min_price,'premi'=>$req2->premi,'total_insurance'=>$total]);  

         
            return response()->json(['status'=>true,'premi'=>$req2->premi,'total_insurance'=>$total],200); 

          }

    }

  


    public function vendorTransport(Request $request)
    {
        $_res = RequestCart::getVendorTransport($request);         
        return response()->json($_res);          
    } 


     public function vendorPrice(Request $request)
    {
        
        $_res = RequestCart::getTransportFee($request);         
        return response()->json($_res);     
    }  



    public function insertOrder(Request $request)
    { 
        
        $integerIDs = array_map('intval', explode(',', $request->product_id)); 
        $product =  json_decode($request->product);  

        // return response()->json($product,200);
        // exit();

        // Filter data berdasarkan id 
        $product_check = array_filter($product, function ($item) use ($integerIDs) {
            return in_array($item->id, $integerIDs);
        });
        // return response()->json($product_check,200);
        // exit();

        $res = array();
        $res = ValidationOrder::validation($product,$product_check); 
        $checkForm = ValidationOrder::checkForm($res['message']); 
                  
        if($checkForm->status == false)
        {  
            if($checkForm->validation == true)
            {
                $result = array($res);
                return response()->json($result,400);  
            }   
             
        }else{

             //check budget
            $token = RequestServer::GetToken();
            $json = RequestServer::ConvertJson($token,'Prod'); 
            $dataSDA = RequestCart::ConvertFormatBudget($product_check);
            $checkBudget = RequestServer::CekBudget($json->token,$json->cookies,$dataSDA);

           // return response()->json($checkBudget);
                      
            if($checkBudget->status == true)
            {
                
               //check Action
                $checkAction = RequestCart::CheckAction();
                if($checkAction ==true)
                {          
               
                  $result = RequestCart::CheckingOrder($product_check); 
                  if($result->status == true)
                  {
                     return response()->json($result,200);
                  }else{
                     return response()->json($result,400);
                  }  
                  
                }else{
                    return response()->json(['status'=>false,'type'=>'revisi','message'=>'Tidak bisa order, Terdapat Order PO Revisi'],400);
                }  

            }else{
                return response()->json(['status'=>false,'type'=>'server','message'=>'Gagal checking budget, budget produk yang di izinkan adalah '.RequestFrontend::Rupiah($checkBudget->budget)],400);
            }       
        }    
      
       
        
    
         

    }





   

   

    


}    