<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\GeneralPaginate;

use App\Models\User;
use App\Models\RoleUser;
use App\Models\Vendor;


use App\Http\Request\RequestDashboard;
use App\Http\Request\RequestOrderProduk;
use Auth;
use File;


class DashboardApiController extends Controller
{

   
    public function __construct(){
          
         
           
    }


    public function index(){
         
         $result = array();
         $result['store'] = RequestDashboard::getStore();
         $result['order']  = array('all'=>RequestOrderProduk::TotalAll(),'new'=>RequestOrderProduk::TotalNew(),'revision'=>RequestOrderProduk::TotalRev());
         $result['chart']  = [] ;
           
         return response()->json($result);

    }

    

 


}    