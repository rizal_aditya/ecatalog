<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\RoomChat;
use App\Models\Wishlist;
use App\Helpers\GeneralPaginate;
use App\Helpers\GeneralEncrypt;
use App\Http\Request\RequestFrontend;
use App\Http\Request\Search\SearchFrontend;
use App\Http\Request\RequestChat;
use Auth;
use DB;
use Illuminate\Support\Facades\Cache;
use Exception;

class FrontendApiController extends Controller
{

   
    public function __construct()
    {   
        $this->perPage = GeneralPaginate::limit();
       
    }

    public function index(Request $request)
    {
        

        $param = RequestFrontend::ParamPage($request->param);
        $cache = RequestFrontend::CreateCache($request,$param);
        
        if($request->pr_request_id)
        {
            $query = RequestFrontend::ProductsWithPR();
        }else{
            $query = RequestFrontend::ProductWithoutPR();
        }    
    
          
        try {

    
            // if($cache->data)
            // {
            //     $checkTotal = RequestFrontend::CheckTotalCount($query,$param,$request);
            //     if($cache->count == $checkTotal)
            //     {  
            //         // Jika data ditemukan di cache, kembalikan data tersebut
            //         return response()->json($cache->data);
            //     }else{

            //         $result = RequestFrontend::RealtimeQuery($query,$param,$request,$this->perPage);
            //         return response()->json($result);
            //     } 
            // } 

           
            // $result = RequestFrontend::RealtimeQuery($query,$param,$request,$this->perPage);
            // return response()->json($result); 
            
              $result = RequestFrontend::RealtimeQuery($query,$param,$request,$this->perPage);
              
           // $result = $query->paginate($this->perPage);
            return response()->json($result);

        } catch (Exception $e) {

            return response()->json(['error' => $e->getMessage()], 500);
        }

    }

    
    public function ListRequestPR(Request $request)
    {
         
        $query = RequestFrontend::QueryListPRRequest();
        if($request->product_id)
        {
             $query->join('product as c','b.kode_sda','=','c.code_1');
             $query->where(['c.id' => $request->product_id]); 
        }    

        // $query->where(['i.status' => 1]); // Specific filters for getlist
        $data = $query->get();
        $result = RequestFrontend::GetDataRequestPR($data);
        return response()->json($result);
      
    }    
    


    public function preview(Request $request)
    {
       
        $_res = array();
        $id = $request->id;
        
        if($request->pr_request_id)
        {
            $query = RequestFrontend::ProductsWithPR();
        }else{
            $query = RequestFrontend::ProductWithoutPR();
        }  
       
         
        if($request->pr_request_id)
        {
            $query->where('a.id', $request->pr_request_id);//pr request
        } 

        $query->where(['c.id'=>$id,'c.is_deleted' => 0]);
        // $baseQuery = clone $query; // Cloning for separate handling

        $product  =  $query->first();
        $result['product'] = RequestFrontend::GetPreview($product);
       
                
        return response()->json($result);

    }


    public function previewPrice(Request $request)
    {
       
        $_res = array();
        $id = $request->id;

        $product = DB::table('payment_product as a')
                   ->select(DB::raw('IFNULL(b.price,a.price) as price_real'),DB::raw('IFNULL(b.location_id,a.location_id) as location_id_real') ) 

                         // ->leftJoin('project_product_price as b', function($join)
                         // {
                         //     $join->on('a.product_id','=', 'b.product_id');
                         //     $join->on('a.payment_id','=', 'b.payment_id');
                         //     $join->on('b.is_deleted','=',DB::raw("0"));
                         // })

               
                     ->leftJoin('project_product_price as b', function($join) {
                        $join->on('a.product_id', '=', 'b.product_id');
                        $join->on('a.payment_id','=', 'b.payment_id');
                        $join->where('b.price', '>', 100);
                        $join->where('b.is_deleted', '=', 0);
                        $join->whereRaw('b.amandemen_id IN (select MAX(a2.amandemen_id) from project_product_price as a2 where a2.product_id=a.product_id)');
                       
                    })
                   
                   ->where(['a.payment_id'=>$request->payment_id,'a.product_id'=>$request->product_id])
                   ->OrderBy('b.amandemen_id','DESC')->first();
        $_res = RequestFrontend::GetPaymentPrice($product,$request->uom_id);
        return response()->json($_res);

    }

        


    public function wishlist(Request $request)
    {
        $_res = array();
        $product = DB::table('wishlist as a')
         ->select('b.id','b.name','b.vendor_id','b.created_at','b.verified_status','b.code_1','b.uom_id')
         ->join('product as b','a.product_id','=','b.id')
         ->where(['a.user_id'=>Auth::User()->id])
         ->orderBy('a.product_id', 'ASC')->paginate($this->perPage);
       
        $title = '';
        $description = '';
        $type = "page";
        $_res = RequestFrontend::GetDataPage($product,$this->perPage,$request,$title,$description,$type);
        return response()->json($_res);
    }

    public function SearchWishlist(Request $request){
      
       $product = DB::table('wishlist as a')
        ->select('b.id','b.name','b.vendor_id','b.created_at','b.verified_status','b.code_1','b.uom_id')
         ->join('product as b','a.product_id','=','b.id')
         ->where('name','LIKE',''.$request->name.'%')
        ->orderBy('a.product_id', 'ASC')
        ->paginate($this->perPage);
            

        $title = '';
        $description = '<b>'.$request->name.'</b>';
        $type = "page";
        $_res = RequestFrontend::GetDataPage($product,$this->perPage,$request,$title,$description,$type);
           
        return response()->json($_res);

    }

    public function saveWishlist(Request $request)
    {
         if($request->status == 'true'){   $status = true;  }else{ $status = false;  }
         $product = DB::table('product')->where('id',$request->product_id)->first()->name;
         $product_id = $request->product_id;
         $vendor_id = $request->vendor_id;
         $user_id = Auth::User()->id;

         $check = Wishlist::where(['product_id'=>$product_id,'user_id'=>$user_id])->first();
         if($check !=null)
         {

           if($status == false)
           {
             
              Wishlist::where(['product_id'=>$request->product_id,'user_id'=>$user_id])->delete();
             return response()->json(['status'=>true,'wishlist'=>$status,'messages'=>''.$product.' berhasil dihapus dari list favorit anda'],200);

           }else{
              Wishlist::create(['product_id'=>$request->product_id,'user_id'=>$user_id,'vendor_id'=>$vendor_id]); 
             return response()->json(['status'=>true,'wishlist'=>$status,'messages'=>''.$product.' berhasil disimpan ke dalam list favorit anda'],200);
           } 
          
         }else{
           Wishlist::create(['product_id'=>$request->product_id,'user_id'=>$user_id,'vendor_id'=>$vendor_id]);  
           return response()->json(['status'=>true,'wishlist'=>$status,'messages'=>''.$product.' berhasil disimpan ke dalam list favorit anda'],200);
         }   
        

             
            
         
    }


    
    public function openChat(Request $request)
    {

         $send_to = DB::table('users')->where('vendor_id',$request->vendor_id)->first();
         $check = RequestChat::CheckOpenChat($send_to->id);
       
         if($check == false)
         {
           $room = RoomChat::Create(['from'=>Auth::User()->id,'to'=>$send_to->id,'active'=>'true','created_at'=>date('Y-m-d h:i:sa')]);

          

         }else{
           $room = RoomChat::where(['from'=>Auth::User()->id,'to'=>$send_to->id])->first();  
             DB::table('room_chat')->where(['from'=>Auth::User()->id,'to'=>$send_to->id])->update(['active'=>'true','updated_at'=>date('Y-m-d h:i:sa')]);
         }   

         $message = DB::table('room_chat_detail')->insert(['room_id'=>$room->id,'from'=>Auth::User()->id,'to'=>$send_to->id,'textmessages'=> $request->messages,'reading'=>'false','type'=>'json','created_at'=>date('Y-m-d h:i:sa')]);
        
          return response()->json(['status'=>true,'result'=>$room->id,'messages'=>'Open Chat with vendor '.$send_to->first_name.' '.$send_to->last_name],200);

    


    }

     public function ads(Request $request){

            $query  = DB::table('list_ads')->where('status','active')->get();
           $_res = RequestFrontend::GetDataSlider($query);

          return response()->json(array("status"=>true,"results"=>$_res,"message"=>"Data Ads"),200);  
    }


    



}    