<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Wishlist;
use App\Helpers\GeneralPaginate;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestProduct;
use App\Http\Request\Validation\ValidationProduct;
use Auth;
use DB;

class ProductApiController extends Controller
{

   
    public function __construct()
    {   
        $this->perPage = GeneralPaginate::limit();
        $this->UploadFolder = GeneralPaginate::uploadPhotoFolder();
        $this->UploadFileFolder = GeneralPaginate::uploadFileFolder();
    }

    public function index(Request $request)
    {
       
        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $product = DB::table('product as a')
                  ->where(['vendor_id'=>$id,'a.is_deleted'=>0])
                   ->select('a.id','a.name','a.vendor_id','a.created_at','a.verified_status','a.code_1','a.uom_id')  
                    ->orderBy('id', 'Desc')
                    ->paginate($this->perPage);
        $description = '';
        $title = '';
        $type = 'page';
        $_res = RequestFrontend::GetDataPage($product,$this->perPage,$request,$title,$description,$type);
        return response()->json($_res);

    }

    public function report(Request $request)
    {
       
        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $product = DB::table('viewer as b')
                  ->leftJoin('product as a','a.id','=','b.product_id')
                  ->where(['a.vendor_id'=>$id,'a.is_deleted'=>0])
                   ->select('a.id','a.name','a.vendor_id','a.created_at','a.verified_status','a.code_1','a.uom_id')  
                    ->groupBy('b.product_id')
                    ->paginate($this->perPage);
        $description = '';
        $title = '';
        $type = 'page';
        $_res = RequestFrontend::GetDataPage($product,$this->perPage,$request,$title,$description,$type);
        return response()->json($_res);

    }



    public function archive(Request $request)
    {
       
        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $product = DB::table('product as a')
                  ->where(['vendor_id'=>$id,'a.is_deleted'=>1])
                   ->select('a.id','a.name','a.vendor_id','a.created_at','a.verified_status','a.code_1','a.uom_id')  
                    ->orderBy('id', 'Desc')
                    ->paginate($this->perPage);
        $description = '';
        $title = '';
        $type = 'page';
        $_res = RequestFrontend::GetDataPage($product,$this->perPage,$request,$title,$description,$type);
        return response()->json($_res);

    }

     public function search(Request $request)
    {
       
        $_res = array();
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $product = DB::table('product as a')
                  ->where(['vendor_id'=>$id])
                  ->where('a.name','LIKE','%'.$request->product_name.'%')
                   ->select('a.id','a.name','a.vendor_id','a.created_at','a.verified_status','a.code_1','a.uom_id')  
                    ->orderBy('id', 'Desc')
                    ->paginate($this->perPage);
        $description = $request->product_name;
        $title = '';
        $type = 'page';
        $_res = RequestFrontend::GetDataPage($product,$this->perPage,$request,$title,$description,$type);
        return response()->json($_res);

    }


    public function create(Request $request)
    {
         $result = array();
         $result['category1'] = RequestProduct::getCategory1();
         $result['category2']  = [];
         $result['category3']  =  [];
         $result['category4']  =  [];
         $result['category5']  =  [];
         $result['category6']  =  [];
         $result['uom_list']  =  RequestProduct::getBeratUnit();
         $result['tod']  =  RequestProduct::getTOD();
         $result['payment']  =  RequestProduct::getPayment();
         $result['location']  =  RequestProduct::getLocation();
         return response()->json($result);

    }

    public function store(Request $request)
    {
        $result = array();    
        $validation = ValidationProduct::validation($request);
        if($validation !=null || $validation !="")
        {
          return response()->json($validation,400);  
        }else{

            $level1 = $request->category_id1;
            $level2 = $request->category_id2;
            $level3 = $request->category_id3;
            $level4 = $request->category_id4;
            $level5 = $request->category_id5;
            $level6 = $request->category_id6;

            if (!empty($level2) && empty($level3)){
                $kode_sda = $level2;
            }else if (!empty($level3) && empty($level4)){
                $kode_sda = $level3;
            }else if (!empty($level4) && empty($level5)){
                $kode_sda = $level4;
            }else if (!empty($level5) && empty($level6)){
                $kode_sda = $level5;
            }else if (!empty($level6)){
                $kode_sda = $level6;
            }else{
                $kode_sda = $level1;
            }

            if(empty($_FILES['file_pdf']))
            {
                $filename = null;
            }else{
                $type = explode('/',$_FILES['file_pdf']['type']);
                $extentions = $type[1];
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->name)));
                $filename = time() . '-' . $slug.'.'.$extentions;
                $filePath = public_path() . '/file/product/brosur/';
                $terupload = move_uploaded_file($_FILES['file_pdf']['tmp_name'], $filePath.$filename);
            }    

           

            $product = Product::create([
                'code_1'=>$kode_sda,
                'name'=>$request->name,
                'attachment'=>$filename,
                'vendor_id'=>Auth::User()->vendor_id,
                'uom_id'=>$request->uom_id,
                'berat_unit'=>$request->weight,
                'uom_id'=>$request->uom_id,
                'width'=>$request->width,
                'height'=>$request->height,
                'tod_id'=>$request->tod_id,
                'note'=>$request->description,
                'tgl_harga_valid'=>$request->date_price,
                
            ]);
            
            $photo_list = json_decode($request->photo);
            $photo1 = json_encode($photo_list);
            $photo2 = json_decode($photo1);

            $res = array();
            foreach($photo2 as $key =>$val)
            {
               
                $source = explode(";base64,", $val->photo);
                $extFile = explode("image/", $source[0]);
                $extentions = $extFile[1];
                $image = base64_decode($source[1]);
                $filePath = public_path() . '/images/product/';
                $filename = time() . '-' . $val->id.'.'.$extentions;
                $success = file_put_contents($filePath.$filename, $image);
                DB::table('product_gallery')->insert(['product_id'=>$product->id,'filename'=>$filename]);  
            }  

            $payment_list = json_decode($request->payment);
            $payment1 = json_encode($payment_list);
            $payment = json_decode($payment1);
            $rey = array();
            $pay = array(); 
            foreach($payment as $key =>$val)
            {

                $rey[$key]['payment_id'] = $val->payment_id;
                foreach($val->location_id as $key1 => $value)
                {
                    $pay[$key1] = $value->value;
                }
                $rey[$key]['location_id'] = implode(',', $pay);
                $rey[$key]['price'] = $val->price;
                $rey[$key]['notes'] = $val->noted;
                $rey[$key]['product_id'] = $product->id;
                $rey[$key]['product_id'] = $product->id;
                $rey[$key]['json_location'] = Null;

                DB::table('payment_product')->insert($rey); 
                
            }


           $result['status'] = true;
           $result['messages'] = "Berhasil menambahkan produk";
         
            return json_encode($result);
        }

    } 

     public function edit($id,Request $request)
    {
         $result = array();
         $checkId = RequestProduct::CheckProductID($id);
         if($checkId ==FALSE)
         {
             return response()->json(['status'=>false,'message'=>'id produk tidak valid']);
         }  
        

         $result['photo'] = RequestProduct::getphoto($id);
         $result['payment_product'] = RequestProduct::getpaymentProduct($id);
         $result['product'] = RequestProduct::getProduct($id);

         
         $checkLevel = RequestProduct::CheckLevel($result['product']->code_1);
         
           if($checkLevel ==6){
               $result['category_id6']  =  RequestProduct::CategoryCode($result['product']->code_1,'one');
               $result['category_id5']  =  RequestProduct::CategoryCode($result['product']->code_1,'two');
               $result['category_id4']  =  RequestProduct::CategoryCode($result['product']->code_1,'two');
               $result['category_id3']  =  RequestProduct::CategoryCode($result['product']->code_1,'two');
               $result['category_id2']  =  RequestProduct::CategoryCode($result['category_id3'],'two');
               $result['category_id1']  =  RequestProduct::CategoryCode($result['category_id2'],'two'); 
              
          }  

          if($checkLevel ==5){
               $result['category_id5']  =  RequestProduct::CategoryCode($result['product']->code_1,'one');
               $result['category_id4']  =  RequestProduct::CategoryCode($result['product']->code_1,'two');
               $result['category_id3']  =  RequestProduct::CategoryCode($result['product']->code_1,'two');
               $result['category_id2']  =  RequestProduct::CategoryCode($result['category_id3'],'two');
               $result['category_id1']  =  RequestProduct::CategoryCode($result['category_id2'],'two'); 
              
          }  


          if($checkLevel ==4){
               $result['category_id4']  =  RequestProduct::CategoryCode($result['product']->code_1,'one');
               $result['category_id3']  =  RequestProduct::CategoryCode($result['product']->code_1,'two');
               $result['category_id2']  =  RequestProduct::CategoryCode($result['category_id3'],'two');
               $result['category_id1']  =  RequestProduct::CategoryCode($result['category_id2'],'two'); 
              
          }

            if($checkLevel ==3){

               $result['category_id3']  =  RequestProduct::CategoryCode($result['product']->code_1,'one');
               $result['category_id2']  =  RequestProduct::CategoryCode($result['category_id3'],'two');
               $result['category_id1']  =  RequestProduct::CategoryCode($result['category_id2'],'two'); 
              
          }

            if($checkLevel ==2){

               $result['category_id2']  =  RequestProduct::CategoryCode($result['category_id3'],'one');
               $result['category_id1']  =  RequestProduct::CategoryCode($result['category_id2'],'two'); 
              
          }

            if($checkLevel ==1){
           
               $result['category_id1']  =  RequestProduct::CategoryCode($result['category_id2'],'one'); 
              
          }

          
         
        
        
        
         $result['category1'] = RequestProduct::getCategory1();
         if(!empty($result['category_id1']))
         {
            $result['category2']  = RequestProduct::getCategory2($result['category_id1']);
         }else{
            $result['category2']  = [];
         }

         if(!empty($result['category_id2']))
         {
             $result['category3']  =  RequestProduct::getCategory3($result['category_id2']);
         }else{
            $result['category3']  = [];
         }   

         if(!empty($result['category_id3']))
         {
             $result['category4']  =  RequestProduct::getCategory4($result['category_id3']);

         }else{
            $result['category4']  = [];
         } 
         
         if(!empty($result['category_id4']))
         {
            $result['category5']  =  RequestProduct::getCategory5($result['category_id4']);
             $result['weight'] = RequestProduct::GetWeight($result['category_id4']); 
             $result['weight_id'] = $result['product']->weight;
         }else{
            $result['category5']  = [];
         }  
        
         if(!empty($result['category_id5']))
         {
            $result['category6']  =  RequestProduct::getCategory6($result['category_id5']);

           
         }else{
            $result['category6']  = [];
         } 
         
        
         $result['uom_list']  =  RequestProduct::getBeratUnit();
         $result['tod']  =  RequestProduct::getTOD();
         $result['payment']  =  RequestProduct::getPayment();
         $result['location']  =  RequestProduct::getLocation();
         return response()->json($result);

    } 


     public function update($id,Request $request)
    {
        $result = array();    
        $validation = ValidationProduct::validation($request);
        if($validation !=null || $validation !="")
        {
          return response()->json($validation,400);  
        }else{

            $level1 = $request->category_id1;
            $level2 = $request->category_id2;
            $level3 = $request->category_id3;
            $level4 = $request->category_id4;
            $level5 = $request->category_id5;
            $level6 = $request->category_id6;

            if (!empty($level2) && empty($level3)){
                $kode_sda = $level2;
            }else if (!empty($level3) && empty($level4)){
                $kode_sda = $level3;
            }else if (!empty($level4) && empty($level5)){
                $kode_sda = $level4;
            }else if (!empty($level5) && empty($level6)){
                $kode_sda = $level5;
            }else if (!empty($level6)){
                $kode_sda = $level6;
            }else{
                $kode_sda = $level1;
            }

            $productid = Product::where('id',$id)->first();

            if(empty($_FILES['file_pdf']))
            {
                $filename = $productid->attachment;
            }else{
                $type = explode('/',$_FILES['file_pdf']['type']);
                $extentions = $type[1];
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->name)));
                $filename = time() . '-' . $slug.'.'.$extentions;
                $filePath = public_path() . '/file/product/brosur/';
                $terupload = move_uploaded_file($_FILES['file_pdf']['tmp_name'], $filePath.$filename);
            }    

           

            $product = Product::where('id',$id)->update([
                'code_1'=>$kode_sda,
                'name'=>$request->name,
                'attachment'=>$filename,
                
                'uom_id'=>$request->uom_id,
                'berat_unit'=>$request->weight,
                'uom_id'=>$request->uom_id,
                'width'=>$request->width,
                'height'=>$request->height,
                'tod_id'=>$request->tod_id,
                'note'=>$request->description,
                'tgl_harga_valid'=>$request->date_price,
                
            ]);
            
            


           $result['status'] = true;
           $result['messages'] = "Berhasil update produk";
         
            return json_encode($result);
        }

    }  

     public function destroy($id,Request $request)
    {
        $data = DB::table('product')->where('id',$id)->update(['is_deleted'=>1]);
        return json_encode(['status'=>true,'message'=>'produk berhasil diarsipkan'],200);
    } 

     public function revert($id,Request $request)
    {
        $data = DB::table('product')->where('id',$id)->update(['is_deleted'=>0]);
        return json_encode(['status'=>true,'message'=>'produk berhasil dipindahkan'],200);
    } 

    public function deletePayment(Request $request)
    {
        DB::table('payment_product')->where('id',$request->id)->delete();
        $payment = RequestProduct::getpaymentProduct($request->product_id);
        return $payment;
    }

     public function addPhoto(Request $request)
    {
            
        
        $source = explode(";base64,", $request->photo);
        $extFile = explode("image/", $source[0]);
        $extentions = $extFile[1];
        $image = base64_decode($source[1]);
        $filePath = public_path() . '/images/product/';
        $filename = time() . '-' . $request->id.'.'.$extentions;
        $success = file_put_contents($filePath.$filename, $image);
        DB::table('product_gallery')->insert(['product_id'=>$request->product_id,'filename'=>$filename]);
         
        $payment = RequestProduct::getphoto($request->product_id);
        return $payment;
    }

    public function deletePhoto(Request $request)
    {
        DB::table('product_gallery')->where('id',$request->id)->delete();
        $payment = RequestProduct::getphoto($request->product_id);
        return $payment;
    }


     public function addPayment(Request $request)
    {
            
        $location1 = json_decode($request->location_id);
        $location2 = json_encode($location1);
        $location3 = json_decode($location2);
        $pay = array();
        foreach($location3 as $key1 =>$value)
        {
            $pay[$key1] = $value->value;
        }    
         

        DB::table('payment_product')->insert([
            'product_id'=>$request->product_id,
            'payment_id'=>$request->payment_id,
            'location_id'=> implode(',', $pay),
            'price'=>$request->price,
            'notes'=>$request->noted,
            ]);

        
        $payment = RequestProduct::getpaymentProduct($request->product_id);
        return response()->json($payment);
    }

    public function GetLevel(Request $request)
    {
      $result = array();
      if($request->type =="2")
      {
        $result['category2'] = RequestProduct::getCategory2($request->code);
      }else if($request->type =="3"){
        $result['category3'] = RequestProduct::getCategory3($request->code);
      }else if($request->type =="4"){
        $result['category4'] = RequestProduct::getCategory4($request->code);
      }else if($request->type =="5"){
         $result['category5'] = RequestProduct::getCategory5($request->code);
         $result['weight'] = RequestProduct::GetWeight($request->code);
      }else if($request->type =="6"){ 
        $result['category6'] = RequestProduct::getCategory6($request->code);
      }  
      
         return response()->json($result);
    }    

   
      
}    