<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\GeneralPaginate;
use App\Helpers\SSO\SsoManagement;
use App\Http\Request\RequestPrivy;
use App\Http\Request\RequestCart;
use App\Http\Request\RequestOrder;
use App\Http\Request\Validation\ValidationRequestOTP;
use App\Http\Request\Validation\ValidationTokenOTP;
use Illuminate\Support\Facades\Http;
use Auth;
use DB;

class PrivyApiController extends Controller
{

   
    public function __construct()
    {  
      $connect = SsoManagement::Connection();
    }


    public function sendWa(){
             
             $api_key = env('APIKEYWA');
             $sender =  env('SENDERWA');
             $phone = "6285726242220";
             $message = "tess";
             $input = array('api_key'=>$api_key,'sender'=>$sender,'number'=>$phone,'message'=> $message);
            // $input = 'api_key=c15978155a6b4656c4c0276c5adbb5917eb033d5&sender=6282211121525&number=6285726242220&message=tes';
             $url = "https://wa-api.wika.co.id/send-message";
             $response = Http::post($url,$input);
            $result = json_decode($response);

             return $result;

        
             

    }


     public function sendNotix(){
             
        $result = RequestPrivy::sendNotix();
        return $result;

        
             

    }


    // public function GetOrder(Request $request){

    //      $order = DB::table('order')->select('id','order_no','no_surat','pdf_name')->where(['order_no'=>$request->order_no,'token'=>null])->first();
    //      if($order !=null)
    //      {
           
           
    //            $result =  $this->DocumentUpload($order->id,$order->order_no,$order->no_surat,$order->pdf_name);  
             
    //          $response = response()->json(['status'=>'SUCCESS','data'=>$result],200);
    //          return $response;
        
    //      }else{
    //            $result = array('status'=>'ERROR','action'=>'privy','messages'=>'Upload dokumen privy gagal');
    //            $response = response()->json($result,400);
    //            return $response;
    //      }   

        
             
    // }


   

    


    // public function DocumentUpload($id,$order_no,$no_surat,$filename){

    //   $path = url('/file/order/'.$filename);
    //   $type = pathinfo($path, PATHINFO_EXTENSION);
    //   $data = file_get_contents($path);
    //   $base64 = 'data:application/' . $type . ';base64,' . base64_encode($data);
    //   $title = "Test Document Upload and Share 13";

    //     $privy = [];
    //     // $privyAccess = RequestCart::privyAccess();
      
    //     // foreach($privyAccess as $key =>$val)
    //     // {
    //     //       $arr[$key] = [
    //     //         'identifier' => $val->privy_id,
    //     //         'type' => $val->type,
    //     //         'pos_x'=>$val->pos_x,
    //     //         'pos_y'=>$val->pos_y,
    //     //         'page'=>$val->page,
    //     //       ];   
               
              
    //     // }         
    //     $file = array('filename'=>$base64,'title'=>$title,'recipients'=>json_encode($privy)); 
    //     $access = env('PRIVYID');
    //     if($access =="LOCAL")
    //     {
    //         $type = "document";
    //         $sender = array('access'=>'privy/upload','data'=>$file); 
    //         $result = $this->AccessPrivy($id,$type,$order_no,$no_surat,$sender);

    //     }else{
    //         $sender = array('access'=>'document/upload','data'=>$file);
    //         $result = RequestPrivy::DocumentUpload($id,$order_no,$no_surat,$sender);
    //     }  
    
    //     return $result;
          
    // } 

    public function GetOrderBackend(Request $request)
    {

          
         $order = DB::table('order')->select('id','order_no','no_surat','pdf_name')->where(['order_no'=>$request->order_no])->whereNull('token')->first();
          
            
        
         if($order !=null)
         {

         
           
            $access = env('PRIVYID',true);
            if($access =="LOCAL")
            {
                    
                $path = url('/file/order/'.$order->pdf_name);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);
                $base64 = 'data:application/' . $type . ';base64,' . base64_encode($data);
                $title = "Test Document Upload and Share 13"; 

               $privy = 
                [
                    [
                        'identifier' => 'DEVWI0989',
                        'type' => 'signer',
                        'pos_x' => 60,
                        'pos_y' => 440,
                        'page' => 1
                    ],
                    [
                        'identifier' => 'DEVWI6048',
                        'type' => 'signer',
                        'pos_x' => 455,
                        'pos_y' => 440,
                        'page' => 1
                    ]
                ];

                $file = array('filename'=>$base64,'title'=>$title,'recipients'=>json_encode($privy));
               
                $type = "document";
                $sender = array('access'=>'privy/upload','data'=>$file); 
                $level_id ="";
                $status_id ="";
                $noted = "";
                $result = $this->AccessPrivy($order->id,$type,$order->order_no,$level_id,$status_id,$noted,$order->no_surat,$sender);

            }else{
                $url = env('SERVER_BACKEND',true).'api_new/order/generatepdf';
                $action = "generate";
                $result =  RequestPrivy::ServerBackend($order->id,$order->order_no,$order->no_surat,$url,$action); 
            }  



            
               
             //$response = response()->json(['status'=>'SUCCESS','data'=>$result],200);
             return $result;
        
         }else{
               $result = array('status'=>'ERROR','action'=>'privy','messages'=>'Upload dokumen privy gagal');
               $response = response()->json($result,400);
               return $response;
         }   


    }

    public function RequestSign(Request $request){
      $data = DB::table('request_otp as a')
           ->select('b.id','b.no_surat','b.token','a.level_id','a.status_id','a.noted','a.order_no')
           ->join('order as b','a.order_no','=','b.order_no')
           ->where(['a.order_no'=>$request->order_no,'a.user_id'=>Auth::User()->id])->first();
       if($data !=null)
       {   
            
            $result =  $this->SignRequest($data->id,$data->order_no,$data->level_id,$data->status_id,$data->noted,$data->no_surat,$data->token);        
            return $result;
           
        }else{
           return response()->json(['status'=>'ERROR','action'=>'privy','action'=>'privy','messages'=>'No Order tidak valid'],400); 
        } 

    }

   

     public  function RequestOtp(Request $request)
    {
       $data = DB::table('order')->where('order_no',$request->order_no)->first();
       if($data !=null)
       {   
            $validation = ValidationRequestOTP::validation($request);
            if($validation !=null || $validation !="")
            {
                return response()->json($validation,400);  
            }else{
                  

                $result =  $this->SignRequest($data->id,$data->order_no,$request->level_id,$request->status_id,$request->noted,$data->no_surat,$data->token);        
                return $result;
            }
        }else{
           return response()->json(['status'=>'ERROR','action'=>'privy','action'=>'privy','messages'=>'No Order tidak valid'],400); 
        }   
    }
    
    public function SignRequest($id,$order_no,$level_id,$status_id,$noted,$no_surat,$token){

        $file = array('token'=>$token,'privyid'=> Auth::User()->privy_id);
        $access = env('PRIVYID');
        if($access =="LOCAL")
        {
            $type = "step1";  
            $sender = array('access'=>'privy/sign/request','data'=>$file); 
            $result = $this->AccessPrivy($id,$type,$order_no,$level_id,$status_id,$noted,$no_surat,$sender);
        }else{
            $type = "step1";
            $sender = array('access'=>'document/sign','data'=>$file);
            $result = RequestPrivy::SignRequest($id,$type,$order_no,$level_id,$status_id,$noted,$no_surat,$sender);
        }  
            
        return $result;

        

    } 


     public  function ProsesOtp(Request $request)
    {
       $data = DB::table('order')->where('order_no',$request->order_no)->first();
       if($data !=null)
       {  
            $validation = ValidationTokenOTP::validation($request);
            if($validation !=null || $validation !="")
            {
                return response()->json($validation,400);  
            }else{
                $level_id ="";
                $status_id ="";
                $noted ="";
                $result =  $this->SignProcess($data->id,$data->order_no,$level_id,$status_id,$noted,$data->no_surat,$request->otp_code,$data->token);
                return $result;
            }
        }else{
           return response()->json(['status'=>'ERROR','action'=>'privy','messages'=>'No Order tidak valid'],400); 
        }   
    }
    
    public function SignProcess($id,$order_no,$level_id,$status_id,$noted,$no_surat,$otp,$token)
    {
        $file = array('token'=>$token,'privyid'=> Auth::User()->privy_id,'otp'=>$otp);
        $access = env('PRIVYID');
        if($access =="LOCAL")
        {
            $type = "step2"; 
            $sender = array('access'=>'privy/sign/process','data'=>$file); 
            $result = $this->AccessPrivy($id,$type,$order_no,$level_id,$status_id,$noted,$no_surat,$sender);
        }else{
            $type = "step2"; 
            $sender = array('access'=>'document/sign/process','data'=>$file);
            $result = RequestPrivy::SignProcess($id,$type,$order_no,$level_id,$status_id,$noted,$no_surat,$sender);
            
        
        }  
            
        return $result;

    } 


    public function AccessPrivy($id,$type,$order_code,$level_id,$status_id,$noted,$no_surat,$sender)
    {
          $url = $sender['access'];
          $postData = $sender['data'];
          $server = env('SERVER_PRIVYID').$url;
          $response = Http::post($server, $postData);
          $result = json_decode($response);

          $dataPrivy = RequestPrivy::ResponsePrivy($id,$type,$result,$order_code,$level_id,$status_id,$noted,$no_surat);
          return $dataPrivy;

    }

    public function GetFormAssign(Request $request){
         
      
      
        for ($i = 1; $i < 4; $i++) 
        {
           $data[$i]['value'] = $i;
           if($i ==1)
           {
            $data[$i]['text'] = 'Approve';
           }else if($i ==2){
             $data[$i]['text'] = 'Revisi'; 
           }else{
             $data[$i]['text'] = 'Cancel';
           }
           
        }

       $result['status'] =  $data;
       if($request->status_id !="")
       {

          $arr = array();
          $apprv = DB::table('alasan_cancel')->where('jenis',$request->status_id)->get();
       
          foreach ($apprv as $key => $val)
          {
           $arr[$key]['value'] = $val->id;
           $arr[$key]['text'] = $val->alasan;
          }      
    

          $result['alasan'] =  $arr;
       } 
     
       return  $result;

    }

    public function GetLcts($id){
        
        $order_code = $id;
        
         $transport = RequestOrder::CheckTransportLCTS($order_code);
                           if($transport['status'] == true)
                           { 
                               $Createtoken = RequestPrivy::CreateTokenLCTS();
                               $token = $Createtoken['access_token'];
                               $lcts =  RequestPrivy::RequestLCTS($order_code);
                                $data = json_encode($lcts);

                                $ch = curl_init(); 
                                curl_setopt($ch, CURLOPT_URL, env('SERVER_LCTS',true).'/api/sync/create-so');
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json','Authorization: Bearer '.$token]);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                                $response = curl_exec($ch); 
                                curl_close($ch); 
                                $response = json_decode($response, true);

                                if($response['success'] ==true)
                                    {
                                         $update = DB::table('order')
                                           ->where('order_no',$order_code)
                                              ->update(['so_transportasi'=>$response['data']['order_code']]);

                                         // return response()->json(['status'=>'SUCCESS','send_wa'=>$SendWa,'messages'=>'Dokumen privy approval '.$sequece.' & Projek LCTS berhasil di verifikasi,  menunggu approval '.$seq.' '.$UserApprov.''],200); 

                                        return response()->json(['status'=>'SUCCESS','action'=>'privy','data'=>$response['data']['order_code'],'messages'=>'Projek LCTS berhasil'],200);  

                                    }else{
                                        return response()->json(['status'=>'ERROR','action'=>'privy','data'=>[],'messages'=>'Projek Ecatalog belum terdaftar di ESCM'],400); 
                                    }  



                           }else{
                                return response()->json(['status'=>'ERROR','action'=>'lcts','data'=>'','messages'=>'Request Token LCTS Gagal'],400); 

                           }
     

                return $lcts;
      
    }




    


    

}    