<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\GeneralPaginate;
use App\Helpers\SSO\SsoManagement;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\RoomChat;
use App\Models\RoomChatDetail;
use App\Models\Vendor;
use App\Models\LogPassword;
use App\Http\Request\RequestChat;
use Auth;
use File;
use Illuminate\Support\Facades\Hash;
use DB;

class ChatApiController extends Controller
{

   
    public function __construct()
    {  
      //$connect = SsoManagement::Connection();
    }

     
    public function checkNotif(){
   
       $res = array();
       $auth = Auth::User();
       if($auth)
       {
           $user_data = array('id'=>$auth->id,'username'=>$auth->username);
           return response()->json(['status'=>true,'user'=>$user_data],200);

       }else{
           
           $user_data = array('id'=>$auth->id,'username'=>$auth->username);
           return response()->json(['status'=>false,'user'=>[]],400);
       } 
       
     
    }

    public function chat(){
   
       $res = array();
       $auth = Auth::User();
       $id = $auth->id;
       $user_data = RequestChat::getDataUserChat();

       $list_chat = RequestChat::getListChat();
       return response()->json(['status'=>true,'user_view'=>$user_data,'list_chat'=>$list_chat],200);
     
    }

    public function RoomList(Request $request){

       $auth = Auth::User();
       $id = $auth->id;
       $Room = RequestChat::GetRoomList($request->room_id,$id);
       return response()->json(['status'=>true,'room_chat'=>$Room],200);
    }

    public function countChat(){

      $recipient = DB::table('room_chat_detail')->where(['from'=>Auth::User()->id])->first()->to;
      DB::table('room_chat_detail')->where(['from'=>$recipient,'to'=>Auth::User()->id,'reading'=>'false'])->update(['reading'=>'true']);
      $reading = DB::table('room_chat_detail')->where(['from'=>$recipient,'to'=>Auth::User()->id,'reading'=>'false'])->count();
     return array('status'=>true,'count'=>$reading,'messages'=>'berhasil update notif chat');

    }

    public function send(Request $request)
    {
         $message = array();   
         $check = DB::table('room_chat')->count();
         if($check >0)
         {
             $chatroom = DB::table('room_chat')->where(['from'=> Auth::User()->id])->orWhere('to', Auth::User()->id)->first();
         }else{
           
            $chatroom = RoomChat::create(['to'=>$request->to,'from'=> Auth::User()->id,'active'=>'false','created_at'=>date("Y-m-d H:i:s")]);
              
           
         }  

         $result = array();
            
            $photo_list = json_decode($request->photo);
            $photo1 = json_encode($photo_list);
            $photo2 = json_decode($photo1);

            if($request->photo != null || $request->photo !="")
            {
                $res = array();
                foreach($photo2 as $key =>$val)
                {
                   if($key >0)
                   {
                    $no = $key;
                   }else{
                    $no = 1;
                   } 
                    
                   
                    $source = explode(";base64,", $val->photo);
                    $extFile = explode("image/", $source[0]);
                    $extentions = $extFile[1];
                    $image = base64_decode($source[1]);
                    $filePath = public_path() . '/images/chat/';
                    $filename = time() . '-' . $val->id.'.'.$extentions;
                    $success = file_put_contents($filePath.$filename, $image);
                     
                    if($request->textmessages !="")
                    {
                      $mes = $request->textmessages;
                    }else{
                      $mes =  '';
                    } 

                    $data[$key] = array('id'=>$no,'photo'=>url('/images/chat/').'/'.$filename,'replay'=>$mes);
                   
                }  

                $result = RoomChatDetail::create(['room_id'=>$chatroom->id,'from'=>Auth::User()->id,'to'=>$request->to,'textmessages'=>json_encode($data),'created_at'=>date("Y-m-d H:i:s"),'type'=>'photo','reading'=>'false']); 

                   
             
            }else{
                
                $result = RoomChatDetail::create(['room_id'=>$chatroom->id,'from'=>Auth::User()->id,'to'=>$request->to,'textmessages'=>$request->textmessages,'created_at'=>date("Y-m-d H:i:s"),'type'=>$request->type,'reading'=>'false']); 

            }    

          
            //post user login
            $message['id'] = $result->id;
            $message['created_at'] = RequestChat::GetSplitTime($result->created_at);
            $message['from'] = $result->from;
            $message['from_fullname'] = Auth::User()->first_name.' '.Auth::User()->last_name;
            $message['to'] = $result->to;
            $message['textmessages'] = $result->textmessages;
            $message['type'] = $result->type;
            $message['room_id'] = $chatroom->id;
            $count = RoomChatDetail::where(['to'=>$result->to,'reading'=>'false'])->count();
            $notif = array('to'=>(int)$result->to,'count'=>$count); 
            $Room = RequestChat::GetRoomList($chatroom->id,$result->from); 
       

            return response()->json(['status'=>true,'message'=>$message,'room_chat'=> $Room,'reading_count'=> $notif],200);
    }

    public function searchVendor(Request $request)
    {

         $chatVendor = DB::table('room_chat as a')->select('a.id','a.to','a.from','a.active')->join('users as b','a.to','=','b.id')
         ->where('b.username','LIKE','%'.$request->search_name.'%')
         ->orWhere('b.first_name','LIKE','%'.$request->search_name.'%')
         ->get();
         $user_data = RequestChat::getDataUserChat();

         $list_chat = RequestChat::GetChatList($chatVendor);
         return response()->json(['status'=>true,'user_view'=>$user_data,'list_chat'=>$list_chat],200);

    }
   
    


}    