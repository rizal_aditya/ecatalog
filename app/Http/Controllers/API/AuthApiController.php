<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\GeneralPaginate;
use App\Helpers\SSO\SsoManagement;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\User;
use App\Models\RoleUser;
use App\Models\Vendor;
use App\Models\Group;

use App\Models\LogPassword;
use App\Http\Request\Validation\ValidationAuth;
use App\Http\Request\RequestAuth;
use App\Http\Request\RequestFrontend;
use Auth;
use File;
use Illuminate\Support\Facades\Hash;
use DB;
class AuthApiController extends Controller
{

   
    public function __construct()
    {  
     // $connect = SsoManagement::Connection();
    }

      public function Login(Request $request)
    {
        $credentials = $request->only('username', 'password');
       // RequestAuth::requestHash($request->username,$request->password); 
        $validation = ValidationAuth::validation($request);
        
        
       
        if($validation)
        {
             return response()->json(['status'=>false,'messages' => $validation],400);          
           

        }else{
           
           $user = User::where('username',$credentials['username'])->first();
           if($user)
           {

                if ($user->status == 'T')
                {  
                    $messages3 = array('status'=>'Akun anda ditangguhkan admin');
                    return response()->json(['status'=>false,'messages' => $messages3],400);
                }else{
            
                    // Authentication successful
                      
                        try
                        {
                            //access login api
                            if (! $token = JWTAuth::attempt($credentials)){
                                $messages1 = array('username'=>'Nama Pengguna tidak valid');
                                $messages2 = array('password'=>'Kata Sandi tidak valid');
                               
                               
                                if ($user ==null)
                                {
                                    return response()->json(['status'=>false,'messages' => $messages1],400);
                                }

                               
                                 
                                if (!Hash::check($credentials['password'], $user->password))
                                {
                                    return response()->json(['status'=>false, 'messages' => $messages2],400); 
                                }
         
                            }
                            

                           $auth = Auth::User();
                           $RoleUser = RoleUser::where('user_id',$auth->id)->first();
                           $access =  $RoleUser->role_id;
                          
                           $userHeader = array('id'=>$auth->id,'username'=>$auth->id);
                           $token = compact('token');
                           return response()->json(['status'=>true,'user_header'=>$userHeader,'access'=>$access,'token'=>$token['token']],200); 
                                

                        } catch (JWTException $e) {
                           return response()->json(['error' => 'validasi tidak valid'], 500);
                        } 

                        
                }
               
            }
        
        
        }    
    }
       

     // public function Login(Request $request){
        
     //    $credentials = $request->only('username', 'password');
     //    $user = User::where('username',$credentials['username'])->first();
        
     //    // DB::table('cart')->where('created_by',$user->id)->delete();
     //    if($user)
     //    {
     //        $log = LogPassword::where('user_id',$user->id)->count();
     //        if($log ==null)
     //        {
     //            if(sha1($credentials['password']) == strtolower($user->password))
     //            {
     //               LogPassword::create(['user_id'=>$user->id,'status'=>'true','password_last'=>$user->password]); 
     //               User::where('username',$credentials['username'])->update(['password'=>bcrypt($credentials['password'])]);
     //            }
     //        }     
     //    }    
        
     //   // $Auth = array('username'=>$credentials['username'],'password'=>$credentials['password']); 
     //    try {
     //        if (! $token = JWTAuth::attempt($credentials))
     //        {   
     //            $validation = RequestAuth::LoginAuth($credentials['username'],$credentials['password']);
     //            if($validation !=null || $validation !="")
     //            {
     //               return response()->json($validation,400);  
     //            }
              
     //        }
     //    } catch (JWTException $e) {
     //        return response()->json(['error' => 'could_not_create_token'], 500);
     //    }
       
     //    $token = compact('token');
     //    $auth = Auth::User();
     //    $RoleUser = RoleUser::where('user_id',$auth->id)->first();
     //    DB::table('users')->where('id',$auth->id)->update(['active_chat'=>'true']);
     //    $access =  $RoleUser->role_id;
     //    $clear = $this->ClearCache();
     //   // $callback = SsoManagement::ConvertCode('decrypt',$credentials['callback']);
     //    $data = SsoManagement::LoginSso($credentials['username'],$credentials['password'],$access,$token['token']);
     //    return response()->json($data,200);
     // }


     public function LoginWa(Request $request){
        
        $credentials = $request->only('phone');

        $user = User::where('phone',$credentials['phone'])->first();
        if($user !=null)
        {
            if (!$token=JWTAuth::fromUser($user)) {
             return response()->json(['error' => 'invalid_credentials'], 401);
            }

            $token = compact('token');
            $RoleUser = RoleUser::where('user_id',$user->id)->first();
            DB::table('users')->where('id',$user->id)->update(['active_chat'=>'true']);
            $access =  $RoleUser->role_id;
            $clear = $this->ClearCache();
            $data = SsoManagement::LoginSsoWa($user->username,$access,$token['token']);
            
        }else{
            $data = response()->json(['status' => false,'message'=>'user belum login']);
        }    
        

         return response()->json($data,200);

        
    


     }

     public function ClearCache(){

            $url = env('SERVER_BACKEND',true).'api_new/auth/logout';
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL,  $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);      

            return $output;

     }


      public function Logout(Request $request)
      {
        
           $user = SsoManagement::GetUser();
           if($user['status'] == true)
           {
               try {
                JWTAuth::invalidate($user['token']);
                //DB::table('login_session')->where(['user_id'=>Auth::user()->id])->delete();
                $clear = $this->ClearCache();
                SsoManagement::LogoutSso();
                return response()->json(['status' => true, 'message'=> "You have successfully logged out."]);
                

               

                } catch (JWTException $e) {
                    // something went wrong whilst attempting to encode the token
                    return response()->json(['status' => false, 'error' => 'Failed to logout, please try again.'], 500);
                }


           } 
       
           

       }
       

     public function getAuthUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        
        $data = compact('user');
        $grup = Group::where('id', $data['user']->group_id)->first() ?: null;  
        $result = array('id'=>$data['user']->id,'name'=>$data['user']->username,'first_name'=>$data['user']->first_name,'last_name'=>$data['user']->last_name,'email'=>$data['user']->email,'group'=>$grup->name);
        return $result;
    }

    public function countView(){

     $notif =  DB::table('notification')->where('id_penerima',Auth::User()->id)->update(['is_read'=>1]);
     $update =  DB::table('notification')->where(['id_penerima'=>Auth::User()->id,'is_read'=>0])->count();
     return array('status'=>true,'count'=>$update,'messages'=>'berhasil update notif');
    }

     public function top(){
       // SsoManagement::Connection(); 
       // $data = SsoManagement::GetUser();

      
       $res = array();
       $auth = Auth::User();
       $id = $auth->id;
       $profile = RequestAuth::getProfileHeader();
       $category = RequestAuth::getCategoryHeader();
       $location = RequestAuth::getLocationHeader();
     
       $notif = RequestAuth::getNotif();
       $notifIsRead = RequestAuth::getCountNotifIsRead();


       $recipient = DB::table('room_chat')->where(['to'=>Auth::User()->id])->first();
       if($recipient !=null)
       {
          $reading = DB::table('room_chat_detail')->where(['to'=>Auth::User()->id,'reading'=>'false'])->count();
       }else{
         $reading = 0;
       } 

       $cart = DB::table('cart')->where(['created_by'=>Auth::User()->id])->first();
       if($cart !=null)
       {
         $readCart = DB::table('cart')->where(['created_by'=>Auth::User()->id,'reading'=>'false'])->count();
       }else{
         $readCart = 0;
       } 

      
      
       return response()->json(['status'=>true,'vendor'=>$profile,'category'=>$category,'location'=>$location,'notif'=>$notif,'count_view'=>$notifIsRead,'count_chat'=>$reading,'count_cart'=> $readCart],200);
       
    }


    public function location(){

        $location = RequestAuth::getLocationHeader();
        return response()->json($location,200);
    }



   

    public function sidebar(){
       $res = array();
       $auth = Auth::User();
       $id = $auth->id;
       $profile = RequestAuth::GetAccountVendor();
       return response()->json(['status'=>true,'vendor'=>$profile],200);
      
    }

    public function getvendor(){
        $auth = Auth::User();
        $id = $auth->vendor_id;
        $vendor = Vendor::where('id',$id)->first();
        $FileAkun = GeneralPaginate::FileAkun();
        $FilePublic = GeneralPaginate::FilePublic();
        if($auth->photo ==null || $auth->photo =="")
        {
            $photo = '';
        }else{
            $photo = url($FileAkun.$auth->photo);
        } 

       
      
       $data = array('photo'=>$photo,'username'=>$auth->username,'vendor_name'=>$vendor->name,'vendor_owner'=>$vendor->nama_direktur,'position'=>$vendor->nama_direktur,'description'=>$vendor->description,'address'=>$vendor->address,'phone'=>$vendor->no_telp,'name_approve'=>$vendor->ttd_name,'position_approve'=>$vendor->ttd_pos);

       return response()->json(['status'=>true,'vendor'=>$data],200);
         
    }


    public function UpdateVendor(Request $request){
       $auth = Auth::User();
       $id = $auth->vendor_id;
       $FileAkun = GeneralPaginate::FileAkun();
       $image = $request->photo; 
        
       
        
       if($image !=null)
       {
            if($auth->photo !="")
            {
              File::delete($FileAkun.$auth->photo);
            }  
         
         $data = explode(";base64,", $image);//crop 
       
         $fileName = 'vendor-'.time().'.png';
         $path =  $FileAkun.$fileName;
         \File::put($path, base64_decode($data[1]));
          User::where('id',$auth->id)->update(['photo'=>$fileName]);
       }  
        


       Vendor::where('id',$id)->update([
        'name' => $request->vendor_name,
        'nama_direktur' => $request->vendor_owner,
        'description' => $request->description,
        'address' => $request->address,
        'no_telp' => $request->phone,
        'ttd_name' => $request->name_approve,
        'ttd_pos' => $request->position_approve,
        
       ]);

      

       $vendor = Vendor::where('id',$id)->first();
       $data = array('photo'=>$auth->photo,'username'=>$auth->username,'vendor_name'=>$vendor->name,'vendor_owner'=>$vendor->nama_direktur,'description'=>$vendor->description,'address'=>$vendor->address,'phone'=>$vendor->no_telp,'name_approve'=>$vendor->ttd_name,'position_approve'=>$vendor->ttd_pos);

       return response()->json(['status'=>true,'message'=>'Vendor berhasil diupdate','vendor'=>$data],200);
         
    }

    public function DeletePhoto()
    {

       $auth = Auth::User();
       $id = $auth->vendor_id;
       $FileAkun = GeneralPaginate::FileAkun();
      
       if($auth->photo !="")
       {
          File::delete($FileAkun.$auth->photo);
          User::where('id',$auth->id)->update(['photo'=>'']);
       }  
        return response()->json(['status'=>true,'message'=>'Photo berhasil didelete'],200);
    }



     public function updateSingle($id,Request $request)
    {
        
        $check = ValidationGlobalUser::validationCheck($request);
        if(count($check) !=null || count($check) !=0 )
        {
            return response()->json($check,400);  
        }else{
            $update = RequestGlobalUser::fieldsSingle($request);
            $UpdateAccount = User::where('id',$id)->update($update);
            return response()->json(['status'=>true,'id'=>$UpdateAccount,'message'=>'Update data sucessfully']);
        }
    }

    
   
    


}    