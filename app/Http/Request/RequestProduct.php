<?php

namespace App\Http\Request;

use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\User;

use DB;
class RequestProduct
{
   public static function CheckLevel($code){
     
       $temp = array();
       $category = DB::table('resources_code')->select('level')->where('code',$code)->first();
       
       $level ='';
       if($category !=null)
       {
          $level = $category->level;
       }else{
          $level = "";
       } 
       return $level;

   }

   public static function CategoryCode($code,$type){
     
       $temp = array();
       $category = DB::table('resources_code')->select('code','parent_code')->where('code',$code)->first();
       
       $level ='';
       if($category !=null)
       {
          if($type =="one")
          {
             $level = $category->code; 
          }else{
            $level = $category->parent_code; 
          }  
          
       }else{
          $level = "";
       } 
       return $level;

   }
    

    public static function CategoryParent($code){
     
       $temp = array();
       $category = DB::table('resources_code')->select('parent_code')->where('parent_code',$code)->first();
       
       $level ='';
       if($category !=null)
       {
          $level = $category->parent_code;
       }else{
          $level = "";
       } 
       return $level;

   }
   

    public static function SearchCategory4($code,$type){
     
       $temp = array();
      
       $category = DB::table('resources_code')->where('parent_code',$code)->first();
       
       
       return $category;

   }




   

   public static function getCategory1()
   {
       $temp = array();
       $category = DB::table('resources_code')->where(['level'=>1,'status'=>1])->get();
       foreach ($category as $key => $val)
       {
           $temp[$key]['value'] = $val->code;
           $temp[$key]['text'] = $val->name;
       }   
       return  $temp; 
   }

  

   public static function getCategory2($code)
   {
       $temp = array();
       $category = DB::table('resources_code')->where(['level'=>2,'status'=>1,'parent_code'=>$code])->get();
       foreach ($category as $key => $val)
       {
           $temp[$key]['value'] = $val->code;
           $temp[$key]['text'] = $val->name;
       }   
       return  $temp;
   }

   public static function getCategory3($code)
   {
       $temp = array();
       $category = DB::table('resources_code')->where(['level'=>3,'status'=>1,'parent_code'=>$code])->get();
       foreach ($category as $key => $val)
       {
           $temp[$key]['value'] = $val->code;
           $temp[$key]['text'] = $val->name;
       }   
       return  $temp;  
   }

    public static function GetWeight($code)
    {
        $data = DB::table('resources_berat')->where('code',$code)->get();
     
        $arr = [];
        
            foreach ($data as  $row)
            {       
                
                $arr[0]['text'] = $row->berat1;
                $arr[0]['value'] = $row->berat1;
                $arr[1]['text'] = $row->berat2;
                $arr[1]['value'] = $row->berat2;
                $arr[2]['text'] = $row->berat3;
                $arr[2]['value'] = $row->berat3;     
               
                   
            }
            
         return $arr;
    }

   public static function getCategory4($code)
   {
       $temp = array();
       $category = DB::table('resources_code')->where(['level'=>4,'status'=>1,'parent_code'=>$code])->get();
       foreach ($category as $key => $val)
       {
           $temp[$key]['value'] = $val->code;
           $temp[$key]['text'] = $val->name;
       }   
       return  $temp;  
   }

   public static function getCategory5($code)
   {
       $temp = array();
       $category = DB::table('resources_code')->where(['level'=>5,'status'=>1,'parent_code'=>$code])->get();
       foreach ($category as $key => $val)
       {
           $temp[$key]['value'] = $val->code;
           $temp[$key]['text'] = $val->name;
       }   
       return  $temp;  
   }


   public static function getCategory6($code)
   {
       $temp = array();
       $category = DB::table('resources_code')->where(['level'=>6,'status'=>1,'parent_code'=>$code])->get();
       foreach ($category as $key => $val)
       {
           $temp[$key]['value'] = $val->code;
           $temp[$key]['text'] = $val->name;
       }   
       return  $temp;  
   }
   
   public static function getBeratUnit()
   {
       $temp = array();
       $berat = DB::table('uoms')->where(['is_deleted'=>0])->get();
       foreach ($berat as $key => $val)
       {
           $temp[$key]['value'] = $val->id;
           $temp[$key]['text'] = $val->name;
       }   
       return  $temp;  
   }

   public static function getTOD()
   {
       $temp = array();
       $berat = DB::table('tod')->where(['is_deleted'=>0])->get();
       foreach ($berat as $key => $val)
       {
           $temp[$key]['value'] = $val->id;
           $temp[$key]['text'] = $val->name;
       }   
       return  $temp;  
   }

   public static function getPayment(){

            $payment = DB::table('payment_method as a')
                    ->select('a.id','a.day','b.name')
                    ->join('enum_payment_method as b','b.id','=','a.enum_payment_method_id')
                    ->get();

       
            $temp = array(); 
            foreach ($payment as $key => $val)
            {
               $temp[$key]['value'] = $val->id;
               $temp[$key]['text'] = ucfirst(strtoupper($val->name.' '.$val->day.' hari')); //strtolower
            }   
           return  $temp;              
   }

   public static function getLocation(){

            $payment = DB::table('vendor_lokasi')->where('vendor_id',Auth::User()->vendor_id)->select('wilayah_id','wilayah_name')->get();
            $temp = array(); 
            foreach ($payment as $key => $val)
            {
               $temp[$key]['value'] = $val->wilayah_id;
               $temp[$key]['text'] = $val->wilayah_name;
            }   
           return  $temp;              
   }

   public static function CheckProductID($id){

        $product = DB::table('product')->where('id',$id)->first();
        if($product !=null)
        {
          $res = TRUE;
        }else{
           $res = FALSE;
        }    
        return $res;

   }

   public static function getphoto($id)
   {
        $res = array();
        $gallery = DB::table('product_gallery')->where('product_id',$id)->get();
        if($gallery !=null)
        {

           foreach($gallery as $key => $row)
           {
               $res[$key]['id'] = $row->id;
               $res[$key]['photo'] = url('images/product/'.$row->filename); 
           } 
          
        }else{
           $res = '';
        }    
        return $res;
   }

   public static function getpaymentProduct($id)
   {
         $res = array();
        $payment = DB::table('payment_product')->where('product_id',$id)->get();
        if($payment !=null)
        {
           foreach($payment as $key => $row)
           {
               $res[$key]['id'] = $row->id;
               $res[$key]['payment_id'] = $row->payment_id;
               $res[$key]['payment_name'] = RequestProduct::GetPaymentId($row->payment_id);
               $res[$key]['location_id'] = RequestProduct::GetLocationId($row->location_id);
               $res[$key]['price'] = $row->price;
               $res[$key]['noted'] = $row->notes;
               
           } 
        }else{
           $res = '';
        }    
        return $res;
   }

   public static function GetPaymentId($id){
            $temp = '';
            $payment = DB::table('payment_method as a')
                    ->select('a.id','a.day','b.name')
                    ->join('enum_payment_method as b','b.id','=','a.enum_payment_method_id')
                    ->where('a.id',$id)
                    ->first();

       
            $temp = ucfirst(strtoupper($payment->name.' '.$payment->day.' hari')); //strtolower
           
           return  $temp; 
   }

   public static function GetLocationId($arr){
            
            $xpl = explode(',',$arr);
            foreach($xpl as $row =>$val)
            {
                 $data[$row] = (int)$xpl[$row];
            }
 


            $payment = DB::table('vendor_lokasi')
            ->select('wilayah_id','wilayah_name')
            ->whereIn('wilayah_id',$data)
            ->where('vendor_id',Auth::User()->vendor_id)
            ->get();
            $temp = array(); 
            foreach ($payment as $key => $val)
            {
               $temp[$key]['value'] = $val->wilayah_id;
               $temp[$key]['text'] = $val->wilayah_name;
            } 

            return $temp;   

   }

   public static function getProduct($id){

        $product = DB::table('product')->select('id','name','attachment','code_1','term_of_delivery_id','note','tgl_harga_valid','width','height','uom_id','berat_unit as weight')->where('id',$id)->first();
        if($product !=null)
        {
           $res = $product;
        }else{
           $res = '';
        }    
        return $res;
   }


}