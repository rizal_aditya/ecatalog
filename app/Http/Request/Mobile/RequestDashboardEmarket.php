<?php

namespace App\Http\Request\Mobile;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use DB;


class RequestDashboardEmarket
{

   public static function TopPanelEmarket($category_id){

        $data = array();
      


        $cat = substr($category_id, 0, 2);
        $today = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
        $lastMonth = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + 30, date('Y')));

        $builder = DB::table("product");
      
        if ($category_id != "") {
            $builder->where("code_1", "LIKE", ''.$cat.'%');
        }

        $emarketTotalProduct = $builder->where('is_deleted',0)->count();

        $emarketVerifiedProduct = $builder->where(["is_deleted" => 0, "verified_status" => 1])->count();
       
        $emarketNearlyExp =  $builder->where(["is_deleted" => 0, "verified_status" => 1])->whereBetween('tgl_harga_valid', [$today, $lastMonth])->count();
        
        $emarketNearlyExp = $builder->where(["is_deleted" => 0, "verified_status" => 1])->whereBetween('tgl_harga_valid', [$today, $lastMonth])->count();
     
        $emarketExpired = $builder->where(["is_deleted" => 0, "verified_status" => 1])->where('tgl_harga_valid', '>=', ''.$today.'')->count();

        $data['emarketTotalProduct'] = $emarketTotalProduct;
        $data['emarketVerifiedProduct'] = $emarketVerifiedProduct;
        $data['emarketNearlyExp'] = $emarketNearlyExp;
        $data['emarketExpired'] = $emarketExpired;

        return $data;
    

   } 

   




  
   


}