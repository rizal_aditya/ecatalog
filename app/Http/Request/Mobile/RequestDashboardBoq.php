<?php

namespace App\Http\Request\Mobile;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use DB;


class RequestDashboardBoq
{
   
   public static function GetBoqPanel($departemen)
    {
        //BOQ
        //total
        $builder = DB::table("boq_product as a");
        $builder->join('boq as b', 'b.id','=','a.boq_id');
        $builder->join('product as c', 'c.id','=','a.product_id');
        $builder->join('vendor as d', 'c.vendor_id','=','d.id');
        $builder->where('a.sumber_data','<>','');

        $builder->join('users as e', 'e.id','=','a.created_by');
        $builder->join('rfq as f','e.id','=','f.created_by');


        if ($departemen != "")
        {
            $builder->where('e.group_id',$departemen);
        }

        $BOQProduct = $builder->count();

        //lelang
        $pricebookProduct = $builder->where('a.sumber_data','Pricebook')->count();

        $pricebookProductPersen = ($BOQProduct == 0) ? 0 : round(($pricebookProduct / $BOQProduct) * 100, 2);
        //emarket
        $eMarketProduct = $builder->where('a.sumber_data','Pricebook')->count();
        $eMarketProductPersen = ($BOQProduct == 0) ? 0 : round(($eMarketProduct / $BOQProduct) * 100, 2);

        //manual
        $manualProduct = $builder->where('a.sumber_data','Manual')->count();
        $manualProductPersen = ($BOQProduct == 0) ? 0 : round(($manualProduct / $BOQProduct) * 100, 2);

        $pemenuhanBOQPersen = ($BOQProduct == 0) ? 0 : round((($pricebookProduct / $BOQProduct) * 100) + (($eMarketProduct / $BOQProduct) * 100), 2);
        $pemenuhanBOQTotal  =  $pricebookProduct   +   $eMarketProduct;
       
        $boqBerjalan = $builder->where(['b.is_deleted'=>0,'b.status'=>0])->count();

        $boqTersubmit = $builder->where(['b.is_deleted'=>0,'b.status'=>1])->count();


        $data['pemenuhanBOQPersen'] = $pemenuhanBOQPersen;
        $data['pemenuhanBOQTotal'] = $pemenuhanBOQTotal;
        $data['BOQProduct'] = $BOQProduct;
        $data['pricebookProduct'] = $pricebookProduct;
        $data['pricebookProductPersen'] = $pricebookProductPersen;
        $data['eMarketProduct'] = $eMarketProduct;
        $data['eMarketProductPersen'] = $eMarketProductPersen;
        $data['manualProduct'] = $manualProduct;
        $data['manualProductPersen'] = $manualProductPersen;
        $data['boqBerjalan'] = $boqBerjalan;
        $data['boqTersubmit'] = $boqTersubmit;
        return $data;
    }
}