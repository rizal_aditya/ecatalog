<?php

namespace App\Http\Request\Mobile;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use DB;


class RequestDashboardRfq
{
   
   public static function GetRfqPanel($departemen)
    {
        //BOQ
        //total
        $builder = DB::table("boq_product as a");
        $builder->join('boq as b', 'b.id','=','a.boq_id');
        $builder->join('product as c', 'c.id','=','a.product_id');
        $builder->join('vendor as d', 'c.vendor_id','=','d.id');
        $builder->where('a.sumber_data','<>','');

        $builder->join('users as e', 'e.id','=','a.created_by');
        $builder->join('rfq as f','e.id','=','f.created_by');


        if ($departemen != "")
        {
            $builder->where('e.group_id',$departemen);
        }

        


        //RFQ
        $rfq_all = $builder->where('f.is_deleted', 0)->count();
        
        $rfq_submit =  $builder->where(['f.is_deleted'=>0,'b.status'=>2])->count();

        
        $for_not_in[] = 2;
        $rfq_berjalan = $builder->where('f.is_deleted', 0)->whereNotIn("f.status", $for_not_in)->count();

    
        $queryRfq = DB::table('rfq')->select(DB::raw('SUM(DATEDIFF(DATE (update_at),DATE(created_at))) AS hari'), DB::raw('COUNT(*) as pembagi'))->first();

        $rfqWaktu = is_null($queryRfq->hari) ? 0 : ($queryRfq->hari / $queryRfq->pembagi);


        

        $rfqWaktuTercepatSQL = DB::table('rfq')->select(DB::raw('DATEDIFF(DATE (update_at),DATE(created_at)) AS order_date'), DB::raw('TIMEDIFF(TIME(update_at),TIME(created_at)) as order_time'))
        ->where('status',2)->orderBy('order_date','ASC')->first();

        if( $rfqWaktuTercepatSQL !=null)
        {
            $rfqWaktuTercepat = $rfqWaktuTercepatSQL->order_date;
        }else{
            $rfqWaktuTercepat = 0;
        }    


        $data['rfq_all'] = $rfq_all;
        $data['rfq_submit'] = $rfq_submit;
        $data['rfq_berjalan'] = $rfq_berjalan;

        $str = number_format($rfqWaktu, 2);
        $waktu = explode(".", $str);
        $waktuHari =  $waktu[0] . ' hari ' . number_format(($waktu[1] * 24 / 100)) . ' jam';
        $data['rfqWaktu'] = $waktuHari;
        $data['rfqWaktuTercepat'] = $rfqWaktuTercepat;

        return $data;
    }
}