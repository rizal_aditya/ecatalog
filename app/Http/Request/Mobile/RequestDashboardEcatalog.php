<?php

namespace App\Http\Request\Mobile;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use DB;


class RequestDashboardEcatalog
{

   public static function GetEvalMon($groupDept, $tahun,$bulan,$category)
   {
     // $bulan =  12;
      $monev = RequestDashboardEcatalog::get_data_monev_chart_qty($bulan,$tahun, $groupDept, $category);

      $series = [];
        $categories = [];
        $ket = [
            'total',
            'transaksi',
        ];

        $color_wika = [
            '#ff1a1a',
            '#66b3ff',
            '#0039e6',
        ];

        $color_vendor = [
            '#2ED1A2',
            '#6A8EF9',
            '#00ff00',
        ];

        $vendor_index = [];
        $jml_terpakai = 0;
        $jml_total = 0;
        foreach (range(0, 1) as $v) {
            // ngisi nilai default nya
            $data = [];
            if (count($monev) <> 0) {
                foreach ($monev as $k2 => $v2) {
                    if ($v == 0) {
                        $categories[] = $v2['vendor_name'];
                        $vendor_index[] = $v2['vendor_id'];
                    }
                    // 2 = jumlah terpakai
                    //                    if ($v == 2 && $v2['vendor_id'] != '-1') {
                    $jml_terpakai   = $v2['nilai_' . $ket[1]];
                    $jml_total   = $jml_total + $v2['nilai_' . $ket[1]];
                    //                    }


                    $data[] = [
                        'y' => $v2['nilai_' . $ket[$v]],
                        'name' => "Jumlah " . $ket[$v]  . ' ' . $v2['vendor_name'],
                        'color' => $v2['vendor_id'] == '-1' ? $color_wika[$v] : $color_vendor[$v],
                    ];
                }
            } else {
                $categories[] = null;
                $vendor_index[] = null;
                $data[] = null;
            }
            $_temp_series = [
                'data' => $data,
                'showInLegend' => false,
            ];

            $series[] = $_temp_series;
        }

        $ret['series'] = $series;
        $ret['categories'] = $categories;
        $ret['vendor_index'] = $vendor_index;
        $ret['total_trx'] = $jml_total;

        if($bulan !="")
        {
           $ret['month_select_text'] = GeneralHelpers::mounthChart($bulan);
        }else{
           $ret['month_select_text'] = 'Semua Bulan';
        }    
        
        //        $ret['month_select_count'] = count($monthSet) ;

        $ret['year_select'] = $tahun;
       
        if ($groupDept != "") {
           

            $builder = DB::table("groups");
            $builder->select("name");
            $builder->where('id', $groupDept);

            $query = $builder->first();
            
            $ret['divisi_name'] =  $query->name;
        } else {
            $ret['divisi_name'] =  "Semua Divisi";
        }
         
        $cat = substr($category, 0, 2);  
        $rebuilder = DB::table("resources_code")->select("code","name")->where('code','LIKE',$cat.'%')->first();
        if($rebuilder)
        {
           $ret['resource_name'] = $rebuilder->name;
        }else{
           $ret['resource_name'] = '';
        }

        

      return $ret;
   }

   public static function PenyerapanDept($groupDept, $tahun,$bulan,$category)
   { 
      
      $dept = RequestDashboardEcatalog::get_penyerapan_dept($groupDept, $tahun, $category); //nm departemen
      $per_bulan  = RequestDashboardEcatalog::get_penyerapan_dept_per_bulan($tahun, $dept, $category);
      $result = RequestDashboardEcatalog::ConvertChart($dept,$bulan,$per_bulan);
      return $result; 


   }

   public static function GetPembelianVendor($groupDept, $tahun,$bulan,$category){

       
        
        $vendors = RequestDashboardEcatalog::get_top_5_vendor($groupDept, $tahun,$category);
        $per_bulan = RequestDashboardEcatalog::get_top_5_vendor_per_bulan($groupDept, $tahun, $vendors, $category);
        $result = RequestDashboardEcatalog::ConvertChart($vendors,$bulan,$per_bulan);
        return $result; 

   }



   public static function GetTop10Product($tahun,$bulan,$category)
   {
   
    
    $products = RequestDashboardEcatalog::Top10Product($tahun, $category);
    $per_bulan = RequestDashboardEcatalog::Top10ProductPerBulan($tahun, $category, $products);

    $result = RequestDashboardEcatalog::ConvertChart($products,$bulan,$per_bulan);
    return $result;  
        
  }


   public static function TopPanelEcatalog()
   {
      $data = array();
      $totalMatgis = RequestDashboardEcatalog::totalMatgis(); 
      $totalReg = RequestDashboardEcatalog::PenggunaReg();
      $totalSumberDaya = RequestDashboardEcatalog::totalSumberDaya();
      $totalTerdaftar = RequestDashboardEcatalog::totalTerdaftar();
      $data = array('total_matgis'=>$totalMatgis,'total_reg'=>$totalReg,'total_sumber_daya'=> $totalSumberDaya,'total_terdaftar'=>$totalTerdaftar);

      
      return $data;
   }





  public function get_data_monev_chart_qty($bulan,$tahun, $groupDept, $category)
  {
        $builder = array();
        $cat = substr($category, 0, 2); 
        $limit = 11;
        //$splt = json_encode($bulan);
        //$blnnew = str_replace(array("[", "]"), "", $splt);
        $blnnew = $bulan;
       $project =  DB::query()
     ->select('project.vendor_id',DB::raw('IFNULL(product.total_transaksi, 0) as total_trx'),DB::raw('IFNULL(project.tot_harga, 0) as total_harga'),DB::raw('IFNULL(jum_trans.jumlah_transaksi, 0) as jumlah_trx'),'k.name as vendor_name')
      
      
      ->fromSub(function ($queryone) use ($category,$tahun){
           
            $queryone->from('project as a');
            $queryone->select(DB::raw('SUM(a.harga) AS tot_harga'),'a.vendor_id');
            $queryone->Leftjoin('project_departement as b','a.id','=','b.project_id');
            $queryone->where('a.is_deleted',0);
            $queryone->where('a.category_id',$category);
            $queryone->whereYear('a.created_at','<=',$tahun);
            //$queryone->whereYear('a.created_at','<=','2020');
            $queryone->groupBy('a.vendor_id');

      }, 'project')

        ->LeftjoinSub(RequestDashboardEcatalog::queryProduct($blnnew,$tahun,$groupDept,$cat), 'product', function ($join) {
            $join->on('product.vendor_id', '=', 'project.vendor_id');
        })

        ->LeftjoinSub(RequestDashboardEcatalog::jumTrans($blnnew,$tahun,$groupDept,$cat), 'jum_trans', function ($join) {
            $join->on('jum_trans.vendor_id', '=', 'project.vendor_id');
        })

        ->join('vendor as k','k.id','=','project.vendor_id')
        ->orderBy('vendor_name')
        ->get();
           
        
        $for_not_in = [];
        if(count($project) <> 0) {

            $vendors = $project;
            foreach ($vendors as $k => $v) {
                $for_not_in[] = $v->vendor_id;
                $ret[] = [
                    'vendor_id' => $v->vendor_id,
                    'nilai_total' => (float)$v->total_trx,
                    'nilai_transaksi' =>  (float)$v->jumlah_trx,
                    'vendor_name' => $v->vendor_name,
                ];
            }


            if (count($ret) < $limit) {
                $sisa = $limit - count($ret);


                    $builder = DB::table("vendor as a");
                    $builder->select('a.id', 'a.name');
                    $builder->join('project as b','b.vendor_id','=','a.id');
                    $builder->where('a.is_deleted', 0);
                    $builder->where('b.is_deleted', 0);
                    $builder->where('b.category_id', $category);
                    $builder->limit($limit);
                    $builder->inRandomOrder();


                if (!empty($for_not_in)) {
                    $builder->whereNotIn('a.id', $for_not_in);
                }

                $q = $builder->get();
                foreach ($q as $k => $v) {
                    $ret[] = [
                        'vendor_id' => $v->id,
                        'nilai_total'   => 0,
                        'nilai_transaksi'       => 0,
                        'vendor_name'       => $v->name,
                    ];
                }
            }
        } else {
            $ret[] = [
                'vendor_id' => "",
                'nilai_total'   => 0,
                'nilai_transaksi'       => 0,
                'vendor_name'       =>  "",
            ];
        }

       return $ret;   

        
      
  }

  









  public static function queryProduct($blnnew,$tahun,$groupDept,$cat){


           $querytwo = DB::table('order as a');
           $querytwo->join('order_product as b','a.order_no','=','b.order_no');
           $querytwo->select('a.vendor_id', DB::raw('SUM(b.qty * b.weight * b.price) AS total_transaksi'));
          
           $querytwo->leftJoin('product as c','c.id','=','b.product_id');
           $querytwo->where('a.order_status','!=',3);
           $querytwo->whereYear('a.created_at','=',''.$tahun.'');
           //$querytwo->whereYear('a.created_at','=','2020');
       
           if($blnnew !="")
           {
                $querytwo->whereMonth('a.created_at','=',''.$blnnew.''); 
           }  

           if($groupDept !="")
           {  
                $querytwo->join('project_new as d','d.id','=','a.project_id');
                $querytwo->where('d.departemen_id',$groupDept); 
           }  

           $querytwo->where('c.code_1','LIKE',$cat.'%');
           $querytwo->groupBy('a.vendor_id');
 
           return  $querytwo;

  }

  public static function jumTrans($blnnew,$tahun,$groupDept,$cat){

        $jumTrans = DB::table('order as a');
        $jumTrans->select('a.vendor_id',DB::raw('COUNT(*) AS jumlah_transaksi'));

        $jumTrans->joinSub(RequestDashboardEcatalog::querySub($cat), 'b', function ($join) {
               $join->on('a.order_no', '=', 'b.order_no');
       });

         if($groupDept !="")
        {  
           $jumTrans->join('project_new as c','c.id','=','a.project_id'); 
           $jumTrans->where('c.departemen_id',$groupDept); 
        }  
         
        $jumTrans->whereYear('a.created_at','<=',$tahun);
        // $jumTrans->whereYear('a.created_at','<=','2020');
        if($blnnew !="")
        {
           $jumTrans->whereMonth('a.created_at','<=',$blnnew);
        } 
        $jumTrans->where('a.order_status','!=',3);   
        $jumTrans->groupBy('a.vendor_id'); 

        return $jumTrans;
         

  }

  public static function querySub($cat){

    $querySub = DB::table('order_product as a')
                   ->select('a.order_no')
                   ->leftJoin('product as b','b.id','=','a.product_id')
                   ->where('b.code_1','LIKE',''.$cat.'%')
                   ->groupBy('a.order_no'); 

     return $querySub;              

  }


  public static function TopVendor(){

        $result = DB::table("vpi_vendor")
                    ->select(DB::raw('max(vvh_id) max_vvh_id'),'vk_score_total','vendor_name')
                    ->groupBy('vvh_vendor_id')
                    ->orderBy('vk_score_total', 'DESC')
                    ->limit(10)->get();
     
     return $result;                 
  }



   public static function Category(){

      $category = DB::table('resources_code')->select('code as value','name as text')->where('level',2)->OrderBy('resources_code_id','DESC')->get();
      return $category;
   }

   public static function Devision(){

      $devision = DB::table('groups')->select('id as value','name as text')->get();
      return $devision;
   }

   public static function Project()
   {
       $__temp_ = array();
       
       //$groups = DB::table('groups')->where('id', Auth::User()->group_id)->first();

       $shipping = DB::table('project_new');
       $shipping->select('id as value','name as text');
       // if($groups->akses_proyek ==0 || $groups->akses_proyek ==null)
       // {
       //    $shipping->where('departemen_id',Auth::User()->group_id); 
       // } 
       $shipping->where('is_deleted',0);
       $result = $shipping->get();
       return $result;


   }


   public static function Vendor()
   {
       $__temp_ = array();
       $vendor = DB::table('vendor');
       $vendor->select('id as value','name as text');
       $result = $vendor->get();
       return $result;
   }


   public static function Products()
   {
       $__temp_ = array();
       $products = DB::table('product');
       $products->select('id as value','name as text');
       $products->where(['is_deleted'=>0]);
       $result = $products->get();
       return $result;
   }


  public static function get_penyerapan_dept($groupDept, $tahun, $category)
  {

    $cat = substr($category, 0, 2);
    

    $builder = DB::table("order as a");
    $builder->select('d.id','d.name', DB::raw('SUM(b.weight * b.qty) as total_volume'));
   
    $builder->join('order_product as b', function($join){
          $join->on('a.order_no', '=', 'b.order_no');
          $join->where('a.order_status', '<>', 3);
          //$join->whereYear('a.created_at', '=', '2021');
          $join->whereYear('a.created_at', '=', ''.$tahun.'');                                    
    });

    $builder->join('project_new as c', 'c.id','=','a.project_id');
    $builder->join('groups as d','d.id','=','c.departemen_id');
    $builder->join('product as e', 'e.id','=','b.product_id');
    $builder->where('e.code_1', 'LIKE' ,''.$cat.'%');

    if ($groupDept != "") 
    {
      $builder->where('d.id', $groupDept);
    }

    $builder->where('d.is_deleted', 0);
    $builder->where('c.is_deleted', 0);
    $builder->groupBy('d.id');
    $builder->orderBy('total_volume', 'DESC');

    $query = $builder->get();
    $ret = [];
    foreach ($query as $k => $v) {
      $ret[$v->id] = $v->name;
    }

    return $ret;
  }


  public static function get_penyerapan_dept_per_bulan($tahun, $arr_dept, $category)
  {

    $cat = substr($category, 0, 2);
    if (empty($arr_dept)) {
      return [];
    }

    $dept_in = [];
    foreach ($arr_dept as $k => $v) {
      $dept_in[] = $k;
    }

    $for_not_in[] = 3;

    $builder = DB::table("order as a");
    $builder->select(DB::raw('ROUND(SUM(b.qty * ROUND(b.weight,4)),4) as totalnya'),'e.id as departemen_id',DB::raw('EXTRACT(MONTH FROM a.created_at) as bulannya'));
    $builder->join('order_product as b', 'a.order_no','=','b.order_no');
    $builder->join('product as c', 'c.id','=','b.product_id');
    $builder->join('project_new as d', 'd.id','=','a.project_id');
    $builder->join('groups as e', 'e.id','=','d.departemen_id');
    $builder->where('c.code_1','LIKE',''.$cat.'%');
    $builder->where('e.is_deleted', 0);
    $builder->where('d.is_deleted', 0);
    $builder->whereNotIn('a.order_status', $for_not_in);  
    $builder->whereIn('e.id', $dept_in);
   // $builder->whereYear('a.created_at', '=', '2020');
    $builder->whereYear('a.created_at', '=', ''.$tahun.'%'); 
    $builder->groupBy('e.id');
    $builder->groupBy('bulannya');
    $builder->orderBy('bulannya', 'DESC');

    $query = $builder->get();
    $ret = [];
    foreach ($query as $k => $v) {
      $index = $v->departemen_id . "_" . $v->bulannya;
      $ret[$index] = $v->totalnya;
    }

    return $ret;
  }






   



  public static function get_top_5_vendor($groupDept, $tahun, $category_id)
  {
    $cat = substr($category_id, 0, 2);

    $limit = 10;
    $for_not_in[] = 3;

      $builder = DB::table("order as a");
      $builder->select(DB::raw('COUNT(*) as banyaknya'),DB::raw('COUNT(*) as totalnya'),'a.vendor_id','b.name as vendor_name');
      $builder->leftJoin('vendor as b', 'b.id', '=','a.vendor_id');

      $builder->leftJoin('order_product as op', 'a.order_no','=','op.order_no');
      $builder->leftJoin('product as p', 'p.id','=','op.product_id');

      $builder->leftJoin('project as c', 'b.id','=','c.vendor_id');
      $builder->leftJoin('project_new as c_new', 'c_new.id','=','a.project_id');

      $builder->leftJoin('users as d', 'a.created_by','=','d.id');
      $builder->whereNotIn('a.order_status', $for_not_in);
      $builder->where('b.is_deleted', 0);
      $builder->where('c.is_deleted', 0);

      $builder->where('code_1','LIKE',''.$cat.'%');
      $builder->whereYear('a.created_at', '=', '2021');
      //$builder->whereYear('a.created_at', '=', ''.$tahun.'')
      if ($groupDept != "")
      {
        $builder->where('c_new.departemen_id', $groupDept);
      }

      $builder->groupBy('a.vendor_id');
      $builder->orderBy('totalnya', 'desc');

      $query = $builder->get();
      //    echo $this->db->last_query();
      $ret = [];
      $for_not_in = [];
      foreach ($query as $k => $v) {
        $ret[$v->vendor_id] = $v->vendor_name;
        $for_not_in[] = $v->vendor_id;
      }


    return $ret;
  }


  public static function get_top_5_vendor_per_bulan($groupDept, $tahun, $arr_vendor, $category_id)
  {
    if (empty($arr_vendor)) {
      return [];
    }

    $vendor_in = [];
    foreach ($arr_vendor as $k => $v) {
      $vendor_in[] = $k;
    }

    $for_not_in2[] = 3;

    $splts = json_encode($vendor_in);
    $splt = str_replace(array("[", "]"), "", $splts);
    $cat = substr($category_id, 0, 2);

    $builder = DB::table("order as a");
    $builder->select(DB::raw('COUNT(*) as banyaknya'),DB::raw('COUNT(*) as totalnya'),'a.vendor_id',DB::raw('EXTRACT(MONTH FROM a.created_at) as bulannya'),'b.name as vendor_name');
    $builder->leftJoin('vendor as b', 'b.id', '=','a.vendor_id');

    $builder->join(DB::raw('(SELECT op.order_no FROM order_product as op left join product as pr ON pr.id = op.product_id WHERE pr.code_1 LIKE "'.$cat.'%"
    GROUP BY op.order_no)
               da'), 
    function($join)
    {
        $join->on('a.order_no', '=', 'da.order_no');
    });


  
    
     


     $builder->whereNotIn('a.order_status',$for_not_in2);
     $builder->whereIn('a.vendor_id', [$splt]);
     $builder->whereYear('a.created_at', '=', '2021');
      //$builder->whereYear('a.created_at', '=', ''.$tahun.'')

     $builder->groupBy('a.vendor_id');
     $builder->groupBy('bulannya');
     $builder->orderBy('bulannya', 'DESC');
     
    

 
      
      if ($groupDept != "")
      {
        $builder->where('c_new.departemen_id', $groupDept);
      }
      
      
  

      $query = $builder->get();

    $ret = [];
    foreach ($query as $k => $v) {
      $index = $v->vendor_id . "_" . $v->bulannya;
      $ret[$index] = $v->totalnya;
    }

    return $ret;
  }

  public static function Top10Product($tahun, $category){

    $cat = substr($category, 0, 2);
    $limit = 10;
    $for_not_in2[] = 3;
    $product = DB::table('order as a')
               ->select(DB::raw('COUNT(b.product_id) as total_volume'),'b.product_id','c.name as product_name')
               ->join('order_product as b','a.order_no','=','b.order_no')
               ->join('product as c','c.id','=','b.product_id')
               ->where('c.code_1','LIKE','%'.$cat.'%')
               ->whereNotIn('a.order_status',$for_not_in2)
               ->where('a.is_deleted',0)
               ->whereYear('a.created_at', '=', '2021')
               //->whereYear('a.created_at', '=', ''.$tahun.'')
               ->groupBy('b.product_id')
               ->orderBy('total_volume','DESC')
               ->limit(10)
               ->get();

    //echo $this->db->last_query();
    $ret = [];
    $for_not_in = [];
    foreach ($product as $k => $v) {
      $ret[$v->product_id] = $v->product_name;
      $for_not_in[] = $v->product_id;
    }

    // // jika lebih kecil dari 10, cari sisanya random;
    if (count($ret) < 10) {
      $sisa = $limit - count($ret);
      $query = DB::table('product');
                 $query->select('name','id');
                 $query->where('product.is_deleted', 0);
                 $query->limit($sisa);
                 $query->inRandomOrder();
                

      if (!empty($for_not_in)) {
         $query->whereNotIn('id', $for_not_in);
      }
      // q = query
      $results = $query->get();
      foreach ($results as $k => $v) {
        $ret[$v->id] = $v->name;
      }
    }
    return $ret;
  }

  public static function Top10ProductPerBulan($tahun, $category, $arr_product){

    $cat = substr($category, 0, 2);
    if (empty($arr_product)) {
      return [];
    }

    $product_in = [];
    foreach ($arr_product as $k => $v) {
      $product_in[] = $k;
    }
    $for_not_in2[] = 3;

    $query = DB::table('order as a')
              ->select(DB::raw('COUNT(b.product_id) as totalnya'),'b.product_id','c.name as product_name',DB::raw('EXTRACT(MONTH FROM a.created_at) as bulannya'))
              ->join('order_product as b','a.order_no','=','b.order_no')
              ->join('product as c','c.id','=','b.product_id')
              ->where('c.code_1','LIKE','%'.$cat.'%')
              ->whereNotIn('a.order_status',$for_not_in2)
              ->whereIn('b.product_id', $product_in)
              ->whereYear('a.created_at', '=', '2021')
               //->whereYear('a.created_at', '=', ''.$tahun.'')
              ->groupBy('b.product_id')
              ->groupBy('bulannya')
              ->orderBy('bulannya', 'DESC')
              ->get();
            
    // $ret = array();
    // foreach ($query as $row => $val)
    // {
    //   $ret[$row]['product_name'] = $val->product_name;
    //   $ret[$row][GeneralHelpers::mounth($val->created_at)] = $val->totalnya;
    // }

    $ret = [];
    foreach ($query as $k => $v) {
      $index = $v->product_id . "_" . $v->bulannya;
      $ret[$index] = $v->totalnya;
    }

    return $ret;

    



  }


  public static function GetForecast($month){


        $total_data = $k = '';       
        $builder =DB::table("forecast_dashboard");
        $builder->select(DB::raw('DATE_FORMAT(date, "%M %Y") as date'), 'value', 'harga_real');
        $data = $builder->get();

        // $this->db->select('DATE_FORMAT(date, "%M %Y") AS date, value, harga_real');
        // $data = $this->db->get('forecast_dashboard')->result_array();
        if ($month == 1) {
            array_pop($data);
            array_pop($data);
            $total_data = count($data);
            $k = 1;
        } elseif ($month == 2) {
            $total_data = count($data);
            array_pop($data);
            $k = 3;
            // print_r(count($data));
            // echo "<br>";
            // print_r($k);
            // die;
        } else {
            $total_data = count($data);
            $k = 3;
        }

        $result = $bulan = $value = $tigaatas = $tigabawah = $data_real = [];
        $i = 1;
        foreach ($data as $val) {
            $bulan[] = $val->date;
            $value[] = (int)$val->value;
            if ($val->harga_real > 0)
                $harga_real[] = (int)$val->harga_real;
            else
                $harga_real[] = '';

            if ($i > $total_data - $k) {
                $tigaatas[] = (int)$val->value + ($val->value * 3 / 100);
                $tigabawah[] = (int)$val->value - ($val->value * 3 / 100);
            } else {
                $tigaatas[] = '';
                $tigabawah[] = '';
            }
            $i++;
        }

        $result = [
            'bulan' => $bulan,
            'harga' => $value,
            'harga_real' => $harga_real,
            'hargaatas' => $tigaatas,
            'hargabawah' => $tigabawah,
        ];
        //print_r($result);
        return $result;

  }


  
  public static function PenggunaReg(){

    $userReg = DB::table('users')->where('active',1)->count();
    return $userReg;
  }
  
  public static function totalMatgis(){

    $matgis = DB::table('order')->where('is_matgis',1)->count();
    return $matgis;
  }

  public static function totalSumberDaya(){

    $Sumberdaya = DB::table('resources_code')->where('status',1)->count();
    return $Sumberdaya;
  }
   
  public static function totalTerdaftar(){

    $Sumberdaya = DB::table('product')->where('is_deleted',0)->count();
    return $Sumberdaya;
  }
   
  public static function Mounth(){
     $mounth = array();
      $text = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
      for( $i = 0; $i < 12; $i++ )
      {

         $mounth[$i]['value'] = $i+1;
         $mounth[$i]['text'] = $text[$i];
         
      }
    
    return $mounth;
  }

  public static function ConvertChart($dept,$bulan,$per_bulan){
     $arr_bulan = RequestDashboardEcatalog::_get_array_bulan_dan_warnanya();
     // index bulan key = warnanya, value = bulannya;
        $bulan_index = [];
        foreach ($arr_bulan as $k => $v) {
            foreach ($v as $k2 => $v2) {
                if ($k2 == 'warna') {
                    $bulan_index[$v2] = $k;
                }
            }
        }

        $series = [];
        // isi data default, sesuai dengan jumlah departemen yang ada;
        $data_default = [];
        foreach (range(1, count($dept)) as $v) {
            $data_default[] = 0;
        }
        // untuk menampung array vendor index;
        $dept_index = [];
        for ($i = $bulan; $i >= 1; $i--) {
            $data = $data_default;
            $inc = 0;
            foreach ($dept as $k => $v) {
                if (!in_array($k, $dept_index)) {
                    $dept_index[] = $k;
                }

                $index = $k . "_" . $i;
                if (array_key_exists($index, $per_bulan)) {
                    $data[$inc] = (int)$per_bulan[$index];
                }

                $inc++;
            }

            $series[] = [
                'name' => $arr_bulan[$i]['nama'],
                'data' => $data,
                'color' => $arr_bulan[$i]['warna'],
            ];
        }

        $ret = [
            'category' => array_values($dept),
            'series' => $series,
            'dept_index' => $dept_index,
            'bulan_index'  => $bulan_index,
        ];
       
       return $ret;

  }

    public static function get_data_chart_forecast()
    {
        $total_data = $k = '';
        //   $month = $this->input->post('month', true);
        $month =  $_POST['month'];
        $db = db_connect();
        $this->builder = $db->table("forecast_dashboard");
        $this->builder->select('DATE_FORMAT(date, "%M %Y") AS date, value, harga_real');

        $query = $this->builder->get();
        $data = $query->getResultArray();
        // $this->db->select('DATE_FORMAT(date, "%M %Y") AS date, value, harga_real');
        // $data = $this->db->get('forecast_dashboard')->result_array();
        if ($month == 1) {
            array_pop($data);
            array_pop($data);
            $total_data = count($data);
            $k = 1;
        } elseif ($month == 2) {
            $total_data = count($data);
            array_pop($data);
            $k = 3;
            // print_r(count($data));
            // echo "<br>";
            // print_r($k);
            // die;
        } else {
            $total_data = count($data);
            $k = 3;
        }

        $result = $bulan = $value = $tigaatas = $tigabawah = $data_real = [];
        $i = 1;
        foreach ($data as $val) {
            $bulan[] = $val['date'];
            $value[] = (int)$val['value'];
            if ($val['harga_real'] > 0)
                $harga_real[] = (int)$val['harga_real'];
            else
                $harga_real[] = '';

            if ($i > $total_data - $k) {
                $tigaatas[] = (int)$val['value'] + ($val['value'] * 3 / 100);
                $tigabawah[] = (int)$val['value'] - ($val['value'] * 3 / 100);
            } else {
                $tigaatas[] = '';
                $tigabawah[] = '';
            }
            $i++;
        }

        $result = [
            'bulan' => $bulan,
            'harga' => $value,
            'harga_real' => $harga_real,
            'hargaatas' => $tigaatas,
            'hargabawah' => $tigabawah,
        ];
       
        return $result;
    }

  public static function _get_array_bulan_dan_warnanya()
    {
        return [
            '1' => [
                'nama' => 'Jan',
                'warna' => '#fe7a03',
            ],
            '2' => [
                'nama' => 'Feb',
                'warna' => '#2b3537',
            ],
            '3' => [
                'nama' => 'Mar',
                'warna' => '#0594fc',
            ],
            '4' => [
                'nama' => 'Apr',
                'warna' => '#f3ca0c',
            ],
            '5' => [
                'nama' => 'Mei',
                'warna' => '#2fc621',
            ],
            '6' => [
                'nama' => 'Jun',
                'warna' => '#3f01cd',
            ],
            '7' => [
                'nama' => 'Jul',
                'warna' => '#3c5f70',
            ],
            '8' => [
                'nama' => 'Aug',
                'warna' => '#1c03fg',
            ],
            '9' => [
                'nama' => 'Sep',
                'warna' => '#34ff78',
            ],
            '10' => [
                'nama' => 'Okt',
                'warna' => '#12cd88',
            ],
            '11' => [
                'nama' => 'Nov',
                'warna' => '#cabede',
            ],
            '12' => [
                'nama' => 'Des',
                'warna' => '#e0069f',
            ],
        ];
    }

   


}