<?php

namespace App\Http\Request\Validation;
use Illuminate\Support\Facades\Validator;

use App\Http\Request\RequestCart;
use DB;

class ValidationOrder
{
   public static function validation($product,$product_check)
   {   
        
        $res = array();
        foreach($product as $key => $val)
        {
           
          foreach($product_check as $i => $value)
          {
            if($val->id == $value->id)
            {
                if($value->checklist == "true")
                {
                
                        $res[$key]['id'] =  $value->id; 
                        if($val->project_id < 1)
                        {
                          $res[$key]['project_message'] = "Projek belum dipilih!";
                        }else{
                          $res[$key]['project_message'] = "";
                        }
                       
                        if($val->shipping_id < 1)
                        {
                          $res[$key]['shipping_id_message'] = "Jenis pengiriman belum dipilih!";
                        }else{
                         $res[$key]['shipping_id_message'] = "";
                        }

                        if($val->transport ==true)
                        {

                             if($val->vendor_transport_id ==0)
                             {
                                $res[$key]['vendor_transport_id_message'] = "<span class='text-center span-alert-cart'>Vendor transportasi belum dipilih!</span>";
                             }else{
                                $res[$key]['vendor_transport_id_message'] = "";
                             }

                             if($val->transport_id ==0)
                             {
                                $res[$key]['transport_id_message'] = "<span class='text-center span-alert-cart'>Transportasi belum dipilih!</span>";
                             }else{
                                $res[$key]['transport_id_message'] = "";
                             }  

                             if($val->transport_name =="")
                             {
                                $res[$key]['price_transport_message'] = "<span class='text-center span-alert-cart'>Harga transportasi belum dipilih!</span>";
                             }else{
                                $res[$key]['price_transport_message'] = "";
                             }
                             
                             $res[$key]['up_transport'] =  $val->up_transport;

                             if($val->up_transport == "")
                             {
                                $res[$key]['up_transport_message'] = "UP transportasi belum ditulis!";  
                             }else{
                                $res[$key]['up_transport_message'] = ""; 
                             } 
                             
                             $res[$key]['perihal_transport'] =  $val->perihal_transport;
                             if($val->perihal_transport == "")
                             {
                                $res[$key]['perihal_transport_message'] = "Perihal transportasi belum ditulis!";  
                             }else{
                                $res[$key]['perihal_transport_message'] = ""; 
                             } 

                             $res[$key]['catatan_transport'] =  $val->catatan_transport;
                             if($val->perihal_transport == "")
                             {
                                $res[$key]['catatan_transport_message'] = "Catatan transportasi belum ditulis!";  
                             }else{
                                $res[$key]['catatan_transport_message'] = ""; 
                             } 


                        }

                        if($val->insurance ==true)
                        {
                            if($val->insurance_vendor_id < 1)
                            {
                               $res[$key]['insurance_vendor_id_message'] = "Asuransi belum dipilih!"; 
                            }else{
                               $res[$key]['insurance_vendor_id_message'] = "";
                            }    


                             $res[$key]['up_insurance'] =  $val->up_insurance;

                             if($val->up_insurance == "")
                             {
                                $res[$key]['up_insurance_message'] = "UP asuransi belum ditulis!";  
                             }else{
                                $res[$key]['up_insurance_message'] = ""; 
                             } 

                             $res[$key]['perihal_insurance'] =  $val->perihal_insurance;

                             if($val->perihal_insurance == "")
                             {
                                $res[$key]['perihal_insurance_message'] = "Perihal asuransi belum ditulis!";  
                             }else{
                                $res[$key]['perihal_insurance_message'] = ""; 
                             } 

                             $res[$key]['catatan_insurance'] =  $val->catatan_insurance;

                             if($val->catatan_insurance == "")
                             {
                                $res[$key]['catatan_insurance_message'] = "Catatan asuransi belum ditulis!";  
                             }else{
                                $res[$key]['catatan_insurance_message'] = ""; 
                             } 
                            
                            
                        } 

                        if($val->tgl_ambil == "0000-00-00" || $val->tgl_ambil == "null")
                        {
                            $res[$key]['tgl_ambil_message'] = "Tanggal belum dipilih!";  
                        }else{
                            $res[$key]['tgl_ambil_message'] = "";
                        }

                        if($val->up_product == "")
                        {
                            $res[$key]['up_product_message'] = "UP produk belum ditulis!";  
                        }else{
                            $res[$key]['up_product_message'] = ""; 
                        } 

                        if($val->perihal_product == "")
                        {
                            $res[$key]['perihal_product_message'] = "Perihal produk belum ditulis!";  
                        }else{
                            $res[$key]['perihal_product_message'] = ""; 
                        } 


                        if($val->catatan_product == "")
                        {
                            $res[$key]['catatan_product_message'] = "Catatan produk belum ditulis!";

                        }else{
                            $res[$key]['catatan_product_message'] = "";
                            
                        }


                        if($val->tgl_ambil == "")
                        {
                            $res[$key]['tgl_ambil_message'] = "Tanggal pengambilan belum diset!";

                        }else{
                            $res[$key]['tgl_ambil_message'] = "";
                            
                        }

              


                       if($res[$key]['project_message'] =="" && $res[$key]['shipping_id_message'] =="" && $res[$key]['tgl_ambil_message'] =="" && $res[$key]['up_product_message'] =="" && $res[$key]['perihal_product_message'] =="" && $res[$key]['catatan_product_message'] =="")
                       {


                           if($val->transport =='true')
                           {
                              if($res[$key]['vendor_transport_id_message'] =="" && $res[$key]['transport_id_message'] =="" && $res[$key]['price_transport_message'] =="")
                              {

                                $res[$key]['status'] =  true;
                              }else{
                                $res[$key]['status'] =  false;
                              }  

                           }else{
                              $res[$key]['status'] =  true;
                           } 

                            if($val->insurance =='true')
                            {
                                if($res[$key]['insurance_vendor_id_message'] =="")
                                {
                                   $res[$key]['status'] =  true;
                                }else{
                                   $res[$key]['status'] =  false;
                                }    

                            } else{
                                 $res[$key]['status'] =  true;
                            }
                       }else{
                        $res[$key]['status'] =  false;
                       }

                }else{
                   $res[$key]['status'] =  false; 
                }


               
            



                if($val->premi ==""){ $premi = 0; }else{ $premi = $val->premi; }
           
                $res[$key]['checklist'] =  $val->checklist;
                $res[$key]['project'] =    $val->project;
                $res[$key]['project_id'] =  $val->project_id;
                $res[$key]['pr_request'] = $val->pr_request;
                $res[$key]['pr_request_id'] = $val->pr_request_id;
                $res[$key]['shipping'] =  $val->shipping;
                $res[$key]['shipping_id'] =  $val->shipping_id;
                $res[$key]['transport'] =  $val->transport;
                $res[$key]['vendor_transport_id'] =  $val->vendor_transport_id;
                $res[$key]['transport_id'] =  $val->transport_id;
                $res[$key]['transport_fee'] =  $val->transport_fee;
                $res[$key]['transport_name'] =  $val->transport_name;
                $res[$key]['total_transport'] =  $val->total_transport;
                $res[$key]['insurance'] =  $val->insurance;
                $res[$key]['insurance_name'] =  $val->insurance_name;
                $res[$key]['insurance_min_price'] =  $val->insurance_min_price;
                $res[$key]['premi'] =  $premi;
                $res[$key]['total_insurance'] =  $val->total_insurance;
                $res[$key]['tgl_ambil'] =  $val->tgl_ambil;
                $res[$key]['perihal_product'] =  $val->perihal_product;
                $res[$key]['catatan_product'] =  $val->catatan_product;
               
                $res[$key]['type'] = $val->type;
                $res[$key]['location_id'] = $val->location_id;
                $res[$key]['location_name'] = $val->location_name;
                $res[$key]['vendor'] = $val->vendor;
                $res[$key]['project_address'] = $val->project_address;
                $res[$key]['product'] = $val->product;
                $res[$key]['min_weight_transport'] =  $val->min_weight_transport;
              
                $res[$key]['total_price'] =  $val->total_price;
                $res[$key]['total_volume'] =  $val->total_volume; 
                


              
                
                $res[$key]['vendor_transport'] = $val->vendor_transport;
                $res[$key]['vendor_transport_id'] = $val->vendor_transport_id;
                
            
              
                $res[$key]['list_vendor_transport'] =  true;
                

                if($val->vendor_transport_id =='')
                {
                      $res[$key]['loading_vendor_transport'] = "Pilih Vendor Transport";
                }    
              
                if($val->transport_id =='')
                {
                      $res[$key]['loading_transport_id'] = "Pilih Transportasi";
                } 

                if($val->min_weight_transport =='')
                {
                      $res[$key]['loading_transport_fee'] = "Pilih Harga";
                }    
                  

                if($val->insurance_vendor_id =='')
                {
                    $res[$key]['loading_list_insurance'] = "Pilih Vendor Asuransi";
                }  
                    
                     
                  $res[$key]['transport_logistic'] = $val->transport_logistic;
                  $res[$key]['transport_id'] = $val->transport_id;
                  $res[$key]['min_weight_transport'] = $val->min_weight_transport;

                  

                   if($val->transport_fee !=0)
                  {  
                         //transport_id,location_id,vendor_id,min_weight_transport,total_volume  
                      
                        $res[$key]['transport_price'] = $val->transport_price;
                        //$res[$key]['total_volume'] = $reqPrice->total_volume;
                        $res[$key]['transport_fee'] = $val->transport_fee;
                        $res[$key]['transport_name'] = $val->transport_name;          

                  }else{
                        $res[$key]['transport_price'] =[];
                       // $res[$key]['total_volume'] = 0;
                        $res[$key]['transport_fee'] = 0;
                        $res[$key]['transport_name'] = '';
                  }  


                   $res[$key]['division'] = $val->division;
                   $res[$key]['list_insurance']  = $val->list_insurance;
                   $res[$key]['insurance_vendor_id'] = $val->insurance_vendor_id;
                   $res[$key]['insurance_name'] = $val->insurance_name;
                   $res[$key]['insurance_min_price'] = $val->insurance_min_price;
                   $res[$key]['premi'] = $premi;
                   $res[$key]['total_insurance'] = $val->total_insurance;
                   
                    
                  
         

              
            }  
               
               
           } 

          
            
            
           // if($val['status'] =='true'){ $status = true;}else{$status = false;}    
           

        }    

      
            
       return array('message'=>$res,'type'=>'validation');
   }


 

 


   public static function checkForm($data)
   {
     
     $status = array();
     foreach($data as $i =>$val)
     {
        

         if($val['checklist'] ==true)
         {
            if($val['status'] == false)
            {
                
                $status['status'] = false; 
                $status['validation'] = true; 
                
            }else{
                $status['status'] = true;
                $status['validation'] = false; 
            }  

        
         }


     }
   
     $result = json_encode($status);
     $res = json_decode($result);
     return $res;   

   

   }


 
    

}
