<?php

namespace App\Http\Request\Validation;
use Illuminate\Support\Facades\Validator;

class ValidationRequestOTP
{
   public static function validation($request){
        $err = array(); 
        
        $fields = [
            'status_id'  => 'Status',
            'level_id' => 'Alasan',
            'noted'  => 'Keterangan',
        ];

        $validator =  Validator::make($request->all(), 
        [
            'status_id'  => 'required',
            'level_id'  => 'required',
            'noted'  => 'required',
    
        ]);

        $validator->setAttributeNames($fields); 
        if ($validator->fails()) {
         
            $errors = $validator->errors();
            
            if($errors->has('status_id')){
                $err['messages']['status_id'] = $errors->first('status_id');
            }

            if($errors->has('level_id')){
                $err['messages']['level_id'] = $errors->first('level_id');
            }

            if($errors->has('noted')){
                $err['messages']['noted'] = $errors->first('noted');
            }


            return $err;
       }
  }

}
