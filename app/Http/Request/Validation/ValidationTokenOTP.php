<?php

namespace App\Http\Request\Validation;
use Illuminate\Support\Facades\Validator;

class ValidationTokenOTP
{
   public static function validation($request){
        $err = array(); 
        
        $fields = [
            'otp_code'  => 'Kode OTP',
        ];

        $validator =  Validator::make($request->all(), 
        [
            'otp_code'  => 'required',
        ]);

        $validator->setAttributeNames($fields); 
        if ($validator->fails()) {
         
            $errors = $validator->errors();
            
            if($errors->has('otp_code')){
                $err['messages']['otp_code'] = $errors->first('otp_code');
            }

            return $err;
       }
  }

}
