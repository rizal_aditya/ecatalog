<?php

namespace App\Http\Request\Validation;
use Illuminate\Support\Facades\Validator;

class ValidationProduct
{
   public static function validation($request){
        $err = array(); 
        
        $fields = [
            'name'  => 'Nama Produk',
            'category_id1' => 'Kategori',
            'uom_id'  => 'Satuan Berat',
            'width'  => 'Panjang',
            'height'  => 'Lebar',
            'tod_id'  => 'Term of delivery',
            'description' =>'Deskripsi',
            'date_price' => 'Tanggal berlaku',
        ];

        $validator =  Validator::make($request->all(), 
        [
            'name'  => 'required',
            'category_id1'  => 'required',
            'uom_id'  => 'required',
            'width'  => 'required|max:155',
            'height'  => 'required|max:155',
            'tod_id'  => 'required',
            'description'  => 'required',
            'date_price'  => 'required',
        ]);

        $validator->setAttributeNames($fields); 
        if ($validator->fails()) {
         
            $errors = $validator->errors();
            
            if($errors->has('name')){
                $err['messages']['name'] = $errors->first('name');
            }

            if($errors->has('category_id1')){
                $err['messages']['category_id1'] = $errors->first('category_id1');
            }

           
            
            if($errors->has('uom_id')){
                $err['messages']['uom_id'] = $errors->first('uom_id');
            }

            if($errors->has('width')){
                $err['messages']['width'] = $errors->first('width');
            }

            if($errors->has('height')){
                $err['messages']['height'] = $errors->first('height');
            }

             if($errors->has('tod_id')){
                $err['messages']['tod_id'] = $errors->first('tod_id');
            }

            if($errors->has('description')){
                $err['messages']['description'] = $errors->first('description');
            }

            if($errors->has('date_price')){
                $err['messages']['date_price'] = $errors->first('date_price');
            }

            return $err;
       }
  }

}
