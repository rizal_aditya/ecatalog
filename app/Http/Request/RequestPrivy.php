<?php

namespace App\Http\Request;
use Auth;
use DB;
use App\Models\User;
use App\Models\Order;
use App\Http\Request\RequestCart;
use App\Http\Request\RequestOrder;
use App\Models\OrderProduct;
use App\Models\OrderTransport;
use Illuminate\Support\Facades\Http;
class RequestPrivy
{

   public static function DocumentUpload($id,$order_no,$no_surat,$sender)
    {
        $timestamp = date("c", strtotime(date('Y-m-d H:i:s')));

        $url = $sender['access'];
        $base64 = $sender['data']['filename'];
        $title = $sender['data']['title'];
        $recipients = $sender['data']['recipients'];
       

        $URL =  env('URL_DEV_HASH') . '/'.$url;
        $config['MERCHANT_KEY'] = env('MERCHANT_KEY');
        $config['USERNAME'] = env('USERNAME');
        $config['PASSWORD'] = env('PASSWORD');
        $config['MERCHANT_KEY'] = env('MERCHANT_KEY');
        $config['CLIENT_ID'] = env('CLIENT_ID');
        $config['CLIENT_SECRET'] = env('CLIENT_SECRET');
        
        
        $dataUploadPrivy['title'] = $title;
        $dataUploadPrivy['document'] = $base64;
        $dataUploadPrivy['recipients'] = $recipients;
        $test = [$dataUploadPrivy];
        $signature = RequestPrivy::signature($test, 'POST', $timestamp);


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST => 1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($test),
            CURLOPT_HTTPHEADER => array(
                'X-Authorization-Signature: ' . $signature,
                'X-Authorization-Timestamp: ' . $timestamp,
                'X-Flow-Process: default',
                'cache-control: no-cache',
                'Content-Type: application/json',
                'Merchant-Key:' . $config['MERCHANT_KEY'],
                'User-Agent: wika/1.0'
            ),
        ));

        $response = curl_exec($ch);
        curl_close ($ch);
        $result = json_decode($response);
       // $dataPrivy = RequestPrivy::ResponsePrivy($id,$result,$order_no,$no_surat); 
        return $result;   
        
    
    }


    public static function SignRequest($id,$type,$order_no,$level_id,$status_id,$noted,$no_surat,$sender){

         # code...
        $timestamp = date("c", strtotime(date('Y-m-d H:i:s')));
        
        $url = $sender['access'];
        $token = $sender['data']['token'];
        $privyid = $sender['data']['privyid'];
       

        $URL =  env('URL_DEV_HASH') . '/'.$url;
        $config['MERCHANT_KEY'] = env('MERCHANT_KEY');
        $config['USERNAME'] = env('USERNAME');
        $config['PASSWORD'] = env('PASSWORD');
        $config['MERCHANT_KEY'] = env('MERCHANT_KEY');
        $config['CLIENT_ID'] = env('CLIENT_ID');
        $config['CLIENT_SECRET'] = env('CLIENT_SECRET');


        $dataUploadPrivy['doc_token'] = $token;
        $dataUploadPrivy['identifier'] = $privyid;

        $test = $dataUploadPrivy;
        $signature = RequestPrivy::signature($test, 'POST', $timestamp); 

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST => 1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($test),
            CURLOPT_HTTPHEADER => array(
                'X-Authorization-Signature: ' . $signature,
                'X-Authorization-Timestamp: ' . $timestamp,
                'X-Flow-Process: default',
                'cache-control: no-cache',
                'Content-Type: application/json',
                'Merchant-Key:' . $config['MERCHANT_KEY'],
                'User-Agent: wika/1.0'
            ),
        ));


         $response = curl_exec($curl);
         curl_close($curl);

        $hasil = json_decode($response);
        $result = RequestPrivy::ResponsePrivy($id,$type,$hasil,$order_no,$level_id,$status_id,$noted,$no_surat); 
        return $result;
        
    }



    public static function SignProcess($id,$type,$order_no,$level_id,$status_id,$noted,$no_surat,$sender)
    {
        // send otp
        # code...
        $timestamp = date("c", strtotime(date('Y-m-d H:i:s')));
        $url = $sender['access'];
        $token = $sender['data']['token'];
        $privyid = $sender['data']['privyid'];
        $otp = $sender['data']['otp'];

        $URL =  env('URL_DEV_HASH') . '/'.$url;
        $config['MERCHANT_KEY'] = env('MERCHANT_KEY');
        $config['USERNAME'] = env('USERNAME');
        $config['PASSWORD'] = env('PASSWORD');
        $config['MERCHANT_KEY'] = env('MERCHANT_KEY');
        $config['CLIENT_ID'] = env('CLIENT_ID');
        $config['CLIENT_SECRET'] = env('CLIENT_SECRET');


        // $dataUploadPrivy['doc_token'] = "NPRMSCRPRT-1271cf25ce51953b0431434099bf4203bfd307bfc7ce778f511bbf51d00b4e59b0c8f1";
        $dataUploadPrivy['doc_token'] = $token;

        $dataUploadPrivy['identifier'] = $privyid;
        $dataUploadPrivy['reason'] = "For testing only";
        $dataUploadPrivy['otp_code'] = $otp;

        $test = $dataUploadPrivy;
        $signature = RequestPrivy::signature($test, 'POST', $timestamp); 


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST => 1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($test),
            CURLOPT_HTTPHEADER => array(
                'X-Authorization-Signature: ' . $signature,
                'X-Authorization-Timestamp: ' . $timestamp,
                'X-Flow-Process: default',
                'cache-control: no-cache',
                'Content-Type: application/json',
                'Merchant-Key:' . $config['MERCHANT_KEY'],
                'User-Agent: wika/1.0'
            ),
        ));


        $response = curl_exec($curl);
         curl_close($curl);

        $hasil = json_decode($response);
        $result = RequestPrivy::ResponsePrivy($id,$type,$hasil,$order_no,$level_id,$status_id,$noted,$no_surat); 
        return $result;
        
    }

    public static function ResponsePrivy($id,$type,$result,$order_code,$level_id,$status_id,$noted,$no_surat){

         if($result->status =="SUCCESS")
         {
               
                //mencari aprroval
                $checkSequece = RequestCart::checkSequece($order_code);
                $sequece = $checkSequece + 1;
                if($type =="document")
                {
                  $check = RequestCart::checkApprov($order_code);
                  if($check > 0)
                  {
                       $update = DB::table('order')->where('id',$id)->update(['token'=>$result->data[0]->doc_token]);
                 
                   }else{
                        $InsertApprov = RequestCart::InsertApprov($order_code); //insert Approval  
                    
                   }  
                  

                  
                  $access = RequestCart::AccessApprov($order_code,$sequece); 
                  $SendNotif = RequestPrivy::SendNotif($order_code,$access,$sequece,$no_surat); 
                  $SendWa =  RequestPrivy::SendWa($order_code,$access,$no_surat);


                  return  array('status'=>$result->status,'send_wa'=>$SendWa,'token'=>$result->data[0]->doc_token,'messages'=>' Dokumen No order '.$order_code.' berhasil disetujui.','approval'=>$check.' Manager'); 
                }else{

                    if($type =="step1")
                    {
                     
                        DB::table('request_otp')->insert(['order_no'=>$order_code,'level_id'=>$level_id,'status_id'=>$status_id,'noted'=>$noted,'user_id'=>Auth::User()->id]);

                        

    
                      $UserApprov =  Auth::User()->first_name.' '.Auth::User()->last_name; 
                      return  array('status'=>'SUCCESS','messages'=>'Request token privy aproval '.$sequece.' '.$UserApprov.' berhasil'); 
                    }else{
                       
                       $check = RequestCart::checkApprov($order_code);
                       if($sequece <  $check)
                       {
                          $LogApproval = RequestPrivy::LogApproval($order_code);
                          $updateStatusApprov = RequestPrivy::UpdateStatusApprov($order_code,$sequece);
                          $UpdateSquenceOrder = RequestPrivy::UpdateSquenceOrder($order_code,$sequece);
                          $seq =  $sequece + 1;
                          $access = RequestCart::AccessApprov($order_code,$seq);
                          
                          $penerima_id = RequestCart::RoleUserID($access->role_id); 
                          $user =  User::select('first_name','last_name')->where('id',$penerima_id)->first(); 
                          $UserApprov = $user->first_name.' '.$user->last_name;
                          $SendNotif =  RequestPrivy::SendNotif($order_code,$access,$no_surat); 
                          $SendWa =  RequestPrivy::SendWa($order_code,$access,$no_surat);

                          
                          return array('status'=>'SUCCESS','send_wa'=>$SendWa,'messages'=>'Dokumen privy approval '.$sequece.' berhasil di verifikasi,  menunggu approval '.$seq.' '.$UserApprov.''); 
                         
                          
                       }else{
                           $LogApproval = RequestPrivy::LogApproval($order_code);
                           $updateStatusApprov = RequestPrivy::UpdateStatusApprov($order_code,$sequece);
                           $complite =  RequestPrivy::UpdateOrderComplite($order_code);
                           //replace document
                           $url = env('SERVER_BACKEND').'api_new/order/approved_pdf';
                           $action ="replace";
                           $replace = RequestPrivy::ServerBackend($id,$order_code,$no_surat,$url,$action);
                           
                           $transport = RequestOrder::CheckTransportLCTS($order_code);
                           if($transport['status'] == true)
                           { 
                                $Createtoken = RequestPrivy::CreateTokenLCTS();
                                $token = $Createtoken['access_token'];
                                $lcts =  RequestPrivy::DataLCTS($order_code);
                                $data = json_encode($lcts);

                                $response = RequestPrivy::RequestLCTS($token,$data);

                                if($response['success'] ==true)
                                {
                                         $update = DB::table('order')
                                           ->where('order_no',$order_code)
                                              ->update(['so_transportasi'=>$response['data']['order_code']]);

                                         // return response()->json(['status'=>'SUCCESS','send_wa'=>$SendWa,'messages'=>'Dokumen privy approval '.$sequece.' & Projek LCTS berhasil di verifikasi,  menunggu approval '.$seq.' '.$UserApprov.''],200);
                                          
                                        $uploadLCTS = RequestPrivy::UploadPoLCTS($order_code);
                                        return response()->json(['status'=>'SUCCESS','messages'=>'Dokumen privy approval & Projek LCTS berhasil di verifikasi'],200); 

                                        // return response()->json(['status'=>'SUCCESS','action'=>'privy','data'=>$response['data']['order_code'],'messages'=>'Projek LCTS berhasil'],200);  

                                }else{
                                        return response()->json(['status'=>'ERROR','action'=>'privy','data'=>[],'messages'=>'Projek Ecatalog belum terdaftar di ESCM'],400); 
                                }  



                           }else{
                                return response()->json(['status'=>'ERROR','action'=>'lcts','data'=>'','messages'=>'Request Token LCTS Gagal'],400); 

                           }


                           //return array('status'=>'SUCCESS','messages'=>'Dokumen privy berhasil di verifikasi'); 
                       }
                            
                           
                    }    
                 
                }     
              
         }else{
        
           return response()->json(['status'=>'ERROR','action'=>'privy','messages'=>$result->message.' '.$result->errors],400);
        } 

          
    }

    public static function RequestLCTS($token,$data)
    {
        $url = env('SERVER_LCTS',true).'api/sync/create-so';
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json','Authorization: Bearer '.$token]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $response = curl_exec($ch); 
        curl_close($ch); 
        $response = json_decode($response, true);

        return $response;

    }


    public static function UploadPoLCTS($order_code)
    {
        $url = env('SERVER_BACKEND',true).'api_new/order/upload_lcts';
        $data = array('order_no'=>$order_code);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $response = curl_exec($ch); 
        curl_close($ch); 
        $response = json_decode($response, true);

        return $response;

    }


    public static function SendNotif($order_code,$access,$no_surat)
    {

        if($access)
        {
            
            $penerima_id = RequestCart::RoleUserID($access->role_id); 
            $message = "No Order " .  $order_code . " dengan no surat " . $no_surat . " Perlu di approve";
          return  RequestCart::SendNotif(Auth::User()->id,$penerima_id,$message);
        }

    }

    public static function SendWa($order_code,$access,$no_surat)
    {

        if($access)
        {
            $api_key = env('APIKEYWA');
            $sender =  env('SENDERWA');
            $penerima_id = RequestCart::RoleUserID($access->role_id); 
            //$phone = DB::table('users')->where('id',$penerima_id)->first()->phone;
            $phone = '6285726242220';
            $message = "No Order " .  $order_code . " dengan no surat " . $no_surat . " Perlu di approve";
            
             $input = array('api_key'=>$api_key,'sender'=>$sender,'number'=>$phone,'message'=> $message);
           
             $url = "https://wa-api.wika.co.id/send-message";
             $response = Http::post($url,$input);
             $result = json_decode($response);

            return $result;
        }

    }


    public static function LogApproval($order_code){
         $data = DB::table('request_otp')->where('order_no',$order_code)->first();
         if($data !=null)
         {
             DB::table('log_cancel_po')->insert(['id_alasan'=>$data->level_id,'status_cancel'=>$data->status_id,'keterangan'=>$data->noted,'user_id'=>Auth::User()->id,'no_order'=>$order_code]);
             DB::table('request_otp')->where(['order_no'=>$order_code,'user_id'=>Auth::User()->id])->delete();
         } 
    
   }

   public static function UpdateStatusApprov($order_code,$sequence)
   {
      DB::table('approve_po_list as a')
      ->where(['a.order_no'=>$order_code,'a.sequence'=>$sequence])
      ->update(['status_approve'=>1]);

   }

     public static function UpdateSquenceOrder($order_no,$sequence){
         $approv = DB::table('order')->where(['order_no'=>$order_no])
                 ->update(['order_no'=>$order_no,'approve_sequence'=>$sequence]);
        return $approv;

    }

    public static function UpdateOrderComplite($order_code){
       //hitung jumlah sequnce 
        $count = DB::table('approve_po_list')->where('order_no',$order_code)->count();
        DB::table('order')->where('order_no',$order_code)->update(['approve_sequence'=>$count,'is_approve_complete'=>1,'order_status'=>2]);


    }

    public static function CreateTokenLCTS(){
        
        $clientId =  env('CLIENTID_LCTS',true);
        $secret_key =  env('SECRETKEY_LCTS',true);
      
        $post = array('client_id'=>$clientId,'secret_key'=>$secret_key);

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, env('SERVER_LCTS',true).'/api/auth/create-token');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $response = curl_exec($ch); 
        curl_close($ch); 
          $response = json_decode($response, true);     
        return $response;

        


    }

    public static function DataLCTS($order_code){


          $lcts = Order::where('order_no',$order_code)->first();
          $mounth = date("Y");
          $number = RequestPrivy::CreateNumberLCTS($mounth);
          $ship = RequestPrivy::ShipmentLCTS($lcts,$order_code,$mounth);
         
          $arr = array();
        
          $arr['order_desc'] = 'Pemesanan '.$lcts->project->name;
          $arr['eta'] = date("Y-m-d");
          $arr['project_code'] = $lcts->project->no_spk;
          $arr['division_code'] = DB::table('groups')->where('id',$lcts->project->departemen_id)->first()->departemen_code2;
          // $arr['project_code'] = 'JIS';
          // $arr['division_code'] = 'DSU1';
          
          $arr['created_by'] =  $lcts->user->username;
          $arr['currency'] = 'IDR';
          $arr['tot_amount'] = $lcts->total_price;
          
          $insurance = RequestOrder::CheckInsuranceLCTS($lcts->order_no);

          if($insurance['status'] ==true )
          {
             $arr['insurance_vendor_name'] = $insurance['vendor_name'];
             $arr['insurance_amount'] = $insurance['total_insurance'];
             $arr['insurance_cover_code'] = $insurance['vendor_nocargo']; 
            

          }else{
             $arr['insurance_vendor_name'] = "";
             $arr['insurance_amount'] = 0;
             $arr['insurance_cover_code'] = "";
          }

          $transport = RequestOrder::CheckTransportLCTS($lcts->order_no);
          if($transport['status'] ==true )
          {
             $arr['transport_vendor_id'] = $transport['vendor_transport_id'];
             $arr['transport_vendor_name'] = $transport['vendor_transport'];
             $arr['transport_amount'] = $transport['total_transport']; 
             $arr['transport_unit_price'] = $transport['fee_transport']; 
             $arr['transport_uom'] ='KG';
             $arr['contract_no_transport'] = $lcts->no_surat;

          }else{
             $arr['transport_vendor_id'] = "";
             $arr['transport_vendor_name'] = 0;
             $arr['transport_amount'] = "";
             $arr['transport_unit_price'] = "";
             $arr['transport_uom'] ='';
             $arr['contract_no_transport'] ='';
          }
         
          // $arr['transport_vendor_id'] = (String)RequestOrder::CheckTransportLCTS($lcts->order_no)['vendor_transport_id'];
          // $arr['transport_vendor_name'] = RequestOrder::CheckTransportLCTS($lcts->order_no)['vendor_transport'];
          // $arr['transport_amount'] = RequestOrder::CheckTransportLCTS($lcts->order_no)['total_transport'];

           // $arr['transport_vendor_id'] = '47617';
           // $arr['transport_vendor_name'] = 'POS LOGISTIK INDONESIA';
           // $arr['transport_amount'] = 1330755;

           $arr['shipment_planning'] = $ship;


          return $arr;
    }

    public static function ShipmentLCTS($data,$order_code,$mounth){
          $ship = array();
          $orderTransport = DB::table('order_transportasi as a')
                           ->where(['a.order_no'=>$order_code,'b.is_deleted'=>0])
                           ->join('transportasi as c','a.transportasi_id','=','c.id')
                           ->join('kontrak_transportasi as b','c.vendor_id','=','b.vendor_id')->get();
         $res = RequestPrivy::ResourcesLCTS($data,$order_code);

         foreach($orderTransport as $key => $val)
         { 
             $ship[$key]['shipment_name'] = 'Pemesanan '.$data->project->name;
             $ship[$key]['po_no_sap'] = '';
             $ship[$key]['eta'] = date("Y-m-d");
             $ship[$key]['contract_no'] = $data->no_surat;
             $ship[$key]['contract_name'] = $data->project->name;
             $ship[$key]['sign_date'] =  $val->tgl_kontrak;
             $ship[$key]['start_date'] = $val->start_date;
             $ship[$key]['contract_amount'] = $data->total_price;
             $ship[$key]['material_vendor_name'] = $data->vendor_name;

           
         }
         return $ship;

    }

    public static function ResourcesLCTS($data,$order_code){
            $res = array();

            $product = OrderProduct::where('order_no',$order_code)->get();
            $vendor_location = DB::table('vendor_lokasi')->where('wilayah_id',$data->location_id)->first();
            foreach($product as $key => $val)
            { 


                $res[$key]['jml_barang'] = $val->weight;
                $res[$key]['qty'] = $val->qty;
                $res[$key]['weight'] = $val->qty*$val->weight;
                $res[$key]['item_code'] =  $val->product_id;
                $res[$key]['item_name'] = $val->product->name;
                $res[$key]['item_price'] = $val->price;
                $res[$key]['uom'] = $val->uom_name;
                $res[$key]['weight_uom'] = $val->uom_name;
                $res[$key]['incoterm_id'] = 'EXW';
                $res[$key]['incoterm_location'] = '';
                $res[$key]['pickup_address'] = $vendor_location->alamat;
                $res[$key]['pickup_google_address'] =  $vendor_location->alamat;
                $res[$key]['pickup_lat'] = $vendor_location->latitude;
                $res[$key]['pickup_long'] = $vendor_location->longitude;
                $res[$key]['pickup_pic'] = '';
                $res[$key]['pickup_phone'] = '';
                $res[$key]['pickup_phone_code'] = '+62';
                $res[$key]['delivery_address'] = $data->project->alamat;
                $res[$key]['delivery_google_address'] = $data->project->alamat;
                $res[$key]['delivery_lat'] = $data->project->lat;
                $res[$key]['delivery_long'] = $data->project->long;
                $res[$key]['delivery_pic'] = '';
                $res[$key]['delivery_phone'] = $data->project->no_hp;
                $res[$key]['delivery_phone_code'] = '+62';
                $res[$key]['shipment_note'] = $data->perihal;
                $res[$key]['ready_date'] = $data->tgl_diambil;
                $res[$key]['project_note'] = $data->perihal;
           

           }
          
          return $res;
    }

    

    public static function CreateNumberLCTS($mounth){

      $lctc_number = DB::table('order')->whereYear('created_at',$mounth)->count();
      if($lctc_number <1)
      {
        $number = '001';
      }else{
        if($lctc_number <10)
        {
           $number = '00'.$lctc_number; 
        }else if($lctc_number <100){
           $number = '0'.$lctc_number; 
        }else if($lctc_number <1000){
           $number = $lctc_number; 
        }else{
           $number = $lctc_number;   
        }    
        
      }  
      return $number; 
    }


     public static function ServerBackend($id,$order_no,$no_surat,$url,$action)
    {

          $input = array('order_no'=> $order_no);
          $response = Http::post($url,$input);
          $result = json_decode($response);
         
          if($action =="generate")
          {
            $dataPrivy = RequestPrivy::ResponseBackendPrivy($id,$order_no,$no_surat);
            return $dataPrivy;  
          }else{
            return $result;
          } 
        
    }



    public static function ResponseBackendPrivy($id,$order_code,$no_surat)
    {
          $check = RequestCart::checkApprov($order_code);
          if(!$check)
          {
            //$token = "NPRMSCRPRT-1271cf25197f81526feb166b2114f4be4b3a6bb40ffffda341fff8f1ae5a4bf44261be";
           // $update = DB::table('order')->where('id',$id)->update(['token'=>$token]);
            $InsertApprov = RequestCart::InsertApprov($order_code); //insert Approval  
          }   
          //mencari aprroval
            $checkSequece = RequestCart::checkSequece($order_code);
            $sequece = $checkSequece + 1;
            $access = RequestCart::AccessApprov($order_code,$sequece);
            if($access)
            {
                $penerima_id = RequestCart::RoleUserID($access->role_id); 
                $message = "No Order " .  $order_code . " dengan no surat " . $no_surat . " Perlu di approve";
                $notif =  RequestCart::SendNotif(Auth::User()->id,$penerima_id,$message);
            }
            return  array('status'=>"SUCCESS",'messages'=>'Upload dokumen privy berhasil','notif'=>$notif); 
    }

    public static function signature($jsonBody, $method, $timestamp)
    {
        
        $clientId = env('CLIENT_ID'); 
        $clientSecret = env('CLIENT_SECRET'); 
        $jsonBody2 = json_encode($jsonBody);
        $jsonBody2 = trim(preg_replace('/\s/', '', $jsonBody2));
        $jsonBody2 = trim(preg_replace('/\n/', '', $jsonBody2));
        $jsonBody2 = trim(str_replace('\\', '', $jsonBody2));
        $bodyMD5 = md5($jsonBody2, true);
        $bodyMD5 = base64_encode($bodyMD5);

        $hmac_signature = $timestamp . ":" . $clientId . ":" . $method . ":" . $bodyMD5;
        $hmac = hash_hmac('sha256', $hmac_signature, $clientSecret, true);
        $hmac_base64 = base64_encode($hmac);

        $signature = "#" . $clientId . ":#" . base64_encode($hmac);
        $signature = base64_encode($signature);

        return $signature;
    }

    public static function sendNotix() {
 
        $penerima_id = RequestCart::RoleUserID(304); 
        $user = DB::table('users')->select('first_name','last_name','photo')->where('id',$penerima_id)->first();

        $imageIcon = url('img/logo.png');
        $title = Auth::User()->first_name.' '.Auth::User()->last_name.' meminta persetujuan';
        $summary = "No Order 1905230006 dengan no surat TP.01.03/B.DSU1.4715/2019 Perlu di approve";

        $image = Auth::User()->photo;
        $url = "";
        $fields = array(
            'ttl' => 120,
            'message' => array(
                'icon' => $imageIcon,
                'image' => $image,
                'text' => $summary,
                'title' => $title,
                'url' => $url . '?utm_source=push_notification&utm_medium=notix'
            )
        );

        $fields = json_encode($fields);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://notix.io/api/send?app=1004f86b362c0431dd8d1209de0b48b",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $fields,
            CURLOPT_HTTPHEADER => array(
                'Authorization-Token: 70135d012b2240225a22a20224b0c49b14684f63316b08d2',
                'Content-Type: application/json'
              ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        set_time_limit(0);
        curl_close($curl);

        if ($err) {
             return $err;
        }

        return $response;
    }


   


}