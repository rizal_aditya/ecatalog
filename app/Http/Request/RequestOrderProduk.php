<?php

namespace App\Http\Request;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\ResourceCode;
use App\Models\ProjectProductPrice;
use App\Models\ProductGallery;
use App\Models\PaymentProduct;
use App\Models\PaymentMethod;
use App\Models\Location;
use App\Models\ProjectProduct;
use App\Models\Project;
use App\Models\Vendor;
use App\Models\VpiVendor;
use App\Models\Wishlist;
use App\Http\Request\RequestAuth;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestOrder;
use DB;

class RequestOrderProduk 
{
   
     
   public static function OrderProduct($order_code){
     $temp = array();
     $product = DB::table('order_product as a')
                 ->select(
                  'b.id',
                  'b.name as product_name',
                  'a.qty',
                  'a.price',
                  'a.weight',
                  'b.uom_id',
                  'b.code_1 as sda_code', 

               )
                 ->join('product as b','a.product_id','=','b.id')
                 ->where(['a.order_no'=>$order_code])
                 ->limit(2)
                 ->get();

     
      foreach ($product as $key => $val)
      {
           $temp[$key]['id'] = $val->id;
           $temp[$key]['photo'] = RequestFrontend::PhotoProduct($val->id);
           $temp[$key]['product_name'] = RequestFrontend::limitText($val->product_name,'50');
           $temp[$key]['qty'] = $val->qty;
           $temp[$key]['weight'] = $val->weight.' Kg';
           $temp[$key]['volume'] = $val->weight;
           $temp[$key]['total_volume'] = $val->weight * $val->qty;
           $temp[$key]['payment'] = RequestOrder::OrderPayment($order_code);
           $temp[$key]['price'] = GeneralHelpers::Rupiah($val->price);
           $temp[$key]['total_price'] = GeneralHelpers::Rupiah($val->qty * $val->weight * $val->price);
          
      }       

      return $temp;

  
   }

    public static function TotalProduct($order_code){

       $payment = DB::table('order_product')
        ->select(DB::raw('SUM(qty) AS total_qty'),DB::raw('count(id) AS total_item'),DB::raw('SUM(qty*weight*price) AS total_price'), DB::raw('SUM(qty*weight) AS total_volume'))
        ->where('order_no',$order_code)
        ->first();

        if($payment)
        { 
           $res = array('total_item'=>$payment->total_item,'total_qty'=>$payment->total_qty,'total_price'=>$payment->total_price,'total_volume'=>$payment->total_volume);
        }else{
            $res = array('total_item'=>'','total_qty'=>'','total_price'=>'','total_volume'=>'');
        } 
         $result = json_decode(json_encode($res), FALSE);
         return $result;
        
    }

   


    public static function TotalAll(){
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order as a');
        $order->join('users as b','a.created_by','=','b.id');
        $order->where(['a.vendor_id'=>$id,'a.is_approve_complete'=>1]);
        $results = $order->count();
        return $results;
 
    }

     public static function TotalNew(){
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order as a');    
        $order->join('users as b','a.created_by','=','b.id');
      
        $order->where(['a.vendor_id'=>$id,'a.is_approve_complete'=>1]);
        $order->where('a.order_status',2);
        $results = $order->count();

        return $results;
 
    }

    public static function TotalRev(){
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order as a');
        $order->join('users as b','a.created_by','=','b.id');
        $order->where(['a.vendor_id'=>$id,'a.is_approve_complete'=>1]);
        $order->where('a.order_status',4);
        $results = $order->count();

        return $results;
 
    }

  
   

}