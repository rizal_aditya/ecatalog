<?php

namespace App\Http\Request\Search;
use App\Http\Request\RequestFrontend;
use DB;

class SearchFrontend 
{
   
   

    
   public static function SearchTextHeader($request,$query)
   {
        // mencari inputan text
        if($request->filter_type =='input_text')
        {
            $query->leftJoin('vendor as g','c.vendor_id','=','g.id');
            $query->where(function($subQuery) use ($request) {
                $subQuery->where('g.name', 'LIKE', '%' . $request->text_search . '%')
                         ->orWhere('c.name', 'LIKE', '%' . $request->text_search . '%');
            }); 
        } 

        return $query;

   }

   public static function SearchProject($request,$query)
   {
                       
        if($request->pr_request_id =='')
        {
            if($request->project_id)
            {
                 $query->leftJoin('pr_request_product as b','c.code_1','=','b.kode_sda')
                    ->join('pr_request as a','b.pr_request_id','=','a.id');
                $query->where('a.project_id', $request->project_id);//project  
            }    
        } 

        return $query; 

   }


   public static function SearchPRRequest($request,$query)
   {

        if($request->pr_request_id)
        {          
               
            $query->where('a.id', $request->pr_request_id);//project
        } 

        return $query;

   }


   public static function SearchSumberDaya($request,$query)
   {

        if($request->category_id)
        {
            $query->whereRaw("b.kode_sda LIKE CONCAT(?, '%')", [$request->category_id]);
        }  

        return $query;

   }


   public static function SearchLocation($request,$query)
   {

        if($request->location)
        {    
             $query->leftJoin('payment_product as e','c.id','=','e.product_id');
             $query->where('e.location_id',$request->location);
             $query->groupBy('e.product_id');
        }else{
             $query->groupBy('c.id');
        }  

        return $query;

   }


   public static function SearchSumberDayaSpecial($request,$query,$param)
   {
        if($param)
        {
            if(in_array($param,["A","C","D","E"]))
            {
               $query->where('c.code_1', 'like', ''.$param.'%');
            }

            return $query;

        }    
        
   } 

   public static function SearchHigher($request,$query)
   {
           
            if(!empty($request->price_min) || !empty($request->price_max))
            {
                $query->leftJoin('payment_product as e','c.id','=','e.product_id');
                $query->addSelect(
                    DB::raw('IFNULL(MIN(j.price), IFNULL(MIN(e.price), 0)) as price')   
                );
                $query->leftJoin('project_product_price as j','c.id','=','j.product_id');
                
                if($request->price_min)
                {
                    $query->orderBy('price','DESC'); 
                }else if($request->price_max){
                    $query->orderBy('price','ASC');  
                } 


                return $query;

            }



          
        
   } 


   public static function SearchHigherInSelect($request,$query,$param)
   {
        if($param)
        {
            
            if(in_array($param,["tertinggi","terendah"]))
            {
               
                $query->addSelect(
                    DB::raw('IFNULL(MIN(j.price), IFNULL(MIN(e.price), 0)) as price')   
                );
                $query->leftJoin('project_product_price as j','c.id','=','j.product_id');
                
                if($param =="tertinggi")
                {
                    $query->orderBy('price','DESC'); 
                }else{
                    $query->orderBy('price','ASC');  
                } 

            }


            return $query;

        }    
        
   } 

   public static function SearchContract($request,$query)
   {
        
            if($request->status_vendor =="terkontrak" || $request->status_vendor =="non-kontrak")
            {
                $productid = RequestFrontend::FilterTypeProduct();
                if($request->status_vendor =="terkontrak"){  
                   $query->whereIn('c.id',$productid);
                }else if($request->status_vendor =="non-kontrak"){
                  $query->whereNotIn('c.id',$productid);
                }


            }
            return $query;
       

   }

   public static function SearchContractInSelect($request,$query,$param)
   {
        if($param)
        { 
            if($param =="terkontrak" || $param =="non-kontrak")
            {
                $productid = RequestFrontend::FilterTypeProduct();
                if($param =="terkontrak"){  
                   $query->whereIn('c.id',$productid);
                }else if($param =="non-kontrak"){
                  $query->whereNotIn('c.id',$productid);
                }


            }
            return $query;
        }

   }


   public static function SearchRatingStar($request,$query)
   {
        if($request->rating)
        { 
            $rate = RequestFrontend::SwitchRating($request->rating);
            if($rate)
            {
                  $start =  $rate['start'];
                  $end   =  $rate['end'];
            }

         
             
                  if($request->text_search =="")
                  {
                     $query->leftJoin('vendor as g','c.vendor_id','=','g.id');
                  }  
                  $query->addSelect('h.vk_score_total');
                  $query->leftJoin(DB::raw('(SELECT  MAX(vvh_id) max_vvh_id, vvh_vendor_id, vk_score_total 
                    FROM      vpi_vendor 
                    GROUP BY  vvh_vendor_id) as h'),
                  function($join)
                  {
                        $join->on('g.scm_id', '=', 'h.vvh_vendor_id');
                  });
                  
                  if($param !="start-0")
                  {
                     $query->whereBetween('h.vk_score_total',[$start,$end]); 
                  }else{
                     $query->whereNull('h.vk_score_total'); 
                  } 
           

     

             return $query;
        }

   }


   public static function SearchRatingStarInSelect($request,$query,$param)
   {
        if($param)
        { 
            $rate = RequestFrontend::ExplodeRating($param);
            if($rate)
            {
                  $start =  $rate['start'];
                  $end   =  $rate['end'];
            }

         
             if(in_array($param,["start-0","start-1","start-2","start-3","start-4","start-5"]))
             {
                  if($request->text_search =="")
                  {
                     $query->leftJoin('vendor as g','c.vendor_id','=','g.id');
                  }  
                  $query->addSelect('h.vk_score_total');
                  $query->leftJoin(DB::raw('(SELECT  MAX(vvh_id) max_vvh_id, vvh_vendor_id, vk_score_total 
                    FROM      vpi_vendor 
                    GROUP BY  vvh_vendor_id) as h'),
                  function($join)
                  {
                        $join->on('g.scm_id', '=', 'h.vvh_vendor_id');
                  });
                  
                  if($param !="start-0")
                  {
                     $query->whereBetween('h.vk_score_total',[$start,$end]); 
                  }else{
                     $query->whereNull('h.vk_score_total'); 
                  } 
           

             }

             return $query;
        }

   }

   public static function SearchStatusVendor($request,$query){


    if($request->status_price)
    {
         if($request->status_price =='unverified')
         {
            $query->where(function ($query) {
                $query->where('c.verified_status', 0)
                      ->orWhereNull('c.verified_status');
            });
         }else{
           $query->where('c.verified_status',1); 
         } 

         return $query;  
    }


   }


   


   

}