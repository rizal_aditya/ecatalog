<?php

namespace App\Http\Request\Search;
use App\Http\Request\RequestHistory;
use DB;
use Auth;
class SearchHistory 
{
   
   
  public static function ListUserApproval($query){
         

        $id = Auth::User()->id; 
        $check = RequestHistory::CheckApproval($id);  
        if($check)
        {

            $query->leftJoin('approve_po_list as d', function($join) {
                $join->on('a.order_no', '=', 'd.order_no'); 
                $join->whereRaw('a.approve_sequence + 1 = d.sequence');
                $join->where('a.order_status', '=', 1);
                $join->where('a.is_approve_complete', '=', 0);
            });

            $query->where('e.id',$check->departemen_id);
            $query->orderBy('a.id','DESC');  
        }else{

            $query->where('a.created_by',$id);
            $query->orderBy('a.id','DESC');  
        }

         return $query;

  }
    
  


   public static function SearchStatus($request,$query)
   {
        
        if($request->status =='waiting-approval-1')
        {          
            $query->where(['a.is_approve_complete'=>0,'a.approve_sequence'=>0]);
        } 

        if($request->status =='waiting-approval-2')
        {          
            $query->where(['a.is_approve_complete'=>0,'a.approve_sequence'=>1]);
        } 

        if($request->status =='approved')
        {          
            $query->where(['a.is_approve_complete'=>1,'a.order_status'=>2]);
        } 

         if($request->status =='revisi')
        {          
            $query->where(['a.order_status'=>4]);
        }

        if($request->status =='canceled')
        {          
            $query->where(['a.order_status'=>3]);
        }

        

        return $query;

   }





   public static function SearchOrderCode($request,$query)
   {

        if($request->order_code)
        {          
               
             $query->where('a.order_no',$request->order_code); 
        } 

        return $query;

   }

    public static function SearchLocation($request,$query)
   {

        if($request->location_id)
        {          
           $query->where(['a.location_id'=>$request->location_id]); 
        } 
        return $query;

   }

   public static function SearchVendor($request,$query)
   {

        if($request->vendor_id)
        {          
            $query->where(['a.vendor_id'=>$request->vendor_id]);
        } 

        return $query;

   }


    public static function SearchDate($request,$query)
   {

        if($request->startdate && $request->enddate)
        {          
            $query->whereBetween('created_at', [ date('Y-m-d 00:00:00', strtotime($request->startdate)),
    date('Y-m-d 23:59:59', strtotime($request->enddate))]);
        }

        return $query;

   }
   

   


   

}