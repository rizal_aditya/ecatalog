<?php

namespace App\Http\Request;
use App\Http\Request\RequestOrder;
use App\Http\Request\RequestFrontend;
use App\Helpers\GeneralHelpers;
use Auth;
use DB;
use PDF;
class RequestPDF
{

   public static function CreatePO($product,$request,$payment_id,$order_code,$no_surat)
   {
        $shipping = collect($request->shipping)
                ->first(fn($shipping) => $shipping->value === $request->shipping_id)
                ->text ?? null; // mencari shiping name

         $project_name = collect($request->project)
                ->first(fn($project) => $project->value === $request->project_id)
                ->text ?? null; // mencari shiping name        


        $payment =  'Transfer';
        $project_name = $project_name;
        $devision =  strtoupper($request->division->branch_manager);
        $vendor_name = $request->vendor->text;
        $directur_name = $request->vendor->directur_name;
        $address = $request->vendor->address;
        $no_fax = $request->vendor->no_fax;
        // $contract = RequestOrder::OrderNoContractPDF($project_id); 
        // $contract_update = RequestOrder::OrderUpdateContract($project_id);
        $contract = '-';
        $contract_update = '-';
        $created_at = GeneralHelpers::dates(date('Y-m-d'));
        $gm_devision = $request->division->general_manager;
        $branch_manager = $request->division->branch_manager;
        $res = [];
        foreach($product as $key =>$val)
        {
               $res[$key]['product_name'] = $val->product_name;
               $res[$key]['volume'] = $val->volume;
               $res[$key]['qty'] = $val->qty;
               $res[$key]['weight'] = $val->weight; 
               $res[$key]['price'] = GeneralHelpers::Rupiah($val->price);
               $res[$key]['sub_total'] = GeneralHelpers::Rupiah($request->total_price); 
               $res[$key]['jml_weight'] = $val->weight * $val->qty;
               $res[$key]['jml_qty'] =  $val->qty;
        }  
       

            $ppn = $request->total_price * 11 / 100;  
            $pph = $request->total_price * 1.5 / 100; 
            $total = GeneralHelpers::Rupiah($request->total_price + $ppn);
            $bill = GeneralHelpers::Rupiah($request->total_price - $pph);  

            $data = array(
                'project_name'=>$project_name,
                'up_product'=>$request->up_product,
                'perihal_product'=>$request->perihal_product,
                'order_code'=>$order_code,
                'no_surat'=>$no_surat,
                'vendor_name'=>$vendor_name,
                'vendor_address'=>$address,
                'vendor_no_fax'=>$no_fax,
                'vendor_directur_name'=>$directur_name,
                'created_at'=>$created_at,
                'devision'=>$devision,
                'contract'=>$contract,
                'contract_update'=>$contract_update,
                'product'=>$res,
                'sub_total'=>GeneralHelpers::Rupiah($request->total_price),
                'ppn'=>GeneralHelpers::Rupiah($ppn),
                'pph'=>GeneralHelpers::Rupiah($pph),
                'total'=>$total,
                'bill'=>$bill,
                'shipping_name'=>$shipping,
                'payment_name'=>$payment,
                'gm_devision'=>$gm_devision,
                'branch_manager'=>$branch_manager
            );

            $result = json_encode($data);
            $order = array('order'=>json_decode($result));
            $pdf = PDF::loadview('orderPOProduct', $order);
            $path = public_path('file/order/');
            $filename = "PO_Product-" . $order_code . '-' . time().'.'.'pdf';
            $pdf->save($path . '/' . $filename);
           
            return $filename;
        

    
   }

       
     public static function CreatePOTransport($product,$request,$order_code,$no_surat)
   {
        
            $shipping = collect($request->shipping)
                ->first(fn($shipping) => $shipping->value === $request->shipping_id)
                ->text ?? null; // mencari shiping name

            $project_name = collect($request->project)
                ->first(fn($project) => $project->value === $request->project_id)
                ->text ?? null; // mencari shiping name  

            $project_address = collect($request->project)
                ->first(fn($project) => $project->value === $request->project_id)
                ->address ?? null; // mencari shiping name          

            $transport_name = collect($request->transport_logistic)
                ->first(fn($transport) => $transport->value === $request->transport_id)
                ->text ?? null; // mencari shiping name          

            $payment =  'Transfer';
           
            $devision =  strtoupper($request->division->branch_manager);
            $vendor_name = $request->vendor->text;
            $directur_name = $request->vendor->directur_name;
            $address = $request->vendor->address;
            $no_fax = $request->vendor->no_fax;
           // $order_code = $request->order_no;
            $created_at = GeneralHelpers::dates(date('Y-m-d'));
            $gm_devision = $request->division->general_manager;
            $branch_manager = $request->division->branch_manager;
            $res = [];
            foreach($product as $key =>$val)
            {
                   $res[$key]['no'] = $key + 1;
                   $res[$key]['product_name'] = $val->product_name;
                   $res[$key]['volume'] = $val->volume;
                   $res[$key]['qty'] = $val->qty;
                   $res[$key]['weight'] = $val->weight; 
                   $res[$key]['price'] = GeneralHelpers::Rupiah($val->price);
                   $res[$key]['sub_total'] = GeneralHelpers::Rupiah($request->total_price); 
                  
            }          

            $data = array(
                    'project'=>['id'=>$request->project_id,'name'=>$project_name,'address'=>$project_address],
                    'product' => $res,
                    'total_qty' =>  $request->total_qty,
                    'total_weight' => $request->total_weight,
                    'sub_total'=> number_format($request->total_price,0,',','.') ,
                    'terbilang'=> RequestOrder::TerbilangNumber($request->total_price),
                    'order_no'=>$order_code,
                    'devision'=>$devision,
                    'up'=>$request->up_transport,
                    'perihal'=>$request->perihal_transport,
                    'catatan'=>$request->catatan_transport,
                    'vendor_product'=> $request->vendor,
                    'vendor_product_location'=> $request->location_name,
                    'vendor_transport'=> RequestOrder::VendorTransportAmandemenID($request->transport_id,$transport_name),
                    'transport_name' => $request->transport_name,
                    'transport_price'=> GeneralHelpers::Rupiah($request->total_transport),
                    'transport_fee'=> $request->transport_fee,
                    'created_by'=>$request->created_by,
                    'weight_minimum'=>$request->min_weight_transport,
                    'created_at'=> $request->created_at,
                    'updated_at'=> $request->updated_at,
                    'gm_devision'=>$gm_devision,
                    'branch_manager'=>$branch_manager
            );



            $result = json_encode($data);
            $order = array('order'=>json_decode($result));
            $pdf = PDF::loadview('orderPOTransport', $order);
            $path = public_path('file/order/');
            $filename = "PO_Transport-" . $order_code . '-' . time().'.'.'pdf';
            $pdf->save($path . '/' . $filename);
           
            return $filename;
        

        

   }


   public static function CreatePOAsuransi($product,$request,$order_code,$no_surat)
   {
        
           
            $project_name = collect($request->project)
                ->first(fn($project) => $project->value === $request->project_id)
                ->text ?? null; // mencari shiping name  

            $project_address = collect($request->project)
                    ->first(fn($project) => $project->value === $request->project_id)
                    ->address ?? null; // mencari shiping name   

            $transport_name = collect($request->transport_logistic)
                    ->first(fn($transport) => $transport->value === $request->transport_id)
                    ->text ?? null; // mencari shiping name  
            
            

            $payment =  'Transfer';
            // $project_name = $project_name;
            $devision =  strtoupper($request->division->branch_manager);
            $vendor_name = $request->vendor->text;
            $directur_name = $request->vendor->directur_name;
            $address = $request->vendor->address;
            $no_fax = $request->vendor->no_fax;
           
         
            $order_code = $request->order_no;
          
            $gm_devision = $request->division->general_manager;
            $branch_manager = $request->division->branch_manager;
            $res = [];
            foreach($product as $key =>$val)
            {
                   $res[$key]['no'] = $key + 1;
                   $res[$key]['product_name'] = $val->product_name;
                   $res[$key]['volume'] = $val->volume;
                   $res[$key]['qty'] = $val->qty;
                   $res[$key]['weight'] = $val->weight; 
                   $res[$key]['price'] = GeneralHelpers::Rupiah($val->price);
                   $res[$key]['sub_total'] = ['original'=>$request->total_price,'convert'=>GeneralHelpers::Rupiah($request->total_price)]; 
            }          

            $data = array(
                    'project'=>['id'=>$request->project_id,'name'=>$project_name,'address'=>$project_address],
                    'product' => $res,
                    'total_qty' =>  $request->total_qty,
                    'total_weight' => $request->total_weight,
                    'sub_total'=> ['original'=>$request->total_price,'convert'=>GeneralHelpers::Rupiah($request->total_price)] ,
                    'terbilang'=> RequestOrder::TerbilangNumber($request->total_price),
                    'order_no'=>$order_code,
                    'devision'=>$devision,
                    'up'=>$request->up_insurance,
                    'perihal'=>$request->perihal_insurance,
                    'catatan'=>$request->catatan_insurance,
                    'vendor_product'=> $request->vendor,
                    'vendor_transport'=> RequestOrder::VendorTransportAmandemenID($request->transport_id,$transport_name),
                    'vendor_asuransi'=> RequestOrder::VendorAsuransiAmandemenID($request->insurance_id),
                   
                    'created_by'=>$request->created_by,
                    'transport'=>$request->transport,
                    'created_at'=>  $request->created_at,
                    'tgl_ambil'=>  GeneralHelpers::tanggal($request->tgl_ambil),
                    'updated_at'=> $request->updated_at,
                    'gm_devision'=>$gm_devision,
                    'branch_manager'=>$branch_manager
            );


            $result = json_encode($data);
            $order = array('order'=>json_decode($result));
            $pdf = PDF::loadview('orderPOAsuransi', $order);
            $path = public_path('file/order/');
            $filename = "PO_Asuransi-" . $order_code . '-' . time().'.'.'pdf';
            $pdf->save($path . '/' . $filename);
           
            return $filename;
        

        

   }




   


}