<?php

namespace App\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\User;
use App\Models\Vendor;
use App\Models\Location;
use App\Models\LogPassword;
use Illuminate\Support\Facades\Hash;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestNotif;
use App\Helpers\SSO\SsoManagement;
use DB;
class RequestAuth
{ 

   public static function requestHash($username,$password)
    {   

        $user = User::where('username',$username)->first(); 
        if ($user !=null)
        {
           if (!Hash::check($password, $user->password)) {
             if($password == $user->password)
             {
                  User::where('username',$username)->update(['password'=> Hash::make($password)]);
             } 
            
           }
        }

    }  

   public static function LoginAuth($username,$password)
   {
       if($username !="" && $password !="")
       {    
            $user = User::where('username',$username)->first();
            if ($user ==null)
            {            
                return array('username'=>'Username tidak valid');          
            }

            
             if(!Hash::check($password, $user->password))
             {           
                return array('password'=>'Password tidak valid');
             }

             // if(sha1($password) != $user->password)
             // {
             //      return array('password'=>'Password tidak valid');
             // }   
                       
            
       }else{
            
            $messages3 = array('username'=>'Username masih kosong');
            $messages4 = array('password'=>'Password masih kosong');          
            if($credentials['username'] =="" && $credentials['password'] =="")
            {
                $merge = array_merge($messages3,$messages4);
                return $merge;
            }  

            if($credentials['username'] =="")
            {
                return $messages3;
            }  

            if($credentials['password'] =="")
            {
                return $messages4;
            } 


        }    
   }

   

   public static function getProfileHeader(){
        // SsoManagement::Connection();
        // $data = SsoManagement::GetUser();
        $auth = Auth::User();
        $id = $auth->id;

        $role = DB::table('users_roles')->where('user_id',$id)->first();

        if($role->role_id == 3)
        {
           $callback = url('vendor'); 
        }else{
          $callback = 'https://dev2-ecatalog.scmwika.com/dashboard';
        }    
        
        $FileAkun = GeneralPaginate::FileAkun();
        $FilePublic = GeneralPaginate::FilePublic();   
        if($auth->photo ==null || $auth->photo =="")
        {
            $photo = url($FilePublic.'2010526.png');
        }else{
            $photo = url($FileAkun.$auth->photo);
        }   
         
        $profile = array('id'=>Auth::User()->id,'username'=>RequestAuth::limitText($auth->username,'10'),'fullname'=>$auth->first_name.' '.$auth->last_name,'phone'=>$auth->phone,'address'=>$auth->address,'photo'=>$photo,'callback'=>$callback,'access'=>$role->role_id,'user_chat'=>Auth::User()->active_chat);

        return $profile;

   }

   public static function getCategoryHeader(){

    $__temp_ = array();
    
    $category =  DB::select(DB::raw("SELECT `b`.`kode` as `id`, `a`.`name`
                  FROM resources_code a
                  JOIN (SELECT LEFT(code_1, 2) as kode FROM product where product.is_deleted=0 GROUP by kode) b ON a.code LIKE CONCAT(b.kode, '%')
                  where a.level=3 order by a.name  
                "));

     foreach ($category as $key => $val)
     {
       $__temp_[$key]['value'] = $val->id;
       $__temp_[$key]['text'] =  RequestFrontend::FilterCategoryName($val->name,'select');
     }   

     return  $__temp_;

   }

   public static function getLocationHeader(){

    $__temp_ = array();
    
    $location = DB::table('ref_locations as b')
               // ->join('ref_locations as b','a.location_id','=','b.location_id')
                ->select('b.location_id as id','b.province_name','b.regency_name')
                ->Where(['b.level'=>3,'b.level'=>2])
               
   // ->orwhereNotNull('regency_name')
    // ->limit(514)
    ->orderBy('b.location_id','ASC')
    ->get(); 

     foreach ($location as $key => $val)
     {
           $temp[$key]['value'] = $val->id;
           $temp[$key]['text'] = ucfirst(strtolower($val->province_name)); 
     }   

     return  $temp;

   }


   
   public static function getNotif(){
          $perPage = 10;
        
          $notif = DB::table('notification as a')
                ->select('a.id','a.created_at','a.deskripsi','a.id_pengirim','a.id_penerima','b.photo')
                ->join('users as b','a.id_pengirim','=','b.id') 
                ->where(['a.id_penerima'=>Auth::User()->id])->OrderBy('a.id','DESC')->limit($perPage)->get();

        $_res = RequestNotif::GetDataPopUp($notif);
        return $_res;        
    
   }

    public static function getCountNotifIsRead(){
        
        
          $notif = DB::table('notification as a')
                ->select('a.id','a.created_at','a.deskripsi','a.id_pengirim','a.id_penerima','b.photo')
                ->join('users as b','a.id_pengirim','=','b.id') 
                ->where(['a.id_penerima'=>Auth::User()->id,'is_read'=>0])->count();

      
        return $notif;        
    
   }



   public static function GetAccountVendor(){
        $auth = Auth::User();
        $id = $auth->vendor_id;
        $vendor = Vendor::where('id',$id)->first();
        $rate = RequestFrontend::RateProduct($id);  
        $FileAkun = GeneralPaginate::FileAkun();
        $FilePublic = GeneralPaginate::FilePublic();   
        if($auth->photo ==null || $auth->photo =="")
        {
            $photo = url($FilePublic.'2010526.png');
        }else{
            $photo = url($FileAkun.$auth->photo);
        }

         if($vendor->description)
        {
            $description = RequestAuth::limitText(ucfirst(strtolower($vendor->description)),'25');
        }else{
            $description = 'Identitas masih kosong';
         } 

        $profile = array('username'=>RequestAuth::limitText($auth->username,'10'),'photo'=>$photo,'vendor_name'=> ucfirst($vendor->name),'vendor_owner'=> ucfirst(strtolower($vendor->nama_direktur)),'description'=>$description,'address'=> ucfirst(strtolower($vendor->address)),'phone'=>$vendor->no_telp,'position'=>ucfirst(strtolower($vendor->dir_pos)),'rate'=>$rate);

        return $profile;

   }

    public static function limitText($string,$limit)
   {

        $string = strip_tags($string);
        if (strlen($string) > $limit) 
        {

            $stringCut = substr($string, 0, $limit);
            $endPoint = strrpos($stringCut, ' ');
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...';
        }

        return  $string;

   }



   

   


   


}