<?php

namespace App\Http\Request;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\ResourceCode;
use App\Models\ProjectProductPrice;
use App\Models\ProductGallery;
use App\Models\PaymentProduct;
use App\Models\PaymentMethod;
use App\Models\Location;
use App\Models\ProjectProduct;
use App\Models\ProjectNew;
use App\Models\Vendor;
use App\Models\Viewer;
use App\Models\VpiVendor;
use App\Models\Wishlist;
use App\Http\Request\RequestAuth;
use App\Http\Request\Search\SearchFrontend;
use Illuminate\Support\Facades\Cache;
use Exception;

use DB;

class RequestFrontend 
{

    public static function ProductWithoutPR()
    {

        $query = DB::table('product as c');
        $query->select(
            'c.id',
            'c.name',
            'c.code_1 as sda_code',
            'c.berat_unit',
            'c.vendor_id',
            'c.verified_status',
            'c.uom_id',
            'c.created_at',
            'c.update_at as updated_at',
            'c.note',
            'c.volume'
            
            );
          // $query->where(['c.id' => 27]);
          $query->where(['c.is_deleted' => 0]);
          
          return  $query;
   }
     
    
    public static function ProductsWithPR(){

        $query = DB::table('product as c');
        $query->leftJoin('pr_request_product as b','c.code_1','=','b.kode_sda')
                 ->join('pr_request as a','b.pr_request_id','=','a.id');

       

                 
        $query->select(
            'c.id',
            'c.name',
            'c.code_1 as sda_code',
            'c.berat_unit',
            'c.vendor_id',
            'c.verified_status',
            'c.uom_id',
            'c.created_at',
            'c.update_at as updated_at',
            'c.note',
            'c.volume'
            );

          $query->where(['c.is_deleted' => 0]);
          return  $query;
    }






    public static function QueryListPRRequest(){

        $query = DB::table('pr_request as a')
                ->join('pr_request_product as b','a.id','=','b.pr_request_id')
               
                ->join('project_new as d','a.project_id','=','d.id')
                ->leftJoin('ref_locations as f','d.location_id','=','f.location_id');     
        $query->select(
           
            'd.id as project_id',
            'd.name as project_name',
            'a.id as pr_request_id',
            'a.no_pr_request',
            'f.location_id',
            'f.name as location_name',

        );
         $query->groupBy('a.no_pr_request');
         return  $query;
    }  
   

   

    public static function RealtimeQuery($query,$param,$request,$perPage)
    {
       
        $result = array();
        $data = array();
        
       // $baseQuery = clone $query; // Cloning for separate handling
        
        //mencari inputan text
        SearchFrontend::SearchTextHeader($request,$query);



         //Group Modal Filter
       // mencari project
         SearchFrontend::SearchProject($request,$query);
       // mencari pr request
        SearchFrontend::SearchPRRequest($request,$query);
       // mencari sumber daya 
       SearchFrontend::SearchSumberDaya($request,$query);      
       //mencari rating bintang
       SearchFrontend::SearchRatingStar($request,$query);
        // mencari harga tertingi atau terendah
       SearchFrontend::SearchHigher($request,$query);
        //mencari status kontrak dan non kontrak
       SearchFrontend::SearchContract($request,$query);
        //mencari status verified dan unverified
       SearchFrontend::SearchStatusVendor($request,$query);


        //select in header
        //  mencari lokasi 
       SearchFrontend::SearchLocation($request,$query);
        //mencari sumber daya pilihan select
        SearchFrontend::SearchSumberDayaSpecial($request,$query,$param);
       // mencari harga tertingi atau terendah
        SearchFrontend::SearchHigherInSelect($request,$query,$param);
       // mencari rating bintang
        SearchFrontend::SearchRatingStarInSelect($request,$query,$param);
        //mencari terkontrak atau non kontrak
        SearchFrontend::SearchContractInSelect($request,$query,$param);
       
           
            $data  =  $query->paginate($perPage);

            $description =  RequestFrontend::Description(
                $request->text_search,
                $request->project_id,
                $request->pr_request_id,
                $request->price_min,
                $request->price_max,
                $request->rating,
                $request->category_id,
                $request->location_id,
                $request->status_vendor,
                $request->status_price,
                $request->payment_id,
                $request->vendor_name
            );
            
            $type = "index";
            $title = RequestFrontend::ProvinceName($request->location);
           $result['product'] = RequestFrontend::GetDataPage($data,$perPage,$request,$title,$description,$type);

            //khusus untuk frotend depan
            $filter = [
                "param" =>$request->param,
                "project_id" => $request->project_id,
                "location_id" => $request->location,
                "category_id" => $request->category_id,
                "price_min" => $request->price_min,
                "price_max" => $request->price_max,
                "rating" => $request->rating,
                "status_vendor" => $request->status_vendor,
                "status_price" => $request->status_price,
            ];

           // Filter elemen yang nilainya tidak null
            $filteredData = array_filter($filter, function($value) {
                return !is_null($value); // Hanya menyimpan nilai yang tidak null
            });

            if (empty($filteredData)) {
                 $result['category_fast'] = RequestFrontend::CategoryFast(); 
            } 

            
            //RequestFrontend::SaveCache($request,$param,$result,$data);

            return $result;

        

    }
   

   public static function GetDataPage($data,$perPage,$request,$title,$description,$type)
   {
        $temp = array();
        $getRequest = $request->all();
        $page = isset($getRequest['page']) ? $getRequest['page'] : 1;
        $numberNext = (($page*$perPage) - ($perPage-1));
       

        foreach ($data as $key => $val)
        {
            $weight = $val->berat_unit == "" ? 0 : $val->berat_unit;
            $desc = empty($val->note) ? "Deskripsi Kosong" : $val->note;
            

            $temp[$key]['number'] = $numberNext++;
            $temp[$key]['id'] = $val->id;
            $temp[$key]['lock'] = false;
           
            $temp[$key]['name'] =  [
                                        'original'=>$val->name,
                                        'limit'=>RequestFrontend::limitText($val->name,'50'),
                                        'code'=>$val->sda_code.' '.$val->name
                                       ];

            $temp[$key]['price'] = [
                                         'original'=>RequestFrontend::PriceProduct($val->id,'original'),
                                         'convert' =>RequestFrontend::PriceProduct($val->id,'convert').' / '.RequestFrontend::SatuanProduct($val->uom_id)
                                       ];

            $temp[$key]['type'] = ['slug'=>RequestFrontend::TypeProduct($val->id,'slug'),'name'=>RequestFrontend::TypeProduct($val->id,'name')];
            $temp[$key]['contract'] = [
                                           'no'=> RequestFrontend::NoContractProduct($val->id),
                                           'description'=>RequestFrontend::ContractProduct($val->id)
                                         ];

             $temp[$key]['vendor'] = RequestFrontend::Vendor($val->sda_code,$val->vendor_id);
             $temp[$key]['volume'] = $val->volume;

         
            $temp[$key]['weight'] = [
                                       'original'=>(float)$weight,
                                       'convert'=>$weight.' '.RequestFrontend::SatuanProduct($val->uom_id)
                                       ];
       
    
            $temp[$key]['photo'] = RequestFrontend::PhotoProductMulti($val->id);
            $temp[$key]['location'] = RequestFrontend::LocationProduct($val->id);
         
           
            $temp[$key]['wishlist'] = RequestFrontend::WishlistProduct($val->id);
            $temp[$key]['status'] =  [
                                'verified'=>RequestFrontend::StatusProduct($val->verified_status),
                                'matgis'=>RequestFrontend::StatusMatgis($val->sda_code)
                            ];
        
          
            $temp[$key]['description'] = $desc;
            
            $temp[$key]['total_selling'] = RequestFrontend::TotalSelling($val->id);
            $temp[$key]['archive'] = RequestFrontend::ArchiveProduct($val->id);


            $temp[$key]['viewer'] = [
                                           'total'=>RequestFrontend::TotalView($val->id),
                                           // 'detail'=>RequestFrontend::DetailView($val->id)
                                        ];

            $temp[$key]['project'] = RequestFrontend::ProjectByProduct($val->sda_code);                           
            $temp[$key]['pr_request'] = RequestFrontend::RequestPRByProduct($val->sda_code,$request);
            $temp[$key]['sumberdaya'] = RequestFrontend::SumberDaya($val->sda_code);
           
                                       
                                                                 
            //$temp[$key]['btn_active'] = RequestFrontend::CheckRoleButton();
            $temp[$key]['created_at'] = GeneralHelpers::tanggal_indo($val->created_at);
            $temp[$key]['updated_at'] = GeneralHelpers::tanggal_indo($val->updated_at);
           // $__temp_[$key]['location_list'] = RequestAuth::getLocationHeader();
           // $__temp_[$key]['payment_list'] = RequestFrontend::getPayment($val->id);
        }

      
       
        $results['result'] = $temp;
        if($description !="")
        {  
           if($data->total() !=0)
           {
             $results['cari'] = 'Pencarian "'.$description.'" Total : '.$data->total().' berhasil ditemukan'; 
           }else{
             $results['cari'] = 'Pencarian tidak ditemukan "'.$description.'" '; 
           } 
            
        }else{
            $results['cari'] = ''; 
        } 

        if($title !="")
        {
            $results['title'] = $title; 
        }    

        $results['total'] = $data->total();
        $results['lastPage'] = $data->lastPage();
        $results['perPage'] = $data->perPage();
        $results['currentPage'] = $data->currentPage();
        $results['nextPageUrl'] = $data->nextPageUrl();
        return $results;

   }


   public static function GetDataRequestPR($data){
        $temp = array();
        foreach ($data as $key => $val)
        {   
                $temp[$key]['value'] = $val->pr_request_id;
                $temp[$key]['text'] = $val->no_pr_request;
                $temp[$key]['project_id'] = $val->project_id;
                $temp[$key]['project_name'] = $val->project_name;
                $temp[$key]['location_id'] = $val->location_id;
                $temp[$key]['location_name'] = $val->location_name;
                $temp[$key]['sumberdaya'] = RequestFrontend::RequestPRByNoPR($val->pr_request_id); 
        }  

        return $temp;  

   }


    

   


   public static function CheckTotalCount($query,$param,$request)
   { 
       // mencari inputan text
         SearchFrontend::SearchTextHeader($request,$query);
        //mencari project
         SearchFrontend::SearchProject($request,$query);
        //mencari pr request
         SearchFrontend::SearchPRRequest($request,$query);
        //mencari sumber daya 
         SearchFrontend::SearchSumberDaya($request,$query);      
        //mencari lokasi 
         SearchFrontend::SearchLocation($request,$query);

        //select in header
        //mencari sumber daya pilihan select
         SearchFrontend::SearchSumberDayaSpecial($request,$query,$param);
        //mencari harga tertingi atau terendah
         SearchFrontend::SearchHigher($request,$query,$param);
        //mencari rating bintang
         SearchFrontend::SearchRatingStar($request,$query,$param);
        //mencari terkontrak atau non kontrak
         SearchFrontend::SearchContract($request,$query,$param);
       
       $result =   DB::table(DB::raw("({$query->toSql()}) as sub"))->mergeBindings($query)->count(); 
       return $result;
   }

   public static function CreateCache($request,$param){

        if($request->pr_request_id =='' || $param =='' || $request->location == '')
        {
       
          $key = 'list-'.$request->page;
          $cachedData = Cache::get($key);
          $keyCount = 'count-'.$request->page;
          $cachedCount = Cache::get($keyCount);

          $cache = array('keydata'=>$key,'data'=>$cachedData,'keycount'=>$keyCount,'count'=> $cachedCount);
          $result = json_decode(json_encode($cache), FALSE);
          return $result;
       
        }

   }

   public static function SaveCache($request,$param,$result,$data)
   {

     if($request->pr_request_id =='' || $param =='' || $request->location == '')
     {      
            $cache = RequestFrontend::CreateCache($request,$param); 
            Cache::put($cache->keydata, $result, 86400); // 86400 detik =  (24 jam)
            Cache::put($cache->keycount, $data->total(), 86400); // 86400 detik =  (24 jam) 

      }  
   }


   public static function GetDataSlider($data)
   {
        $__temp_ = array();
      
       

        foreach ($data as $key => $val)
        {
           
            $__temp_[$key]['id'] = $val->id;
            $__temp_[$key]['photo'] = url('file/ads/'.$val->img_cover);
            $__temp_[$key]['name'] = $val->judul;
        }
        return $__temp_;

   }


   
   public static function GetPreview($val)
   {
        $temp = array();
       
        if($val->note ==""){ $desc = "Deskripsi Kosong"; }else{ $desc =  $val->note; } 
        if($val->berat_unit ==""){ $weight = 0; }else{ $weight =  $val->berat_unit; }  
       

        $temp['id'] = $val->id;
        $temp['name'] =  [
                              'original'=>$val->name,
                              'limit'=>RequestFrontend::limitText($val->name,'50'),
                              'code'=>$val->sda_code.' '.$val->name
                            ];

        $temp['price'] = [
                             'original'=>RequestFrontend::PriceProduct($val->id,'original'),
                             'convert' =>RequestFrontend::PriceProduct($val->id,'convert').' / '.RequestFrontend::SatuanProduct($val->uom_id)
                           ];
     
    
        $temp['contract'] =  [
                                   'no'=> RequestFrontend::NoContractProduct($val->id),
                                   'description'=>RequestFrontend::ContractProduct($val->id)
                                ];                   

        $temp['weight'] = [
                                'original'=>(float)$weight,
                                'convert'=>$weight.' '.RequestFrontend::SatuanProduct($val->uom_id)
                             ];

        $temp['description'] = $desc;
        $temp['volume'] = $val->volume;
        $temp['vendor'] = RequestFrontend::Vendor($val->sda_code,$val->vendor_id);
        $temp['status'] = ['verified'=>RequestFrontend::StatusProduct($val->verified_status),'matgis'=>RequestFrontend::StatusMatgis($val->sda_code)];
        $temp['type'] = ['slug'=>RequestFrontend::TypeProduct($val->id,'slug'),'name'=>RequestFrontend::TypeProduct($val->id,'name')];
        
        $temp['photo'] = RequestFrontend::PhotoProductMulti($val->id);
       
        $temp['location'] = RequestFrontend::LocationProduct($val->id);
        // $__temp_['rate'] = RequestFrontend::RateProduct($val->scm_id);
        $temp['wishlist'] = RequestFrontend::WishlistProduct($val->id); 
        
        $temp['uom'] = ['id'=>$val->uom_id,'name'=> RequestFrontend::SatuanProduct($val->uom_id)];

          
        $temp['created_at'] = GeneralHelpers::tanggal_indo($val->created_at);
        $temp['updated_at'] = GeneralHelpers::tanggal_indo($val->updated_at);
       
        $temp['payment_list'] = RequestFrontend::getPayment($val->id);
        $temp['btn_active'] = RequestFrontend::CheckRoleButton();
        $temp['project'] = RequestFrontend::ProjectByProduct($val->sda_code);                           
        
        $temp['info_product_rev'] = RequestFrontend::CheckAddproductAction($val->vendor_id);
        $temp['info_not_approval']  = RequestFrontend::CheckApprovByNotNUll();
        $temp['info_not_access'] = RequestFrontend::CheckGroupByNotNUll();
        $temp['info_contract'] = RequestFrontend::CheckTypeVendor(RequestFrontend::TypeProduct($val->id,'slug'));

        $results['result'] = $temp;
        if(Auth::User()->vendor_id != $val->vendor_id)
        {
           $check = RequestFrontend::CheckViewer($val->id,$val->vendor_id);  
        }
       
        return $results;

   }

    


   public static function CheckViewer($product_id,$vendor_id){
        //$viewer = DB::table('viewer')->select('count')->where(['user_id'=>Auth::User()->id,'product_id'=>$product_id,'vendor_id'=>$vendor_id])->first();


       //if($viewer ==null)
       // {
           $insert = DB::table('viewer')->insert(['user_id'=>Auth::User()->id,'product_id'=>$product_id,'created_at'=>date('Y-m-d H:i:s'),'vendor_id'=>$vendor_id]);
        // }else{
        //   $count = $viewer->count + 1;
        //   $update = DB::table('viewer')->where(['product_id'=>$product_id,'vendor_id'=>$vendor_id,'user_id'=>Auth::User()->id])->update(['count'=>$count,'user_id'=>Auth::User()->id,'product_id'=>$product_id,'updated_at'=>date('Y-m-d H:i:s'),'vendor_id'=>$vendor_id]);
        // }    

   }


   public static function HandleStatusVendor($code_sda,$vendor_id)
   {
     //kode sumber daya transportasi = D5
    //kode sumber daya asuransi = ED
    //kode sumber daya simbesi = AF
      $prefix = substr($code_sda, 0, 2); // Mengambil dua huruf pertama

      if($prefix == "AF")
      {
         $quota =  RequestFrontend::VendorQuota($vendor_id);
         $used =  RequestFrontend::RemainingVendorQuota($vendor_id);
         
       
         $count = DB::table('info_besi_vendor')->where('id_vendor',$vendor_id)->count();
         if($count >0)
         {
           
               
                $result = array(
                    'status'=>true,
                    'quota' => ['original'=>$quota->total,'convert'=> number_format($quota->total, 2)],
                    'quota_used' => number_format($used, 2),
                    'quota_limit' => ['original'=>$quota->total - $used,'convert'=>number_format($quota->total - $used, 2)],
                    'quota_satuan' => 'Kg',
                    'quota_expired' => $quota->expired,
                );   
            
            
         }else{
            $result = array(
                'status'=>false,
                'quota' => 0,
                'quota_used' => 0,
                'quota_limit' => 0,
                'quota_satuan' => '',
                'quota_expired' => '',
            );
         }   
         
      
      }else{
           $result = array(
            'status'=>false,
            'quota' => 0,
            'quota_used' => 0,
            'quota_limit' => 0,
            'quota_satuan' => '',
            'quota_expired' => '',
            );
      } 

      return $result;
   }


   public static function VendorQuota($vendor_id)
   {
      
      $total = 0;
      $expired = date('Y-m-t');
      $result = array('total'=>$total,'expired'=>$expired);
      $data = DB::table('info_besi_vendor')->where('id_vendor',$vendor_id)->first();
      if($data)
      { 
        $yearMonth = date("Y-m", strtotime($data->tanggal));
        $total = ($yearMonth === date("Y-m")) ? $data->kuota : 0;
        $result = array('total'=>$total,'expired'=>$expired);

      }  

      return  json_decode(json_encode($result), FALSE);
   }


   public static function RemainingVendorQuota($vendor_id)
   {
      $yearMonth = date("Y-m");
      $result = 0;
      $data = DB::table('order as a')
              ->select(DB::raw('SUM(b.qty * b.weight) as total'))
              ->join('order_product as b','a.order_no','=','b.order_no')
              ->whereIn('a.order_status',[0,1,2])
              ->where('a.created_at','like',''.$yearMonth.'%')
              ->where('a.vendor_id',$vendor_id)->get();
      if($data)
      {
         $result = $data[0]->total;
      }        

      return  $result;    

   }


   
   public static function getPayment($productid){
     $__temp_ = array();

    $check =  RequestFrontend::checkpayment($productid);

    if($check == true)
    {
           
          
            $payment = DB::table('payment_product as a')
                     ->select('b.id','b.day','c.name','a.price')
                     ->join('payment_method as b','b.id','=','a.payment_id') 
                     ->leftJoin('enum_payment_method as c','c.id','=','b.enum_payment_method_id') 
                     ->join('project_product_price as d', function($join) {
                        $join->on('d.product_id', '=', 'a.product_id');
                        $join->on('d.payment_id', '=', 'a.payment_id');
                        $join->where('d.price', '>', 100);
                        $join->where('d.is_deleted', '=', 0);
                        $join->whereRaw('d.amandemen_id IN (select MAX(a2.amandemen_id) from project_product_price as a2 where a2.product_id=d.product_id)'); 
                         
                      
                    })
                     ->where('a.product_id',$productid)
                     ->orderBy('c.id','desc')
                     ->get();
              

    }else{
        
            $payment = DB::table('payment_method as a')
           ->select('a.id','a.day','b.name','c.price')
           ->join('enum_payment_method as b','b.id','=','a.enum_payment_method_id')
           ->join('payment_product as c','a.id','=','c.payment_id')
           ->where('c.product_id',$productid)
           ->where('c.price','>',0)
           ->get();


    }    

   

    $temp = array(); 
    foreach ($payment as $key => $val)
    {
       if($val->name =="Transfer")
       {

         $temp[$key]['value'] = $val->id;
         $temp[$key]['text'] = ucfirst(strtoupper($val->name)); 
         // if($val->name!='Transfer')
         // {
         //   $temp[$key]['text'] = ucfirst(strtoupper($val->name.' '.$val->day.' Hari')); 
         // }else{
         //    $temp[$key]['text'] = ucfirst(strtoupper($val->name));  
         // }   
           
       } 
        
    } 

    // $transfer = [['value' => 'transfer', 'text' => 'Transfer']];
    // $merge = array_merge($transfer,$temp);
    
     return  $temp;           
   }


   public static function checkpayment($productid)
   {
       return DB::table('project_product_price')->where('product_id', $productid)->exists();
   }

   public static function Vendor($sda_code,$vendor_id)
   {
     $result = array();
     $data = Vendor::select('id as vendor_id','name as vendor_name','registration_number')->where('id',$vendor_id)->first();

   

     if($data)
     { 
          
        $result = [
                      'id'=>$vendor_id,
                      'name'=>RequestFrontend::limitText($data->vendor_name,'30'),
                      'tooltips'=>$data->vendor_name,
                      'rate' =>RequestFrontend::RateProduct($data->registration_number),
                      'simbesi'=>RequestFrontend::HandleStatusVendor($sda_code,$vendor_id),
                      
                   ];
     }

     return $result;

   }

   public static function limitText($string,$limit)
   {

        $string = strip_tags($string);
        if (strlen($string) > $limit) 
        {

            $stringCut = substr($string, 0, $limit);
            $endPoint = strrpos($stringCut, ' ');
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...';
        }

        return  $string;

   }

   public static function NoContractProduct($id)
   {

        $query  = DB::table('project_product_price as a');
        $query->select('b.no_contract', DB::raw('IFNULL(c.no_amandemen, 0) as no_amandemen'));
        $query->join('project as b','a.project_id','=','b.id');
        $query->join('amandemen as c','b.id','=','c.id_project');
        $query->where('a.product_id',$id); 
        
        $results = $query->first();
        if($results !=null)
        {  
            if($results->no_amandemen ==0)
            {
              $result = $results->no_contract;
            }else{
              $result = $results->no_contract.'-'.$results->no_amandemen; 
            }    
     
        }else{
             $result = "";
        }    
        return $result;
   }




   public static function CategoryProduct($code)
   {
        $product = ResourceCode::where('code','LIKE',''.$code.'%')->first();
        return RequestFrontend::limitText($product->name,'15');

   }

   public static function PriceProduct($product_id,$type){


     $pricelist = DB::table('product as a')
       ->select(DB::raw('IFNULL(MIN(c.price), IFNULL(MIN(b.price), 0)) as product_min_price'))
              //  ->leftJoin('payment_product as b','a.id','=','b.product_id')
                ->leftJoin('payment_product as b', function($join) {
                        $join->on('a.id', '=', 'b.product_id');
                        $join->where('b.price', '>', 100);
                    })
                ->leftJoin('project_product_price as c', function($join) {
                        $join->on('a.id', '=', 'c.product_id');
                        $join->where('c.is_deleted', '=', 0);
                        $join->where('c.price', '>', 100);
                        $join->whereRaw('c.amandemen_id IN (select MAX(a2.amandemen_id) from project_product_price as a2 where a2.product_id=c.product_id)');
                    })
                ->where([
                    'a.is_deleted' => 0,
                    'a.id' => $product_id,
                  ])
                 
                ->groupBy('a.id')
                ->get();

             if($type =='convert')
             {
                $price = count($pricelist) != 0 ? RequestFrontend::Rupiah($pricelist[0]->product_min_price) : "Rp 0";
             }else{
                $price = count($pricelist) != 0 ? $pricelist[0]->product_min_price : 0;
             }   
      
     return $price;

   }

   public static function GetPaymentPrice($product,$uom_id){
     $result = array();
     if($product !=null)
     {
        $price = RequestFrontend::Rupiah($product->price_real).' / '.RequestFrontend::SatuanProduct($uom_id);
     }else{
        $price = '';
     }


     $result['price'] = ['convert'=> $price,'original'=>$product->price_real];
   
     $result['location_id'] =  $product->location_id_real;
     $result['location'] =  RequestFrontend::GetLocationReal($product->location_id_real);
     return $result;

   }

   public static function GetLocationReal($location_id){
     $loc_id = explode(',',$location_id);
     $location = DB::table('ref_locations')
     ->select('location_id as value','full_name as text')
      ->whereIn('location_id',$loc_id)
     //->where('location_id',3578)
     ->get();
     return $location;


   }

   public static function PriceProductOri($product_id){


     $pricelist = DB::table('product as a')
       ->select(DB::raw('IFNULL(MIN(c.price), IFNULL(MIN(b.price), 0)) as product_min_price'))
                ->join('payment_product as b','a.id','=','b.product_id')
                ->join('project_product_price as c','a.id','=','c.product_id')
                ->where([
                    'a.is_deleted' => 0,
                    'a.id' => $product_id,
                  ])
                ->groupBy('a.id')
                ->get();
      
     if(count($pricelist) !=0)
     {
        $price =  $pricelist[0]->product_min_price;
     }else{
         $price = 0;
     }   
     return $price;

   }

   public static function SatuanProduct($uoms_id)
   {
     $satuan = DB::table('uoms')->where('id',$uoms_id)->first()->name;
     return $satuan;
   } 

   public static function PhotoProduct($product_id){
       
       // $FileProduct = 'https://dev2-ecatalog.scmwika.com/product_gallery/';
        $FilePublic = GeneralPaginate::FilePublic();
    
       $cover = ProductGallery::where('product_id',$product_id)->first();
       if($cover !=null)
       {
           $photo = '/images/product/'.$cover->filename;
       }else{
           $photo = url($FilePublic.'no-image.png');
       } 
        
        return $photo;       
   }

   public static function PhotoProductMulti($product_id){
       
       $FileProduct = '/images/product/';
     
       $temp = array();
       $cover = ProductGallery::where('product_id',$product_id)->skip(0)->take(5)->get();
       foreach ($cover as $key => $val)
       {
          $temp[$key]['id'] = $val->id;
          if($key ==0)
          {
            $temp[$key]['active'] = true;
          }else{
            $temp[$key]['active'] = false; 
          }   
          $temp[$key]['image'] = $FileProduct.$val->filename;
       } 
       return $temp;    
        
   }

   public static function LocationProduct($product_id){

     $product = PaymentProduct::where('product_id',$product_id)->first();
     if($product)
     {
        $location = ['id'=>(int)$product->location_id,'name'=>RequestFrontend::Location($product->location_id)];
     }else{
        $location = ['id'=>0,'name'=>'']; 
     }   
     return $location;

   }

    public static function CheckAddproductAction($vendor_id)
    {

      $data = DB::table('cart')->where(['vendor_id' => $vendor_id, 'created_by' => Auth::User()->id, 'action' => 'rev'])->exists();
      return $data;    
         
    }

    public static function CheckApprovByNotNUll()
    {
       
        $groups = DB::table('approve_po_rules')->where('departemen_id',Auth::User()->group_id)->exists();
        return !$groups;  
    }

    public static function CheckGroupByNotNUll()
    {
        $groups_id = Auth::User()->group_id;

        $status = $groups_id && DB::table('groups')
            ->where('id', $groups_id)
            ->whereNotNull('role_id_general_manager')
            ->exists();

        return !$status;

    }

    public static function CheckTypeVendor($type)
    {
         if($type == 'terkontrak')
         {
            $result = false;
         }else{
            $result = true;
         } 

         return $result;  
    }


   public static function Location($location_id)
   {
        $location = Location::where('location_id',$location_id)->first()->name;
        return $location;
   }


   public static function RateProduct($registration_number){

    
      $rate = VpiVendor::select(DB::raw('max(year) year'), DB::raw('max(month) month'),'overall_score')
              // ->groupBy('vvh_vendor_id')
              ->where('vendor_registration_number',$registration_number)
              // ->whereNotNull('overall_score')
              ->first();
    
      $total = 0;
      if($rate)
      {
        if($rate->overall_score !=null)
        {
          $total =  RequestFrontend::ConditionRate((int)$rate->overall_score); 
        }else{
          $total = 0;   
        }     
    
      } 
      
      return $total;

   }




   public static function ConditionRate($total)
   {
        if ($total > 0 && $total < 201) 
        {
            $star = 1;
        } else if ($total > 200 && $total < 401) {
            $star = 2;
        } else if ($total > 400 && $total < 601) {
            $star = 3;
        } else if ($total > 600 && $total < 801) {
            $star = 4;
        } else if ($total > 800) {
            $star = 5;
        }
        return $star;
   }

   public static function WishlistProduct($product_id){

      $Wishlist = Wishlist::where(['product_id'=>$product_id,'user_id'=>Auth::User()->id])->first();
      if($Wishlist !=null)
      {
        $wish = true;
      }else{
        $wish = false;
      }  
      return $wish;

   }

   public static function StatusProduct($verified_status){

         if($verified_status ==0)
         {
            $status = 'unverified';
         }else{
           $status = 'verified';
         }   

          return $status;              

   }

   public static function TypeProduct($product_id,$type){
      $result = "";
      $status = DB::table('project_product_price as a')
      // ->select(
      //   'a.product_id',
      //   'b.start_contract',
      //   'b.end_contract',
      //   'c.start_contract as start2',
      //   'c.end_contract as end2',
      //   'b.id',
      //   'c.id')
                ->join('project as b','a.project_id','=','b.id')
                ->join('amandemen as c','b.last_amandemen_id','=','c.id')
                ->where([
                    'a.is_deleted' => 0,
                    'b.is_deleted' => 0,
                    'a.product_id' => $product_id,
                  ])
                ->whereRaw("IFNULL(c.start_contract, b.start_contract) <= CURRENT_DATE ()")
                ->whereRaw("IFNULL(c.end_contract, b.end_contract) >= CURRENT_DATE ()")
                ->first();
        
        if($status !=null)
        {
            
            if($type =='slug')
            {
                $result = "terkontrak";
            }else{
                $result = "Terkontrak";
            } 
        }else{
           if($type =='slug')
           {
             $result = "non-kontrak";
           }else{
             $result = "Non Kontrak";
           } 
            
        }    
       return $result;       
   }

   

   public static function CategoryFast()
   {
      $__temp_ = array();
    
      $category =  DB::select(DB::raw("SELECT `b`.`kode` as `id`, `a`.`name`,`a`.`image`
                  FROM resources_code a
                  JOIN (SELECT LEFT(code_1, 2) as kode FROM product where product.is_deleted=0 GROUP by kode) b ON a.code LIKE CONCAT(b.kode, '%')
                  where a.level=2 
                "));

     foreach ($category as $key => $val)
     {
       $__temp_[$key]['id'] = $val->id;
       $__temp_[$key]['photo'] = url('file/category/'.$val->image);
       $__temp_[$key]['name'] =  RequestFrontend::FilterCategoryName($val->name,'listview');
       $__temp_[$key]['tooltip_name'] = $val->name;
     }   

     return  $__temp_;

   }




  
   public static function FilterCategoryName($name,$category){

      $filter = explode('/',$name);
      if(count($filter) > 1)
      { 
        if($category =='listview')
        {
           $category = RequestFrontend::limitText($filter[0],'15');
        }else{
           $category = $filter[0];
        }    
        
      }else{
        if($category =='listview')
        {
            $category = RequestFrontend::limitText($name,'15');
        }else{
            $category = $name;
        }    
      }  
      return $category; 

   }


   public static function ContractProduct($product_id){

        $contract =  DB::table('project_products as a')
                 ->select(DB::raw('MIN(c.start_contract) as start_contract'),DB::raw('MAX(c.end_contract) as end_contract') )
                 ->join('project as b','a.project_id','=','b.id')

                ->join('amandemen as c','b.last_amandemen_id','=','c.id')
                ->where([
                    'a.is_deleted' => 0,
                    'b.is_deleted' => 0,
                    'a.product_id' => $product_id,
                  ])
                ->whereRaw("IFNULL(c.start_contract, b.start_contract) <= CURRENT_DATE ()")
                ->whereRaw("IFNULL(c.end_contract, b.end_contract) >= CURRENT_DATE ()")

                ->groupBy('a.product_id')
                ->get();

        if($contract !="[]")
        {
            $result = GeneralHelpers::tanggal($contract[0]->start_contract).' - '.GeneralHelpers::tanggal($contract[0]->end_contract);  
        }else{
           $result = "-";
        }    
        return  $result;         

   }


  public static function StatusMatgis($resource_code){
     $result = array();
     $data = DB::table('resources_code')->select('sts_matgis')->where('code',$resource_code)->first();
     if($data)
     {
        $matgis = $data->sts_matgis == 1 ? 'Matgis' : 'Non Matgis';
        $result = $matgis;
     }   

     return $result;

  }


  public static function ProjectByProduct($code_sda){
    $result = array();
      $prefix = substr($code_sda, 0, 2); // Mengambil dua huruf pertama

      $data = DB::table('pr_request_product as a')
              ->join('pr_request as b','a.pr_request_id','=','b.id')
              ->join('project_new as c','b.project_id','=','c.id')
              ->select('b.id','c.id as project_id','c.name as project_name')
              ->where('a.kode_sda',$code_sda)
              ->groupBy('b.id')
              ->get();
      if($data)
      {
        $result  = $data;  
      }

       return $result; 
  }


  public static function RequestPRByProduct($code_sda,$request){
      
      $prefix = substr($code_sda, 0, 2); // Mengambil dua huruf pertama
      $result  =  array();
      $query = DB::table('pr_request_product as a')
              ->join('pr_request as b','a.pr_request_id','=','b.id')
              ->select('b.id','b.no_pr_request','a.kode_sda as sda_code','a.volume','a.tgl_dibutuhkan as date_required','a.keterangan as description');
         if($request->pr_request_id)
         {
             $query->where('b.id',$request->pr_request_id);
         }   
              $query->where('a.kode_sda',$code_sda);
        $data = $query->first();
      if($data)
      {
        $result  =  array('status'=>true,'data'=>$data,'message'=>'menampilkan Request PR'); 
      }else{
        $result = array('status'=>false,'data'=>'','message'=>'Request PR Belum di setting admin');
      
      }

       return $result;

  }

  public static function RequestPRByNoPR($pr_request_id){
      $result = array();
     

      $data = DB::table('pr_request_product as a')
              ->join('pr_request as b','a.pr_request_id','=','b.id')
              ->leftJoin('resources_code as c','a.kode_sda','=','c.code')
              ->select(
                'a.id as pr_request_id',
                'b.no_pr_request',
                'c.code as sda_code',
                'c.name as sda_name',
                'a.volume',
                'a.tgl_dibutuhkan as date_required',
                'a.keterangan as description'
              )
              ->where('a.pr_request_id',$pr_request_id)
              ->get();
        
        foreach($data as $key =>$val)
        {
          $result[$key]['value'] = $val->sda_code;
          $result[$key]['text'] = $val->sda_name;
          $result[$key]['pr_request_id'] = $val->pr_request_id;
          $result[$key]['no_pr_request'] = $val->no_pr_request;
          $result[$key]['volume'] = $val->volume;
          $result[$key]['date_required'] = $val->date_required;
          $result[$key]['description'] = $val->description; 
        }

       return $result; 

  }

  public static function SumberDaya($resource_code){
     $result = array();
     $data = DB::table('resources_code')->where('code',$resource_code)->first();
     if($data)
     {
       $result = ['code_sda'=>$data->code,'sda_name'=>$data->name,'code'=> substr($data->code, 0, 2)];
     }   

     return $result;

  }

 
   
 

  //filter

    public static function FilterSearch($project_id,$pr_request_id,$project_name,$pr_request,$price_min,$price_max,$rating,$category_id,$location_id,$status_vendor,$status_price,$payment_id,$vendor_name,$perPage)
    {
         

        $rate = RequestFrontend::SwitchRating($rating);
        $type = RequestFrontend::FilterTypeProduct();
        foreach($type as $row =>$val){
             $data[$row] = $val->id;
        }

        // $query  = DB::table('product as a');
        // $query->select('a.id','a.name','a.vendor_id','a.verified_status','a.created_at','a.code_1','a.uom_id');
        // $query->join('payment_product as b','a.id','=','b.product_id'); //harga  
        // $query->join('vendor as d','a.vendor_id','=','d.id'); // rating   

         $query = DB::table('pr_request as a')
                 ->select('c.id','c.name','c.vendor_id','c.verified_status','c.uom_id','c.created_at','a.id as pr_request_id','a.no_pr_request','a.project_id','d.name as project_name','b.kode_sda','b.nama_sda','b.volume','b.tgl_dibutuhkan','b.keterangan') 
                 ->join('pr_request_product as b','a.id','=','b.pr_request_id')
                 ->join('product as c','b.kode_sda','=','c.code_1')
                 ->join('project_new as d','a.project_id','=','d.id')
                 ->join('payment_product as e','c.id','=','e.product_id')
                 ->join('vendor as j','c.vendor_id','=','j.id'); // rating 


         if($project_id)
         {
            $query->where('a.project_id', 'like', '%'.$project_id.'%');//project
         } 

         if($pr_request_id)
         {
            $query->where('a.id', 'like', '%'.$pr_request_id.'%');//pr request
         }         
          
        if($category_id)
        {
            $query->whereRaw("b.kode_sda LIKE CONCAT(?, '%')", [$category_id]);
        } 

        if($location_id)
        {
             $query->where('d.location_id',$location_id);
             
        } 
            
         
                 
        if($price_min !="" && $price_max !="") 
        {
            
            $query->join('project_product_price as h','h.id','=','c.product_id');//harga
           
            //$query->whereBetween('c.price',[$price_min,$price_max]);//harga
            //$query->orWhereBetween('b.price',[$price_min,$price_max]);//harga

            $query->where(function($querybe) use ($price_min, $price_max){
                  $querybe->whereBetween('h.price', [$price_min,$price_max])
                        ->orWhereBetween('e.price', [$price_min,$price_max]);
                });
            

        }  

        if($rating !="")
        {
           // $query->join('vendor as d','a.vendor_id','=','d.id'); // rating
           // $query->join('vpi_vendor as e','d.scm_id','=','e.vvh_vendor_id');// rating
            

            $query->Join(DB::raw('(SELECT  MAX(vvh_id) max_vvh_id, vvh_vendor_id, vk_score_total 
                FROM      vpi_vendor 
                GROUP BY  vvh_vendor_id) as i'),
              function($join)
              {
                    $join->on('j.scm_id', '=', 'i.vvh_vendor_id');
              });
              
              if($rating !="0")
              {
                 $query->whereBetween('i.vk_score_total',[$rate['start'],$rate['end']]);// rating 
              }else{
                 $query->whereNull('i.vk_score_total'); 
              } 

        }

        if($status_vendor !="")
        {
             if($status_vendor =="non-kontrak")
             {
                  $query->whereNotIn('h.id',$data);
             }else if($status_vendor =="terkontrak"){
                 $query->whereIn('h.id',$data);
             }
        }

        if($status_price !="")
        {
            if($status_price =='unverified')
             {
               $query->where('h.verified_status',0); 
             }else{
               $query->where('h.verified_status',1); 
             }   
        }

        if($vendor_name !="")
        {
            // $query->where(function($querybe) use ($vendor_name){
            //       $querybe->where('d.name','LIKE','%'.$vendor_name.'%') 
            //             ->orWhere('a.name','LIKE','%'.$vendor_name.'%'); 
            //     });
           $query->where('j.name','LIKE','%'.$vendor_name.'%');  
           $query->orWhere('j.name','LIKE','%'.$vendor_name.'%'); 
        }    


        if($payment_id !="")
        {
           //$query->join('payment_product as h','a.id','=','h.product_id');
           $query->where('e.payment_id',$payment_id);  
        }    
        
        $query->where(['c.is_deleted' => 0]);
        $query->groupBy('e.product_id');
        $query->orderBy('c.id','ASC');
        $results = $query->paginate($perPage);
                
       

        
    
     //
       $desc = RequestFrontend::FilterDescription($project_name,$pr_request,$price_min,$price_max,$rating,$category_id,$location_id,$status_vendor,$status_price,$payment_id,$vendor_name);
        
      
     return array('result'=>$results,'description'=>$desc);
       
    }   


    


    public static function FilterTypeProduct(){

         $results = DB::table('project_product_price as a')
                    ->join('project as b', 'a.project_id', '=', 'b.id')
                    ->join('amandemen as c', 'b.last_amandemen_id', '=', 'c.id')
                    ->join('product as d', 'a.product_id', '=', 'd.id')
                    ->where([
                        'a.is_deleted' => 0,
                        'b.is_deleted' => 0,
                    ])
                    ->whereRaw("IFNULL(c.start_contract, b.start_contract) <= CURRENT_DATE()")
                    ->whereRaw("IFNULL(c.end_contract, b.end_contract) >= CURRENT_DATE()")
                    ->groupBy('a.product_id')
                    ->pluck('d.id')
                    ->toArray();

        return $results;        
    } 


     public static function Description($text_search,$project_id,$pr_request_id,$price_min,$price_max,$rating,$category_id,$location_id,$status_vendor,$status_price,$payment_id,$vendor_name)
    {
        

        

        if($text_search)
        {
            $text = '<b>'.$text_search.'</b>';
        }   

        if($project_id)
        {
           $Project = ProjectNew::find($project_id);
           $project_name = 'Project <b>'.$Project->name.'</b>'; 
        }else{
           $project_name = ''; 
        } 

         if($pr_request_id !="")
        {
           $PRRequest = DB::table('pr_request')->find($pr_request_id);
           $pr_request = 'No PR Request <b>'.$PRRequest->no_pr_request.'</b>'; 
        }else{
           $pr_request = ''; 
        }         
 
       if($category_id !="")
       {
            $category = ResourceCode::where('code', 'like', ''.$category_id.'%')->where('level',3)->first();
            if($category !=null)
            {
                $category_name = ' <b>'.$category->name.'</b>';
            }else{
                if($text_search !="")
                {
                    $category_name = $category_id; 
                }else{
                     $category_name = '';  
                }    
              
            }  
            
            $kategori = $category_name;
       }else{
            $kategori = "";
       }

       if($location_id !="")
       {
           $loc = Location::where('location_id',$location_id)->first();
           if($loc !="")
           {
            $lokasi = ' <b>'.$loc->name.'</b>';
           }else{
             $lokasi = "";
           } 
        
       }else{
          $lokasi = "";
       } 


       if($price_min !="" && $price_max)
        {
            $price = 'Harga dari '.$price_min.' - '.$price_max;
        }else{
            $price = "";
        } 

        if($rating !="")
        {
           $rat = 'rating bintang <b>'.$rating.'</b>'; 
        }else{
           $rat = ''; 
        }   

       if($status_vendor !="")
       {
        $st_vendor = 'status vendor <b>'.$status_vendor.'</b>';
       }else{
        $st_vendor = '';
       }

        if($status_price !="")
       {
        $st_price = 'status harga <b>'.$status_price.'</b>';
       }else{
        $st_price = '';
       }

       if($payment_id !="")
       {
          $pay = DB::table('payment_method as a')
                    ->select('a.id','a.day','b.name')
                    ->join('enum_payment_method as b','b.id','=','a.enum_payment_method_id')
                    ->where('a.id',$payment_id)
                    ->first();

          if($pay !=null)
          {
            $payment = 'metode pembayaran <b>'.ucfirst(strtolower($pay->name.' '.$pay->day.' hari')).'</b>';
          }else{
            $payment =  '';
          }           
        
        }else{
            $payment =  '';
        }

         return RequestFrontend::HandleDescription($text_search,$project_name,$pr_request,$kategori,$lokasi,$price,$rat,$st_vendor,$st_price,$payment);
         //return $project_name.'  '.$pr_request.' '.$kategori.' '.$lokasi.' '.$price.' '.$st_vendor.' '.$st_price.' '.$payment; 


    }

    public static function HandleDescription($text_search,$project_name,$pr_request,$kategori,$lokasi,$price,$rating,$st_vendor,$st_price,$payment){

          


        if($text_search !="")
        {

            if ($kategori == "" && $lokasi == "") {
                return ucfirst($text_search);
            }

            if ($kategori != "" && $lokasi == "" ) {
                return ucfirst($text_search) . ' dalam kategori ' . ucfirst($kategori);
            }

            if ($kategori != "" && $lokasi != "" ) {
                return ucfirst($text_search) . ' dalam kategori ' . ucfirst($kategori) . ' di lokasi ' . ucfirst($lokasi);
            }



        }   


        if ($project_name != "") {
            if ($pr_request == "" && $kategori == "" && $lokasi == "" && $price == "" && $rating == "" && $st_vendor == "" && $st_price == "" && $payment == "") {
                return $project_name;
            }

            if ($pr_request != "" && $kategori == "" && $lokasi == "" && $price == "" && $rating == "" && $st_vendor == "" && $st_price == "" && $payment == "") {
                return $project_name . ' dengan ' . $pr_request;
            }

            if ($pr_request != "" && $kategori != "" && $lokasi == "" && $price == "" && $rating == "" && $st_vendor == "" && $st_price == "" && $payment == "") {
                return $project_name . ' dengan ' . $pr_request . ' dalam kategori ' . $kategori;
            }

            if ($pr_request != "" && $kategori != "" && $lokasi != "" && $price == "" && $rating == "" && $st_vendor == "" && $st_price == "" && $payment == "") {
                return $project_name . ' dengan ' . $pr_request . ' dalam kategori ' . $kategori . ' di lokasi ' . $lokasi;
            }

            if ($pr_request != "" && $kategori != "" && $lokasi != "" && $price != "" && $rating == "" && $st_vendor == "" && $st_price == "" && $payment == "") {
                return $project_name . ' dengan ' . $pr_request . ' dalam kategori ' . $kategori . ' di lokasi ' . $lokasi.' dengan rentang '.$price;
            }

            if ($pr_request != "" && $kategori != "" && $lokasi != "" && $price != "" && $rating != "" && $st_vendor == "" && $st_price == "" && $payment == "") {
                return $project_name . ' dengan ' . $pr_request . ' dalam kategori ' . $kategori . ' di lokasi ' . $lokasi.' dengan rentang '.$price.' , pilihan '.$rating;
            }

            if ($pr_request != "" && $kategori != "" && $lokasi != "" && $price != "" && $rating != "" && $st_vendor != "" && $st_price == "" && $payment == "") {
                return $project_name . ' dengan ' . $pr_request . ' dalam kategori ' . $kategori . ' di lokasi ' . $lokasi.' dengan rentang '.$price.' , pilihan '.$rating.' dan '.$st_vendor;
            }


            if ($pr_request != "" && $kategori != "" && $lokasi != "" && $price != "" && $rating != "" && $st_vendor != "" && $st_price != "" && $payment == "") {
                return $project_name . ' dengan ' . $pr_request . ' dalam kategori ' . $kategori . ' di lokasi ' . $lokasi.' dengan rentang '.$price.' , pilihan '.$rating.' dan '.$st_vendor.' , '.$st_price;
            }


            if ($pr_request != "" && $kategori != "" && $lokasi != "" && $price != "" && $rating != "" && $st_vendor != "" && $st_price != "" && $payment != "") {
                return $project_name . ' dengan ' . $pr_request . ' dalam kategori ' . $kategori . ' di lokasi ' . $lokasi.' dengan rentang '.$price.' , pilihan '.$rating.' dan '.$st_vendor.' , '.$st_price.' dengan menggunakan '.$payment;
            }

            
        }


         if ($price != "" ) 
         {
            if ($price != "" && $rating == "" && $st_vendor == "" && $st_price == "" && $payment == "") {
                return ' rentang '.$price;
            }

            if ($price != "" && $rating != "" && $st_vendor == "" && $st_price == "" && $payment == "") {
                return ' rentang '.$price.' dengan pilihan '.$rating;
            }

            if ($price != "" && $rating != "" && $st_vendor != "" && $st_price == "" && $payment == "") {
                return ' rentang '.$price.' dengan pilihan '.$rating.' dan '.$st_vendor;
            }


            if ($price != "" && $rating != "" && $st_vendor != "" && $st_price != "" && $payment == "") {
                return ' rentang '.$price.' dengan pilihan '.$rating.' , '.$st_vendor.' dan '.$st_price;
            }


            if ($price != "" && $rating != "" && $st_vendor != "" && $st_price != "" && $payment != "") {
                return ' rentang '.$price.' dengan pilihan '.$rating.' , '.$st_vendor.' dan '.$st_price.' dengan menggunakan '.$payment;
            }   
            
         }  


         if ($rating != "" ) 
         {
           

            if ($rating != "" && $st_vendor == "" && $st_price == "" && $payment == "") {
                return ' pilihan '.$rating;
            }

            if ($rating != "" && $st_vendor != "" && $st_price == "" && $payment == "") {
                return ' pilihan '.$rating.' dan '.$st_vendor;
            }


            if ($rating != "" && $st_vendor != "" && $st_price != "" && $payment == "") {
                return ' pilihan '.$rating.' , '.$st_vendor.' dan '.$st_price;
            }


            if ($rating != "" && $st_vendor != "" && $st_price != "" && $payment != "") {
                return' pilihan '.$rating.' , '.$st_vendor.' dan '.$st_price.' dengan menggunakan '.$payment;
            }   
            
         } 


         if ($st_vendor != "" ) 
         {
           
            if ($st_vendor != "" && $st_price == "" && $payment == "") {
                return $st_vendor;
            }


            if ($st_vendor != "" && $st_price != "" && $payment == "") {
                return $st_vendor.' dan '.$st_price;
            }


            if ($st_vendor != "" && $st_price != "" && $payment != "") {
                return $st_vendor.' , '.$st_price.' dengan menggunakan '.$payment;
            }   
            
         }  


         if ($st_price != "" ) 
         {
           
            if ($st_price != "" && $payment == "") {
                return $st_price;
            }


            if ($st_price != "" && $payment != "") {
                return $st_price.' dengan menggunakan '.$payment;
            }   
            
         } 

          if ($payment != "") {
                return ' menggunakan '.$payment;
           }    
  


    }
   
   

    public static function ParamPage($id){


        $_res = array();
        switch ($id)
        {
          case 'all':
            $param = 'all';
          break;  
          case 'material':
            $param = 'A';
          break;
          case 'upah':
             $param = 'C';
          break;
          case 'alat':
            $param = 'D';
          break;
          case 'subkon':
            $param = 'E';
          break;

          case 'tertinggi':
            $param = 'tertinggi';
           
          break;
          case 'terendah':
            $param = 'terendah';
          break;

          case 'terkontrak':
            $param = 'terkontrak';
           
          break;
          case 'non-kontrak':
            $param = 'non-kontrak';
          break;
          case 'verified':
            $param = 'verified';
          break; 
          case 'start-0':
            $param = 'start-0';
          break;
          case 'start-1':
            $param = 'start-1';
          break;
          case 'start-2':
            $param = 'start-2';
          break;
          case 'start-3':
            $param = 'start-3';
          break;
          case 'start-4':
            $param = 'start-4';
          break;
          case 'start-5':
            $param = 'start-5';
          break;    
          default:
            $param = '';
         
        }
        return $param;
    }

    public static function ProvinceName($id)
    {

        $province = DB::table('ref_locations')->select('province_name')->where('location_id',$id)->first();
        if($province !=null)
        {
           $title = "Provinsi ".ucfirst(strtolower($province->province_name)); 
        }else{
           $title = "";  
        }  

        return $title;      
    }

    public static function ExplodeRating($rate){

        if($rate =="start-0" || $rate =="start-1" || $rate =="start-2" || $rate =="start-3" || $rate =="start-4" || $rate =="start-5")
        {
           $rating = explode('-',$rate);
           $result = RequestFrontend::SwitchRating($rating[1]); 
        }else{
            $result = "";
        }    
       return $result;
    }

    public static function SwitchRating($rating)
    {

        $start = '';
        $end = '';

        switch ($rating) {
          case '1':
            $start = 0;
            $end = 200;
          break;
          case '2':
            $start = 201;
            $end = 400;
          break;
          case '3':
            $start = 401;
            $end = 600;
          break;
          case '4':
            $start = 601;
            $end = 800;   
          break;
          case '5':
            $start = 801;
            $end = 1000;
          break;
          default:
            $start = 0;
            $end = 0;
        }
        return array('start'=>$start,'end'=>$end);
    }


    //Order
    public static function OrderStatusApproved($order_code){
     $status = DB::table('approve_po_list')->select('status_approve')->where('order_no',$order_code)->first();
     if($status !=null)
     {
         
        $approv = "Approved ".$status->status_approve;
        
     }else{
         $approv = "Belum Approved";
     } 

     return $approv;  


    }

    public static function OrderDestionation($destination_id){
       $destination = DB::table('ref_locations')->select('province_name','regency_name')->where('location_id',$destination_id)->first();
       if($destination !=null)
       {
             
            $dest = $destination->province_name.' ( '.$destination->regency_name.' )';
            
       }else{
             $dest = "Pengiriman destinasi kosong";
       } 

       return $dest;

    }

    public static function OrderShipping($order_code){
       $shipping = DB::table('order as a')
                     ->select('b.name')
                     ->join('tod as b','a.shipping_id','=','b.id')
                     ->where('a.order_no',$order_code)
                     ->first();
       if($shipping !=null)
       {  
            $ship = $shipping->name;
       }else{
            $ship = "Shipping kosong";
       } 

       return $ship;

    }


    public static function OrderDevision($group_id){
      $division = DB::table('groups')->select('name')->where('id',$group_id)->first();
      if($division !=null)
      {
         $dev = $division->name;
      }else{
         $dev = "divisi masih kosong";
      }  

      return $dev; 
    }


    
    public static function CheckRoleButton()
    {

        $user = DB::table('users_roles')->where('user_id', Auth::user()->id)->first();
        $result = $user && $user->role_id != 3;
        return $result;

       
    }


    public static function ServerBackend($url,$type)
    {


            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $response = curl_exec($ch); 
            curl_close($ch);      
            $result = json_decode($response);
           
            if($type =="single")
            {
                $hasil =  $result->data[0]->file;  
            }else{
                $arr = array();
                foreach($result->data as $key =>$val)
                {
                    $arr[$key]['id'] = $key;
                    if($key ==0)
                    {
                      $arr[$key]['active'] = true;
                    }else{
                     $arr[$key]['active'] = false; 
                    }   
                    
                    $arr[$key]['image'] = $val->file;
                    
                }
                $hasil = $arr;   
            }   
            return $hasil;



    }

    public static function TotalSelling($product_id)
    {
        $product = DB::table('product as a')
         //->select( DB::raw('SUM(b.qty) AS total'))
         ->join('order_product as b','a.id','=','b.product_id')
         ->join('order as c','b.order_no','=','c.order_no')
         ->where(['c.is_approve_complete'=>1,'a.id'=>$product_id])->sum('b.qty');

        return $product.' Item'; 
    }

    public static function TotalView($product_id)
    {
        $product = Viewer::where(['product_id'=>$product_id])->count();
         if($product)
         {
            $result = $product.'x dikunjungi';
         }else{
            $result = '0x dikunjungi';
         }   


        return $result; 
    }

    public static function DetailView($product_id)
    {
         $product = Viewer::where(['product_id'=>$product_id])->paginate(10);
         $result = array();

         $page = $product->currentPage();
        

         foreach($product  as $key =>$value)
         {
           $result[$key]['no'] =   $page++; 
           $result[$key]['fullname'] = $value->user->first_name.' '.$value->user->last_name;
           $result[$key]['departement'] = RequestFrontend::Groups($value->user->group_id);
           $result[$key]['last_date'] = GeneralHelpers::tanggal_indo($value->created_at);
         }
           


        return $result; 
    }

    public static function Groups($group_id)
    {
        if($group_id !=null)
        {
            $result = DB::table('groups')->where('id',$group_id)->first()->name;
        }else{
            $result = '-';
        }    
        return $result;
    }

    public static function ArchiveProduct($product_id){
         $product = DB::table('product as a')->where(['a.is_deleted'=>1,'a.id'=>$product_id])->first();
         if($product !=null){
            return array('status'=>true,'class'=>'opac-03');
         }else{
            return array('status'=>false,'class'=>'opac-none');
         }

    }


    public static function Rupiah($angka){
    
        $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
        return $hasil_rupiah;
 
    }


   

}