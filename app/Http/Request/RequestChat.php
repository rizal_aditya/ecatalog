<?php

namespace App\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\User;
use App\Models\Vendor;
use App\Models\RoomChat;
use App\Models\RoomChatDetail;
use Illuminate\Support\Facades\Hash;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestNotif;
use App\Helpers\SSO\SsoManagement;
use DB;

class RequestChat
{

   public static function CheckOpenChat($send_to){

     $check = DB::table('room_chat')->where(['from'=>Auth::User()->id,'to'=>$send_to])->first();
     if($check !=null)
     {
        $data = true;
     }else{
         $data = false;
     }
     return $data;   
   }

   public static function getDataUserChat(){
         $auth = Auth::User();
         $id = $auth->id;
        $user = DB::table('users as a')
                ->select('a.id','a.username','a.first_name','a.last_name','a.photo','a.last_chat') 
                ->where('id',$id)->first();
      
        
        $FileAkun = GeneralPaginate::FileAkun();
        $FilePublic = GeneralPaginate::FilePublic();   
        if($user->photo ==null || $user->photo =="")
        {
            $photo = url($FilePublic.'2010526.png');
        }else{
            $photo = url($FileAkun.$user->photo);
        }

        if($user->first_name !="" && $user->last_name !="")
        {
            $name = $user->first_name.' '.$user->last_name;
        }else if($user->first_name !="" && $user->last_name ==""){
             $name = $user->first_name;
        }else{
             $name = $user->username;
        }   
         
        $data = array('user_id'=>$user->id,'username'=>$name,'photo'=>$photo,'last_chat'=> GeneralHelpers::last_chat($user->last_chat));
        return $data;

   }

   




   public static function getListChat(){
      $auth = Auth::User();
      $id = $auth->id;
     $access = $_COOKIE['access'];

         
         $room = RoomChat::where('to',$id)->orWhere('from',$id)->first();
         if($room != null)
         {
            
             $query = DB::table('room_chat as a');     
             if($access ==3)
             {
                 $query->where('a.to',$room->to); 
             }else{
                 $query->where('a.from',$room->from) ;
             }   
             
             $query->orderBy('a.id','DESC');
             $listRoom = $query->get();
             $arr =   RequestChat::GetChatList($listRoom);

         }   
      
    
      return $arr;

   }


   public static function GetChatList($listRoom){

            $access = $_COOKIE['access'];
            $arr = array();
            foreach($listRoom as $key => $value)
             {

                if($access !=3)
                {
                  $arr[$key]['user_id'] = $value->to;
                  $arr[$key]['view'] = RequestChat::GetCheckText($value->id);
                  $arr[$key]['fullname'] = RequestChat::GetVendor($value->to,'name');
                  $arr[$key]['photo'] = RequestChat::GetVendor($value->to,'photo');
                  $arr[$key]['last_chat'] = RequestChat::GetVendor($value->to,'last');
                  $arr[$key]['user_chat'] = RequestChat::GetUserChat($value->to);
                }else{
                  $arr[$key]['user_id'] = $value->from;
                  $arr[$key]['fullname'] = RequestChat::GetVendor($value->from,'name');
                  $arr[$key]['photo'] = RequestChat::GetVendor($value->from,'photo');
                  $arr[$key]['last_chat'] = RequestChat::GetVendor($value->from,'last');
                  $arr[$key]['user_chat'] = RequestChat::GetUserChat($value->from);
                }   

                  if($arr[$key]['user_chat'] == true)
                  {
                     $arr[$key]['user_chat_color'] = "color-online";
                  }else{
                     $arr[$key]['user_chat_color'] = "color-offline";
                  }  
                  

                 $arr[$key]['id'] = $value->id;
                 $arr[$key]['last_text'] = RequestChat::GetlastText($value->id);
                 $arr[$key]['active_chat'] = $value->active;
                 // $arr[$key]['chat_list'] = RequestChat::GetRoomList($value->id,$data['id']);
              } 
          return $arr;
   }


   public static function GetVendor($id,$type)
   {
     $vendor = DB::table('users')->select('username','first_name','last_name','photo','last_chat','active_chat')->where('id',$id)->first();
     if($type =="name")
     {
        if($vendor->first_name !="" && $vendor->last_name !="")
        {
             $result = $vendor->first_name.' '.$vendor->last_name;
        }else if($vendor->first_name !="" && $vendor->last_name ==""){
             $result = $vendor->first_name;
        }else{
             $result = $vendor->username;
        } 

     }else if($type =="photo"){

        $FileAkun = GeneralPaginate::FileAkun();
        $FilePublic = GeneralPaginate::FilePublic();   
        if($vendor->photo ==null || $vendor->photo =="")
        {
            $result = url($FilePublic.'2010526.png');
        }else{
            $result = url($FileAkun.$vendor->photo);
        }

     }else if($type =="last"){

        if($vendor->last_chat ==null)
        {
         
          DB::table('users')->where('id',$id)->update(['last_chat'=>date('Y-m-d h:i:s')]);
        }
        $vendor_new = DB::table('users')->select('username','first_name','last_name','photo','last_chat','active_chat')->where('id',$id)->first();
        $result =  GeneralHelpers::last_chat($vendor_new->last_chat);  
           
          
     }  

     return $result;

   }

   public static function GetUserChat($id)
   {
     $vendor = DB::table('users')->where('id',$id)->first();
     
     if($vendor->active_chat == 'false')
     {
        return false;
     }else{
        return true; 
     }   

     

   }

   public static function GetCheckText($room_id){
       $chat = DB::table('room_chat_detail')->where(['room_id'=>$room_id])->count();
       if($chat > 0)
       {
         $status = true;
       }else{
         $status = false;
       } 
       return $status;
   }

   public static function GetlastText($id)
   {
         $last = DB::table('room_chat_detail')->where(['room_id'=>$id])->first();
         if($last !=null)
         {
            if($last->type !="json")
             {
                $text = $last->textmessages;
             }else{
                $text = '';
             }   
            return $text; 
         }else{
            return ''; 
         }   
         
 
   }

   public static function GetRoomList($id,$user_id)
   {
      $data = DB::table('room_chat_detail')->where('room_id',$id)->paginate(2);
      
      $arr = array();
      foreach($data as $key => $value)
      {
         $arr[$key]['id'] = $value->id;
         $arr[$key]['created_at'] = RequestChat::GetSplitTime($value->created_at);
         if($user_id ==  $value->from)
         {
            $arr[$key]['position'] = "right";
         }else{
            $arr[$key]['position'] = "left";
         }
         $arr[$key]['type'] = $value->type;
         $arr[$key]['from'] = $value->from;
         if($value->type =="photo")
         {
              $arr[$key]['textmessages'] = json_decode($value->textmessages);
         }else if($value->type =="json"){
            $arr[$key]['textmessages'] = json_decode($value->textmessages); 
         }else{
            $arr[$key]['textmessages'] = $value->textmessages;
         }   
         
      }  
      return $arr;
   }


    public static function GetRoomListLast($id,$user_id)
   {
      $data = DB::table('room_chat_detail')->where('room_id',$id)->first();
      
      $arr = array();
      
         $arr['id'] = $data->id;
         $arr['created_at'] = RequestChat::GetSplitTime($data->created_at);
         if($user_id ==  $data->from)
         {
            $arr['position'] = "right";
         }else{
            $arr['position'] = "left";
         }
         $arr['type'] = $data->type;
         $arr['from'] = $data->from;
         if($data->type =="photo")
         {
              $arr['textmessages'] = json_decode($data->textmessages);
         }else if($data->type =="json"){
            $arr['textmessages'] = json_decode($data->textmessages); 
         }else{
            $arr['textmessages'] = $data->textmessages;
         }   
         
   
      return $arr;
   }


   public static function GetSplitTime($time)
   {
       $expl = explode(' ',$time);
       $times = explode(':',$expl[1]);

       return $times[0].':'.$times[1];
 
   }
   

   


   


}