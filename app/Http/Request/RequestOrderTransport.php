<?php

namespace App\Http\Request;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\ResourceCode;
use App\Models\ProjectProductPrice;
use App\Models\ProductGallery;
use App\Models\PaymentProduct;
use App\Models\PaymentMethod;
use App\Models\Location;
use App\Models\ProjectProduct;
use App\Models\Project;
use App\Models\Vendor;
use App\Models\VpiVendor;
use App\Models\Wishlist;
use App\Http\Request\RequestAuth;
use App\Http\Request\RequestFrontend;
use DB;

class RequestOrderTransport 
{



    public static function VendorTransport($order_code){
       $transport = DB::table('order_transportasi as a')
       ->select(
        'c.id as vendor_transport_id',
        'c.name as vendor_transport',
        'c.address as vendor_address',
        'a.biaya_transport as fee_transport',
        'a.keterangan_transport as transport_price',
        'a.weight_minimum',
        'd.name as transport_name',
        'a.pdf_name',
        DB::raw('SUM(e.qty*e.weight*e.price) AS total_price'),
        DB::raw('SUM(e.qty*e.weight) AS total_berat')
      )
       ->join('transportasi as b','a.transportasi_id','=','b.id')
       ->join('vendor as c','b.vendor_id','=','c.id')
       ->join('resources_code as d','b.sda_code','=','d.code')
       ->join('order_transportasi_d as e','a.order_no','=','e.order_no')
       ->where('a.order_no',$order_code)->first();
       

       return $transport;
    }













   
     public static function OrderOrigin($vendor_id){

        $origin = DB::table('vendor_lokasi')->select('prov_name','kota_name')->where('vendor_id',$vendor_id)->first();

        if($origin !=null)
        {
            $orig = $origin->prov_name.' ( '.$origin->kota_name.' )';
        }else{
            $orig = '';
        }    

        return $orig;

    }

    public static function OrderDestionation($project_id){
       $destination = DB::table('project_new')->select('alamat')->where('id',$project_id)->first();
       if($destination !=null)
       {
             
            $dest = $destination->alamat;
            
       }else{
             $dest = "";
       } 

       return $dest;

    }

     public static function OrderByLocation($destination_id){
       $destination = DB::table('ref_locations as a')
                      ->select('a.province_name')
                      ->where('a.location_id',$destination_id)->first();
       if($destination !=null)
       {
             
            $dest = $destination->province_name;
            
       }else{
             $dest = "";
       } 

       return $dest;

    }


    public static function OrderTransport($order_code){
     $__temp_ = array();
     $product = DB::table('order_transportasi_d as a')
                 ->select('c.biaya_transport','b.id','b.name as product_name','a.qty','a.price','a.weight','b.uom_id')
                 ->join('product as b','a.product_id','=','b.id')
                 ->join('order_transportasi as c','a.order_no','=','c.order_no')
                 
                 ->where(['a.order_no'=>$order_code])
                 ->limit(2)
                 ->get();

     
      foreach ($product as $key => $val)
      {
           $__temp_[$key]['id'] = $val->id;
           $__temp_[$key]['photo'] = RequestFrontend::PhotoProduct($val->id);
           $__temp_[$key]['product_name'] = RequestFrontend::limitText($val->product_name,'50');
           $__temp_[$key]['qty'] = $val->qty;
           $__temp_[$key]['weight'] = $val->weight.' Kg';
           $__temp_[$key]['volume'] = $val->weight;
           $__temp_[$key]['payment'] = RequestOrder::OrderPayment($order_code);
           $__temp_[$key]['price'] = GeneralHelpers::Rupiah($val->biaya_transport);
           $__temp_[$key]['total_price'] = RequestOrder::OrderTotalPriceItem($val->qty,$val->weight,$val->biaya_transport);
      }       
     
     return $__temp_;

   }

    public static function TotalQty($order_code){
        $payment = DB::table('order_transportasi_d')
        ->select( DB::raw('SUM(qty) AS total_qty'))
        ->where('order_no',$order_code)
        ->first();

        if($payment !=null)
        {
           $total = $payment->total_qty.' Item';
        }else{
           $total = "0 Item";
        } 
        return $total;

    }

    public static function TotalWeight($order_code){
       
        $payment = RequestOrderTransport::TotalWeightTransport($order_code);    
        if($payment !=null)
        {
           $total = $payment.' Kg';
        }else{
           $total = "";
        }    
         
        return $total;
    }

    public static function TotalWeightTransport($order_code){

         $payment = DB::table('order_transportasi_d')
        ->select( DB::raw('SUM(qty*weight) AS total_berat'))
        ->where('order_no',$order_code)
        ->first();

        if($payment !=null)
        {
           $total = $payment->total_berat;
        }else{
           $total = "";
        } 

        return $total;
    }

     public static function TotalItem($order_code)
     {
       
        $payment = DB::table('order_transportasi_d')->where('order_no',$order_code)
        ->count();
        if($payment !=null)
        {
           $total = $payment.' Item';
        }else{
           $total = "0 Item";
        } 

        return $total;

    }

    public static function TotalPrice($order_code){

         $payment = DB::table('order_transportasi_d')
        ->select( DB::raw('SUM(qty*weight*price) AS total_price'))
        ->where('order_no',$order_code)
        ->first();

        if($payment !=null)
        {
           $total = $payment->total_price;
        }else{
           $total = "";
        } 

        return $total;
    }


    public static function OrderTotalTransport($total_berat,$weight_min,$biaya_transport){

         $total_weigh_min = $weight_min;
         if($total_berat < $total_weigh_min)
         {
           $total_harga = $biaya_transport * $total_weigh_min;
         }else{
           $total_harga = $biaya_transport * $total_berat; 
         }   

        return $total_harga;
    }


    public static function TotalAll(){
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_transportasi as a');
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>$id,'c.is_approve_complete'=>1]);
        $results = $order->count();

        return $results;
 
    }

     public static function TotalNew(){
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_transportasi as a');    
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>$id,'c.is_approve_complete'=>1]);
        $order->where('c.order_status',2);
        $results = $order->count();

        return $results;
 
    }

    public static function TotalRev(){
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_transportasi as a');
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>$id,'c.is_approve_complete'=>1]);
        $order->where('c.order_status',4);
        $results = $order->count();

        return $results;
 
    }

      public static function PDFTransport($order_no)
    {
        $check = DB::table('order_transportasi')
        ->where(['order_no'=>$order_no])
        ->whereNotNull('pdf_name')
        ->first();
        if($check != null)
        {
            $filename = env('SERVER_BACKEND')."pdf/po/".$check->pdf_name;   
        } else{
            $filename = "";
        }   
      
        return $filename;
    }



   

}