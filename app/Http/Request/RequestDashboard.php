<?php

namespace App\Http\Request;

use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\User;

use DB;
class RequestDashboard
{
   
   public static function getStore()
   {
        $data = array(
         'wishlist'=>RequestDashboard::getWishlist(),
         'viewer_product'=>RequestDashboard::getViewer(),
         'viewer_all'=>RequestDashboard::getViewerAll(),
         'viewer_user'=>RequestDashboard::getViewerUser()
      );
        return $data; 
   }

   public static function getViewerUser(){
       
        $viewer = DB::table('viewer')->select( DB::raw('SUM(user_id) AS total'))->where(['vendor_id'=> Auth::User()->vendor_id])->first();
        if($viewer)
        {
           $view = $viewer->total;
        }else{
           $view = 0;
        }  
        return  $view; 
       
   }

   public static function getWishlist()
   {
        $wishlist = DB::table('wishlist')->where(['vendor_id'=>Auth::User()->vendor_id])->count();
        return  $wishlist; 
   }

   public static function getViewer()
   {

        $viewer = DB::table('viewer')->where(['vendor_id'=> Auth::User()->vendor_id])->count();
        return  $viewer;  
   }

    public static function getViewerAll()
   {
        $viewer = DB::table('viewer')->select( DB::raw('SUM(vendor_id) AS total'))->where(['vendor_id'=> Auth::User()->vendor_id])->first();
        if($viewer !=null)
        {
           $view = $viewer->total.' pengunjung';
        }else{
           $view = '0 pengunjung';
        }  
        return  $view;  
   }



   


   


}