<?php

namespace App\Http\Request;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\ResourceCode;
use App\Models\ProjectProductPrice;
use App\Models\ProductGallery;
use App\Models\PaymentProduct;
use App\Models\PaymentMethod;
use App\Models\Location;
use App\Models\ProjectProduct;
use App\Models\Project;
use App\Models\Vendor;
use App\Models\VpiVendor;
use App\Models\Wishlist;
use App\Http\Request\RequestAuth;
use App\Http\Request\RequestFrontend;
use DB;

class RequestOrderAsuransi 
{
   
    public static function OrderInsurance($order_code){
     $__temp_ = array();
     $product = DB::table('order_asuransi_d as a')
                 ->select('b.id','b.name as product_name','a.qty','a.price','a.weight','b.uom_id','c.nilai_asuransi as insurance_val')
                 ->join('product as b','a.product_id','=','b.id')
                 ->join('order_asuransi as c','a.order_no','=','c.order_no')
                 ->where(['a.order_no'=>$order_code])
                 ->limit(2)
                 ->get();

     
      foreach ($product as $key => $val)
      {

           $__temp_[$key]['id'] = $val->id;
           $__temp_[$key]['photo'] = RequestFrontend::PhotoProduct($val->id);
           $__temp_[$key]['product_name'] = RequestFrontend::limitText($val->product_name,'50');
           $__temp_[$key]['qty'] = $val->qty;
           $__temp_[$key]['weight'] = $val->weight.' Kg';
           $__temp_[$key]['payment'] = RequestOrder::OrderPayment($order_code);
           $__temp_[$key]['price'] = GeneralHelpers::Rupiah($val->price);
           $__temp_[$key]['total_price'] = GeneralHelpers::Rupiah($val->qty * $val->weight * $val->price);
      }       
     
     return $__temp_;

   }


   public static function OrderPremi($order_code,$type){
      $insurance = DB::table('order_asuransi')
                 ->select('nilai_asuransi','nilai_harga_minimum')
                 ->where(['order_no'=>$order_code])
                 ->first();

       if($insurance !=null)
       {
           if($type =="premi")
           {
              $result = $insurance->nilai_asuransi;
           }else{
               $result = $insurance->nilai_harga_minimum;
           }  

       }else{
        $result = "kosong";
       }          

       return $result;

   }


   public static function TotalQty($order_code){
        $payment = DB::table('order_asuransi_d')
        ->select( DB::raw('SUM(qty) AS total_qty'))
        ->where('order_no',$order_code)
        ->first();

        if($payment !=null)
        {
           $total = $payment->total_qty.' Item';
        }else{
           $total = "0 Item";
        } 
        return $total;

    }

     public static function TotalItem($order_code)
     {
       
        $payment = DB::table('order_asuransi_d')->where('order_no',$order_code)
        ->count();
        if($payment !=null)
        {
           $total = $payment.' Produk';
        }else{
           $total = "0 Produk";
        } 

        return $total;

    }

    

    public static function OrderMinimumInsurance($total,$premi,$min_price){

         
        $total_asuransi = $total * $premi / 100;
         if($total_asuransi < $min_price)
         {
           $total_harga = $min_price;
         }else{
           $total_harga = $total_asuransi; 
         }   

        return $total;
    }

    public static function TotalAll(){
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_asuransi as a');
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>$id,'c.is_approve_complete'=>1]);
        $results = $order->count();

        return $results;
 
    }

     public static function TotalNew(){
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_asuransi as a');    
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>$id,'c.is_approve_complete'=>1]);
        $order->where('c.order_status',2);
        $results = $order->count();

        return $results;
 
    }

    public static function TotalRev(){
        $auth = Auth::User();
        $id = $auth->vendor_id; 
        $order = DB::table('order_asuransi as a');
        $order->join('users as b','a.created_by','=','b.id');
        $order->join('order as c','a.order_no','=','c.order_no');
        $order->where(['a.vendor_id'=>$id,'c.is_approve_complete'=>1]);
        $order->where('c.order_status',4);
        $results = $order->count();

        return $results;
 
    }


      public static function VendorAsuransi($order_code,$type){
        
        $order = DB::table('order_asuransi as a');
        $order->select('c.name','c.address','b.no_cargo_insurance');
        $order->join('asuransi as b','a.asuransi_id','=','b.id');
        $order->join('vendor as c','b.vendor_id','=','c.id');
        $order->where('a.order_no',$order_code);
        $results = $order->first();

        if($results !=null)
        {   
            if($type =="vendor_name")
            {
               $res = $results->name;
            }else if($type =="vendor_name"){
               $res = $results->no_cargo_insurance;
            }else{
               $res = $results->address;
            }    
            
        }else{
             $res = '';
        }    

        return $res;
 
    }

    public static function PDFAsuransi($order_no)
    {
        $check = DB::table('order_asuransi')
        ->where(['order_no'=>$order_no])
        ->whereNotNull('pdf_name')
        ->first();
        if($check != null)
        {
            $filename = env('SERVER_BACKEND')."pdf/po/".$check->pdf_name;   
        } else{
            $filename = "";
        }   
      
        return $filename;
    }



   

}