<?php

namespace App\Http\Request;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\ResourceCode;
use App\Models\ProjectProductPrice;
use App\Models\ProductGallery;
use App\Models\PaymentProduct;
use App\Models\PaymentMethod;
use App\Models\Location;
use App\Models\ProjectProduct;
use App\Models\Project;
use App\Models\Vendor;
use App\Models\VpiVendor;
use App\Models\Wishlist;
use App\Models\OrderGabungan;
use App\Http\Request\RequestAuth;
use App\Http\Request\RequestPrivy;
use App\Http\Request\RequestOrder;
use App\Http\Request\RequestOrderTransport;
use App\Http\Request\RequestOrderAsuransi;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestPDF;
use App\Http\Request\RequestServer;

use PDF;
use DB;
use App\Models\Cart;
class RequestCart 
{

  public static function QueryGetCart(){


     $cart = DB::table('cart as a')
         ->select(
           'a.id',
           'a.type',
           'a.premi',
           'a.pr_request_id',
           'a.transport',
           'a.insurance',
           'a.insurance_id',
           'a.shipping_id',
           'a.project_id',
           'a.vendor_transport_id',
           'a.transport_id',
           'a.min_weight_transport',
           'a.transport_fee',
           'a.transport_name',
           'a.total_transport',
           'a.up_transport',
           'a.perihal_transport',
           'a.catatan_transport',
           'a.insurance_vendor_id',
           'a.total_insurance',
           'a.insurance_name',
           'a.insurance_min_price',
           'a.up_insurance',
           'a.perihal_insurance',
           'a.catatan_insurance',
           'a.location_id',
           'a.location_name',
           'a.destination_id',
           'a.project_address',
           'a.vendor_id',
           'a.order_no',
           'a.tgl_ambil',
           'a.up_product',
           'a.perihal_product',
           'a.catatan_product',
           'a.status',
           'b.kode_sda',
           'b.no_pr',
           'a.created_at',
           'a.updated_at',
           'a.created_by',
         )
         ->LeftJoin('pr_request_product as b','a.pr_request_id','=','b.pr_request_id')
         ->where('a.created_by',Auth::User()->id)
         ->GroupBy('a.id')
         ->orderBy('a.id','DESC')
         ->get();

         return $cart;

  }   
   
   
  public static function getCart($cart)
  {
    
   
    $res = array();
    foreach($cart as $key => $val)
    {
       if($val->type =="Non-kontrak")
       {
             $res[$key]['shipping'] = RequestCart::GetShipping('true'); // shipping loco
             $transport = false;
             $insurance = false;
             $transport_checkbox = true; //checkbox disable true
             $insurance_checkbox = true; //checkbox disable true
             $shipping_id = 1;
             $premi  = 0;
             $shipping_checkbox = true; 
        }else{
            $res[$key]['shipping'] = RequestCart::GetShipping('false'); // shipping normal
            $shipping_id = $val->shipping_id;
            if($val->premi ==""){ $premi = 0; }else{ $premi = $val->premi; }
            

             if($val->project_id !=0)
            {
                $PRRequest = RequestCart::CheckPRAttribut($val->pr_request_id,$val->transport,$val->insurance,$val->shipping_id);
                $transport = $PRRequest->transport; 
                $transport_checkbox = $PRRequest->transport_checkbox; 
                
                $insurance = $PRRequest->asuransi; 
                $insurance_checkbox = $PRRequest->asuransi_checkbox;

                


                            
                $shipping_checkbox = false;
               
            }else{
                $transport = false;
                $transport_checkbox = true;
                $insurance = false;
                $insurance_checkbox = true;  

                $shipping_checkbox = true;    
            }

        }   

        

       if($val->project_id)
       {
         
          $reqVendor = RequestCart::RequestOnstage("vendor_transport",$val); 
          $res[$key]['vendor_transport'] = RequestCart::getVendorTransport($reqVendor);
          $res[$key]['vendor_transport_id'] = $val->vendor_transport_id;
          
          if(count($res[$key]['vendor_transport']) > 0)
          {
              $res[$key]['list_vendor_transport'] =  true;
          }else{
             $res[$key]['list_vendor_transport'] =  false;
          }  
           

          if($val->transport_id !=0)
          {
              $reqLogistic = RequestCart::RequestOnstage("transport",$val);
              $res[$key]['transport_logistic'] = RequestCart::getVendorTransport($reqLogistic);
              $res[$key]['transport_id'] = $val->transport_id;
              $res[$key]['min_weight_transport'] = $val->min_weight_transport;

          }else{
              $res[$key]['transport_logistic'] = [];
              $res[$key]['transport_id'] = 0;
              $res[$key]['min_weight_transport'] = 0;
          }

           if($val->transport_fee !=0)
          {  
                 //transport_id,location_id,vendor_id,min_weight_transport,total_volume  
                $reqPrice = RequestCart::RequestOnstage("transport_fee",$val); 
                $res[$key]['transport_price'] = RequestCart::getTransportFee($reqPrice);
                //$res[$key]['total_volume'] = $reqPrice->total_volume;
                $res[$key]['transport_fee'] = $val->transport_fee;
                $res[$key]['transport_name'] = $val->transport_name;          

          }else{
                $res[$key]['transport_price'] =[];
               // $res[$key]['total_volume'] = 0;
                $res[$key]['transport_fee'] = 0;
                $res[$key]['transport_name'] = '';
          }  
         
       
           
         

       }else{
          $res[$key]['vendor_transport'] = [];
          $res[$key]['vendor_transport_id'] = '';
          $res[$key]['min_weight_transport'] = '';

          $res[$key]['transport_logistic'] = [];
          $res[$key]['transport_id'] = '';

          $res[$key]['transport_fee'] = 0;
          $res[$key]['transport_name'] = '';
          $res[$key]['transport_price'] = [];
          $res[$key]['total_transport'] = 0;

       } 

       $res[$key]['total_transport'] = $val->total_transport;
       
       $res[$key]['list_insurance']  = RequestCart::getInsurance();
       if($val->insurance =="true" && $val->insurance_vendor_id !=0 && $val->premi !="" && $val->total_insurance !=0)
       { 
           

           $res[$key]['insurance_id'] = $val->insurance_id;
           $res[$key]['insurance_vendor_id'] = $val->insurance_vendor_id;
           $res[$key]['insurance_name'] = $val->insurance_name;
           $res[$key]['insurance_min_price'] = $val->insurance_min_price;
           $res[$key]['premi'] = $premi;
           $res[$key]['total_insurance'] = RequestCart::getTotalInsurance($val->id,$val->insurance_min_price,$val->premi);
       }else{
           $res[$key]['insurance_id'] = 0;
           $res[$key]['insurance_vendor_id'] = 0;
           $res[$key]['insurance_name'] = '';
           $res[$key]['insurance_min_price'] = 0;
           $res[$key]['premi'] = $premi;
           $res[$key]['total_insurance'] = 0;
           RequestCart::InsuranceFalse($val->id,$insurance);
       }

      
       
       
       $res[$key]['total_qty'] =  RequestCart::Totalqty($val->id); 
       $res[$key]['total_weight'] =  RequestCart::TotalWeight($val->id); 
       $res[$key]['total_volume'] =  RequestCart::TotalVolume($val->id);
       $res[$key]['total_price'] = RequestCart::TotalPrice($val->id);
 
       $res[$key]['order_no'] = $val->order_no;
       $res[$key]['type'] = $val->type;
       $res[$key]['id'] = $val->id;
       $res[$key]['checklist'] = RequestCart::CheckList($val->id);
       $res[$key]['project'] = RequestCart::GetRelasiProject($val->project_id);
       $res[$key]['project_id'] = $val->project_id;
       $res[$key]['pr_request_id'] = $val->pr_request_id;
       $res[$key]['pr_request'] = RequestCart::GetRequestPR($val->pr_request_id); 

       $res[$key]['up_transport'] = $val->up_transport; 
       $res[$key]['perihal_transport'] = $val->perihal_transport; 
       $res[$key]['catatan_transport'] = $val->catatan_transport;  
       
       

       
       // $res[$key]['vendor_name'] = $val->vendor_name;
       $res[$key]['location_id'] = $val->location_id;
       $res[$key]['location_name'] = $val->location_name;
       $res[$key]['vendor'] = RequestCart::VendorDetail($val->vendor_id);
       
       $res[$key]['project_address'] = $val->project_address;
     
       $res[$key]['destination_id'] = $val->destination_id;
      
      
       
       $res[$key]['shipping_checkbox'] =  $shipping_checkbox;
       $res[$key]['loading_shipping'] =  'Pilih Jenis Pengiriman';
       $res[$key]['shipping_id'] =  $shipping_id;
       $res[$key]['transport'] =  $transport;
       $res[$key]['transport_checkbox'] =  $transport_checkbox;
       $res[$key]['loading_vendor_transport'] =  'Pilih Vendor Transport';
       $res[$key]['loading_transport_id'] =  'Pilih Transportasi';
       $res[$key]['loading_transport_fee'] =  'Pilih Harga';
      
       $res[$key]['loading_list_insurance'] =  'Pilih Vendor Asuransi';



       $res[$key]['up_insurance'] = $val->up_insurance; 
       $res[$key]['perihal_insurance'] = $val->perihal_insurance;
       $res[$key]['catatan_insurance'] = $val->catatan_insurance;
     //  $res[$key]['transport_fee'] = $val->transport_fee;
       $res[$key]['insurance'] = $insurance;
       $res[$key]['insurance_checkbox'] = $insurance_checkbox;
     
       $res[$key]['tgl_ambil'] = $val->tgl_ambil;
       $res[$key]['up_product'] = $val->up_product;
       $res[$key]['perihal_product'] = $val->perihal_product;
       $res[$key]['catatan_product'] = $val->catatan_product;
       $res[$key]['product'] = RequestCart::CartProduct($val->id,'GET');
       //$res[$key]['min_weight'] = $val->min_weight;
       $res[$key]['status'] = $val->status;
    
       $res[$key]['division'] = RequestCart::OrderDevisionNew(Auth::User()->group_id);
       
       $res[$key]['check_budget'] = RequestCart::CheckBudget(substr($val->kode_sda, 0, 2),$val->no_pr,RequestCart::TotalPrice($val->id),$res[$key]['vendor']['code_bp']);
        
      
       $res[$key]['created_by'] = $val->created_by;
       $res[$key]['created_at'] =  GeneralHelpers::tanggal($val->created_at);
       $res[$key]['updated_at'] =  GeneralHelpers::tanggal($val->updated_at);

    }
    $result = json_decode(json_encode($res), FALSE);
    return $result;

  }

  public static function CheckPRAttribut($pr_request_id,$transport,$insurance,$shipping_id)
  {
     

     $prefixTR = 'D5'; //kode untuk transportasi
     $prefixAS = 'ED';//kode untuk asuransi
     $result = DB::table('pr_request_product')
     ->where('pr_request_id',$pr_request_id)
     ->selectRaw("SUM(CASE WHEN kode_sda LIKE '%".$prefixTR."%' THEN 1 ELSE 0 END) as d5_count")
     ->selectRaw("SUM(CASE WHEN kode_sda LIKE '%".$prefixAS."%' THEN 1 ELSE 0 END) as ed_count")
     ->first();

     $d5Count = $result->d5_count ?? 0;  // Nilai default 0 jika null
     $edCount = $result->ed_count ?? 0;  // Nilai default 0 jika null


     // $transport = $d5Count > 0;
     // $transport_checkbox = !$transport;

     // $insurance = $edCount > 0;
     // $insurance_checkbox = !$insurance;

     // $res = array(
     //    'transport'=> $transport,
     //    'transport_checkbox'=>$transport_checkbox,
     //    'asuransi'=> $insurance,
     //    'asuransi_checkbox'=>$insurance_checkbox
     //  );


      $res = [
            'transport' => $shipping_id == 1 ? false : $d5Count > 0,
            'transport_checkbox' => $shipping_id == 1 ? true : $d5Count == 0,
            'asuransi' => $shipping_id == 1 ? false : $edCount > 0,
            'asuransi_checkbox' => $shipping_id == 1 ? true : $edCount == 0,
        ];

      $result = json_decode(json_encode($res), FALSE); 
      return  $result;


      
  }

  public static function CheckList($id){
       $cart = DB::table('cart')->select('checklist')->where('id',$id)->first();
       if($cart->checklist == 'true')
       {
        $check = true;
       }else{
        $check = false;
       } 

       return $check;
  }


  public static function GetProductSummary()
  {
      $cart = DB::table('cart as a')
               ->join('cart_product as b','a.id','=','b.cart_id')
               ->where('a.checklist',true)
               ->where('a.created_by',Auth::User()->id)
               ->get();
      $res = array();
       
   
     

       if(count($cart) > 0)
       {
           foreach($cart as $key => $val)
           {
               $res[$key]['product_id'] = $val->product_id;
               $res[$key]['product_name'] = $val->product_name;
               $res[$key]['quantity'] = $val->qty;
               $res[$key]['price'] = $val->price;
           }    
       }else{
               $res[0]['quantity'] = 0;
               $res[0]['price'] = 0;

       } 

     

      return $res;


  }

  public static function GetOrderID($order_code){
     $order = DB::table('order as a')
     ->select(
        'a.order_no',
        'a.vendor_id',
        'a.vendor_name',
        'a.location_id',
        'a.location_name',
        'a.created_by',
        'a.project_id',
        'b.location_id as destination_id',
        'b.alamat as project_address',
        'a.shipping_id',
        'a.total_price',
        'a.up_transport',
        'a.perihal_transport',
        'a.catatan_transport',
        'a.up_insurance',
        'a.perihal_insurance',
        'a.catatan_insurance',
        'a.up_product',
        'a.perihal_product',
        'a.catatan_product',
        'a.tgl_diambil',
        'a.pr_request_id',
        'a.pdf_name'
    )
     ->join('project_new as b','a.project_id','=','b.id')
     ->where(['a.created_by'=>Auth::User()->id,'a.order_no'=>$order_code])->first();
     return $order; 
  }

  public static function GetOrderProduct($order_code){

     $orderProduct = DB::table('order_product as a')
     ->select('a.product_id','b.name as product_name','a.qty','a.price','a.weight','c.filename as photo','a.product_uom_id','a.payment_method_id as payment_id','a.payment_method_name as payment_name')
     ->join('product as b','a.product_id','=','b.id')
     ->join('product_gallery as c','b.id','=','c.product_id')
     ->where(['a.created_by'=>Auth::User()->id,'a.order_no'=>$order_code])
     ->first();

     return $orderProduct; 
  }

  public static function GetOrderTransport($order_code)
  {
        
     $transport = DB::table('order_transportasi as a')
     ->select('b.vendor_id as vendor_transport_id','a.transportasi_id as transport_id','a.biaya_transport as transport_fee','a.weight_minimum as min_weight_transport')
     ->join('transportasi as b','a.transportasi_id','=','b.id') 
     ->where(['a.created_by'=>Auth::User()->id,'a.order_no'=>$order_code])
     ->first();

  

     if($transport != null)
     {
          $transport_name = RequestOrder::VendorTransport($order_code,'transport_name');
          $total_transport =  RequestOrderTransport::OrderTotalTransport(RequestOrderTransport::TotalWeightTransport($order_code),$transport->min_weight_transport,$transport->transport_fee);

         $trans = array('transport'=>'true','vendor_transport_id'=>$transport->vendor_transport_id,'transport_id'=>$transport->transport_id,'transport_fee'=>$transport->transport_fee,'transport_name'=>$transport_name,'min_weight_transport'=>$transport->min_weight_transport,'total_transport'=>$total_transport); 
     }else{
         $trans = array('transport'=>'false','vendor_transport_id'=>'','transport_id'=>'','transport_fee'=>'','transport_name'=>'','min_weight_transport'=>'','total_transport'=>''); 
     }   


     return $trans; 
  }


  public static function GetOrderInsurance($order_code)
  {
        
     $insurance = DB::table('order_asuransi as a')
     ->select('b.vendor_id as insurance_vendor_id','a.asuransi_id as insurance_id','a.nilai_asuransi as premi','a.nilai_harga_minimum as min_price')
     ->join('asuransi as b','a.asuransi_id','=','b.id')
     ->where(['a.created_by'=>Auth::User()->id,'a.order_no'=>$order_code])
     ->first();

    

     if($insurance != null)
     {

        $vendor_asuransi =  RequestOrderAsuransi::VendorAsuransi($order_code,'vendor_name');
        $premi = RequestOrderAsuransi::OrderPremi($order_code,'premi');
        $total_produk = RequestOrder::TotalProduct($order_code);
        $total_asuransi = $total_produk * $premi / 100;
        $nilai_min_insurance =  RequestOrderAsuransi::OrderMinimumInsurance($total_produk,$premi,$insurance->min_price);

       $ins = array('insurance'=>'true','insurance_vendor_id'=>$insurance->insurance_vendor_id,'insurance_id'=>$insurance->insurance_id,'insurance_name'=>$vendor_asuransi,'insurance_min_price'=>$nilai_min_insurance,'total_insurance'=>$total_asuransi,'premi'=>$insurance->premi);

     }else{

        $ins = array('insurance'=>'false','insurance_vendor_id'=>'','insurance_id'=>'','insurance_name'=>'','insurance_min_price'=>'','total_insurance'=>'','premi'=>'');
     }   

     return $ins; 
  }

 

  public static function GetTotalTransportSummary()
  {
      $cart = DB::table('cart')->where('checklist',true)->where('created_by',Auth::User()->id)->sum('total_transport');
      return $cart;
  }

  public static function GetTotalInsuranceSummary()
  {
      $cart = DB::table('cart')->where('checklist',true)->where('created_by',Auth::User()->id)->sum('total_insurance');
      return $cart;
  }

   public static function GetTotalPremiSummary()
  {
      $cart = DB::table('cart')->where('checklist',true)->where('created_by',Auth::User()->id)->sum('premi');
      return round($cart, 3);
  }

  public static function GettotalPriceProduct(){
     
      $cart = DB::table('cart_product as a')
              ->join('cart as b','a.cart_id','=','b.id')->where('a.created_by',Auth::User()->id)->where('b.checklist',true)->sum(DB::raw('a.price * a.qty * a.weight'));   
      return  $cart;

  }
  
  public static function GetTotalVolumeSummary(){
   

      $cart = DB::table('cart_product as a')
              ->join('cart as b','a.cart_id','=','b.id')->where('a.created_by',Auth::User()->id)->where('b.checklist',true)->sum(DB::raw('a.weight * a.qty'));   
      return  $cart;

  }

  public static function GetTotalQuantitySummary(){
     
      $cart = DB::table('cart_product as a')
              ->join('cart as b','a.cart_id','=','b.id')->where('a.created_by',Auth::User()->id)->where('b.checklist',true)->sum('a.qty');   
      return  $cart;

  }

     
     public static function GetStatusTransportCart(){
        $cart = DB::table('cart as a')
                ->leftJoin('pr_request_product as b','a.pr_request_id','=','b.pr_request_id')
                ->where('kode_sda','LIKE','D5')
                ->where(['created_by'=>Auth::User()->id,'transport'=>true])
                ->get();
     }    


  // public static function GetStatusTransportCart(){
  //   $cart = DB::table('cart')->select(DB::raw('COUNT(transport) as jml_transport'))->where(['created_by'=>Auth::User()->id,'transport'=>true])->first();
     
  //    if($cart->jml_transport < 1)
  //    {
  //       $result = false;
  //    }else{
  //       $result = true;
  //    }   

  //    return $result;

  // }

  public static function GetStatusInsuranceCart(){
    
    $cart = DB::table('cart')->select(DB::raw('COUNT(insurance) as jml_insurance'))->where(['created_by'=>Auth::User()->id,'insurance'=>true])->first();
     
     if($cart->jml_insurance < 1)
     {
        $result = false;
     }else{
        $result = true;
     }   

     return $result;

    
  }

  

  public static function CartProduct($cart_id,$type)
  {
        $temp = array();
        $cartProduct = DB::table('cart_product')->where('cart_id',$cart_id)->get();
      
        foreach ($cartProduct as $key => $val)
        {
           $temp[$key]['id'] = $val->id;
           $temp[$key]['product_id'] =  $val->product_id;
           $temp[$key]['product_name'] =  $val->product_name;
           $temp[$key]['qty'] =  $val->qty;
           $temp[$key]['weight'] =  $val->weight;
          
           $temp[$key]['price'] =  $val->price;
           $temp[$key]['subtotal'] =  $val->price * $val->qty * $val->weight;
           $temp[$key]['photo'] =  $val->photo;
           $temp[$key]['payment_id'] =  $val->payment_id;
           $temp[$key]['payment_name'] =  $val->payment_name;
           $temp[$key]['product_uom_id'] =  $val->product_uom_id;
           $temp[$key]['product_uom_name'] =  $val->product_uom_name;
           $temp[$key]['volume'] =  $val->volume;
           $temp[$key]['total_volume'] =  $val->weight * $val->qty;
           $temp[$key]['control'] = RequestCart::cekTerkontrak($val->product_id,$val->product_name,$val->weight,$val->qty,$type);
        }   

        return  $temp;

  }

   
   

   public static function GetShipping($status)
   {
        $temp = array();
        if($status == 'false')
        {
            $shipping = DB::table('shipping')->select('id','name')->where('is_deleted',0)->get();
            
        }else{
           $shipping = DB::table('shipping')->select('id','name')->where(['is_deleted'=>0,'name'=>'Loco'])->get();
               
        }   
       
        foreach ($shipping as $key => $val)
        {
           $temp[$key]['value'] = $val->id;
           $temp[$key]['text'] =  $val->name;
          
        }   

          

        return  $temp;

   }
   

   public static function GetRelasiProject($id)
   {
       $temp = array();
       
       //$groups = DB::table('groups')->where('id', Auth::User()->group_id)->first();

       $shipping = DB::table('project_new as a');
       $shipping->select('a.id','a.name','a.alamat','a.location_id','b.province_name');
       $shipping->join('ref_locations as b','a.location_id','=','b.location_id');
       // if($groups->akses_proyek ==0 || $groups->akses_proyek ==null)
       // {
       //    $shipping->where('departemen_id',Auth::User()->group_id); 
       // } 
       $shipping->where(['a.is_deleted'=>0,'a.id'=>$id]);
       $result = $shipping->first();

       $temp = array(['value'=>$result->id,'text'=>$result->name,'address'=> RequestFrontend::limitText($result->alamat,'55'),'location_id'=>$result->location_id,'province_name'=>$result->province_name]);
    
       return $temp;


   }


    public static function GetRequestPR($id)
   {
       $temp = array();
    
       $data = DB::table('pr_request');
       $data->select('id as pr_request_id','no_pr_request');
       $result =  $data->find($id);
    
       return $result;


   }


   public static function VendorDetail($vendor_id)
   {

     $vendor = Vendor::where('id',$vendor_id)->first();
     if($vendor)
     { 

        $result = array('value'=>$vendor->id,'text'=>$vendor->name,'directur_name'=>$vendor->nama_direktur,'address'=>$vendor->address,'no_fax'=>$vendor->no_fax,'code_bp'=>$vendor->code_bp,'phone'=>$vendor->no_telp);
           
        
     }else{

        $result = array('id'=>'','name'=>'','directur_name'=>'','address','','no_fax'=>'','code_bp'=>'','phone'=>'');
     }   

     return $result;

   } 


    public static function OrderDevisionNew($group_id){
      $division = DB::table('groups')->select('id','name as branch_manager','general_manager')->where('id',$group_id)->first();
      if($division)
      {
         $result = array('id'=>$division->id,'branch_manager'=>$division->branch_manager,'general_manager'=>$division->general_manager);
      }else{
         $result =  array('id'=>'','branch_manager'=>'','general_manager'=>'');
      }  

      return $result; 
    }



    public static function GetProject()
   {
       $__temp_ = array();
       
       $groups = DB::table('groups')->where('id', Auth::User()->group_id)->first();

       $shipping = DB::table('project_new');
       $shipping->select('id','name','alamat','location_id');
       if($groups->akses_proyek ==0 || $groups->akses_proyek ==null)
       {
          $shipping->where('departemen_id',Auth::User()->group_id); 
       } 
       $shipping->where('is_deleted',0);
       $result = $shipping->get();


       foreach ($result as $key => $val)
       {
           $__temp_[$key]['value'] = $val->id;
           $__temp_[$key]['text'] =  $val->name;
           $__temp_[$key]['address'] =  RequestFrontend::limitText($val->alamat,'55');
           $__temp_[$key]['location_id'] =  $val->location_id;
          
       }   

        return  $__temp_;


   }

    

   public static function getVendorTransport($request){
        $kodeTransport = 'D5';
        $query = DB::table('kontrak_transportasi as a');
                  
        $query->join('transportasi as b','b.vendor_id','=','a.vendor_id');
        $query->join('pr_request_product as h','h.kode_sda','=','b.sda_code');
        $query->join('vendor as c','c.id','=','b.vendor_id');
        $query ->join('resources_code as d','d.code','=','h.kode_sda');
        $query ->join('ref_locations as e','e.location_id','=','b.origin_location_id');
        $query ->Join('generate_transport_price as f', function($join)
             {
                 $join->on('f.transport_id','=', 'b.id');
                 $join->on('f.is_deleted','=',DB::raw("0"));
             });
          $query->leftJoin('transport_amandemen as g','g.id','=','a.last_amandemen_id');
          //$query->where('a.status','<>',0);
          $query->where('a.is_deleted',0);

          $query->whereRaw("IFNULL(g.start_contract, a.start_date) <= CURRENT_DATE ()");
          $query->whereRaw("IFNULL(g.end_contract, a.end_date) >= CURRENT_DATE ()");

          $query->where('h.kode_sda','LIKE','%'.$kodeTransport.'%');
          $query->where('h.pr_request_id',$request->pr_request_id);
            $query->where([
                'b.destination_location_id'=>$request->project_id,
                'b.origin_location_id'=>$request->location_id,
                'b.vendor_barang' =>$request->vendor_id,
            ]);
          
          
          if($request->type == "vendor_transport")
          {
            $query->select('c.id','c.name','b.destination_location_id','b.origin_location_id','b.vendor_barang','b.vendor_id');  
            $query->groupBy('b.vendor_id',);  
          }else if($request->type == "transport"){
            $query->select('b.id','d.name','b.destination_location_id','b.origin_location_id','b.vendor_barang','b.vendor_id','b.weight_minimum');  
            $query->where('b.vendor_id',$request->vendor_transport_id);
            $query->groupBy('b.id');  
          }    
          $results =  $query->get();
          $_res = RequestCart::GetSelectData($results,$request->type); 
          return $_res;
   }


   public static function getTransportFee($request){
     

        $query = DB::table('kontrak_transportasi as a');
       
        $query->select(
            DB::raw('IFNULL(c.price_fot_scf, b.price_fot_scf) as price_fot_scf_new'),
            DB::raw('IFNULL(c.price_fot_tt, b.price_fot_tt) as price_fot_tt_new'),
            DB::raw('IFNULL(c.price_fog_scf, b.price_fog_scf) as price_fog_scf_new'),
            DB::raw('IFNULL(c.price_fog_tt, b.price_fog_tt) as price_fog_tt_new'),
        );

        $query->join('transportasi as b','b.vendor_id','=','a.vendor_id');
        $query->Join('generate_transport_price as c', function($join)
             {
                 $join->on('c.transport_id','=', 'b.id');
                 $join->on('c.is_deleted','=',DB::raw("0"));
             });
        $query->leftJoin('transport_amandemen as d','d.id','=','a.last_amandemen_id');
        $query->where('a.status','<>',0);

        $query->where([
            'b.id'=>$request->transport_id,
           // 'b.origin_location_id'=>$request->location_id,
           // 'b.vendor_id'=> $request->vendor_id,
            //'b.destination_location_id'=>$request->location_id,
            //'b.origin_location_id' =>$request->vendor_id,

         ]);

         
        $query->whereRaw("IFNULL(d.start_contract, a.start_date) <= CURRENT_DATE ()");
        $query->whereRaw("IFNULL(d.end_contract, a.end_date) >= CURRENT_DATE ()");
        $query->groupBy('b.id');  

        $results = $query->get();

      
        $_res = RequestCart::GetSelectPrice($results,$request->min_weight_transport,$request->total_volume);
        return $_res;     

   }

   public static function getTotalTransport($data,$fee)
   {
      foreach($data as $key => $val)
      {
        if($val['value'] == $fee)
        {
            if($val['total_volume'] < $val['min_weight_transport'])
            {
               return $val['min_weight_transport'] * $fee;
            }else{
               return  $val['min_weight_transport'] * $fee;
            }
        }    
      } 
   }

  

   public static function getInsurance(){

       $__temp_ = array();
       $shipping = DB::table('asuransi as a')
       ->select('a.id','vendor_id','a.nilai_harga_minimum','a.start_date','a.end_date','b.name','a.no_contract','c.no_amandemen',DB::raw('IFNULL(c.nilai_asuransi, a.nilai_asuransi) as nilai_asuransi'),DB::raw('IFNULL(c.jenis_asuransi, a.jenis_asuransi) as jenis_asuransi'))
       ->join('vendor as b','a.vendor_id','=','b.id')
       ->leftJoin('amandemen_asuransi as c','c.id','=','a.last_amandemen_id')
         
       
       ->whereRaw("IFNULL(c.start_contract, a.start_date) <= CURRENT_DATE ()")
       ->whereRaw("IFNULL(c.end_contract, a.end_date) >=  CURRENT_DATE ()")  
       ->where('a.is_deleted',0)->get();


       foreach ($shipping as $key => $val)
       {
           $__temp_[$key]['value'] = $val->vendor_id;
           $__temp_[$key]['text'] =  $val->name;
           $__temp_[$key]['id'] =  $val->id;
           $__temp_[$key]['nilai_asuransi'] =  $val->nilai_asuransi;
           $__temp_[$key]['min_price'] =  $val->nilai_harga_minimum;
          
       }   

        return  $__temp_;
   }
 

   public static function GetSelectData($data,$type)
    {
        $temp = array(); 
        foreach ($data as $key => $val)
        {
            $temp[$key]['value'] = $val->id;
            $temp[$key]['text'] = $val->name;
            

           $temp[$key]['project_id'] =  $val->destination_location_id;
           $temp[$key]['location_id'] =  $val->origin_location_id;
           $temp[$key]['vendor_id'] =  $val->vendor_barang;
           $temp[$key]['vendor_transport_id'] =  $val->vendor_id;
           if($type =="transport")
           {
             $temp[$key]['min_weight_transport'] =  $val->weight_minimum;
           } 
            
           
          
        }
        return $temp;

    }


    public static function GetSelectPrice($data,$min_weight_transport,$total_volume)
    {
             
            // foreach ($data as $key) {
            //     for ($x = 1; $x <= 4; $x++) {
            //         if ($x == 1 ) {
            //             $ret['value'] = $key->price_fot_scf_new;
            //             $ret['text'] = 'Price FOT SCF';
            //             $ret['min_weight_transport'] = $min_weight_transport;
            //             $ret['total_volume'] = $total_volume;
            //         } elseif ($x == 2) {
            //             $ret['value'] = $key->price_fot_tt_new;
            //             $ret['text'] = 'Price FOT TT';
            //             $ret['min_weight_transport'] = $min_weight_transport;
            //             $ret['total_volume'] = $total_volume;
            //         } elseif ($x == 3) {
            //             $ret['value'] = $key->price_fog_scf_new;
            //             $ret['text'] = 'Price FOG SCF';
            //             $ret['min_weight_transport'] = $min_weight_transport;
            //             $ret['total_volume'] = $total_volume; 
            //         } elseif ($x == 4) {
            //             $ret['value'] = $key->price_fog_tt_new;
            //             $ret['text'] = 'Price FOG TT';
            //             $ret['min_weight_transport'] = $min_weight_transport;
            //             $ret['total_volume'] = $total_volume; 
            //         }
            //         $new_data[] = $ret;
            //     }
            // }

        foreach ($data as $key) {
            for ($x = 1; $x <= 4; $x++) {
                if ($x == 2 || $x == 4) {
                    if ($x == 2) {
                        $ret['value'] = $key->price_fot_tt_new;
                        $ret['text'] = 'Price FOT TT';
                    } else {
                        $ret['value'] = $key->price_fog_tt_new;
                        $ret['text'] = 'Price FOG TT';
                    }
                    $ret['min_weight_transport'] = $min_weight_transport;
                    $ret['total_volume'] = $total_volume;
                    $new_data[] = $ret;
                }
            }
        }
          
            $results = $new_data;
            $arr = array();
            foreach($results as $key => $val)
            {
                if($val['value'] >0)
                {
                    $arr[$key]['value'] = $val['value'];
                    $arr[$key]['text'] = $val['text'];
                    $arr[$key]['min_weight_transport'] =  $val['min_weight_transport'];
                    $arr[$key]['total_volume'] =  $val['total_volume'];
  
                }    
                
            }    
            return $arr;

    }

    public static function getTotalInsurance($cart_id,$min_price,$premi)
    {
        $totalprice =  DB::table('cart_product')->where('cart_id',$cart_id)->sum(DB::raw('price * qty * weight'));
       
        if($totalprice > 0)
        {
            $price_insurance = $totalprice * $premi / 100;
            if($price_insurance < $min_price)
             {
               $total_insurance = $min_price;
             }else{
               $total_insurance = $price_insurance; 
             }  

               
      }else{
            $total_insurance = 0;
      }

      return $total_insurance; 
       
    }



    


    public static function CheckingOrder($cart)
    {
        //hasil filter
        $payment = array();
        //$order_code = RequestCart::GenerateOrder(); //create order
        $OrderGab = RequestCart::OrderGabungan();  //order gabungan
        foreach($cart as $key =>$val)
        {
            
            
            $arr = array();
            $payment[$key] = RequestCart::CheckPaymentID($val->product);
            if($payment[$key]->status == true)
            {
                //check Rev / Add
                $order[$key] = RequestCart::CheckActionID($val->id);
                if($order[$key] == true)
                {
                   DB::table('order')->where(['order_no'=>$val->order_no])->delete();
                   DB::table('order_product')->where(['order_no'=>$val->order_no])->delete(); 

                   $transporKey[$key] = RequestCart::CheckTransportID($val->order_no);
                   if($transporKey[$key] == true)
                   {
                      DB::table('order_transportasi')->where(['order_no'=>$val->order_no])->delete();
                      DB::table('order_transportasi_d')->where(['order_no'=>$val->order_no])->delete();     
                   }

                   $insuranceKey[$key] = RequestCart::CheckInsuranceID($val->order_no);
                   if($insuranceKey[$key] == true)
                   {
                      DB::table('order_asuransi')->where(['order_no'=>$val->order_no])->delete();
                      DB::table('order_asuransi_d')->where(['order_no'=>$val->order_no])->delete();
                   } 
                }    
                    
                $no_surat = RequestServer::GetNoSuratWika($val->perihal_product);//create no surat
                if($no_surat->status == true)
                {    
                     $status = true;
                     $type  = 'order_po';
                     $arr[$key]['insert_order'] = RequestCart::InsertOrder($val,$no_surat,$val->product,$val->order_no,$payment[$key]->id,$OrderGab->id);  // insert order

                     RequestCart::InsertApprov($val->order_no);  //insert approval

                     $arr[$key]['insert_order_product'] = RequestCart::InsertOrderProduct($val,$val->product,$val->order_no,$payment[$key]->id); //insert order product

                     //insert transportasi
                     if($val->transport == true)
                     {
                       $arr[$key]['transport'] = RequestCart::InsertOrderTransport($val,$no_surat,$val->product,$val->order_no,$OrderGab->id);
                       $arr[$key]['transportD'] =   RequestCart::InsertOrderTransportD($val->order_no,$val->product);
                     } 

                     //insert asuransi
                     if($val->insurance == true)
                     {
                        $arr[$key]['asuransi'] = RequestCart::InsertOrderInsurance($val,$no_surat,$val->product,$val->order_no,$OrderGab->id); 
                        $arr[$key]['asuransiD'] =RequestCart::InsertOrderInsuranceD($val->order_no,$val->product);
                     }
                      DB::table('cart')->where(['id'=>$val->id])->delete(); 
                
                 }else{
                    $status = false;
                    $type  = 'server';
                    $arr = 'Tidak bisa melakukan order PO, Server sedang sibuk, ulangi beberapa saat lagi!!';

                 }

            }

           
          
        }    
           $cart_count = DB::table('cart')->where(['created_by'=>Auth::User()->id])->count(); 
         

           $arrCart = array('to'=>Auth::User()->id,'count'=>$cart_count);
           return json_decode(json_encode(array('status'=>$status,'cart_count'=>$arrCart,'type'=>$type,'message'=>$arr)), FALSE);
        
         
       
      


    }

   

    public static function InsertOrder($request,$no_surat,$product,$order_code,$payment_id,$ordergab_id){
       
         
        //create pdf PO vendor
        $access = env('PRIVYID',true);
        $fileOrderPo = ($access == "LOCAL") ? RequestPDF::CreatePO($product, $request, $payment_id, $order_code,$no_surat->data) : '';
         
        

        $data = array(
            'project_id'=> $request->project_id,
            'pr_request_id' => $request->pr_request_id,
            'shipping_id'=> $request->shipping_id,
            'total_price'=> $request->total_price,
            'payment_method_id'=>$payment_id,
            'location_id'=>$request->location_id,
            'location_name'=>$request->location_name,
            'vendor_id'=>$request->vendor->value,
            'vendor_name'=>$request->vendor->text,
            'order_no'=>$order_code,
            'no_surat'=>$no_surat->data,
            'up'=>$request->up_product,
            'perihal'=>$request->perihal_product,
            'catatan'=>$request->catatan_product,
            'tgl_diambil'=>$request->tgl_ambil,
            'created_by'=>Auth::User()->id,
            'order_gabungan_id'=>$ordergab_id,
            'pdf_name'=>$fileOrderPo,
            'order_status'=>1);

         $insert =  DB::table('order')->insert($data);
         return $insert; 
          
    }

    public static function InsertOrderProduct($request,$product,$order_code,$payment_id)
    {
       
        foreach($product as $key =>$val)
        {
            $data = array(
                'order_no'=>$order_code,
                'product_id'=>$val->product_id,
                'qty'=>$val->qty,
                'product_uom_id'=>$val->product_uom_id,
                'uom_name'=>$val->product_uom_name,
                'order_product_status'=>1,
                'created_by'=>Auth::User()->id,
                'payment_method_id'=>$val->payment_id,
                'price'=>$val->price,
                'weight'=>$val->weight,
                'full_name_product'=>$val->product_name,
                'payment_method_name'=>$val->payment_name,
                'vendor_id'=>$request->vendor->value,
                'vendor_name'=>$request->vendor->text
            );

          return  $insert[$key] =  DB::table('order_product')->insert($data);

        } 


    }


 
    public static function InsertOrderTransport($request,$no_surat,$product,$order_code,$ordergab_id)
    {

        $access = env('PRIVYID',true);
        $fileOrderPo = ($access == "LOCAL") ? RequestPDF::CreatePOTransport($product, $request, $order_code,$no_surat->data) : '';

        $data = array(
            'order_no'=>$order_code,
            'up'=>$request->up_transport,
            'perihal'=>$request->perihal_transport,
            'catatan'=>$request->catatan_transport,
            'biaya_transport'=>$request->transport_fee,
            'keterangan_transport'=>null,
            'transportasi_id'=>$request->transport_id,
            'vendor_id'=>$request->vendor->value,
            'location_origin_id'=>$request->location_id,
            'location_destination_id'=>$request->destination_id,
            'project_id'=>$request->project_id,
            'category_id'=>0,
            'created_by'=>Auth::User()->id,
            'updated_by'=>null,
            'pdf_name'=>$fileOrderPo,
            'order_gabungan_id'=>$ordergab_id,
            'weight_minimum'=>$request->min_weight_transport,
            'created_at'=>date("Y-m-d H:i:s")
        ); 

        $insert =  DB::table('order_transportasi')->insert($data);
        return $insert;

    }


    public static function InsertOrderTransportD($order_code,$product){
        
      foreach($product as $key =>$val)
      {  
        $data = array('order_no'=>$order_code,'product_id'=>$val->product_id,'qty'=>$val->qty,'price'=>$val->price,'weight'=>$val->weight,'full_name_product'=>$val->product_name);

       $insert =  DB::table('order_transportasi_d')->insert($data);
        return $insert;
      }   
    }

    public static function InsertOrderInsurance($request,$no_surat,$product,$order_code,$ordergab_id)
    {
         $access = env('PRIVYID',true);
        $fileOrderPo = ($access == "LOCAL") ? RequestPDF::CreatePOAsuransi($product, $request, $order_code,$no_surat->data) : '';

         $data = array(
            'order_no'=>$order_code,
            'up'=>$request->up_insurance,
            'perihal'=>$request->perihal_insurance,
            'catatan'=>$request->catatan_insurance,
            'nilai_asuransi'=>$request->premi,
            'jenis_asuransi'=>'percent',
            'vendor_id'=>$request->insurance_vendor_id,
            'asuransi_id'=>$request->insurance_id,
            'project_id'=>$request->project_id,
            'created_by'=>Auth::User()->id,
            'updated_by'=>null,
            'order_gabungan_id'=>$ordergab_id,
            'pdf_name'=>$fileOrderPo,
            'nilai_harga_minimum'=>$request->insurance_min_price
        ); 

         $insert =  DB::table('order_asuransi')->insert($data);
       
         return $insert;
        
    }

    public static function InsertOrderInsuranceD($order_code,$product){
        
        foreach($product as $key =>$val)
       { 

        $data = array('order_no'=>$order_code,'product_id'=>$val->product_id,'qty'=>$val->qty,'price'=>$val->price,'weight'=>$val->weight,'full_name_product'=>$val->product_name);

        $insert =  DB::table('order_asuransi_d')->insert($data);
        return $insert;

       } 
    }

    


   
  

    public static function RoleUser(){
        
        $user = DB::table('users_roles')->where('user_id',Auth::User()->id)->first();
        $role_id = "";
        if($user !=null)
        {
           $role_id = $user->role_id;
        } 
        return $role_id;   
    }

     

    public static function TransportFalse($id,$status){

       DB::table('cart')->where('id',$id)->update(['vendor_transport_id'=>0,'transport_id'=>0,'transport_fee'=>'','transport_name'=>'','min_weight_transport'=>0,'total_transport'=>0,'transport'=>$status,'up_transport'=>'','perihal_transport'=>'','catatan_transport'=>'']); 
    }

    public static function InsuranceFalse($id,$status){

        DB::table('cart')->where('id',$id)->update(['insurance'=>$status,'insurance_vendor_id'=>0,'insurance_name'=>'','insurance_min_price'=>0,'premi'=>'','total_insurance'=>0,'up_insurance'=>'','perihal_insurance'=>'','catatan_insurance'=>'']); 
    }

    public static function TotalVolume($cart_id){
       
     $db = DB::table('cart_product')->where('cart_id',$cart_id)->sum('volume'); 
     return $db;
          
    }

    public static function TotalQty($cart_id){
       
       $db =   DB::table('cart_product')->where('cart_id',$cart_id)->sum('qty'); 
       return $db;   
    }

    public static function TotalWeight($cart_id){
       
       $db =   DB::table('cart_product')->where('cart_id',$cart_id)->sum(DB::raw('qty * weight')); 
       return $db;   
    }

    public static function TotalPrice($cart_id){
       
       $db =   DB::table('cart_product')->where('cart_id',$cart_id)->sum(DB::raw('price * qty * weight')); 
       return $db;   
    }


    // public static function TotalPremi(){

    //     $Premi = DB::table('cart')->where(['status'=>'false','created_by'=>Auth::User()->id])->sum('premi');
    //     return $Premi;
    // }

    // public static function TotalInsurance(){

    //     $Insurance = DB::table('cart')->where(['status'=>'false','created_by'=>Auth::User()->id])->sum('total_insurance');
    //     return $Insurance;
    // }


    // public static function TotalTransport(){

    //     $Transport = DB::table('cart')->where(['status'=>'false','created_by'=>Auth::User()->id])->sum('total_transport');
    //     return $Transport;
    // }

    public static function ActivityUser($order_code){

              // tambah aktifitas user
                $aktifitas_user[] = [
                    'user_id' => Auth::User()->id,
                    'description' => 'Membuat PO. No PO : ' . $order_code,
                    'id_reff' => $order_code,
                    // category 2 = order
                    'aktifitas_category' => 2,
                ];

                DB::table('aktifitas_user')->insert($aktifitas_user);

    }


   

    


  

    public static function InsertApprov($order_code){

        //$Group_appv = DB::table('approve_po_rules')->where('departemen_id',Auth::User()->group_id)->first();

        $sequence = DB::table('approve_po_rules')->where('departemen_id',Auth::User()->group_id)->OrderBy('id','ASC')->get();

        foreach ($sequence as $k2 => $v2) 
        {
            
            $approve_list[] = [
                'order_no' => $order_code,
                'role_id' => $v2->role_id,
                'created_by' =>Auth::User()->id,
                'sequence' => $v2->sequence,
                'departemen_id' =>Auth::User()->group_id,
            ];
        }

        $insert_app_list = DB::table('approve_po_list')->insert($approve_list); 
    
       // $check =  DB::table('approve_po_list')->where(['order_no'=>$order_code,'created_by'=>Auth::User()->id])->first();  
        //if($check !=null)
        //{
           
        //}  
    }



    public static function SendNotif($pengirim_id,$penerima_id,$messages)
    {

        //insert notif ke approval
      $notif =  DB::table('notification')->insert(['id_pengirim'=>$pengirim_id,'id_penerima'=>$penerima_id,'deskripsi'=>$messages,'is_read'=>0]);
      return  $notif;  
            

    }
    

    public static function CheckPaymentID($product)
    {
         $array = array();
         $payment_id = $product[0]->payment_id;
         
         foreach($product as $key=>$val)
         {
            if(count($product) > 1)
            {
                $array[$key] = $val->payment_id;               
            }    
         }

         if(count($product) > 1)
         { 

            if(in_array($payment_id, $array)) {
               $res = array('status'=>true,'id'=>$payment_id);
            } else {
               $res = array('status'=>false,'id'=>RequestCart::GetPaymentID($product));
            }  
         }else{
             $res = array('status'=>true,'id'=>$payment_id);
         }
        $result = json_encode($res);
        $data = json_decode($result);
        return $data;

    }

    public static function CheckActionID($id)
    {
       $check = DB::table('cart')->where(['created_by'=>Auth::User()->id,'action'=>'rev','id'=>$id])->count();
       if($check > 0)
       {
          $status = true;
       }else{
         $status = false;
       } 

       return $status;
    }

    public static function CheckTransportID($order_code)
    {
       $check = DB::table('order_transportasi')->where(['created_by'=>Auth::User()->id,'order_no'=>$order_code])->count();
       if($check > 0)
       {
          $status = true;
       }else{
         $status = false;
       } 

       return $status;

    }

     public static function CheckInsuranceID($order_code)
    {
       $check = DB::table('order_asuransi')->where(['created_by'=>Auth::User()->id,'order_no'=>$order_code])->count();
       if($check > 0)
       {
          $status = true;
       }else{
         $status = false;
       } 

       return $status;

    }

    public static function GetPaymentID($product)
    {
         foreach($product as $key=>$val)
         {
            
            $res[$key]['payment_id'] = $val['payment_id'];               
              
         } 

         return $res;

    }

    public static function CreatePDFVendor($project_id,$shipping_id,$payment_id,$order_code,$no_surat,$vendor_id,$perihal,$catatan,$product,$total_price){

        $shipping = RequestOrder::Shipping($shipping_id);
        $payment =  RequestOrder::getPayment($payment_id);
        $project_name = RequestOrder::OrderProjectName($project_id);
        $devision =  strtoupper(RequestOrder::OrderDevision(Auth::User()->group_id));
        $vendor_name = RequestFrontend::Vendor($vendor_id,'vendor_name');
        $directur_name = RequestFrontend::Vendor($vendor_id,'directur_name');
        $address = RequestFrontend::Vendor($vendor_id,'address');
        $no_fax = RequestFrontend::Vendor($vendor_id,'no_fax');
        // $contract = RequestOrder::OrderNoContractPDF($project_id); 
        // $contract_update = RequestOrder::OrderUpdateContract($project_id);
        $contract = '-';
        $contract_update = '-';
        $created_at = GeneralHelpers::dates(date('Y-m-d'));
        $gm_devision = RequestOrder::getGMGroup(Auth::User()->group_id,'devision');
        $branch_manager = RequestOrder::getGMGroup(Auth::User()->group_id,'branch_manager');

        $res = [];
        foreach($product as $key =>$val)
        {
           $res[$key]['product_name'] = $val->product_name;
           $res[$key]['volume'] = $val->volume;
           $res[$key]['qty'] = $val->qty;
           $res[$key]['weight'] = $val->weight; 
           $res[$key]['price'] = RequestOrder::Rupiah($val->price);
           $res[$key]['sub_total'] = RequestOrder::Rupiah($total_price); 
           $res[$key]['jml_weight'] = $val->weight * $val->qty;
           $res[$key]['jml_qty'] =  $val->qty;
        }  

        $ppn = $total_price * 10 / 100;  
        $pph = $total_price * 1.5 / 100; 
        $total = RequestOrder::Rupiah($total_price + $ppn);
        $bill = RequestOrder::Rupiah($total_price - $pph);  
        $data = array('project_name'=>$project_name,'perihal'=>$perihal,'order_code'=>$order_code,'no_surat'=>$no_surat,'vendor_name'=>$vendor_name,'vendor_address'=>$address,'vendor_no_fax'=>$no_fax,'vendor_directur_name'=>$directur_name,'created_at'=>$created_at,'devision'=>$devision,'contract'=>$contract,'contract_update'=>$contract_update,'product'=>$res,'sub_total'=>RequestOrder::Rupiah($total_price),'ppn'=>RequestOrder::Rupiah($ppn),'pph'=>RequestOrder::Rupiah($pph),'total'=>$total,'bill'=>$bill,'shipping_name'=>$shipping,'payment_name'=>$payment,'gm_devision'=>$gm_devision,'branch_manager'=>$branch_manager);
        $result = json_encode($data);
        $order = array('order'=>json_decode($result));
        $pdf = PDF::loadview('order', $order);
        $path = public_path('file/order/');
        $filename = "PO_" . $order_code . '-' . time().'.'.'pdf';
        $pdf->save($path . '/' . $filename);


        //Privy upload
        $result = RequestPrivy::DocumentUpload($filename);
        $data = array('pdf_name'=>$filename,'token'=>$result->data[0]->doc_token);
        return $data;
    }

    public static function CreatePDFTransport($project_id,$shipping_id,$payment_id,$order_code,$no_surat,$vendor_id,$perihal,$catatan,$product,$total_price){

        $shipping = RequestOrder::Shipping($shipping_id);
        $payment =  RequestOrder::getPayment($payment_id);
        $project_name = RequestOrder::OrderProjectName($project_id);
        $devision =  strtoupper(RequestOrder::OrderDevision(Auth::User()->group_id));
        $vendor_name = RequestFrontend::Vendor($vendor_id,'vendor_name');
        $directur_name = RequestFrontend::Vendor($vendor_id,'directur_name');
        $address = RequestFrontend::Vendor($vendor_id,'address');
        $no_fax = RequestFrontend::Vendor($vendor_id,'no_fax');
        // $contract = RequestOrder::OrderNoContractPDF($project_id); 
        // $contract_update = RequestOrder::OrderUpdateContract($project_id);
        $contract = '-';
        $contract_update = '-';
        $created_at = GeneralHelpers::dates(date('Y-m-d'));
        $gm_devision = RequestOrder::getGMGroup(Auth::User()->group_id,'devision');
        $branch_manager = RequestOrder::getGMGroup(Auth::User()->group_id,'branch_manager');

        $res = [];
        foreach($product as $key =>$val)
        {
           $res[$key]['product_name'] = $val->product_name;
           $res[$key]['volume'] = $val->volume;
           $res[$key]['qty'] = $val->qty;
           $res[$key]['weight'] = $val->weight; 
           $res[$key]['price'] = RequestOrder::Rupiah($val->price);
           $res[$key]['sub_total'] = RequestOrder::Rupiah($total_price); 
           $res[$key]['jml_weight'] = $val->weight * $val->qty;
           $res[$key]['jml_qty'] =  $val->qty;
        }  

        $ppn = $total_price * 10 / 100;  
        $pph = $total_price * 1.5 / 100; 
        $total = RequestOrder::Rupiah($total_price + $ppn);
        $bill = RequestOrder::Rupiah($total_price - $pph);  
        $data = array('project_name'=>$project_name,'perihal'=>$perihal,'order_code'=>$order_code,'no_surat'=>$no_surat,'vendor_name'=>$vendor_name,'vendor_address'=>$address,'vendor_no_fax'=>$no_fax,'vendor_directur_name'=>$directur_name,'created_at'=>$created_at,'devision'=>$devision,'contract'=>$contract,'contract_update'=>$contract_update,'product'=>$res,'sub_total'=>RequestOrder::Rupiah($total_price),'ppn'=>RequestOrder::Rupiah($ppn),'pph'=>RequestOrder::Rupiah($pph),'total'=>$total,'bill'=>$bill,'shipping_name'=>$shipping,'payment_name'=>$payment,'gm_devision'=>$gm_devision,'branch_manager'=>$branch_manager);
        $result = json_encode($data);
        $order = array('order'=>json_decode($result));
        $pdf = PDF::loadview('order', $order);
        $path = public_path('file/order/');
        $filename = "PO_" . $order_code . '-' . time().'.'.'pdf';
        $pdf->save($path . '/' . $filename);
        //GetPrivy
       // $getFile = "";
        // $GetPrivy = RequestCart::GetPrivy($filename);
        // DB::table('order')->where(['order_no'=>$order_code])->update(['pdf_name'=>$filename,'token'=>$GetPrivy]);
       // $data = array('pdf_name'=>$filename,'token'=>$GetPrivy);
      //  return $data;
    }


    
    

    public static function GenerateOrder()
    {
    
       
        $now = date("Y-m-d H:i:s");
        $year = date("Y");
        $month = date("m");
        $day = date("d");
          
        DB::table('stored_id')
          ->where('created_at', $now)
          ->where('jenis_po',1)
          ->insert(['dummy_column'=>1,'jenis_po'=>1]);


     
        $getmax = RequestCart::getmaxcounttoday();
        if ($getmax) {
            $max = $getmax + 1;
            $max = str_pad($max, 4, "0", STR_PAD_LEFT);
        } else {
            $max = 1;
            $max = str_pad($max, 4, "0", STR_PAD_LEFT);
        }

        $order_no = substr($year,2,4). $month . $day . $max;
        return $order_no;
    } 


   public static function getmaxcounttoday()
    {
         $result =  DB::table('stored_id')
          ->select(DB::raw('COUNT(id) as total'))
          //->where('created_at', $now)
          ->whereDate('created_at', '=', \DB::raw('CURDATE()'))
          ->where('jenis_po',1)->first();
        if($result !=null)
        {
            $data = $result->total;
        }else{
            $data = 0;
        }   
    
        return $data;
    } 


   
 
    

    

    public static function CheckAction()
    {

         $Rev = DB::table('cart')->where(['created_by'=>Auth::User()->id,'action'=>'rev'])->count();
         $Add = DB::table('cart')->where(['created_by'=>Auth::User()->id,'action'=>'add'])->count();

         if($Add !=0)
         {
            if($Rev ==0)
            {
                return true;
            }else{
                return false; 
            }    

         }else{

            if($Rev !=0)
            {
                return true;
            }else{
                return false; 
            } 

           
         }   
    }

    

    
    


    public static function OrderGabungan(){

        $data = OrderGabungan::create(['created_by'=>Auth::User()->id,'updated_by'=>null,'created_at'=> date('Y-m-d H:i:s')]);
        return $data;
    }


    public static function CheckVendor($vendor_id){

        $cart = DB::table('cart')->select('vendor_id','id','location_id','type')->where(['created_by'=>Auth::User()->id,'vendor_id'=>$vendor_id])->first();
        return $cart; 


    } 

     public static function CheckLocation($location_id){

        $cart = DB::table('cart')->select('vendor_id','id','location_id','type')->where(['created_by'=>Auth::User()->id,'location_id'=>$location_id])->first();
        return $cart; 


    } 

    public static function CheckProduct($product_id,$payment_id){

        $cart = DB::table('cart_product')->select('product_id','payment_id','qty')->where(['created_by'=>Auth::User()->id,'product_id'=>$product_id,'payment_id'=>$payment_id])->first();
        return $cart; 


    } 
   
    

    public static function InsertCart($project_id,$pr_request_id,$vendor_id,$vendor_name,$location_id,$location_name,$type){
        $project = RequestCart::GetRelasiProject($project_id);
        $destination = array_filter($project, function ($item) use ($project_id) {
            return $item['value'] == $project_id;
        }); 


            $order_code = RequestCart::GenerateOrder(); //create order
                $reqcart = array(
                'checklist'=>true, 
                'order_no'=>$order_code,
                'project_id'=>$project_id,
                'pr_request_id'=>$pr_request_id,
                'destination_id'=>$destination[0]['location_id'],
                'project_address'=>$destination[0]['address'],
                'vendor_id'=>$vendor_id,
                'vendor_name'=>$vendor_name,
                'location_id'=>$location_id,
                'location_name'=>$location_name,
                'shipping_id'=>1,
                'insurance'=>'false',
                'transport'=>'false',
                'created_by'=>Auth::User()->id,
                'type'=>$type,
                'reading'=>'false',
                'action'=>'add',
                'status'=>'false', 
              );
              $cart = Cart::create($reqcart);

              return $cart;

    }

    public static function CheckTotalTransaction()
    {
       $sum = DB::table('cart as a')
             ->join('cart_product as b','a.id','=','b.cart_id')
             ->join('vendor as c','a.vendor_id','=','c.id')
             ->where(['a.created_by'=>Auth::User()->id,'b.created_by'=>Auth::User()->id])
             ->select(DB::raw('sum(b.qty * b.price) as total'),'a.type','c.subholding')->get();

        
         $limit = DB::table('config_purcase')->first();
         if($sum[0]->total > $limit->limit_price_non_sub && $sum[0]->type =='Non-kontrak' && $sum[0]->subholding == 'false')
         { 
               $status = array('status'=>'false','messages'=>'Tidak bisa order, pembelian langsung hanya bisa order tidak lebih dari 100 juta');  

         }else if($sum[0]->total > $limit->limit_price_sub && $sum[0]->type =='Non-kontrak' && $sum[0]->subholding == 'true'){      
             $status = array('status'=>'false','messages'=>'Tidak bisa order, pembelian langsung hanya bisa order tidak lebih dari 1 Milyar');
         }else{
            $status = array('status'=>'true','messages'=>$sum[0]->total,'type'=>$sum[0]->type,'subholding'=>$sum[0]->subholding);  
         }    

        return $status;      

    }


    public static function InsertProductCart($cart_id,$product_id,$photo,$product_name,$qty,$price,$weight,$payment_id,$payment_name,$product_uom_id,$product_uom_name){

         $volume = (float)$weight * (int)$qty;

         $reqCartproduct = array(
            'cart_id'=>$cart_id,
            'product_id'=>$product_id,
            'photo'=>$photo,
            'product_name'=>$product_name,
            'qty'=>$qty,
            'price'=>$price,
            'volume'=>$volume,
            'weight'=>$weight,
            'payment_id'=>$payment_id,
            'payment_name'=>$payment_name,
            'product_uom_id'=>$product_uom_id,
            'product_uom_name'=>$product_uom_name,
            'created_by'=>Auth::User()->id

            );
            $cartProduct = DB::table('cart_product')->insert($reqCartproduct);

    }
     
    public static function UpdateProductCart($cart_id,$product_id,$qty,$payment_id){

           $reqCartproduct = array(
            'cart_id'=>$cart_id,
            'product_id'=>$product_id,
            'payment_id'=>$payment_id,
            'created_by'=>Auth::User()->id, 
            );
            $cartProduct = DB::table('cart_product')->where($reqCartproduct)->update(['qty'=>$qty]);
            
    }

    

    public static function RequestOnstage($type,$val)
    {
           
          if($type =="transport_fee")
          {
              $volume = DB::table('cart_product')->where('cart_id',$val->id)->sum('volume');
              $req = array('location_id'=>$val->location_id,'vendor_id'=>$val->vendor_id,"transport_id"=>$val->transport_id,'type'=>$type,'min_weight_transport'=>$val->min_weight_transport,'total_volume'=> $volume,'pr_request_id'=>$val->pr_request_id,);

                
          }else{

            $req = array('location_id'=>$val->location_id,'vendor_id'=>$val->vendor_id,"project_id"=>$val->project_id,'type'=>$type,'pr_request_id'=>$val->pr_request_id,'vendor_transport_id'=>$val->vendor_transport_id);
          } 


            

          $response = json_encode($req);
          $result = json_decode($response);
          return $result;
    }


    public static function privyAccess(){

        $access = DB::table('privy_approv as a')
        ->select('b.privy_id','c.type','c.pos_x','pos_y','c.page')
        ->Leftjoin('users as b','a.user_id','=','b.id')
        ->Leftjoin('privy_access as c','a.id','=','c.id')
        ->OrderBy('a.id','DESC')->get();
        return $access;
    }

 
     public static function cekTerkontrak($product_id,$product_name,$weight,$qty,$type)
    {
         
          $volume_sisa = DB::table('project_product_price as a')
          ->select('a.product_id', 'b.id as project_id', 'b.volume_sisa', 'c.no_amandemen', 'b.no_contract', 'b.tanggal as tgl_contract',DB::Raw('IFNULL(c.end_contract, b.end_contract) as end_contract'))
          ->Leftjoin('project as b','b.id','=','a.project_id')
          ->Leftjoin('amandemen as c','c.id','=','b.last_amandemen_id')
          ->where('a.product_id',(int)$product_id)
          ->where(['a.is_deleted'=>0,'b.is_deleted'=>0])
          ->whereRaw("IFNULL(c.start_contract, b.start_contract) <= CURRENT_DATE ()")
          ->whereRaw("IFNULL(c.end_contract, b.end_contract) >= CURRENT_DATE ()")
          ->groupBy('a.product_id')
          ->first();
    
          return RequestCart::VolumeSisa($volume_sisa,$product_name,$weight,$qty,$type);
        
        
    }


  


    public static function VolumeSisa($volume_sisa,$product_name,$weight,$qty,$type){
        
        $error_product = '';
        $kontrak_volume = '';
        if ($volume_sisa !== NULL) 
        {      
            $kontrak_volume = $volume_sisa->volume_sisa;
            $volume_per_kontrak = (float)$weight * (int)$qty;     
            if ($volume_per_kontrak > $kontrak_volume) 
            {  
               $error_product = "Volume melebihi batas maksimal";
            }
        }

        if($error_product =="") { $status = false; }else{$status = true; }    
        if($type == "GET")
        {
          $result = array('active'=>false,'messages'=>"");  
        }else{
          $result = array('active'=>$status,'messages'=>$error_product);  
        }   
        return $result;
    }


    public static function CheckBudget($sda_code,$pr_request_id,$total,$code_bp)
    {
       $data = DB::table('escm_purchase_requests as a')
       ->select('a.number as prnum','a.item as pritem','a.type as bsart')
       ->where('a.resource_code','like','%'.$sda_code.'%')
       ->where('a.number',$pr_request_id)
       ->first();
       if($data)
       {
    
            // PRNUM : Nomor PR -> tabel escm_purchase_request colom number
            // PRITM : Nomor Line PR -> kurang tw dapatnya drmn
            // PRVAL : Harga total -> sepertinya harga dari po mu
            // PRCUR : Currency -> IDR
            // LINR : BP Vendor -> tabel vendor_wise colom code_bp
            // BSART : Type PO -> tabel escm_purchase_request colom type
         
        

         
         $arr['data'] = array(
                        'prnum'=>$data->prnum,
                        'pritem'=>$data->pritem,
                        'bsart'=>$data->bsart,
                        'PRCUR'=>'IDR',
                        'PRVAL'=>$total,
                        'LINR'=>$code_bp);


       }else{
        
        
         $arr['data'] = array();
       }
       return $arr;


    }





    public static function ConvertFormatBudget($product_check){


         // Inisialisasi array hasil
            $dataSDA = [];

            foreach ($product_check as $item) 
            {
                $data = $item->check_budget->data;
                
                $dataSDA[] = [
                    "PRNUM" => $data->prnum,
                    "PRITM" => $data->pritem,
                    "PRVAL" => (string) $data->PRVAL, // Mengonversi ke string sesuai contoh
                    "PRCUR" => $data->PRCUR,
                    "LIFNR" => $data->LINR ?? "", // LINR diganti LIFNR, jika null maka jadi string kosong
                    "BSART" => $data->bsart
                ];
            }

            return $dataSDA;

    }


    

    

 






    

}