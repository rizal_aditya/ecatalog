<?php

namespace App\Http\Request;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\ResourceCode;
use App\Models\ProjectProductPrice;
use App\Models\ProductGallery;
use App\Models\PaymentProduct;
use App\Models\PaymentMethod;
use App\Models\Location;
use App\Models\ProjectProduct;
use App\Models\Project;
use App\Models\Vendor;
use App\Models\VpiVendor;
use App\Models\Wishlist;
use App\Http\Request\RequestAuth;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestOrderTransport;
use App\Http\Request\RequestOrderAsuransi;
use App\Http\Request\RequestOrderProduk;
use App\Http\Request\RequestHistory;
use DB;

class RequestOrder 
{
   

   public static function Vendor($vendor_id){

     
     $result = array();
     $data = Vendor::select('id','name')->where('id',$vendor_id)->first();
     if($data)
     {
       $result =  $data;
     }   

     return $result;


   }

 


  
    


    public static function OrderNoContractPDF($id)
   {

        $query  = DB::table('project as a');
        $query->select('a.no_contract', DB::raw('IFNULL(b.no_amandemen, 0) as no_amandemen'));
        $query->join('amandemen as b','a.id','=','b.id_project');
        $query->where('a.id',$id);
   
        $results = $query->first();
        if($results !=null)
        {  
            if($results->no_amandemen ==0)
            {
              $res = $results->no_contract;
            }else{
              $res = $results->no_contract.'-Amd'.$results->no_amandemen; 
            }    
     
        }else{
             $res = "";
        }    
        return $res;
   }


    public static function OrderUpdateContract($id)
   {

        $project = DB::table('order as a')   
         ->select(DB::raw('IFNULL(h.created_at,g.tanggal) as tgl_contract'))
        ->leftJoin('payment_method as b','a.payment_method_id','=','b.id')
        ->leftJoin('enum_payment_method as c','b.enum_payment_method_id','=','c.id')
        ->join('shipping as d','a.shipping_id','=','d.id')
        ->join('users as e','a.created_by','=','e.id')
        ->join('project_new as f','a.created_by','=','e.id')
        ->leftJoin('project as g','a.kontrak_id','=','g.id')
        ->leftJoin('amandemen as h','g.last_amandemen_id','=','h.id')
        ->where(['a.is_deleted'=>0,'a.project_id'=>$id])->first();
   
        if($project !=null)
        {  
            
            $res = GeneralHelpers::tanggal($project->tgl_contract);
              
        }else{
             $res = "-";
        }    
        return $res;
   }

   public static function OrderStatusLevel($data,$count_approve)
   {
         $status = '';       
         if($data->is_approve_complete ==0 && $data->order_status !=4)
         {
           $status = "status-orange";
         } else if ($data->order_status == 4) {
                    if ($data->created_by == Auth::User()->id) {
                        if ($count_approve == $data->approve_sequence + 1) {
                            $status = "status-grey";
                        }else{
                            $status = "status-grey";
                        }
                    }else{
                        $status = "status-grey";
                    }
         } else {
                    if ($data->order_status == 2) {
                        $status = "status-green";
                    } else if ($data->order_status == 3) {
                       if ($count_approve == $data->approve_sequence){
                            $status = "status-grey";
                       }else{

                            $status = "status-red";
                       }
                    }   
        } 

        return $status;  

   }


    public static function PDFStatus($data,$count_approve)
   {
        
     
     if($data)
     {
        
            
             if($data->is_approve_complete ==0 && $data->order_status !=4)
             {
               $status = "nonactive";
             } else if ($data->order_status == 4) {
                        if ($data->created_by == Auth::User()->id) {
                            if ($count_approve == $data->approve_sequence + 1) {
                                $status =  "nonactive";
                            }else{
                                $status =  "nonactive";
                            }
                        }else{
                             $status =  "nonactive";
                        }
             } else {
                        if ($data->order_status == 2) {
                             $status = "active";
                        } else if ($data->order_status == 3) {
                           if ($count_approve == $data->approve_sequence){
                                  $status =  "nonactive";
                           }else{
                              $status = "nonactive";
                           }
                        }   
            } 
         
     }else{
         $status = null;
     } 

     return $status;  

   }

   public static function BtnRevision($order_code)
   {
     $order = DB::table('order')->where('order_no',$order_code)->first();
     if($order->order_status ==2 )
     {
        $status = false;
     }else if($order->order_status ==3){
        $status = false;
     }else{
         $status = true;
     }  

     return $status;

   }


    public static function OrderStatusApproved($data,$count_approve){ 
     if($data)
     {
        
             
             if($data->is_approve_complete ==0 && $data->order_status !=4)
             {
               $status = "Waiting approval " . ($data->approve_sequence + 1);
             } else if ($data->order_status == 4) {
                        if ($data->created_by == Auth::User()->id) {
                            if ($count_approve == $data->approve_sequence + 1) {
                                $status = "Revisi Vendor";
                            }else{
                                $status = "Revisi";
                            }
                        }else{
                             $status = "Revisi";
                        }
             } else {
                        if ($data->order_status == 2) {
                             $status = "Approved";
                        } else if ($data->order_status == 3) {
                           if ($count_approve == $data->approve_sequence){
                                  $status = "Canceled Vendor";
                           }else{
                              $status = "Canceled";
                           }
                        }   
            } 
         
     }else{
         $status = null;
     } 

     return array('result'=>$status,'sequence'=>$data->approve_sequence + 1);  


    }

    public static function OrderCheckNoRev($order_code){
        $check = DB::table('log_cancel_po')->where(['no_order'=>$order_code,'status_cancel'=>3])->count();
        if($check < 1)
        {
           $status = 0;
        }else{
             if($check < 10)
             {
                $status = 'Rev-0'.$check;
             }else{
                $status = 'Rev-'.$check;  
             } 
           
        } 
        return $status;  

    }   

     public static function OrderVendorRev($order_code){
        $check = DB::table('log_cancel_po')->where(['no_order'=>$order_code])->count();
        if($check < 1)
        {
           $status = 0;
        }else{
             if($check < 10)
             {
                $status = 'Rev-0'.$check;
             }else{
                $status = 'Rev-'.$check;  
             } 
           
        } 
        return $status;  

    }   

    // public static function getApprovalByID($order_code){

    //      $status = DB::table('order as a')
    //            ->select('a.created_by','a.is_approve_complete','a.approve_sequence','a.order_status')
    //            ->where('a.order_no',$order_code)->first();

    //      return  $status;     
    // }



    public static function OrderShipping($order_code){
       $shipping = DB::table('order as a')
                     ->select('b.name')
                     ->join('tod as b','a.shipping_id','=','b.id')
                     ->where('a.order_no',$order_code)
                     ->first();
       if($shipping)
       {  
            $ship = $shipping->name;
       }else{
            $ship = "-";
       } 

       return $ship;

    }

    public static function TotalVolume($order_code){

         $weight = DB::table('order_product')
        ->select()
        ->where('order_no',$order_code)
        ->first();

        if($weight !=null)
        {
           $total = $weight->total_volume;
        }else{
           $total = 0;
        } 

        return $total;
    }


    

    public static function OrderPayment($order_code){
         $payment = DB::table('order as a')
                     ->select('c.day','d.name')
                     ->join('order_product as b','a.order_no','=','b.order_no')
                     ->leftJoin('payment_method  as c','b.payment_method_id','=','c.id')
                     ->join('enum_payment_method as d','c.enum_payment_method_id','=','d.id')
                     ->where('a.order_no',$order_code)
                     ->first();
         if($payment)
         {
            if($payment->name =="Transfer")
            {
                 $pay =  $payment->name;
            }else{
                 $pay =  $payment->name.'-'.$payment->day;   
            } 
          
         }else{
             $pay = 'Payment kosong';
         }

          

        return $pay;
    }

    

    public static function CheckTransport($order_code){
      $shipping = RequestOrder::OrderShipping($order_code);  
      $res = ($shipping === "Franco");

      if($res ==true)
      {

          $transport = RequestOrderTransport::VendorTransport($order_code); 

          $data = array(
            'status'=>$res,
            'shipping'=>$shipping,
            'vendor_transport_id'=>$transport->vendor_transport_id,
            'vendor_transport'=>$transport->vendor_transport,
            'vendor_address'=>$transport->vendor_address,
            'transport_name'=>$transport->transport_name,
            'transport_price'=>GeneralHelpers::Rupiah($transport->total_price),
            'fee_transport'=> GeneralHelpers::Rupiah($transport->fee_transport),
            'total_transport'=>RequestOrderTransport::OrderTotalTransport($transport->total_berat,$transport->weight_minimum,$transport->fee_transport),
            'berat_min_kg'=> $transport->weight_minimum,
            'berat_min_ton'=>$transport->weight_minimum / 1000,
            'pdf_transport'=> $transport->pdf_name ? '/file/order/' . $transport->pdf_name : '',
        );

      }else{
         $data = array('status'=>$res,'shipping'=>'-','vendor_transport_id'=>'','vendor_transport'=>'-','transport_name'=>'-','fee_transport'=>GeneralHelpers::Rupiah(0),'total_transport'=>GeneralHelpers::Rupiah(0),'pdf_transport'=>'');  
      }  

      return $data;  
 
   }


   

   public static function VendorTransport($order_code,$type){
       $transport = DB::table('order_transportasi')->select('id','transportasi_id','biaya_transport','keterangan_transport','weight_minimum')->where('order_no',$order_code)->first();
       if($transport !=null)
       {  
          if($type =="fee_transport")
          {
             $result =  $transport->biaya_transport; 
          }else if($type =="vendor_transport_id"){
             $result = RequestOrder::FindTranportVendor($transport->transportasi_id,'vendor_id');
          }else if($type =="vendor_transport"){
             $result = RequestOrder::FindTranportVendor($transport->transportasi_id,'vendor');
          }else if($type =="vendor_address"){
             $result = RequestOrder::FindTranportVendor($transport->transportasi_id,'address');
          }else if($type =="weight_minimum"){
               $result =  $transport->weight_minimum; 
          }else if($type =="transport_name"){
               $result = RequestOrder::FindTranportVendor($transport->transportasi_id,'logistic'); 
          }else if($type =="transport_price"){
            $result =  $transport->keterangan_transport;
          }  
        
       }else{
          $result =  0;
       } 

       return $result;
   }

     public static function FindTranportVendor($transport_id,$type){
       $transport = DB::table('transportasi')->where('id',$transport_id)->first();
       if($transport !=null)
       {
          if($type =="vendor_id")
          {
            $result = $transport->vendor_id;
          }else if($type =="vendor"){  
            $result = RequestFrontend::Vendor($transport->vendor_id,'vendor_name');
          }else if($type =="address"){
            $result = RequestFrontend::Vendor($transport->vendor_id,'address');
          }else if($type =="logistic"){
            $result = RequestOrder::FindTranspotLog($transport->sda_code); 
          }  
          
       }else{
         $result = $type.'transport kosong';
       } 

       return $result;
   }

    public static function FindTranspotLog($sda_code)
    {
      $result = DB::table('resources_code')->where('code',$sda_code)->first();
      if($result)
      {
          $res =  $result->name;   
      }else{
        $res = "";
      }  
      return $res;
    }

    public static function CheckInsurance($order_code){
       $insurance = DB::table('order_asuransi as a')
                ->select(
                    'a.nilai_harga_minimum as minimum',
                    'a.nilai_asuransi as premi',
                    DB::raw('SUM(b.qty*b.weight*b.price) AS total_price'),
                    'd.name as vendor_name',
                    'd.address as vendor_address',
                    'c.no_cargo_insurance',
                    'a.pdf_name'
                )
                ->join('order_product as b','a.order_no','=','b.order_no')
                ->join('asuransi as c','a.asuransi_id','=','c.id')
                ->join('vendor as d','c.vendor_id','=','d.id')
                ->where('a.order_no',$order_code)
                ->first(); 

       if($insurance)
       {
         
         
         $nilai_min_insurance =  RequestOrderAsuransi::OrderMinimumInsurance($insurance->total_price,$insurance->premi,$insurance->minimum);

         $result = array(
            'status'=>true,
            'vendor_name'=>$insurance->vendor_name,
            'vendor_address'=>$insurance->vendor_address,
            'premi'=>$insurance->premi,
            'nilai_min_premi'=>GeneralHelpers::Rupiah($nilai_min_insurance),
            'total_insurance'=>$insurance->total_price  * $insurance->premi / 100,
            'pdf_insurance'=> $insurance->pdf_name ? '/file/order/' . $insurance->pdf_name : '',
        );
         
       }else{
         $result = array('status'=>false,'premi'=>0,'min_premi'=>0,'price_insurance'=>GeneralHelpers::Rupiah(0),'total_insurance'=>GeneralHelpers::Rupiah(0),'pdf_insurance'=>'');
       } 
       return $result;  
 
   }


   

   public static function TotalProduct($order_code){

         $payment = DB::table('order_product')
        ->select( DB::raw('SUM(qty*weight*price) AS total_price'))
        ->where('order_no',$order_code)
        ->first();

        return $payment ? $payment->total_price : null;
    }

   

    public static function getPayment($id){
     $__temp_ = array();
     $payment = DB::table('payment_method as a')
               ->select('a.id','a.day','b.name')
               ->join('enum_payment_method as b','b.id','=','a.enum_payment_method_id')
               ->where('a.id',$id)
               ->first();

    
         $__temp_ = ucfirst(strtolower($payment->name.' '.$payment->day.' Hari'));
    
    
     return  $__temp_;           
   }

   public static function getGMGroup($group_id,$type)
   {
          $groups = DB::table('groups')->select('name','general_manager')->where('id',$group_id)->first();
          if($type =="devision")
          {
             $res =  $groups->name; 
          }else{
            $res =  $groups->general_manager;
          } 

         return  $res;  
   }

   public static function GetApproval($order_code){

           $sequence = DB::table('approve_po_list as a')
            ->select('a.order_no','a.role_id','c.first_name as approval_name','a.updated_by','d.first_name as user_approve_name','a.status_approve','a.sequence')
            ->LeftJoin('users_roles as b','b.role_id','=','a.role_id')
            ->LeftJoin('users as c','b.user_id','=','c.id')
            ->LeftJoin('users as d','a.updated_by','=','d.id')
            ->where(['a.order_no'=>$order_code,'c.is_deleted' =>0])
            ->OrderBy('a.sequence','ASC')->get();
        
        $approve_list = array();
        foreach ($sequence as $k2 => $v2) 
        {
            
            $approve_list[$k2]['approve_no'] = $k2+1;
            $approve_list[$k2]['approval_name'] = $v2->approval_name;
            if($v2->sequence ==1)
            { 
              if($v2->status_approve ==0)
              {
                $status = false;
              }else{
                $status = true;
              }  
               $approve_list[$k2]['approval_status'] = $status;     
            }
            else if($v2->sequence ==2)
            {
               if($v2->status_approve ==0)
              {
                $status = false;
              }else{
                $status = true;
              } 
              $approve_list[$k2]['approval_status'] = $status; 
            }    
             
        }

        return $approve_list;


   }

   public static function OrderDevision($group_id){
      $division = DB::table('groups')->select('name')->where('id',$group_id)->first();
      if($division)
      {
         $dev = $division->name;
      }else{
         $dev = "divisi masih kosong";
      }  

      return $dev; 
    }

  

    public static function TerbilangNumber($angka)
    {
        $angka = str_replace('.', '', $angka); // Menghapus titik pemisah ribuan
        $angka = str_replace(',', '.', $angka); // Mengganti koma desimal menjadi titik

        $satuan = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan"];
        $tingkatan = ["", "ribu", "juta", "milyar", "triliun"];

        $hasil = "";
        $i = 0;

        while ($angka > 0) {
            $grup = $angka % 1000; // Ambil tiga digit terakhir
            if ($grup != 0) {
                $ratusan = floor($grup / 100);
                $puluhan = floor(($grup % 100) / 10);
                $satuan_angka = $grup % 10;

                $terbilang_grup = "";

                if ($ratusan > 0) {
                    $terbilang_grup .= $ratusan == 1 ? "seratus " : $satuan[$ratusan] . " ratus ";
                }

                if ($puluhan == 1) {
                    if ($satuan_angka == 0) {
                        $terbilang_grup .= "sepuluh";
                    } elseif ($satuan_angka == 1) {
                        $terbilang_grup .= "sebelas";
                    } else {
                        $terbilang_grup .= $satuan[$satuan_angka] . " belas";
                    }
                } else {
                    if ($puluhan > 1) {
                        $terbilang_grup .= $satuan[$puluhan] . " puluh ";
                    }
                    if ($satuan_angka > 0) {
                        $terbilang_grup .= $satuan[$satuan_angka];
                    }
                }

                $hasil = trim($terbilang_grup) . " " . $tingkatan[$i] . " " . $hasil;
            }

            $angka = floor($angka / 1000);
            $i++;
        }

        return trim($hasil);
    }


    public static function VendorTransportAmandemenID($transport_id,$transport_name){


        $result = array();
        $data = DB::table('transportasi as a')
    ->select([
        DB::raw('IFNULL(d.created_at, b.tgl_kontrak) as contract_startdate'),
        DB::raw('IFNULL(d.end_contract, b.end_date) as contract_enddate'),
        'd.no_amandemen',
        'b.no_contract as contract_number',
        'c.name as name',
        'c.ttd_name as directur_name',
        'c.address',
        'c.email',
        'c.no_telp as phone',
        'c.npwp',
        'c.vendor_type',
        'c.vendor_status_id',
        'c.bidang_usaha',
    ])
    ->leftJoin('kontrak_transportasi as b', 'b.vendor_id', '=', 'a.vendor_id')
    ->leftJoin('vendor as c', 'c.id', '=', 'a.vendor_id')
    ->leftJoin('transport_amandemen as d', 'd.id', '=', 'b.last_amandemen_id')
    ->where('a.id', $transport_id)
    ->first();


        if($data)
        {
           $startdate = ['original'=>date("d-m-Y", strtotime($data->contract_startdate)),'convert'=> GeneralHelpers::tanggal($data->contract_startdate) ,'terbilang'=>RequestOrder::TerbilangDate(substr($data->contract_startdate, 0, 10))]; 

           $enddate = GeneralHelpers::tanggal($data->contract_enddate);
           


           $result = array(
                'name'=>$data->name,
                'address'=>$data->address,
                'directur_name' => $data->directur_name,
                'phone'=> $data->phone,
                'email' => $data->email,
                'npwp' =>$data->npwp,
                'contract'=> ['number'=>$data->contract_number . ($data->no_amandemen ? '-Amd' . $data->no_amandemen : ''),'startdate'=>$startdate,'enddate'=>$enddate],            
                'type'=>$data->vendor_type,
                'status'=>$data->vendor_status_id,
                'type'=>$data->vendor_type,
                'business_fields'=>$data->bidang_usaha,
                'pengiriman' =>$transport_name,

           ); 
        
        }


          return $result;


    }


    public static function VendorAsuransiAmandemenID($asuransi_id){

        $result = array();
        $data = DB::table('asuransi as a')
    ->select([
        DB::raw('IFNULL(d.created_at, a.tgl_kontrak) as contract_startdate'),
        DB::raw('IFNULL(d.end_contract, a.end_date) as contract_enddate'),
        'd.no_amandemen',
        'a.no_contract as contract_number',
        'c.name as name',
        'c.ttd_name as directur_name',
        'c.address',
        'c.email',
        'c.no_fax as fax',
        'c.no_telp as phone',
        'c.npwp',
        'c.vendor_type',
        'c.vendor_status_id',
        'c.bidang_usaha',
        'a.tahun',
        'a.no_cargo_insurance as cargo_number',
        'a.jenis_asuransi',
        'a.nilai_asuransi',
        'a.nilai_harga_minimum',
    ])
    ->leftJoin('vendor as c', 'c.id', '=', 'a.vendor_id')
    ->leftJoin('amandemen_asuransi as d', 'd.id', '=', 'a.last_amandemen_id')
    ->where('a.id', $asuransi_id)
    ->first();


         if($data)
        {
           $startdate = ['original'=>date("d-m-Y", strtotime($data->contract_startdate)),'convert'=> GeneralHelpers::tanggal($data->contract_startdate) ,'terbilang'=>RequestOrder::TerbilangDate(substr($data->contract_startdate, 0, 10))]; 

           $enddate = GeneralHelpers::tanggal($data->contract_enddate);
           
           

           $result = array(
                'name'=>$data->name,
                'address'=>$data->address,
                'directur_name' => $data->directur_name,
                'phone'=> $data->phone . (!empty($data->fax) ? ' Fax: ' . $data->fax : ''),
                'fax'=> $data->fax,
                'email' => $data->email,
                'npwp' =>$data->npwp,
                'contract'=> ['number'=> $data->contract_number . ($data->no_amandemen ? '-Amd' . $data->no_amandemen : ''),'startdate'=>$startdate,'enddate'=>$enddate],            
                'type'=>$data->vendor_type,
                'status'=>$data->vendor_status_id,
                'type'=>$data->vendor_type,
                'business_fields'=>$data->bidang_usaha,
                'tahun' => $data->tahun,
                'cargo_number' => $data->cargo_number, 
                'jenis_asuransi' => $data->jenis_asuransi, 
                'nilai_asuransi' =>  $data->nilai_asuransi,
                'nilai_harga_minimum' =>   GeneralHelpers::Rupiah($data->nilai_harga_minimum),
                
           ); 
        
        }



          return $result;


    }


    public static function TerbilangDate($tanggal) {
    $parts = explode("-", $tanggal);
    $tahun = RequestOrder::angkaKeTerbilang($parts[0]);
    $bulan = RequestOrder::bulanIndonesia((int)$parts[1]);
    $hari = RequestOrder::angkaKeTerbilang((int)$parts[2]);
    $hariNama = RequestOrder::hariIndonesia($tanggal);

    return 'hari '.$hariNama.', Tanggal '.$hari.' Bulan '.$bulan.' Tahun '.$tahun;
}

public static function hariIndonesia($tanggal) {
    $hariIndo = [
        "Sunday" => "Minggu",
        "Monday" => "Senin",
        "Tuesday" => "Selasa",
        "Wednesday" => "Rabu",
        "Thursday" => "Kamis",
        "Friday" => "Jumat",
        "Saturday" => "Sabtu"
    ];
    $hari = date("l", strtotime($tanggal));
    return $hariIndo[$hari];
}


  public static function angkaKeTerbilang($angka) {
    $terbilang = [
        "", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan",
        "Sepuluh", "Sebelas", "Dua Belas", "Tiga Belas", "Empat Belas", "Lima Belas", 
        "Enam Belas", "Tujuh Belas", "Delapan Belas", "Sembilan Belas"
    ];
    
    if ($angka < 20) {
        return $terbilang[$angka];
    } elseif ($angka < 100) {
        return trim($terbilang[$angka / 10] . " Puluh " . $terbilang[$angka % 10]);
    } elseif ($angka < 200) {
        return "Seratus " . RequestOrder::angkaKeTerbilang($angka - 100);
    } elseif ($angka < 1000) {
        return trim($terbilang[$angka / 100] . " Ratus " . RequestOrder::angkaKeTerbilang($angka % 100));
    } elseif ($angka < 2000) {
        return "Seribu " . RequestOrder::angkaKeTerbilang($angka - 1000);
    } elseif ($angka < 1000000) {
        return trim(RequestOrder::angkaKeTerbilang($angka / 1000) . " Ribu " . RequestOrder::angkaKeTerbilang($angka % 1000));
    } elseif ($angka < 1000000000) {
        return trim(RequestOrder::angkaKeTerbilang($angka / 1000000) . " Juta " . RequestOrder::angkaKeTerbilang($angka % 1000000));
    }
}

public static function bulanIndonesia($bulan) {
    $bulanIndo = [
        1 => "Januari", 2 => "Februari", 3 => "Maret", 4 => "April",
        5 => "Mei", 6 => "Juni", 7 => "Juli", 8 => "Agustus",
        9 => "September", 10 => "Oktober", 11 => "November", 12 => "Desember"
    ];
    return $bulanIndo[$bulan];
}




   

}