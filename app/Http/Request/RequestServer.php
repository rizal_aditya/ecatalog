<?php

namespace App\Http\Request;

use Illuminate\Support\Facades\Http;
use Auth;
use DB;

class RequestServer
{

    public static function findAllById(){

        $group_id = Auth::User()->group_id;
        $groups = DB::table('groups')->where('id',$group_id)->first();  
        if($groups !=null)
        {
            return $groups;
        }    
        return FALSE;
    }

   public static  function GetNoSuratWika($perihal)
   { 
        
        $groups = RequestServer::findAllById();

        $data = [
            'tanggal' => date('Y-m-d'),
            'perihal' => $perihal,
            'nama_user' => Auth::User()->username,
            'penandatangan' => $groups->general_manager,
            'nip' => '4CCFD310AA',
            'unit_kerja' => $groups->departemen_code.'.'.$groups->departemen_code2,
            'tujuan'=> 'Vendor'
        ];

        $encode = json_decode(json_encode($data),false);       

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, env('SERVER_NO_SURAT'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encode);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        //die(var_dump($server_output));
        $result = json_decode($server_output);
        return json_decode(json_encode($result
        ? ($result->status === 'success'
            ? ['status' => true, 'data' => $result->message]
            : ['status' => false, 'data' => ''])
        : ['status' => false, 'data' => '']), FALSE);   

       
        
           
    }


   



    public static function GetToken()
    {

        $URL = env('URL_CHECKBUDGET_PROD');
        $user = env('USER_CHECKBUDGET_PROD');
        $pass = env('PASSWORD_CHECKBUDGET_PROD');

        $headers = array(
            'x-csrf-token: fetch',
            'x-requested-with: XMLHttpRequest',
            'Content-Type:application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $user . ":" . $pass);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        $output = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        curl_close($ch);
        $header = substr($output, 0, $header_size);

        $headers = $header;
        return $headers;
       
       
    }

    
      

    public static function CekBudget($token,$cookies,$dataSDA)
    {
        $URL = env('URL_CHECKBUDGET_PROD');
        $user = env('USER_CHECKBUDGET_PROD');
        $pass = env('PASSWORD_CHECKBUDGET_PROD');
          
            // PRNUM : Nomor PR -> tabel escm_purchase_request colom number
            // PRITM : Nomor Line PR -> kurang tw dapatnya drmn
            // PRVAL : Harga total -> sepertinya harga dari po mu
            // PRCUR : Currency -> IDR
            // LINR : BP Vendor -> tabel vendor_wise colom code_bp
            // BSART : Type PO -> tabel escm_purchase_request colom type

        //  $dataSDA = [
        //     [
        //         "PRNUM" => "1200006940",
        //         "PRITM" => "10",
        //         "PRVAL" => "1",
        //         "PRCUR" => "IDR",
        //         "LIFNR" => "PA00000003",
        //         "BSART" => "ZW01"
        //     ],
        //     [
        //         "PRNUM" => "1200006939",
        //         "PRITM" => "10",
        //         "PRVAL" => "11",
        //         "PRCUR" => "IDR",
        //         "LIFNR" => "PA00000003",
        //         "BSART" => "ZW01"
        //     ],
        //     [
        //         "PRNUM" => "1200006941",
        //         "PRITM" => "10",
        //         "PRVAL" => "11",
        //         "PRCUR" => "IDR",
        //         "LIFNR" => "PA00000003",
        //         "BSART" => "ZW01"
        //     ]
        // ];
        $dataPost['DATA'] = $dataSDA;

        $post = $dataPost;

        $headers = array(
            'x-csrf-token: ' . $token,
            'Cookie: ' . $cookies,
            'Content-Type: application/json',
            'Accept: application/json'
        );


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST => 1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => json_encode($post),
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_USERPWD => $user . ":" . $pass,
            CURLOPT_COOKIEJAR => $cookies,
            CURLOPT_COOKIEFILE => $cookies,
        ));


        $response = curl_exec($curl);

        curl_close($curl);
          $response_sap = ($response);
        $ServerBudget =  json_decode($response_sap);
        $budget = (int)$ServerBudget->DATA[0]->E_BUDGET;
        $status =  RequestServer::FilterBudget($dataSDA,$budget);
        $arr = array('status'=>$status,'budget'=>$budget); 
        $result =  json_decode(json_encode($arr), FALSE);
        return $result; 

    }


    public static function FilterBudget($dataSDA,$budget)
    {

            foreach ($dataSDA as $item) 
            {
                if (floatval($item["PRVAL"]) > $budget) {
                     $checkBudget = false;
                } else {
                     $checkBudget = true;
                }
            }

            return  $checkBudget;


    }


    public static function ConvertJson($httpResponse,$type)
    {

       // Proses parsing
        $lines = explode("\r\n", trim($httpResponse));
        $httpStatus = array_shift($lines);

        $headers = [];
        $cookies = [];
        $convertedHeaders = []; 
        $convertedCookies = [];
        foreach ($lines as $line) {
            if (stripos($line, 'set-cookie:') === 0) {
                $cookieData = substr($line, strlen('set-cookie: '));
                $cookieParts = explode(';', $cookieData);
                $cookieKeyValue = explode('=', array_shift($cookieParts), 2);
                $cookies[$cookieKeyValue[0]] = $cookieKeyValue[1] ?? '';
                foreach ($cookies as $key => $value) {
                    // Mengganti "-" dengan "_" dan mengubah ke lowercase
                    $newKey = str_replace("-", "_", strtolower($key));
                    $convertedCookies[$newKey] = $value;
                }

            } elseif (strpos($line, ':') !== false) {
                list($key, $value) = explode(': ', $line, 2);
                $headers[$key] = $value;
                foreach ($headers as $key => $value) {
                    // Mengganti "-" dengan "_" dan mengubah ke lowercase
                    $newKey = str_replace("-", "_", strtolower($key));
                    $convertedHeaders[$newKey] = $value;
                }


            }
        }

        // Konversi ke JSON
        $jsonOutput = json_decode(json_encode([
            'status' => $httpStatus,
            'headers' => $convertedHeaders,
            'cookies' => $convertedCookies
        ], JSON_PRETTY_PRINT));

       $xcsrftoken = $jsonOutput->headers->x_csrf_token;
       if($type =="Dev")
       {
          $cookies = $jsonOutput->cookies->sap_usercontext.';'.$jsonOutput->cookies->mysapsso2.';'.$jsonOutput->cookies->sap_sessionid_ws1_110;
       }else{
          $cookies = $jsonOutput->cookies->sap_usercontext.';'.$jsonOutput->cookies->sap_sessionid_ws3_230;
       } 
      
        $res = array('token'=>$xcsrftoken,'cookies'=>$cookies);
        return json_decode(json_encode($res), FALSE);;
        
    }


    public static function encryptWithSignature($data, $secretKey) 
    {
    
        $json = json_encode($data);
        $base64Data = base64_encode($json);
        $signature = hash_hmac('sha256', $base64Data, $secretKey, true);
        $encrypted = base64_encode($signature . $base64Data);
        return $encrypted;
     
    }

    public static function decryptData($encryptedData, $secretKey) {
    
    $decoded = base64_decode($encryptedData);
    $signature = substr($decoded, 0, 32);
    $base64Data = substr($decoded, 32);
    $expectedSignature = hash_hmac('sha256', $base64Data, $secretKey, true);

    if (!hash_equals($expectedSignature, $signature)) {
        return "Invalid signature!"; // Signature tidak cocok
    }

    return json_decode(base64_decode($base64Data), true);
}





     

   


}