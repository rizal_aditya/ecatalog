<?php

namespace App\Http\Request;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\ResourceCode;
use App\Models\ProjectProductPrice;
use App\Models\ProductGallery;
use App\Models\PaymentProduct;
use App\Models\PaymentMethod;
use App\Models\Location;
use App\Models\ProjectProduct;
use App\Models\Project;
use App\Models\Vendor;
use App\Models\VpiVendor;
use App\Models\RoleUser;
use App\Models\Wishlist;
use App\Http\Request\RequestAuth;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestOrderTransport;
use App\Http\Request\RequestOrderAsuransi;
use App\Http\Request\RequestOrderProduk;
use App\Http\Request\RequestOrder;
use App\Http\Request\RequestCart;
use App\Http\Request\Search\SearchHistory;

use Illuminate\Support\Facades\Http;
use DB;

class RequestHistory 
{
   
   public static function RealtimeQuery($request){

        $query = DB::table('order as a');
        $query->select(
            'a.id',
            'a.order_no as order_code',
            'a.project_id',
            'a.location_name',
            'a.perihal',
            'a.catatan',
            'a.created_at',
            'a.vendor_id',
            'b.first_name',
            'b.last_name',
            'b.group_id',
            'a.pdf_name',
            'a.is_approve_complete',
            'a.no_surat',
            'a.created_at',
            'a.tgl_diambil',
            'a.order_status',
            'a.approve_sequence',
            'a.created_by',
        );
        $query->join('users as b','a.created_by','=','b.id');
        //$order->join('order as c','a.order_no','=','c.order_no');
        $query->join('groups as e','b.group_id','=','e.id');
        // $query->whereNotNull('a.pdf_name');
        // $query->whereNotNull('a.doc_token');

        SearchHistory::ListUserApproval($query);

        SearchHistory::SearchStatus($request,$query);
        SearchHistory::SearchOrderCode($request,$query);
        SearchHistory::SearchLocation($request,$query);
        SearchHistory::SearchVendor($request,$query);
        SearchHistory::SearchDate($request,$query);
        
        
        return  $query;
    }
   

   public static function GetDataOrder($data,$perPage,$request,$description)
   {
        $temp = array();
        $getRequest = $request->all();
        $page = isset($getRequest['page']) ? $getRequest['page'] : 1;
        $numberNext = (($page*$perPage) - ($perPage-1));
        

        foreach ($data as $key => $val)
        {
            $count_approve = DB::table('approve_po_list')->where(['order_no'=>$val->order_code])->count(); 
            $temp[$key]['number'] = $numberNext++;
            $temp[$key]['id'] = $val->id;
            $project = RequestCart::GetRelasiProject($val->project_id)[0];
            $temp[$key]['project_name'] =  strtoupper($project['text']);
            $temp[$key]['origin'] = RequestFrontend::limitText($val->location_name,'28');
           
            $temp[$key]['destination'] =  RequestFrontend::limitText($project['address'],'40');
            $temp[$key]['order_by_location'] =  $project['province_name'];
            
            $temp[$key]['order_code'] =  $val->order_code;
            $temp[$key]['vendor'] =  RequestOrder::Vendor($val->vendor_id);
            $temp[$key]['division'] =  RequestFrontend::OrderDevision($val->group_id);
            $temp[$key]['order_by'] =  $val->first_name.' '.$val->last_name;
            $temp[$key]['pdf_name'] =  $val->pdf_name ? 'file/order/' . $val->pdf_name : '';
            $temp[$key]['no_surat'] =  $val->no_surat;
            $temp[$key]['created_at'] =  GeneralHelpers::tanggal($val->created_at);
            $temp[$key]['tgl_diambil'] = GeneralHelpers::dates($val->tgl_diambil);
            
           
    
            $temp[$key]['transport']  = RequestOrder::CheckTransport($val->order_code);
            $temp[$key]['insurance']  = RequestOrder::CheckInsurance($val->order_code);


           
            $temp[$key]['product'] = RequestOrderProduk::OrderProduct($val->order_code);

            $total_product = RequestOrderProduk::TotalProduct($val->order_code);
            $temp[$key]['total_item'] = $total_product->total_item.' Produk'; 
            $temp[$key]['total_qty'] =  $total_product->total_qty;
            $temp[$key]['total_price'] =  GeneralHelpers::Rupiah($total_product->total_price);


            $temp[$key]['total_volume'] = round($total_product->total_volume, 3);
            $temp[$key]['status_color'] =  RequestOrder::OrderStatusLevel($val,$count_approve);
            $temp[$key]['status_approved'] =  RequestOrder::OrderStatusApproved($val,$count_approve);
            $temp[$key]['status_order'] =  RequestOrder::OrderCheckNoRev($val->order_code);
            $temp[$key]['about'] =  RequestFrontend::limitText($val->perihal,'50');
            $temp[$key]['notes'] =  $val->catatan;
            $temp[$key]['pdf_status'] = RequestOrder::PDFStatus($val,$count_approve);
            $temp[$key]['btn_revision'] = RequestOrder::BtnRevision($val->order_code);  
            $temp[$key]['verified'] = RequestHistory::RoleVerification($val->order_code);
            
          
            $total_transport = $temp[$key]['transport']['status'] ? RequestOrder::CheckTransport($val->order_code)['total_transport'] : 0;
            $total_insurance = $temp[$key]['insurance']['status'] ? RequestOrder::CheckInsurance($val->order_code)['total_insurance'] : 0;

            $temp[$key]['total'] = GeneralHelpers::Rupiah($total_transport + $total_insurance + $total_product->total_price);

            
          
           
           
        }

           
        $results['roles'] = DB::table('users_roles')->where('user_id',Auth::User()->id)->first()->role_id;
        $results['vendor'] = RequestHistory::GetVendor();
        $results['location'] = RequestHistory::GetLocation();
        // $results['sda'] = RequestHistory::getSumberDaya();
        $results['result'] = $temp;
        if($description !="")
        {  if($data->total() !=0)
           {
             $results['cari'] = 'Pencarian "<b>'.$description.'</b>" berhasil ditemukan Total : '.$data->total(); 
           }else{
             $results['cari'] = 'Pencarian tidak ditemukan "'.$description.'" '; 
           } 
            
        }else{
            $results['cari'] = ''; 
        } 

         
        
        $results['total'] = $data->total();
       
        
      
        $results['lastPage'] = $data->lastPage();
        $results['perPage'] = $data->perPage();
        $results['currentPage'] = $data->currentPage();
        $results['nextPageUrl'] = $data->nextPageUrl();
        return $results;

   }

  

   public static function GetDataOrderDetail($val)
   {
        $temp = array();
       
        
            $temp['id'] = $val->id;
            $count_approve = DB::table('approve_po_list')->where(['order_no'=>$val->order_code])->count(); 
            $project = RequestCart::GetRelasiProject($val->project_id)[0];
            $temp['project_name'] =  strtoupper($project['text']);
            $temp['origin'] = RequestFrontend::limitText($val->location_name,'28');
           
            $temp['destination'] =  RequestFrontend::limitText($project['address'],'40');
            $temp['order_by_location'] =  $project['province_name'];


            $temp['order_code'] =  $val->order_code;
            $temp['vendor'] =  RequestOrder::Vendor($val->vendor_id);


            $temp['division'] =  RequestOrder::OrderDevision($val->group_id);
            $temp['order_by'] =  $val->first_name.' '.$val->last_name;
            $temp['pdf_name'] =  RequestHistory::PDFVendor($val->order_code);
            $temp['no_surat'] =  $val->no_surat;
            $temp['created_at'] =  GeneralHelpers::tanggal($val->created_at);
            $temp['tgl_diambil'] = GeneralHelpers::dates($val->tgl_diambil);
            
            
            $temp['transport']  = RequestOrder::CheckTransport($val->order_code);
            $temp['insurance']  = RequestOrder::CheckInsurance($val->order_code);

          
            
           
           
            $temp['product'] = RequestOrderProduk::OrderProduct($val->order_code);
            $total_product = RequestOrderProduk::TotalProduct($val->order_code);
            $temp['total_item'] = $total_product->total_item.' Produk'; 
            $temp['total_qty'] =  $total_product->total_qty;
            $temp['total_price'] =  RequestOrder::Rupiah($total_product->total_price);

           
            $temp['approv'] =  RequestOrder::GetApproval($val->order_code);
            $temp['status_color'] =  RequestOrder::OrderStatusLevel($val,$count_approve);
            $temp['status_approved'] =  RequestOrder::OrderStatusApproved($val,$count_approve);
            $temp['about'] =  RequestFrontend::limitText($val->perihal,'50');
            $temp['notes'] =  $val->catatan;
            $temp['shipping'] =  RequestHistory::OrderShipping($val->order_code);
           
            
           
            $temp['total_product'] = count($temp['product']);
            if($temp['transport']['status'] == true && $temp['insurance']['status'] == false)
            {
                $temp['transport_status'] = true;
                $temp['insurance_status'] = false;
                $total_transport = RequestOrder::CheckTransport($val->order_code)['total_transport'];
                $temp['total'] =  RequestOrder::Rupiah($total_transport + $total_product->total_price);

            }else if($temp['transport']['status'] == false && $temp['insurance']['status'] == true){    
                $total_insurance = RequestOrder::CheckInsurance($val->order_code)['total_insurance'];
                $temp['total'] =  RequestOrder::Rupiah($total_insurance + $total_product->total_price);
                $temp['transport_status'] = false;
                $temp['insurance_status'] = true;
            }else if($temp['transport']['status'] == true && $temp['insurance']['status'] == true){
                $temp['transport_status'] = true;
                $temp['insurance_status'] = true;
                $total_transport = RequestOrder::CheckTransport($val->order_code)['total_transport'];
                $total_insurance = RequestOrder::CheckInsurance($val->order_code)['total_insurance'];
                $temp['total'] = RequestOrder::Rupiah($total_transport +  $total_insurance + $total_product->total_price);
            }else{
                $temp['total'] = RequestOrder::Rupiah($total_product->total_price);
                $temp['transport_status'] = false;
                $temp['insurance_status'] = false;
                
            }
            
           
            $temp['verified'] = RequestHistory::RoleVerification($val->order_code);
            $temp['status_otp'] = RequestHistory::GetStatusOtp($val->order_code);
            $temp['pdf_status'] = RequestOrder::PDFStatus($val,$count_approve);
           
       
        $results['result'] = $temp;
        return $results;

   }


   


   public static function GetStatusOtp($order_code){
     $status = DB::table('request_otp')->where(['order_no'=>$order_code,'user_id'=>Auth::User()->id])->first();
     if($status !=null)
     {
         $result = true;
     }else{
        $result = false;
     }   

      return $result;

   }

  

   





  
    public static function OrderNoContract($id)
   {

        $query  = DB::table('project as a');
        $query->select('a.no_contract', DB::raw('IFNULL(b.no_amandemen, 0) as no_amandemen'));
        $query->join('amandemen as b','a.id','=','b.id_project');
        $query->where('a.id',$id);
   
        $results = $query->first();
        if($results !=null)
        {  
            if($results->no_amandemen ==0)
            {
              $res = $results->no_contract;
            }else{
              $res = $results->no_contract.'-'.$results->no_amandemen; 
            }    
     
        }else{
             $res = "";
        }    
        return $res;
   }


    public static function OrderUpdateContract($id)
   {

        $query  = DB::table('project as a');
        $query->select('a.updated_at');
        $query->where('a.id',$id);
   
        $results = $query->first();
        if($results !=null)
        {  
            
            $res = GeneralHelpers::tanggal_indo($results->updated_at);
              
        }else{
             $res = "";
        }    
        return $res;
   }


   



    public static function OrderShipping($order_code){
       $shipping = DB::table('order as a')
                     ->select('b.name')
                     ->join('tod as b','a.shipping_id','=','b.id')
                     ->where('a.order_no',$order_code)
                     ->first();
       if($shipping !=null)
       {  
            $ship = $shipping->name;
       }else{
            $ship = "Shipping kosong";
       } 

       return $ship;

    }


    public static function OrderDevision($group_id){
      $division = DB::table('groups')->select('name')->where('id',$group_id)->first();
      if($division !=null)
      {
         $dev = $division->name;
      }else{
         $dev = "divisi masih kosong";
      }  

      return $dev; 
    }

    public static function OrderPayment($order_code){
         $payment = DB::table('order as a')
                     ->select('c.day','d.name')
                     ->join('order_product as b','a.order_no','=','b.order_no')
                     ->leftJoin('payment_method  as c','b.payment_method_id','=','c.id')
                     ->join('enum_payment_method as d','c.enum_payment_method_id','=','d.id')
                     ->where('a.order_no',$order_code)
                     ->first();
         if($payment !=null)
         {
            $pay =  $payment->name.'-'.$payment->day;
         }else{
             $pay = 'Payment kosong';
         }            
        return $pay;
    }

    

    
//product


 



   public static function OrderLocation($project_id)
   {
       $project = DB::table('project_new as a')
                  ->join('ref_locations as b','a.location_id','=','b.location_id')
                  ->select('b.province_name')->where('a.id',$project_id)->first();
       if($project !=null)
       {     
            $pro = $project->province_name;   
       }else{
             $pro = "";
       } 
       return $pro;
   }

   

   

    public static function OrderMaxDate($order_code){
       $dated = DB::table('order')
        ->select('tgl_diambil')
        ->where('order_no',$order_code)
        ->first();
        if($dated !=null)
        {
           $tanggal = $dated->tgl_diambil;
        }else{
           $tanggal = "";
        } 
         
        return  GeneralHelpers::tanggal_indo($tanggal);

    }


    public static function Rupiah($angka){
    
        $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
        return $hasil_rupiah;
 
    }

    public static function RoleVerification($order_code){

        $data = DB::table('users_roles')->where('user_id',Auth::User()->id)->first();
         
            $approv = DB::table('approve_po_list')->where(['order_no'=>$order_code,'role_id'=>$data->role_id])->first();
            if($approv != null)
            {
                if($approv->status_approve == 1)
                {
                    $result = "false";  
                }else{
                   //check Aproval sebelumnya
                   $sequence = $approv->sequence - 1;
                   if($sequence != 0)
                   {
                        $check = DB::table('approve_po_list')->where(['order_no'=>$order_code,'sequence'=>$sequence])->first();
                        if($check->status_approve !=1)
                        {
                          $result = "false";
                        }else{
                          $result = "true"; 
                        } 


                   }else{
                      $result = "true";
                   }
                   
                    
                       
                } 
            }else{
               $result = "nonactive";
            }    
           
        return $result; 
    }

    


    public static function PDFVendor($order_no)
    {
        $check = DB::table('order')->where(['order_no'=>$order_no,'is_approve_complete'=>1])->first();
        if($check)
        {
            $filename = env('SERVER_BACKEND')."pdf/po/".$check->pdf_name;   
        } else{
            $filename = "";
        }   
      
        return $filename;
    }

    public static function TotalApproval(){
        $auth = Auth::User();
        $data = DB::table('order as a')
                 ->select('a.order_no')
                 ->whereNull('a.token')
                 ->where('a.created_by',$auth->id)
                 ->first();

        $count =  DB::table('order as a')
                 ->select('a.order_no')
                 ->whereNull('a.token')
                 ->where('a.created_by',$auth->id)
                 ->count();       
        if($count  !=0)
        {
           $status = true;
           $message = "Waiting $count Dokumen Pemesanan Anda Sedang Di Verifikasi Oleh Pihak Privy ...";
        }else{
           $status = false;
           $message = "Pengajuan $count Dokumen Disetujui Oleh Pihak Privy"; 
        }         
        $result = array('status'=>$status,'messages'=>$message,'total'=>$count,'document'=>$data);
        return  $result;        

   }

  

   public static function GetVendor(){

      $data = DB::table('order as a')
       ->select('b.id','b.name')
       ->join('vendor as b','a.vendor_id','=','b.id')
       ->groupBy('a.location_id')
       ->get();

    $arr = array();  
    foreach($data as $key =>$value)
    {
       $arr[$key]['value'] = $value->id; 
       $arr[$key]['text'] = $value->name;
    }

      return $arr;
   }

   public static function GetLocation(){



       $data = DB::table('order as a')
       ->select('b.location_id','b.name','b.name_prefix')
       ->join('ref_locations as b','a.location_id','=','b.location_id')
       ->groupBy('a.location_id')
       ->get();


    $arr = array();  
    foreach($data as $key =>$value)
    {
       $arr[$key]['value'] = $value->location_id; 
       $arr[$key]['text'] = $value->name_prefix.' '.$value->name;
    }

      return $arr;
   }


   public static function getSumberDaya(){


    $location = DB::select(DB::raw("SELECT `b`.`kode` as `id`, `a`.`name`
                  FROM resources_code a
                  JOIN (SELECT LEFT(code_1, 2) as kode FROM product where product.is_deleted=0 GROUP by kode) b ON a.code LIKE CONCAT(b.kode, '%')
                  where a.level=2 
                "));
    $arr = array();  
    foreach($location as $key =>$value)
    {
       $arr[$key]['value'] = $value->id; 
       $arr[$key]['text'] = $value->name;
    }

      return $arr;
   }



    public static function AccessApprov($order_no,$sequence){
        $approv = DB::table('approve_po_list')->where(['order_no'=>$order_no,'sequence'=>$sequence])->first();
        return $approv;

    }



   public static function checkSequece($order_code)
    {
        $approv = DB::table('approve_po_list')->where(['order_no'=>$order_code,'status_approve'=>1])->count();
        return $approv;

    }

    public static function checkApprov($order_code){

      $approv = DB::table('approve_po_list')->where(['order_no'=>$order_code])->count(); 
      return $approv;
    }

   public static function CheckApproval($id)
   {
      $role_id = RoleUser::where('user_id',$id)->first()->role_id;
      $Approval = DB::table('approve_po_list')->where('role_id', $role_id)->OrderBy('id','DESC')->get();
      if(count($Approval) > 0)
      {
         return $Approval[0];
      }else{
         return null;
      }  
      
   }

   public static function RoleUserID($role_id){
        
        $user = DB::table('users_roles')->where('role_id',$role_id)->first();
        $user_id = 0;
        if($user !=null)
        {
           $user_id = $user->user_id;
        } 
        return $user_id;   
    }



    public static function ServerBackend($url)
    {


            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $response = curl_exec($ch); 
            curl_close($ch);      
            $result = json_decode($response);
            if($result->status ==false)
            {
                $hasil = "";
            }else{
                 $hasil = $result->data[0]->file;
            }    
            return $hasil;
            



    }

    public static function LogApproval($level_id,$status_id,$noted,$order_code){
         // $data = DB::table('request_otp')->where('order_no',$order_code)->first();
         // if($data)
         // {
             DB::table('log_cancel_po')->insert(
                [
                'id_alasan'=>$level_id,
                'status_cancel'=>$status_id,
                'keterangan'=>$noted,
                'user_id'=>Auth::User()->id,
                'no_order'=>$order_code
                ]);
             // DB::table('request_otp')->where(['order_no'=>$order_code,'user_id'=>Auth::User()->id])->delete();
        // } 
    
   }


   public static function UpdateStatusApprov($order_code,$sequence)
   {
      DB::table('approve_po_list as a')
      ->where(['a.order_no'=>$order_code,'a.sequence'=>$sequence])
      ->update(['status_approve'=>1]);

   }

     public static function UpdateSquenceOrder($order_no,$sequence){
         $approv = DB::table('order')->where(['order_no'=>$order_no])
                 ->update(['order_no'=>$order_no,'approve_sequence'=>$sequence]);
        return $approv;

    }

    public static function UpdateOrderComplite($order_code){
       //hitung jumlah sequnce 
        $count = DB::table('approve_po_list')->where('order_no',$order_code)->count();
        DB::table('order')->where('order_no',$order_code)->update(['approve_sequence'=>$count,'is_approve_complete'=>1,'order_status'=>2]);


    }
    

}