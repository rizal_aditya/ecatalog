<?php

namespace App\Http\Request;
use Auth;
use App\Helpers\GeneralHelpers;
use App\Helpers\GeneralPaginate;
use App\Models\ResourceCode;
use App\Http\Request\RequestFrontend;
use App\Http\Request\RequestAuth;
use DB;

class RequestNotif 
{
   
   

   public static function GetDataPage($data,$perPage,$request,$description)
   {
        $__temp_ = array();
        $getRequest = $request->all();
        $page = isset($getRequest['page']) ? $getRequest['page'] : 1;
        $numberNext = (($page*$perPage) - ($perPage-1));
        $FileAkun = GeneralPaginate::FileAkun();
        $FilePublic = GeneralPaginate::FilePublic();  

        foreach ($data as $key => $val)
        {
            if($val->photo ==null || $val->photo =="")
            {
                $photo = url($FilePublic.'2010526.png');
            }else{
                $photo = url($FileAkun.$val->photo);
            }

            $__temp_[$key]['number'] = $numberNext++;
            $__temp_[$key]['id'] = $val->id;
            $__temp_[$key]['order_no'] = RequestNotif::SplitDesc($val->deskripsi,'order_no');
            $__temp_[$key]['photo'] = $photo; 
            $__temp_[$key]['status'] = ucfirst(RequestNotif::SplitDesc($val->deskripsi,'status')); 
            $__temp_[$key]['created_at'] = GeneralHelpers::tanggal_indo($val->created_at);
        }

       
       
        $results['result'] = $__temp_;
        if($description !="")
        {  if($data->total() !=0)
           {
             $results['cari'] = 'Pencarian "'.$s.'" Total : '.$data->total().' berhasil ditemukan'; 
           }else{
             $results['cari'] = 'Pencarian tidak ditemukan "'.$description.'" '; 
           } 
            
        }else{
            $results['cari'] = ''; 
        } 

         

        $results['total'] = $data->total();
        $results['lastPage'] = $data->lastPage();
        $results['perPage'] = $data->perPage();
        $results['currentPage'] = $data->currentPage();
        $results['nextPageUrl'] = $data->nextPageUrl();
        return $results;

   }


   public static function GetDataPopUp($data){
         
         $__temp_ = array(); 
         $FileAkun = GeneralPaginate::FileAkun();
         $FilePublic = GeneralPaginate::FilePublic();   
         foreach ($data as $key => $val)
        {
            if($val->photo ==null || $val->photo =="")
            {
                $photo = url($FilePublic.'2010526.png');
            }else{
                $photo = url($FileAkun.$val->photo);
            }

            $__temp_[$key]['id'] = $val->id;
            $__temp_[$key]['order_no'] = RequestNotif::SplitDesc($val->deskripsi,'order_no');
          
            $__temp_[$key]['photo'] = $photo;
            $__temp_[$key]['status'] = ucfirst(RequestNotif::SplitDesc($val->deskripsi,'status')); 
            $__temp_[$key]['created_at'] = GeneralHelpers::tanggal_indo($val->created_at);
        }

           return $__temp_;

   }

   public static function SplitDesc($deskripsi,$type)
   {
     $explode = explode(' ',$deskripsi);
     if($type =="order_no")
     {  
        if($explode[0] =="No")
        {
           $arr = $explode[2];
        }else{
           $arr = $explode[0].' '.$explode[1].' '.$explode[2].' '.$explode[3].' '.$explode[4];     
        }    
       
     }else if($type =="status"){
    
        if($explode[0] =="No")
        {
            if($explode[7] =="Sudah")
            {
                $arr = $explode[7].' '.$explode[8].' '.$explode[9];
            }else if($explode[7] =="Cancel"){
                 $arr = $explode[7].' '.$explode[8]; 
            }else if($explode[7] =="Perlu"){
                 $arr = $explode[7].' '.$explode[8].' '.$explode[9];
            }else{    
                $arr = $explode[7]; 
            }   
        }else{
           $arr = $explode[5].' '.$explode[6];     
        } 
     }else if($type =="color"){
        if($explode[0] =="No")
        {
            $arr = "status-grey";

        }else{
            $arr = "status-orange";
        }    
     }  

     return $arr;
    
   }

  
   
   

   

}