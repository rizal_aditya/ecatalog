<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Helpers\SSO\SsoManagement;


class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $connect = SsoManagement::Connection();       
        try {
             $access = SsoManagement::GetUser();
            
        } catch (NotAttachedException $e) {
            SsoManagement::LogoutSso();
            return redirect()->to('login');
        } catch (SsoException $e) {
             SsoManagement::LogoutSso();
             return redirect()->to('login');
        }

        

         if(!$access)
        {
             
                SsoManagement::LogoutSso();
                return redirect()->to('login');  
               
        } 
   
       
        

        return $next($request);
    }
}
